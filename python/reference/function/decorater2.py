#! /usr/bin/env python

import time

def timing(func):
	""" 時間計測を行うデコレータ """

	def new_func(*args, **kwargs):
		# 時間を測る
		t = time.time()
		result = func(*args, **kwargs)
		# 関数名: function.__name__
		print(f"{func.__name__}: {time.time() - t}s")
		return result

	return new_func


@timing
def func():
	# １秒待つ
	time.sleep(1)


def accepts(*types): # 以下の例では、*types == (str, int)
	""" 引数の型のチェックを行うデコレータ """

	def _accepts(func): # func == duplicate
		def new_func(*args, **kwargs):
			# zip(iterable1, iterable2)
			# len(iterable1) != len(iterable2)の場合、最小のものに合わせ、長い方は切り詰められる
			# ex) zip(["A", "B", "C"], ["a", "b", "c"]) # [("A", "a"), ("B", "b"), ("C", "c")]
			# 似たようなことは、map(None, iterable1, iterable2)で出来る
			for arg, type_ in zip(args, types):
				if not isinstance(arg, type_):
					raise TypeError(f"{arg} -> {type_}")
			return func(*args, **kwargs)

		return new_func

	return _accepts


@accepts(str, int) # typeを引数に渡す
def duplicate(text, count):
	# map(function, iterable)
	# iterableオブジェクトの各要素にfunctionを適用し、その戻り値を要素とするイテレータを返す
	# ex) map(lambda x: x * 2, (1, 2, 3)) # [2, 4, 6]
	# ex) map(pow, (2, 3, 4), (1, 2, 3)) # [2, 9, 64]
	# map()の中身を確認したければ、print(list(map(func, iter)))
	return "".join(map(lambda x: "".join(x), zip(*(text,)*count)))


def main():
	func()
	print(duplicate("Python", 3)) # PPPyyyttthhhooonnn
	print(duplicate("Python", 3.1)) # TypeError: 3.1 -> <class 'int'>


if __name__ == "__main__":
	main()


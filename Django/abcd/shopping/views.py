import uuid
import pytz # 好きなタイムゾーンに変更
from datetime import datetime
from urllib.parse import urlencode
from collections import deque # 先入れ先出し
from collections import OrderedDict # 値を追加した順序を記憶する事ができる辞書型風のデータ構造
# Django
from django.conf import settings
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic import FormView
from django.views.generic import DetailView
from django.views.generic import TemplateView
from django.views.generic import CreateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic.edit import ModelFormMixin
from django.views.generic.edit import FormMixin
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.views.decorators.http import require_http_methods
from django.http import HttpResponse
from django.http import HttpResponseForbidden
from django.contrib import messages
from django.core.paginator import Paginator
from django.forms import formset_factory # 同じフォームを複数、同じ画面に表示させる
from django.core.mail import send_mail
from django.core.mail import EmailMessage # BCC
from django.template.loader import render_to_string # テンプレートを読み込む
from django.db.models import Prefetch
from django.db import transaction
from django.db import IntegrityError
from django.db import DatabaseError
from django.db.models import Func
from django.db.models import Value
from django.db.models import Subquery
from django.db.models import OuterRef
from django.utils import timezone


# Application
from shopping.models import (
	Item, ItemPhoto, ItemStock, ItemReview,
	G1, Foal, CartItem, Order, OrderDetail,
)
from shopping.forms import (
	ItemForm, G1Form, FoalForm, ItemPhotoForm,
	CartForm, ItemDeleteForm, PaymentForm, OrderHistoryForm, 
	ItemSearchForm, ItemReviewForm,
)

MyUser = get_user_model()

####################################################
###   Shopping Cart
####################################################

def initiate_shopping_session(request):
	""" SESSION['cart']を初期化 """
	if "cart" not in request.session:
		print('☆☆☆ SESSION["cart"]を初期化します ☆☆☆')
		
		###########################################
		#####           SESSION復習
		###########################################
		# https://freeheroblog.com/django-session/
		# 保存できる例：セッションはJSONのような型で指定する必要がある
		# とにかく深くするとダメ
		# request.session["sample"]くらいで抑えないと更新されない
		# [
		# 	{"username": "Tom"},
		#	{"age": "23"}
		# ]
		# 保存できない例：Pythonの辞書型、リストの内部の辞書型のバリューにリストを保存するような値は保存できない
		# [
		#	{"groupuser": ["Tom", "Chris", "Naomi"]},
		#	{"age": "23"}
		# ]
		# 辞書型ライクなセッションの保存のコード
		# request.session['session_array_data'] = dict(username="Micheal", age=26)
		# JSON復習
		# オブジェクトの場合は{"key": "value"}で囲み、配列の場合は[]で囲む。文字列は""で囲む

		# 識別用としてUUIDを作成するが、request.session.session_keyでも良いのかもしれない
		# cf) https://man.plustar.jp/django/topics/http/sessions.html
		# セッションスタート
		# if not request.session.session_key: request.session.create() 
		# セッションキー取得
		# session_key = request.session.session_key
		request.session["cart_uuid"] = uuid.uuid4()
		request.session["cart"] = {}
		request.session.set_expiry(60 * 30) # セッション期限/30分保持

	return None


class ShoppingTopView(FormMixin, ListView): 
	""" Shopping TopPage """
	# TODO: priceによる商品一覧の並び替えなど

	template_name = "shopping/shopping_top.html"
	model = Item
	context_object_name = "items"
	form_class = ItemSearchForm

	# ページにリクエストがきた際にそれをGETやPOSTに振り分ける処理のこと
	# つまり、dispatchは、getやpostの処理が呼ばれる前に呼ばれる処理
	def dispatch(self, request, *args, **kwargs):
		if "cart" not in request.session:
			# session["shopping"]を初期化
			initiate_shopping_session(request)
		return super().dispatch(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		# self.searched_item = self.request.GET.get('searched_item')
		return super().get(request, *args, **kwargs) # 親のGETを呼び出す

	def post(self, request, *args, **kwargs):
		# self.object = self.get_object()
		form = self.get_form()
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		
		# テンプレート内ではitem.item_photo.filter(priority=1)のように出来ないので、
		# まずJOINしてからテンプレートで使う必要があった
		items = ItemPhoto.objects.select_related('item').filter(priority=1)
		context['items'] = items
		return context

	def get_queryset(self, **kwargs):
		queryset = super().get_queryset(**kwargs) # Item.objects.all()と同じ結果
		return queryset

	def form_valid(self, form):
		searched_item = form.cleaned_data['searched_item']
		# GETへのパラメーターを付与
		self.parameters = '?{}'.format(urlencode({'searched_item':searched_item}))
		return super().form_valid(form)
	
	def form_invalid(self, form):
		self.parameters = ''
		messages.add_message(self.request, messages.ERROR,
								"アイテムを検索できませんでした")
		return super().form_invalid(form)

	def get_success_url(self):
		redirect_url = reverse_lazy('shopping:searched_item')
		# parameters = urlencode({'start':self.start, 'end':self.end})
		# url = f'{redirect_url}?{parameters}'
		url_plus_params = '{}{}'.format(redirect_url, self.parameters)
		return url_plus_params


class SearchedItemView(FormMixin, ListView):
	""" アイテム検索の結果を表示するページ """
	template_name = "shopping/item/searched_item.html"
	model = Item
	context_object_name = "items"
	form_class = ItemSearchForm

	def dispatch(self, request, *args, **kwargs):
		if "cart" not in request.session:
			# session["shopping"]を初期化
			initiate_shopping_session(request)
		return super().dispatch(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		if request.GET.get('searched_item', None):
			self.searched_item = request.GET.get('searched_item', None).split() # 空白で区切る
		else:
			self.searched_item = []
		return super().get(request, *args, **kwargs)

	def post(self, request, *args, **kwargs):
		# self.object = self.get_object()
		form = self.get_form()
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		return context

	def get_queryset(self, **kwargs):
		""" ORを使用した、部分一致検索 """
		# OR検索
		q = Q()
		for key in self.searched_item:
			q.add(Q(name__icontains=key), Q.OR) # 大文字小文字を区別しない

		queryset = Item.objects.prefetch_related(
			Prefetch(
				'item_photo', # related_name
	        	queryset=ItemPhoto.objects.select_related('item').filter(priority=1), # query statement
    	    	to_attr='ipr', # name for reference
			),
		).filter(q)

		return queryset

	def form_valid(self, form):
		searched_item = form.cleaned_data['searched_item']
		# GETへのパラメーターを付与
		self.parameters = '?{}'.format(urlencode({'searched_item':searched_item}))
		return super().form_valid(form)
	
	def form_invalid(self, form):
		self.parameters = ''
		messages.add_message(self.request, messages.ERROR,
								"アイテムを検索できませんでした")
		return super().form_invalid(form)

	def get_success_url(self):
		redirect_url = reverse_lazy('shopping:searched_item')
		url_plus_params = '{}{}'.format(redirect_url, self.parameters)
		return url_plus_params


@require_http_methods(['GET', 'POST'])
def item_detail(request, pk): # クラスベースビューで作成したものはviews-sub.pyにて
	"""  """

	if "cart" not in request.session:
		# session["shopping"]を初期化
		initiate_shopping_session(request)

	##########################################################################
	#####     COOKIEセクション(SESSIONでも良いのだが、練習のためCOOKIEで)
	##########################################################################
	if 'checked_items' not in request.COOKIES:
		# COOKIE["checked_item"]を初期化
		print('☆☆☆ COOKIEの閲覧履歴を初期化します ☆☆☆')
		# Djangoでは仕様のためCOOKIEもSESSION同様配列を渡せない
		# PHPではsetcookie("buy[2]", "Video");のように配列を渡せるようだが...
		request.COOKIES['checked_items'] = ''

	# COOKIEが配列を格納できないため、閲覧したアイテムIDを文字列として格納させる（苦肉の策）
	# 本当は、閲覧した商品IDは全て保存し、DBに格納することが望ましいが、ここはテストなので４個で制限
	item_nums = deque(maxlen=4) # 先入れ先出し法
	for i in request.COOKIES.get('checked_items').split(','):
		item_nums.append(i) # maxlenを指定しているのでそれ以上は自動でpopleft()される

	if str(pk) not in item_nums: # 閲覧したアイテムがリストに含まれたいない場合
		if len(item_nums) >= 4: # 格納したアイテム数が4以上の時
			item_nums.popleft() # 枠を１つ開ける

		item_nums.append(str(pk)) # リストに格納
				
	checked_items = ','.join(item_nums) # 文字列として格納する
		
	# POST
	if request.method == 'POST':
		# cart_form
		if 'cart_form' in request.POST:
			cart_form = CartForm(request.POST or None)
			review_form = ItemReviewForm() # これをしていないとlocal variable 'review_form' referenced before assignment
			
			if cart_form.is_valid():
				# 同じ商品がクリックされた場合に、quantityの数をupdateする
				if request.session["cart"].get(str(pk)):
					request.session["cart"][str(pk)]["quantity"] += int(cart_form.cleaned_data['quantity'])
					if request.session["cart"][str(pk)]["stored"] == "yes": # CartItemテーブルに格納済みである場合
						request.session["cart"][str(pk)]["stored"] = "modified" # "modified"に変更
				# 初登録の場合
				else: 
					item_in_cart = {
						"item_id": pk,
						"quantity": int(cart_form.cleaned_data['quantity']),
						"stored": "no", # CartItemテーブルに格納時、"yes"に変更
					}
					request.session["cart"][str(pk)] = item_in_cart
			
				request.session["new_adding_item"] = pk
            
            	# messages.add_message(request, level, message)
				messages.add_message(request, messages.SUCCESS, "アイテムをカートに入れました")

				return redirect("shopping:adding_cart", uuid=request.session.get('cart_uuid'))
			else:
				messages.add_message(request, messages.ERROR, "アイテムをカートに入れられませんでした")

		# review_form
		if 'review_form' in request.POST:
			review_form = ItemReviewForm(request.POST or None)
			cart_form = CartForm() # これをしていないとlocal variable 'cart_form' referenced before assignment

			if review_form.is_valid():
				try:
					review = review_form.save(commit=False) # 以下の情報を格納するためcommit=False
					review.user = request.user
					review.item = get_object_or_404(Item, pk=pk)
					review.save()
				except IntegrityError as err: # UNIQUE constraint failed: item_review.user_id, item_review.item_id
					print(err)
					messages.add_message(request, messages.ERROR, "二重投稿はできません")
					return redirect("shopping:item_detail", pk=pk)

				messages.add_message(request, messages.SUCCESS, "レビューを投稿しました")

				return redirect("shopping:item_detail", pk=pk)
			else:
				messages.add_message(request, messages.ERROR, "レビューを投稿できませんでした")

	else:
		cart_form = CartForm()
		review_form = ItemReviewForm()

	context = {
		'cart_form': cart_form,
		'review_form': review_form,
		'photos': ItemPhoto.objects.filter(item_id=pk),
		'stock':  ItemStock.objects.get(item_id=pk),
		'item': Item.objects.get(pk=pk),
		'G1':  G1.objects.filter(item_id=pk),
		'foals':  Foal.objects.filter(item_id=pk),
		'reviews':  ItemReview.objects.select_related('user').filter(item_id=pk),
	}
	
	#############################################
	#####     DjangoのSQLが実行されるタイミング
	#############################################
	# DjangoのSQLが実行されるタイミング -> 実際にその値が必要になるとき
	# https://docs.djangoproject.com/ja/4.1/ref/models/querysets/
	# https://qiita.com/ta_ta_ta_miya/items/7d02495eeee06958127a
	# 1. first(), last(), get(), count(), exists()などのメソッドが呼ばれたタイミング
	# 2. for(), スライス, pickle, repr(), len(), list(), bool()などクエリが評価されたタイミング
	# 3. all(), filter(), exclude(), values()などはQuerySetを返すだけ

	
	# prefetch_related(Prefetch())を利用した手法！！
	# templateはitem_detail_prefetch.htmlを利用
	"""
	item = Item.objects.prefetch_related(
		Prefetch( # 注意： when using prefetch_related(), you should only really use .all() on the prefetched attribute, 
		          # not try to e.g. .filter() it, because Django won't "emulate" a queryset method on it.
			'item_photo', # related_name
	        queryset=ItemPhoto.objects.select_related('item'), # query statement
    	    to_attr='ipr', # name for reference
		),
		Prefetch(
			'item_stock', # related_name
	        queryset=ItemStock.objects.select_related('item'), # query statement
    	    to_attr='isr', # name for reference
		),
		Prefetch(
			'item_g1', # related_name
	        queryset=G1.objects.select_related('item'), # query statement
    	    to_attr='igr', # name for reference
		),
		Prefetch(
			'item_foal', # related_name
	        queryset=Foal.objects.select_related('item'), # query statement
    	    to_attr='ifr', # name for reference
		),
		Prefetch(
			'item_review', # related_name
	        queryset=ItemReview.objects.select_related('item'), # query statement
    	    to_attr='irr', # name for reference
		),
	).filter(pk=pk)

	# 注意：all()以外のメソッドを呼び出す時はprefetch_related()が上手く機能しない場合がある
	# filter()などもDBを叩いてしまうので、その場合はPrefetch()を利用する

	# 値にアクセスする場合（例：在庫数）
	# item.item_stock.values('stock')[0]['stock'] # => 100
	# item.item_stock.get(pk=pk).stock # => 100
	# item.isr.stock # Best!!

	context = {
		'cart_form': cart_form,
		'review_form': review_form,
		'item': item[0], # 取得されたQuerySetは１つのみなので
		# 'photos': item.item_photo.values(),
	}
	"""

	# 注意：COOKIEをセットする場合は一旦render()をresponseに格納する
	response = render(request, "shopping/item/item_detail.html", context) # prefetch_related(Prefetch())を利用する場合はitem_detail_prefetch.html
	# templateはitem_detail_prefetch.htmlを利用
	# set_cokkies()はrequestではなく、responseに適用する点に注意
	# max_ageにより、30分保持
	response.set_cookie('checked_items', checked_items, max_age=60*30) # 文字列をCOOKIEにセット
	return response


class AddingCartView(TemplateView):
	"""  """
	template_name = "shopping/cart/adding_cart.html"

	# TODO: 自分はuuidで処理したが、AmazonのようにGETでパラメータを渡すべき？
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		adding_item_id = self.request.session.pop("new_adding_item")
		context["item"] = Item.objects.get(pk=int(adding_item_id))
		context['cart_uuid'] = self.request.session.get("cart_uuid")
		return context


@require_http_methods(['GET', 'POST'])
def cart_detail(request, uuid): # クラスベースビューで作成したものは下（マルチフォームは未実装）
	"""  """
	
	if "cart" not in request.session:
		# session["shopping"]を初期化
		initiate_shopping_session(request)

	# POST
	if request.method == 'POST':
		cart_form = CartForm(request.POST or None)
		if cart_form.is_valid():
			
			messages.add_message(request, messages.SUCCESS, "アイテムの数量を変更しました")

			return redirect("shopping:cart_detail", uuid=uuid)
		else:
			messages.add_message(request, messages.ERROR, "アイテムの数量を変更できませんでした")

	else:
		cart_form = CartForm() # initial

	cart = request.session.get("cart")
	# セッションに追加されたitem_idを取得
	item_ids = [int(c) for c in cart]
	
	########################################
	######      select_related復習
	########################################
	# 外部キーの外部キーを取得する場合(BookのForeignKey(Auther)のForeignKey(Item))：
	# Book.objects.select_related('auther__item').get()
	# リレーション先のデータを取得したい場合：
	# Book.objects.select_related('auther').values('auther__name', 'auther__birthday')

	# セッションに追加されたitem_idの商品を取得
	# 注意：ItemPhotoから取得しているので、pk__inではなく、item_id__in。pk__inだとItemPhotoのpkから取得してしまう
	added_items = ItemPhoto.objects.select_related('item').filter(item_id__in=item_ids).filter(priority=1)
	items = []
	total_price: int = 0 # 合計金額

	for added_item in added_items:
		quantity = request.session['cart'][str(added_item.item.pk)].get("quantity")
		item_dict = {
			'item_id': added_item.item.pk,
			'name': added_item.item.name,
			'photo': added_item.photo,
			'price': added_item.item.price,
			'quantity': quantity,
		}
		items.append(item_dict)
		# 合計金額を計算
		total_price += added_item.item.price * quantity
		# form = CartForm()

	# 関数ベースビューにてPaginatorを使用する場合
	paginator = Paginator(items, 3) # ３件ずつ
	page_number = request.GET.get('page')
	page_obj = paginator.get_page(page_number)

	context = {
		'items': items, # Paginationを使わない場合： {% for item in items %}
		'page_obj': page_obj, # Paginationを使う場合： {% for item in page_obj %}
		'total_price': '{:,.0f}'.format(total_price), # 3桁区切りにして整数表記
		'cart_form': cart_form,
	}
	return render(request, "shopping/cart/cart_detail.html", context)


def plus_cart_item(request, item_id):
	""" cart_detaiにて「+」ボタンが押された商品の数をインクリメントする """

	if "cart" not in request.session or "cart_uuid" not in request.session:
		messages.add_message(request, messages.ERROR, "数量を変更できませんでした")
		redirect("shopping:shopping_top")

	stock = ItemStock.objects.get(item=item_id).stock

	if request.session["cart"][str[item_id]].get("quantity"):
		if request.session["cart"][str[item_id]].get("quantity") < stock - 1:
			# 商品の「購入数」をインクリメント
			request.session["cart"][str[item_id]]["quantity"] += 1
		else:
			messages.add_message(request, messages.ERROR, "在庫数を超えています")
	else:
		messages.add_message(request, messages.ERROR, "数量を変更できませんでした")

	return redirect("shopping:cart_detail", uuid=request.session.get('cart_uuid'))


def minus_cart_item(request, item_id):
	""" cart_detaiにて「-」ボタンが押された商品の数をデクリメントする """

	if "cart" not in request.session or "cart_uuid" not in request.session:
		messages.add_message(request, messages.ERROR, "数量を変更できませんでした")
		redirect("shopping:shopping_top")

	if request.session["cart"][str[item_id]].get("quantity"):
		if request.session["cart"][str[item_id]].get("quantity") > 1:
			# 商品の「購入数」をデクリメント
			request.session["cart"][str[item_id]]["quantity"] -= 1
		else:
			messages.add_message(request, messages.ERROR, "数量は１以上になるようにしてください")
	else:
		messages.add_message(request, messages.ERROR, "数量を変更できませんでした")

	return redirect("shopping:cart_detail", uuid=request.session.get('cart_uuid'))


def delete_cart_item(request, item_id):
	""" cart_detaiにて「削除」ボタンが押された商品を削除する """

	if "cart" not in request.session or "cart_uuid" not in request.session:
		messages.add_message(request, messages.ERROR, "アイテムを削除できませんでした")
		redirect("shopping:shopping_top")
		
	if request.session["cart"].get(str(item_id)):
		# 「削除」ボタンを押された商品をpop()にて削除
		request.session["cart"].pop(str(item_id))
	else:
		messages.add_message(request, messages.ERROR, "アイテムを削除できませんでした")

	return redirect("shopping:cart_detail", uuid=request.session.get('cart_uuid'))


####################################################
###   Payment
####################################################

@login_required
@require_http_methods(['GET', 'POST'])
def payment_detail(request, user_id):
	"""  """
	
	if request.user.id != user_id:
		return HttpResponseForbidden("このページは表示出来ません")

	user = get_object_or_404(MyUser, pk=user_id)
	
	if "cart" not in request.session:
		return HttpResponseForbidden("このページは表示出来ません")

	# Sessionの内容をCartItemテーブルに格納 (cart_detail()内でも良いかもしれない)
	for key, value in request.session.get('cart').items():

		if value["stored"] == "no": # DBに新規登録
			item = get_object_or_404(Item, pk=value["item_id"])
			# "CartItem.user" must be a "MyUser" instance
			# "CartItem.item" must be a "Item" instance
			cart = CartItem.objects.create(user=user, item=item, 
				quantity=value["quantity"],)
			# または (https://docs.djangoproject.com/ja/4.1/ref/models/querysets/#create)
			# cart = CartItem(user=request.user.id, item=value["item_id"], quantity=value["quantity"])
			# cart.save() # DBに保存
			# それか
			# cart = CartItem()
			# cart.user = request.user.id
			# cart.item = value["item_id"]
			# cart.quantity = value["quantity"]
			# cart.save() # DBに保存

			request.session["cart"][key]["stored"] = "yes" # 格納フラグを立てる

		elif value["stored"] == "modified": # DBを更新
			CartItem.objects.update_or_create(user=request.user.id, item=value["item_id"], defaults={"quantity":value["quantity"], })
			# または
			# cart = CartItem.objects.get(user=request.user.id, item=value["item_id"])
			# cart.quantity = value["quantity"]
			# cart.save() # DBに保存
			# それか (https://docs.djangoproject.com/ja/4.1/ref/models/querysets/#update)
			# Cart.objects.filter(user=request.user.id, item=value["item_id"]).update(quantity=value["quantity"])
			
			request.session["cart"][key]["stored"] = "yes" # 格納フラグを立てる


	# Closing Section
	total_price: int = 0 # 注文の合計金額
	if request.method == "POST":
		form = PaymentForm(request.POST or None)
		if form.is_valid():

			# create()で作成した場合、order.pkと出来る
			order = Order.objects.create(user=user, total_price=total_price, is_paid=False,)
			print("★★★ Orderテーブルにレコードを作成しました ★★★")

			# CartItemより情報取得
			cart_items = CartItem.objects.select_related('item').filter(user=user)
			for cart_item in cart_items:
				# トランザクション
				# ItemStockのstockを増減
				try:
					with transaction.atomic():
						# デッドロックに注意
						item_stock = ItemStock.objects.select_for_update().get(item_id=cart_item.item_id)
						item_stock.stock -= cart_item.quantity
						item_stock.save()
				except IntegrityError: # 在庫不足の時
					# Rollbackが発生してDBに保存されなかったフィールドの値については自分で戻す必要がある
					# print("IntegrityError: {}".format(item_stock.stock))
					print("★★★ 在庫不足のためRoolbackが発生しました ★★★")
					item_stock.stock += cart_item.quantity
					messages.add_message(request, messages.ERROR, "在庫不足のため{}を削除しました".format(cart_item.item.name))
					continue
				except DatabaseError: # データベースエラーの時
					# Rollbackが発生してDBに保存されなかったフィールドの値については自分で戻す必要がある
					# print("DatabaseError: {}".format(item_stock.stock))
					print("★★★ DatabaseのErrorが発生しました ★★★")
					item_stock.stock += cart_item.quantity

				# 重要！！
				# 金額などのような重要な値は、書き換えられてしまうのでFormやCookieなどで渡してはいけない
				# 重要なデータはサーバー側のテーブルから常に参照しなければいけない
				total = cart_item.item.price * cart_item.quantity
				total_price += total # Orderテーブル

				# Itemごとのtotal_salesを編集
				total_sales = Item.objects.get(pk=cart_item.item_id).total_sales
				Item.objects.filter(pk=cart_item.item_id).update(total_sales=total_sales+total)
				print("★★★ Itemテーブルのtotal_salesフィールドを変更しました ★★★")

				# 注文明細：OrderDetailテーブル
				order_detail = OrderDetail(order=order)
				order_detail.item = cart_item.item # "OrderDetail.item" must be a "Item" instance.
				order_detail.price = cart_item.item.price # 金額などのような重要な値はサーバー側のテーブルから常に参照
				order_detail.quantity = cart_item.quantity
				order_detail.payment = 4 # 銀行振込
				order_detail.save()
				print("★★★ OrderDetailにレコードを作成しました ★★★")

			# 注文書：Orderテーブルに格納
			# create()で作成した場合、order.pkと出来る
			if total_price != 0:
				Order.objects.filter(pk=order.pk).update(total_price=total_price)
				print("★★★ Orderテーブルのtotal_priceを変更しました ★★★")

			# Session["cart"]とCartItemテーブルを破棄
			request.session.pop("cart")
			CartItem.objects.filter(user=user).delete() # all()は不要？
			print("★★★ 注文が確定したので、SESSION['cart']とCartItemの内容を削除しました ★★★")

			# Send Gmail <- 今回はデモなので実装しない
			# 実装方法はiportfolio.contact_confirm()にて

			messages.add_message(request, messages.SUCCESS,
								"お買い上げありがとうございました")
			return redirect('shopping:order_history', pk=user_id)
		else:
			messages.add_message(request, messages.ERROR,
								"ご購入できませんでした")

	else:
		form = PaymentForm()

	# テクニック： CartItemを主体にしたQuerySet!!
	# MySQLを使う場合
	# cart_item_mysql = CartItem.objects.filter(user=user).select_related('item').annotate(
 	# 	item_photos=Func(Value('GROUP_CONCAT'), Value('item_photo.photo'), function='GROUP_CONCAT'),
	# )
	# Subqueryを使用した汎用的な解法
	# U0.item = CartItem.item.id  U0: Subqueryの主体のテーブル, ここではItemPhoto
	subqs = ItemPhoto.objects.filter(item=OuterRef('item')).filter(priority=1) # OuterRef: CartItem
	cart_item_subq = CartItem.objects.filter(user=user).select_related('item').annotate(
 		item_photos=Subquery(subqs.values('photo')), # Subquery
	)
	# photos = [cart_item.item_photos for cart_item in cart_item_subq]

	# total_priceをSubqueryから取得する場合
	for cart_item in cart_item_subq:
	 	total_price += cart_item.quantity * cart_item.item.price # 金額などのような重要な値はサーバー側のテーブルから常に参照

	context = {
		'cart_items': cart_item_subq,
		'total_price': '{:,.0f}'.format(total_price), # 3桁区切りにして整数表記
		'form': form,
	}

	return render(request, 'shopping/payment/payment_detail.html', context)


####################################################
###   Order History
####################################################

# <form method="get">ですれば良いのだが、validationとか使いたいので<form method="post">処理により複雑になった
class OrderHistoryView(LoginRequiredMixin, FormMixin, ListView):
	"""  """

	template_name = "shopping/order/order_history.html"
	# model = Order
	context_object_name = "orders"
	paginate_by = 5
	form_class = OrderHistoryForm

	def dispatch(self, request, *args, **kwargs):
		print(' ★★★ dispatch ★★★ ')
		if self.request.user.id != self.kwargs['pk']:
			return HttpResponseForbidden("このページは表示出来ません")

		# 初期化
		self.start = ''
		self.end = ''

		return super().dispatch(request, *args, **kwargs)

	# get_queryset()より先に呼び出される
	def get(self, request, *args, **kwargs):
		print(' ★★★ GETパラメータを取得します ★★★ ')
		if "start" in self.request.GET:
			self.start = self.request.GET.get('start', None)

		if "end" in self.request.GET:
			self.end = self.request.GET.get('end', None)

		return super().get(request, *args, **kwargs) # 親のGETを呼び出す

	# FormMixinではget()とpost()の両方を実装しないとMethod Not Allowed Errorが発生
	def post(self, request, *args, **kwargs):
		form = self.get_form()
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def get_queryset(self, *args, **kwargs):
		print(' ★★★ QuerySetを取得します ★★★ ')
		user = get_object_or_404(MyUser, pk=self.kwargs['pk'])
		subqs = ItemPhoto.objects.filter(item=OuterRef('item')).filter(priority=1) # OuterRef: OrderDetail
		queryset = Order.objects.prefetch_related( 
    		Prefetch(
        		'orderdetail_orderid', 
        		queryset=OrderDetail.objects.select_related('order', 'item').annotate(
        			item_photos=Subquery(subqs.values('photo')),
        		),
        		to_attr='oor',
    		),
		).filter(user=user).order_by('-created_at')
		# prices = [item.price for order in queryset for item in order.oor]
		# photos = [item.item_photos for order in queryset for item in order.oor]


		# TODO: セキュリティのためバリデーションをかける

		# 重要！
		# warnings.warn("DateTimeField %s received a naive datetime (%s)"発生
		# このままでは.filter(__range)にかけられない
		# よって、awareな（タイムゾーン情報を持つ）datetimeオブジェクトに変更する
		# AwareとNaive: https://docs.python.org/ja/3/library/datetime.html
		if self.start:
			# datetime.strptime()により、日付や時刻を表す文字列からdatetimeオブジェクトを生成
			self.start = '{} 00:00:00'.format(self.start)
			self.start = datetime.strptime(self.start, '%Y-%m-%d %H:%M:%S') # type(self.start): <class 'datetime.datetime'>
			# strftime()で整形するとstr型となってしまい、timezone.localtime()に渡せなくなってしまう
			# self.start = self.start.strftime('%Y-%m-%d %H:%M:%S')

			# .tzinfoがNoneなので、pytzによりタイムゾーンを設定
			# auto_now_addを調べてみると、tzinfo=<UTC>となっているので、'Asia/Tokyo'ではなくUTCで設定
			# cf) https://mizzsugar.hatenablog.com/entry/2019/11/07/094230
			self.start = self.start.astimezone(pytz.timezone('UTC'))

			# 日本時間に設定する場合
			# self.start = self.start.astimezone(pytz.timezone(settings.TIME_ZONE))
			# localtimeを使って時間のデータを呼べば、日本時間で使用できる
			# self.start = timezone.localtime(self.start) # self.start.tzinfo: <DstTzInfo 'Asia/Tokyo' JST+9:00:00 STD>

		if self.end:
			# 本当は.filter(date__lt='2023-03-03 00:00:00')のようにしたいのだが、date__range使うので
			self.end = '{} 23:59:59'.format(self.end)
			self.end = datetime.strptime(self.end, '%Y-%m-%d %H:%M:%S').astimezone(pytz.timezone('UTC'))


		if self.start and self.end:
			print(' ★★★ 両側検索します ★★★ ')
			queryset = queryset.filter(created_at__range=[self.start, self.end])
		elif self.start:
			print(' ★★★ 左側検索します ★★★ ')
			queryset = queryset.filter(created_at__gte=self.start)
		elif self.end:
			print(' ★★★ 右側検索します ★★★ ')
			queryset = queryset.filter(created_at__lte=self.end)
		else:
			print(' ★★★ Nullで検索します ★★★ ')
			# queryset = queryset.filter(created_at__isnull=True)
			queryset = queryset

		return queryset

	def get_context_data(self, **kwargs):
		print(' ★★★ get_context_data ★★★ ')
		context = super().get_context_data(**kwargs)
		# 上記のSubqueryから{{ item.item_photos.photo.url }}が使えないのでMEDIA_URLを強制的に前に付与する
		context['media_url'] = settings.MEDIA_URL
		return context

	def form_valid(self, form):
		print(' ★★★ form_valid ★★★ ')
		self.start = '{}-{}-{}'.format(form.cleaned_data['start_y'], form.cleaned_data['start_m'], form.cleaned_data['start_d'])
		self.end = '{}-{}-{}'.format(form.cleaned_data['end_y'], form.cleaned_data['end_m'], form.cleaned_data['end_d']) # type(self.end): <class 'str'>

		# GETに渡すパラメーター
		self.parameters = '?{}'.format(urlencode({'start':self.start, 'end':self.end}))
		return super().form_valid(form)
	
	def form_invalid(self, form):
		print(' ★★★ form_invalid ★★★ ')
		# TODO: 失敗を報告するスクリプトを記述
		self.parameters = ''
		messages.add_message(self.request, messages.ERROR,
								"期間を指定できませんでした")
		return super().form_invalid(form)

	def get_success_url(self):
		""" 検索後、自身にGETパラメータを渡してリダイレクト """
		# URLにGETパラメータを渡してリダイレクトさせる
		# cf) https://blog.narito.ninja/detail/74
		# cf) https://djangobrothers.com/blogs/django_redirect_with_parameters/

		redirect_url = reverse_lazy('shopping:order_history', kwargs={'pk': self.kwargs['pk']})
		# parameters = urlencode({'start':self.start, 'end':self.end})
		# url = f'{redirect_url}?{parameters}'
		url_plus_params = '{}{}'.format(redirect_url, self.parameters)
		return url_plus_params


####################################################
###   Item Register
####################################################

decorators = [login_required,]
@method_decorator(decorators, name='dispatch')
class ItemRegisterView(LoginRequiredMixin, CreateView):
	template_name = 'shopping/item/item_register.html'
	form_class = ItemForm

	def get_success_url(self):
		return reverse("shopping:item_edit", kwargs={'pk': self.object.pk})


@method_decorator(decorators, name='dispatch')
class ItemEditView(LoginRequiredMixin, UpdateView):
	template_name = ''
	form_class = ''


@login_required
@require_http_methods(['POST', 'GET'])
def item_register(request):
	""" Itemを登録する """
	#if request.user.is_authenticated and request.user.is_admin:

	if request.method == 'POST':
		item_form = ItemForm(request.POST or None)
		itemphoto_form = ItemPhotoForm(request.POST, request.FILES or None)
		g1_form = G1Form(request.POST or None)
		foal_form = FoalForm(request.POST or None)

		if (item_form.is_valid() and itemphoto_form.is_valid() 
			and g1_form.is_valid() and foal_form.is_valid()):

			item = item_form.save(commit=False) # commit=Falseにしないとエラー
			item.save()

			g1 = g1_form.save(commit=False)
			g1.item = item # 上のuserと紐づける
			item.g1.save()

			foal = foal_form.save(commit=False)
			foal.item = item
			item.foal.save()

			itemphoto = itemphoto_form.save(commit=False)
			itemphoto.item = item
			item.itemphoto.save()

			return redirect("shopping:item_edit", pk=self.object.pk)
	else:
		# forms = (SignupForm(), ProfEditForm(), ProfImgEditForm())
		item_form = ItemForm()
		itemphoto_form = ItemPhotoForm()
		g1_form = G1Form()
		foal_form = FoalForm()

	template = "shopping/item/item_register.html"
	context = {
		'item_form': item_form,
		'itemphoto_form': itemphoto_form,
		'g1_form': g1_form,
		'foal_form': foal_form,
	}
	return render(request, template, context)



####################################################
###   Cart
####################################################

# CartやOrderDetailのようにForeignKeyが複数ある場合は、
# prefetch_related()を使っても、別のForeignKeyを取得しようとするとそこでもN+1問題が発生する
# 悪い例
# from .models import Order
# order = Order.objects.prefetch_related('orderdetail_orderid').get()
# order_details = order.orderdetail_orderid.filter(created_at__gte='2022-06-28')
# 以下を実行すると、item（別モデルのForeignKey）を参照する分だけクエリが発生してしまう
# for order_detail in order_details: print(order_detail.item.name)
# 良い例
# prefetch_related(Prefetch($related_name, queryset=$QuerySet, to_attr=$Reference_name))を使用
# from django.db.models import Prefetch
# from .models import Order, OrderDetail # Prefetch()を使用するためにOrderDetailまでインポート
# order = Order.objects.prefetch_related(
# 	Prefetch('orderdetail_orderid', 
#		# つまりOrderDetailとItemをいつもの通りselect_related()で結合しているだけ
# 		queryset=OrderDetail.objects.select_related('item').filter(created_at__gte='2022-06-28'),
# 		to_attr='john_lennon'), )
# o_o = Order.john_lennon
# for o in o_o: print(o.item.name) # 新たなクエリは飛ばない


####################################################
###   Refugee
####################################################

# 以下ではrequest.session['cart']['addning_new_item']が追加されない
# おそらくMixinを使用しているので上書きされるか何かなのだろう
# せっかく書いたが、SESSION絡む場合はやはり関数ビューで書いた方が迷わなくて楽
class ItemDetailView(FormMixin, DetailView): # FormMixin
	""" Item Detail """
	template_name = "shopping/item/item_detail.html"
	model = Item
	form_class = CartForm
	# queryset = Item.objects.filter()
	context_object_name = "item"

	def dispatch(self, request, *args, **kwargs):
		if "cart" not in self.request.session:
			# session["shopping"]を初期化
			initiate_shopping_session(self.request)

		return super().dispatch(request, *args, **kwargs)

	# クラスベースビューでresponseを返す（COOKIEをセットする時くらいしか使わない）
	def render_to_response(self, context, **response_kwargs):
		# COOKIE復習
		# ブラウザ全体で300個のCookieに対応
		# １つのドメインあたり、20個のCookieまでしか登録できない
		response = super().render_to_response(context, **response_kwargs)
		if 'checked_items' not in self.request.COOKIES:
			# COOKIE["checked_item"]を初期化
			print('☆☆☆ COOKIEの閲覧履歴を初期化します ☆☆☆')
			# Djangoでは仕様のためCOOKIEもSESSION同様配列を渡せない
			# PHPではsetcookie("buy[2]", "Video");のように配列を渡せるようだが...
			self.request.COOKIES['checked_items'] = ''

		# COOKIEが配列を格納できないため、閲覧したアイテムIDを文字列として格納させる（苦肉の策）
		item_nums = deque(maxlen=4) # 先入れ先出し法
		for i in self.request.COOKIES.get('checked_items').split(','):
			item_nums.append(i) # maxlenを指定しているのでそれ以上は自動でpopleft()される

		if str(self.kwargs['pk']) not in item_nums: # 閲覧したアイテムがリストに含まれたいない場合
			if len(item_nums) >= 4: # 格納したアイテム数が4以上の時
				item_nums.popleft() # 枠を１つ開ける

			item_nums.append(str(self.kwargs['pk'])) # リストに格納
				
		checked_items = ','.join(item_nums) # 文字列として格納する
		# set_cokkies()はrequestではなく、responseに適用する点に注意
		# max_ageにより、10分保持
		response.set_cookie('checked_items', checked_items, max_age=60*10) # 文字列をCOOKIEにセット

		return response

	def get_queryset(self, **kwargs):
		queryset = Item.objects.filter(pk=self.kwargs['pk'])
		return queryset

	def get_context_data(self, **kwargs):
		# 元々備わっているcontextを失わずに追加していくため、super()を呼び出す
		context = super().get_context_data(**kwargs)
		context['photos'] = ItemPhoto.objects.filter(item_id=self.kwargs['pk'])
		context['stock'] = ItemStock.objects.get(item_id=self.kwargs['pk'])
		context['G1'] = G1.objects.filter(item_id=self.kwargs['pk'])
		context['foals'] = Foal.objects.filter(item_id=self.kwargs['pk'])
		context['review'] = ItemReview.objects.filter(item_id=self.kwargs['pk'])

		return context

	def get(self, request, *args, **kwargs):
		return super().get(request, *args, **kwargs)

	def post(self, request, *args, **kwargs): # post()定義しないと、Method Not Allowed (POST) Error
		# self.object = self.get_object()
		# self.object_list = self.get_queryset()
		form = self.get_form() # Form
		if form.is_valid():
			return self.form_valid(form)
		else:
			return self.form_invalid(form)

	def form_valid(self, form):
		# SESSIONにカート内容を追加
		item_in_cart = {
				'item_id': self.kwargs['pk'],
				'quantity': form.cleaned_data['quantity'],
			}
		self.request.session["cart"][str(pk)] = item_in_cart
		self.request.session["new_adding_item"] = self.kwargs['pk']

		breakpoint()

		return super().form_valid(form) # get_success_url()メソッドの戻り値にリダイレクト
	
	def form_invalid(self, form):
		return super().form_invalid(form)

	def get_success_url(self):
		""" form_valid()後に自身にリダイレクト """
		# urlキーワード引数指定
		return reverse("shopping:adding_cart", kwargs={'pk': self.kwargs['pk']}) 

	# DetailView, Updateiewにて使われる
	#def get_object(self, **kwargs):
	#	obj = ProfImage.objects.get(user_id=self.kwargs['pk'])
	#	return obj

	# Formを使うクラスベースビューにて使われる
	# クラス内で設定しているform_classに値を渡す
	def get_form_kwargs(self, *args, **kwargs): # フォームに初期値を渡す
		kwags = super().get_form_kwargs(*args, **kwargs)
		return kwags


class CartDetailView(ListView): # Deleteや数量変更もあるので、もう関数ビューの方が良さそう
	"""  """
	template_name = "shopping/cart/cart_detail.html"
	model = CartItem
	# context_object_name = ""
	paginate_by = 3 # model=Cartが使用されていないので、Paginateが使用されない？

	def get_context_data(self, **kwargs): # TODO
		context = super().get_context_data(**kwargs)
		cart = self.request.session.get("cart")
		item_ids = [int(c) for c in cart]
		# 注意：ItemPhotoから取得しているので、pk__inではなく、item_id__in。pk__inだとItemPhotoのpkから取得してしまう
		added_items = ItemPhoto.objects.select_related('item').filter(item_id__in=item_ids).filter(priority=1)
		items = []
		total_price: int = 0

		for added_item in added_items:
			item_dict = {
				'item_id': added_item.item.pk,
				'name': added_item.item.name,
				'photo': added_item.photo,
				'price': added_item.item.price,
				'quantity': self.request.session['cart'][str(added_item.item.pk)].get("quantity"),
			}
			items.append(item_dict)
			total_price += added_item.item.price * self.request.session['cart'][str(added_item.item.pk)].get("quantity")

		context['items'] = items
		context['total_price'] = '{:,.0f}'.format(total_price) # 3桁区切りにして整数表記

		# 複数のフォームを使用する場合
		context.update({
			'cart_form': CartForm(**self.get_form_kwargs()),
			'delete_form': ItemDeleteForm(**self.get_form_kwargs()),
		})
		return context



const List = (props) => {
  const { todos, deleteTodo } = props;
  const doneTodo = (id) => {
    deleteTodo(id);
  };
  const complete = (id) => {
    deleteTodo(id);
  };
  return (
    <>
      <div>
        {todos.map((todo) => {
          return (
            <div key={todo.id}>
              {/** ??? onClick={doneTodo(todo.id)}では上手くいかない */}
              <button onClick={() => complete(todo.id)}>完了</button>
              <span>{todo.content}</span>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default List;

# Django
from django.urls import path
from django.urls import re_path
# Application
from shopping import views

app_name = "shopping"

urlpatterns = [
	path("", views.ShoppingTopView.as_view(), name="shopping_top"), # "/shopping/"
	# path("item_detail/<int:pk>/", views.ItemDetailView.as_view(), name="item_detail"), 
	path("item_detail/<int:pk>/", views.item_detail, name="item_detail"), 
	path("item_register/", views.item_register, name="item_register"),
	path("item/<int:pk>/edit/", views.ItemEditView.as_view(), name="item_edit"),
	path("item/searched_item/", views.SearchedItemView.as_view(), name="searched_item"),
	path("cart/adding_cart/<uuid:uuid>/", views.AddingCartView.as_view(), name="adding_cart"),
	# path("cart/detail/<uuid:uuid>/", views.CartDetailView.as_view(), name="cart_detail"),
	path("cart/detail/<uuid:uuid>/", views.cart_detail, name="cart_detail"),
	path("cart/plus_item/<int:item_id>/", views.plus_cart_item, name="plus_item_id"),
	path("cart/minus_item/<int:item_id>/", views.minus_cart_item, name="minus_item_id"),
	path("cart/delete_item/<int:item_id>/", views.delete_cart_item, name="delete_cart_item"),
	path("payment/detail/<int:user_id>/", views.payment_detail, name="payment_detail"),
	path("order/history/<int:pk>/", views.OrderHistoryView.as_view(), name="order_history"),
]

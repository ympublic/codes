/** Presumably, you find several parts which seem weird,
    but I wrote this code redundantly for my study of Golang.  */

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"reflect"
	"regexp"
	"strings"
)

type Profile struct {
	mine  MyProfile   "My datas"
	yours YourProfile "Your datas"
}

type MyProfile struct {
	name       string
	email      string
	phone      string
	occupation []string
	language   []string
	hobby      []string
}

type YourProfile struct {
	name   string
	gender string
}

func NewProfile(arr map[string]map[string]interface{}) *Profile {
	return &Profile{
		// Need to return Object with a nameless function
		mine:  func() (m MyProfile) { m.SetProfile(arr["me"]); return }(),
		yours: func() (u YourProfile) { u.SetProfile(arr["you"]); return }(),
	}
}

type M interface {
	ShowMessage() string
	SetProfile()  // cf: http://ideone.com/5rnnHW
}

type Getter interface {
	Name() string
}

func (p *Profile) ShowMessage() string {
	return fmt.Sprintf("Hi, %s. \nName: %s \nLanguage: %s", p.yours.name, p.mine.name,
		strings.Join(p.mine.language, ", "))
}

func (m *MyProfile) SetProfile(arr map[string]interface{}) {
	for k, v := range arr {
		key := reflect.ValueOf(m).Elem().FieldByName(k)
		if key.IsValid() {
			// Using interface{}, we need to cast the value to String.
			key.SetString(v.(string))
		}
	}
}

func (u *YourProfile) SetProfile(arr map[string]interface{}) {
	for k, v := range arr {
		key := reflect.ValueOf(u).Elem().FieldByName(k)
		if key.IsValid() {
			key.SetString(v.(string))
		}
	}
}

func ArrangeAnswer(ans string) string {
	re := regexp.MustCompile(`(\s|　)+`)
	return strings.TrimSpace(re.ReplaceAllString(ans, " "))
}

func main() {
	// Multidimentional map
	prof := make(map[string]map[string]interface{})

	prof["me"] = map[string]interface{}{
		"name":       "John Lennon",
		"email":      "foobar@gmail.com",
		"phone":      "+81-90-0000-0000",
		"occupation": []string{"Programmer", "System Engineer"},
		"language": []string{"Japanese", "English", "Go", "Java", "Python",
			"PHP", "JavaScript", "SQL"},
		"hobby": []string{"Photography", "Traveling", "Fishing", "Onsen", "Trekking"},
	}

	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Would you please let me know your profile?")
	/* if we don't initialize map, a panic happenes.
	   "panic: assignment to entry in nil map"
	   Because the map's initialized value is nil.
	   prof["you"] = make(map[string]interface{}) is one of the other solutions.  */
	prof["you"] = map[string]interface{}{}
	for {
		fmt.Print("Your name: ")
		scanner.Scan()
		ans := ArrangeAnswer(scanner.Text())  // or fmt.Scanf("%s", &s)
		re := regexp.MustCompile(`^[a-zA-Z ]+$`)
		if re.MatchString(ans) {
			prof["you"]["name"] = ans
			break
		} else {
			fmt.Println("Alphabet please.")
		}
	}
	fmt.Print("Your gender: ")
	scanner.Scan()
	prof["you"]["gender"] = strings.ToLower(ArrangeAnswer(scanner.Text()))
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "Read error: ", err)
	}

	defer func() {
		if x := recover(); x != nil {
			log.SetOutput(os.Stdout)
			switch prof["you"]["gender"] {
			case "f", "female", "woman":
				log.Println("Let's go to a hotel together now!!")
			case "m", "male", "man":
				log.Println("Feel free to send me messages!!")
			default:
				log.Println("Get away from here!!")
			}
		}
	}()

	p := NewProfile(prof)
	fmt.Println(p.ShowMessage()) // My message to you.
	panic("Oh, a panic happened in this code!")
}

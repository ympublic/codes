from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import get_user_model

# Application
from accounts.models import Profile
from accounts.models import ProfImage

MyUser = get_user_model()

class SignupForm(UserCreationForm):
	""" ユーザー登録 """
	class Meta:
		model = MyUser
		fields = ('email',)


class LoginForm(AuthenticationForm):
	pass


# 全角数字を半角数字に
num_to_hankaku = str.maketrans('ー０１２３４５６７８９', '-0123456789')

class ProfEditForm(forms.ModelForm):
	class Meta:
		model = Profile
		exclude = ('user', 'note',)
		# fields = "__all__"としてhidden属性にする場合
		# またはテンプレート内にて{{ form.field_name.as_hidden }}を指定する
		# widgets = {'user': forms.HiddenInput()}

	# clean_<fieldname>メソッドによるクリーニング（正規化）処理 P168
	def clean_zip_code(self):
		zip_code = self.cleaned_data['zip_code']
		return zip_code.translate(num_to_hankaku)

	def clean_phone(self):
		phone = self.cleaned_data['phone']
		return phone.translate(num_to_hankaku)

	# 複数のフィールドに適応させる処理はcleanメソッド内に記述
	# def clean(self):
	#	cleaned_data = super().clean()
	#	phone = cleaned_dat.get('email')
	#	data = self.serialized_data
	#	return data


class ProfImgEditForm(forms.ModelForm):
	class Meta:
		model = ProfImage
		exclude = ('user', 'note',)


class QUserListForm(forms.Form):
	""" ユーザー検索用フォーム """
	q = forms.CharField(label='検索', max_length=128, )


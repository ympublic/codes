def find_the_nuisance(box):
    max_len=0
    max_len_num=[]
    for k, v in box.iteritems():  # cf: difference between enumerate() and iteritems()
        if max_len < len(v):
            max_len = len(v)
            del max_len_num[0:]
            max_len_num.append(k)
        elif max_len == len(v):
            max_len_num.append(k)
    if len(max_len_num) > 1:
        friends_len=0
        min_friends_len=200000
        nuisance=""
        for num in max_len_num:
            #print "num:"+str(num)
            for friend in box[num]:
                #print "friend:"+str(friend)
                friends_len += len(box[friend])
                if min_friends_len > friends_len:
                    min_friends_len = friends_len
                    nuisance = num
    if max_len == 0:
        nuisance = 0
    else:
        nuisance = max_len_num[0]  
    return nuisance
    
def break_the_chain(nuisance, box):
    if nuisance == 0:
        return False
    else:
        friend=[]
        friend=box.pop(nuisance)
        #print friend
        for n in friend:
            box[n].remove(nuisance)
        print box
        return True

def break_rings(rings):
    import collections
    box=collections.defaultdict(list)
    for ring in rings:
        x, y = ring
        box[x].append(y) 
        box[y].append(x)
    print box
    boool=True
    count=0
    while boool:
        nuisance = find_the_nuisance(box)
        print "nuisance:" + str(nuisance)
        boool = break_the_chain(nuisance, box)
        count+=1 
    return count-1




"""
def find_the_nuisance(box):
    max_len=0
    max_len_num=0
    for k, v in box.iteritems():  # cf: enumerate()とiteritems()の違い
        if max(max_len, len(v)) == len(v):
            max_len=len(v)
            max_len_num=k
    if max_len == 0:
        return 0
    else:
        return max_len_num
    
def break_the_chain(nuisance, box):
    if nuisance == 0:
        return False
    else:
        friend=[]
        friend=box.pop(nuisance)
        #print friend
        for n in friend:
            box[n].remove(nuisance)
        print box
        return True


def break_rings(rings):
    import collections
    box=collections.defaultdict(list)
    for ring in rings:
        x, y = ring
        box[x].append(y) 
        box[y].append(x)
    print box
    boool=True
    count=0
    while boool:
        nuisance = find_the_nuisance(box)
        print nuisance
        boool = break_the_chain(nuisance, box)
        count+=1 
    return count-1
"""

"""
    numbers=set()
    temp=[]
    counts={}
    for ring in rings:
        x,y = ring
        temp.append(x)
        temp.append(y)
        numbers.add(x)
        numbers.add(y)
    print sorted(temp)
    print sorted(numbers)
    max_count=0
    for number in numbers:
        if max_count < temp.count(number): max_count = temp.count(number)
        counts.update(number, temp.count(number))
"""
    

if __name__ == '__main__':
    # These "asserts" using only for self-checking and not necessary for auto-testing
    assert break_rings(({1, 2}, {2, 3}, {3, 4}, {4, 5}, {5, 6}, {4, 6})) == 3, "example"
    assert break_rings(({1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 4}, {3, 4})) == 3, "All to all"
    assert break_rings(({5, 6}, {4, 5}, {3, 4}, {3, 2}, {2, 1}, {1, 6})) == 3, "Chain"
    assert break_rings(({8, 9}, {1, 9}, {1, 2}, {2, 3}, {3, 4}, {4, 5}, {5, 6}, {6, 7}, {8, 7})) == 5, "Long chain"
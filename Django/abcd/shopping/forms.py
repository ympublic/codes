from datetime import datetime
# Django 
from django import forms
from django.core.validators import MinValueValidator, MaxValueValidator

# Application
from shopping.models import ItemCategory
from shopping.models import Item
from shopping.models import G1
from shopping.models import Foal
from shopping.models import ItemPhoto
from shopping.models import ItemReview
from shopping.models import CartItem


####################################################
######     Item
####################################################

class ItemCategoryForm(forms.ModelForm):
	class Meta:
		model = ItemCategory
		exclude = ('note',)


class ItemForm(forms.ModelForm):
	class Meta:
		model = Item
		exclude = ('created_at', 'updated_at', 'note',)


class G1Form(forms.ModelForm):
	class Meta:
		model = G1
		fields = ('g1',)


class FoalForm(forms.ModelForm):
	class Meta:
		model = Foal
		fields = ('name',)


class ItemPhotoForm(forms.ModelForm):
	class Meta:
		model = ItemPhoto
		exclude = ('item',)


class ItemSearchForm(forms.Form):
	searched_item = forms.CharField(
		label='商品名',
		required=True, 
		max_length=128, 
		# help_text='検索したいアイテムを入力してください',
		widget=forms.TextInput(
			attrs={
				'placeholder': '検索キーワード',
				'class': 'form-control', # class追加
				'size': '50', # バーの長さ
 			},
		),
	)


class ItemReviewForm(forms.ModelForm):
	class Meta:
		model = ItemReview
		fields = ('stars', 'comment',)

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['comment'].widget.attrs['rows'] = 4
		self.fields['comment'].widget.attrs['cols'] = 35
		self.fields['comment'].widget.attrs['class'] = 'form-control'
		self.fields['stars'].widget.attrs['class'] = 'form-control'



class ItemDeleteForm(forms.Form):
	""" 商品を削除するための仮のフォーム（本当はJSで処理したい） """
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)


####################################################
######     Shopping Cart
####################################################

choice_quantities = ((i, i) for i in range(1, 21)) # 1~20

class CartForm(forms.Form):
	"""
	quantity = forms.IntegerField(
		label='購入数',
		required=True,
		initial=1,
		# 購入数は最小1、最大20とする
		validators=[MinValueValidator(1), MaxValueValidator(20)],
	)
	"""
	quantity = forms.ChoiceField(
		label='購入数',
		required=True,
		choices=choice_quantities,
		initial=1,
		#widget=forms.Select(
		#	attrs={
		#		'class': 'form-select', # class追加
		#	},
		#),
	)

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['quantity'].widget.attrs['class'] = 'quantity_of_item form-select' # フォームのclaaa名を変更


class CartForm_Model(forms.ModelForm): # 上記のCartFormをModelFormで利用する場合
	class Meta:
		model = CartItem
		fields = ('quantity',)

	 # フィールドを初期化
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['quantity'].initial = 1


####################################################
######     Payment
####################################################

class PaymentForm(forms.Form):
	""" 決済するための仮のフォーム """
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)


####################################################
######     Item Register
####################################################

class ItemRegisterForm(forms.ModelForm):
	class Meta:
		model = Item
		# fields = ('name', 'price', 'category', 'runs', 'wins', 'description',)
		exclude = ('created_at', 'updated_at', 'note',)


class ItemEditForm(forms.ModelForm):
	class Meta:
		model = Item
		# fields = ('name', 'price', 'category', 'runs', 'wins', 'description',)
		exclude = ('created_at', 'updated_at', 'note',)	


class ItemPhotoRegisterForm(forms.ModelForm):
	class Meta:
		model = ItemPhoto
		fields = ('photo', 'priority')


####################################################
######     Order History
####################################################

# 本当はDateFieldで実装した方が良い
class OrderHistoryForm(forms.Form):
	""" 開始時期と終了時期 """

	# TODO: カスタムバリデーターを作成し、1~12までなど判別させる

	# TODO: 30日終わりの月やうるう年の処理など
	# 注意：listを()で囲うとジェネレーターになるのでend部が表示されなくなってしまう
	choice_years = [(i, i) for i in range(2023, datetime.now().year+1)] # 2023-現在の年
	choice_months = [(i, i) for i in range(1, 13)] # 1~12
	choice_days = [(i, i) for i in range(1, 32)] # 1~31

	start_y = forms.ChoiceField(
		label='開始年',
		required=True,
		choices=choice_years,
		initial=datetime.now().year,
		# validators=[],
	)
	start_m = forms.ChoiceField(
		label='開始月',
		required=True,
		choices=choice_months,
		initial=1,
		# validators=[],
	)
	start_d = forms.ChoiceField(
		label='開始日',
		required=True,
		choices=choice_days,
		initial=1,
		#validators=[],
	)
	end_y = forms.ChoiceField(
		label='終了年',
		required=True,
		choices=choice_years,
		initial=datetime.now().year,
		#validators=[],
	)
	end_m = forms.ChoiceField(
		label='終了月',
		required=True,
		choices=choice_months,
		initial=datetime.now().month,
		#validators=[],
	)
	end_d = forms.ChoiceField(
		label='終了日',
		required=True,
		choices=choice_days,
		initial=datetime.now().day,
		#validators=[],
	)

	date_min = forms.DateField(
		label='開始',
		required=True,
		# initial='',
		widget=forms.SelectDateWidget(years=[x for x in range(1990, datetime.now().year)])
	)

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		# 全セレクトボックスに同じclassを付与する
		for k, v in self.fields.items():
			v.widget.attrs['class'] = 'form-select'


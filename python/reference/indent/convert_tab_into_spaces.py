#! /usr/bin/env python
# -*- coding: utf-8 -*-

#########################################
#    タブとスペースを互いに変換する
#########################################

def unexpand(astring, tablen=8):
	""" expandtabsの逆、つまりスペースをタブに変換する """
	import re
	# 変更されるスペース部と、非スペース部(文字列)のシーケンスに切断しリストに格納
	pieces = re.split(r'( +)', astring.expandtabs(tablen)) # (pattern, string)
	#print("pieces:{}".format(pieces))
	
	# 文字列長の合計を記録
	lensofar = 0
	for i, piece in enumerate(pieces): # インデックスと要素を取り出す
		thislen = len(piece)
		lensofar += thislen

		if piece.isspace():
			# スペースの並びをタブ+スペースにする
			numblanks = thislen % tablen # 除算した余り # type(numblanks) => int
			# python3は/で小数点以下まで含まれる。python2では切り捨てられていた
			# numtabs = (thislen - numblanks + tablen - 1) / tablen # type(numtabs) => float
			# python3で小数点以下を切り捨てたい場合は、//を使用
			numtabs = (thislen - numblanks + tablen - 1) // tablen 
			pieces[i] = '\t' * int(numtabs) + ' ' * int(numblanks)
			
			#print("thislen:{}".format(thislen))
			#print("lensofar:{}".format(lensofar))
			#print("numblanks:{}".format(numblanks))
	
	#print("pieces:{}".format(pieces))
	return ''.join(pieces)


def main():
	# 以下はスペースではなくタブで空けている
	t = "		apple	banana		orange		"

	# タブを指定した数字のスペースに変換する
	print("expandtabsの場合\n{}".format(t.expandtabs(4))) # default: 8


	# 以下はタブではなくスペースで空けている
	s = "    apple        banana    orange    "
	ss = """    first_tab
        second_tab
            third_tab    """
	# 単一行の文字列
	print("単一行の場合\n{}".format(unexpand(s, 4)))

	# 複数行からなる文字列を扱う場合
	# splitlines(True): 出力されるリストの中に改行コードは除去されず、付与した状態で格納
	print("複数行の場合\n{}".format(''.join([unexpand(s, 4) for s in ss.splitlines(True)]))) 



if __name__ == '__main__':
	main()


import { useEffect, useState } from "react";

const Example = () => {
  const [isDisp, setIsDisp] = useState(true);

  return (
    <>
      {isDisp && <Timer />}
      <button onClick={() => setIsDisp((prev) => !prev)}>トグル</button>
    </>
  );
};

const Timer = () => {
  const [time, setTime] = useState(0);

  useEffect(() => {
    let intervalId = null;
    intervalId = window.setInterval(() => {
      setTime((prev) => prev + 1);
    }, 1000);
    // コールバック関数の戻り値(依存配列を渡さない場合)
    // Timerコンポーネントが消滅する際に実行される
    // コンポーネントが消滅した際の後始末を行う場合に利用
    return () => {
      // クリーンナップ処理
      // メモリリークを防ぐため
      window.clearInterval(intervalId);
    };
  }, []);

  useEffect(() => {
    console.log("Updated init");
    document.title = "counter:" + time;
    window.localStorage.setItem("time-key-end", time);
    // コールバック関数の戻り値(依存配列を渡さない場合)
    // 依存配列のstateが変更される度にコールバック関数が呼ばれるが、
    // コールバック関数が呼ばれる前にreturnが返る
    // つまりここでは"Updated End"が呼ばれてから"Updated init"が呼ばれる
    return () => {
      // クリーンナップ(useEffectで呼ばれた何らかの後始末をする際に利用する)
      // 依存配列が更新される度にクリーンナップが実行される
      console.log("Updated end");
    };
  }, [time]);

  return (
    <h3>
      <time>{time}</time>
      <span>秒経過</span>
    </h3>
  );
};

export default Example;

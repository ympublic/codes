import { useTheme, useUpdateTheme } from "../context/ThemeContext";

const Header = () => {
  const THEMES = ["light", "dark", "red"];

  // コンテキストを読み込み
  const theme = useTheme();
  const setTheme = useUpdateTheme();

  const changeTheme = (e) => setTheme(e.target.value);

  console.log("header");

  return (
    <header className={`content-${theme}`}>
      {THEMES.map((_theme) => {
        return (
          <label key={_theme}>
            <input
              type="radio"
              value={_theme}
              checked={theme === _theme}
              onChange={changeTheme}
            />
            {_theme}
          </label>
        );
      })}
    </header>
  );
};

export default Header;

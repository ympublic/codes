#! /usr/bin/env python3
""" クイックソートを３行で
    注意：行数は少なくて済むが、処理速度では劣る(O(n2)のパフォーマンスを持つ)
"""

def quick_sort(data):
	if len(data) <= 1: return data
	return quick_sort([lt for lt in data[1:] if lt < data[0]]) \
			+ data[0:1] + quick_sort([ge for ge in data[1:] if ge >= data[0]])

if __name__ == '__main__':
	data = [70, 60, 40, 80, 30, 10, 50, 20]
	print(quick_sort(data)) # [10, 20, 30, 40, 50, 60, 70, 80]


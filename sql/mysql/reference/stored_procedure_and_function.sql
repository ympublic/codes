/* ストアドプロシージャ */
DELIMITER $$
CREATE PROCEDURE my_procedure(p INT)
BEGIN
	SELECT * FROM 社員;
	SELECT * FROM 商品 WHERE price >= p;
END
$$
DELIMITER ;
CALL my_procedure(1500) \G
SHOW CREATE PROCEDURE my_procedure \G
DROP PROCEDURE my_procedure \G


/* ストアドファンクション(返すデータの型を指定する必要がある) */
DELIMITER $$
/* 関数定義 */
CREATE FUNCTION bmi(height INT) RETURNS DOUBLE
BEGIN
	RETURN height * height * 22 / 10000;
END
$$
DELIMITER ;
SELECT bmi(178) \G


/* 変数を用いる場合 */
DELIMITER $$
CREATE FUNCTION heikin() RETURNS DOUBLE
BEGIN
	/* 変数定義 */
	DECLARE r DOUBLE; 
	SELECT AVG(price) INTO r FROM 商品;
	RETURN r;
END
$$
DELIMITER ;
SELECT heikin() \G

SHOW CREATE FUNCTION heikin \G
DROP FUNCTION heikin \G


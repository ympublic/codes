import React, { useState, useCallback } from "react";
import Child from "./Child";

const Example = () => {
  console.log("Parent render");

  const [countA, setCountA] = useState(0);
  const [countB, setCountB] = useState(0);

  // 親コンポーネントが再レンダリングされる度に定義されているので、
  // <Child />に渡されたclickHandlerと再レンダリングされた以下のclickHandlerは別物になってしまう
  // useCallback(関数, 依存配列)
  const clickHandler = useCallback(() => {
    setCountB((pre) => pre + 1);
    // 依存配列内のstateが更新されるとReact内部に保持される第一引数の関数が書き換わることになる
  }, []);

  return (
    <div className="parent">
      <div>
        <h3>親コンポーネント領域</h3>
        <div>
          <button
            onClick={() => {
              setCountA((pre) => pre + 1);
            }}
          >
            ボタンA
          </button>
          <span>親のstateを更新</span>
        </div>
      </div>
      <div>
        <p>ボタンAクリック回数：{countA}</p>
      </div>
      <Child countB={countB} onClick={clickHandler} />
    </div>
  );
};

export default Example;

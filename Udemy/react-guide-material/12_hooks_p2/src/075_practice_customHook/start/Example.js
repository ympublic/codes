import useCount from "./hooks/useCount";

const Example = () => {
  const { count, setCount } = useCount();

  return (
    <>
      <h3>練習問題</h3>
      <p>
        記述を変更し、完成コードと同じ状態になるようにしてください。
        startフォルダの中にhooks.jsというファイルを作成し
        その中でuseCountというカスタムフックを作成してください。
      </p>
      <div>Counts: {count}</div>
      <button onClick={() => setCount((prev) => prev + 1)}>Count Up!</button>
    </>
  );
};

export default Example;

#! /usr/bin/env python 
# coding: utf-8

import sys
# sys.setdefaultencoding("utf-8")

#####################################
#    テキストかバイナリか調べる
#####################################
# 注意： Python3では使えない(Python2ではbinaryもstrでくくられていた)

text_characters = "".join(map(chr, range(32, 127))) + "\n\r\t\b"

# 同一のものからtext_charactersを削除したものに変換する
# (置換元文字を連結した文字列, 置換先文字を連結した文字列, 削除する置換元文字列を連結した文字列)
# 第一引数と第二引数は長さを同一にする必要がある
_null_trans = str.maketrans("", "", text_characters) # table作成

def istext(s, text_characters=text_characters, threshold=0.30):
	# null文字が入っていればテキストではない
	if "\0" in s:
		return False

	# 空文字列はテキストである
	if not s:
		return True

	# sから非テキスト文字のみを抜き出し、サブストリングを作る
	t = s.translate(_null_trans) # (table)

	# 非テキスト文字が30%未満であればsをテキストとみなす
	return len(t) / len(s) <= threshold # python3は小数点以下をそのまま扱える
	# 切り捨てる場合は、len(t) // len(s)


# ファイルがバイナリかどうか
def istextfile(filename, blocksize=512, **kwargs):
	# ファイルの最初のブロックのみをistext()でチェックすれば良い
	return istext(open(filename).read(blocksize), **kwargs)


# オブジェクトがUnicode型、もしくは文字列のように振る舞うものかをチェック
def isAString(obj):
	return isinstance(obj, str)


if __name__ == '__main__':
	import os, sys
	
	s = "Shohei Ohtani's home scoreless-inning streak ended at 35 on Thursday,  \
	but he nearly replaced the feat with a greater one: \
	He challenged for a cycle in the Angels' 8-7 win over the Oakland Athletics. \
	Ohtani fell five or so feet short of concluding his afternoon with a home run \
	that would have cemented his second career cycle."

	print("テキストかどうか: {}".format(istext(s)))

	print("strかどうか: {}".format(isAString(s)))

	b = b'\xe3\x81\x82'

	print("テキストかどうか: {}".format(istext(b)))

	print("strかどうか: {}".format(isAString(b)))


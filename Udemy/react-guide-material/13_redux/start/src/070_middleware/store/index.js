import { configureStore } from "@reduxjs/toolkit";
import reducer from "./modules/counter";
import logger from "./middleware/logger";

export default configureStore({
  reducer: {
    counter: reducer,
  },
  // Middleware追加
  // MiddlewareはDispatchが通ると必ず実行される
  middleware: (getDefaultMiddleware) => {
    // 元々登録されているMiddlewareを全て取得
    const middlewares = getDefaultMiddleware();
    // 新しいMiddlewareを追加
    const newMiddlewares = middlewares.concat(logger);
    // return
    return newMiddlewares;
  },
});

#! /usr/bin/env python

# predicateな関数（戻り値がbooleanである関数）
def containsAny1(seq, aset):
	""" シーケンスseq中にasetのアイテムが一つでも含まれるかチェック """
	for c in seq:
		if c in aset:
			return True
	return False


# filter(booleanを返す関数, リスト): リスト型で条件に当てはまるものだけを取り出す
# def my_func(x): return x > 5
# filter(my_func, [7, 4, 2, 10]) # [7, 10]
def containsAny2(seq, aset):
	""" filter()を使用 """
	for item in filter(aset.__contains__, seq):
		return True
	return False


def containsOnly(seq, aset):
	""" シーケンスseqがasetにあるアイテムのみで出来ているかを検査 """
	for c in seq:
		if c not in aset:
			return False
	return True


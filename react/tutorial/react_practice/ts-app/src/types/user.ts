export type User = {
  login: string;
  id: number;
  node_id: string;
  gravatar_id?: string;
};

##################
#    クロージャ
##################

def make_adder(addend): # 引数に関数を渡せば、デコレーターになる
	def adder(augend):
		return augend + addend
	return adder

# 中で名前addendを参照している内部関数adderのクロージャが作られ、addendは値23にバインドされる
p = make_adder(23)

# 別のクロージャが作られ、addendは42にバインドされる
# pとqは互いに依存しない共存が可能
q = make_adder(42)

print(p(100)) # 123
print(q(100)) # 142


# クロージャ
def outer(o): # oに関数を渡せば、デコレーターになる
	def inner(i):
		print("o: {} / i: {}".format(o, i))
		return o + i
	print("o: {}".format(o))
	return inner

# クロージャ作成（o=10を持ったままの関数innerが返る）
testA = outer(10)
# o: 10

# inner(20)を実行
testA(20)
# o: 10 / i: 20
# 30

testA(5)
# o: 10 / i: 5
# 15

# クロージャ作成
testB = outer(7)
# o: 7

testB(7)
# o: 7 / i: 7
# 14

# testAとtestBは依存し合わない
testA(40)
# o: 10 / i: 40
# 50


def translator(frm='', to='', delete='', keep=None):
	if len(to) == 1:
		to = to * len(frm)
	trans = str.maketrans(frm, to, delete)
	if keep is not None:
		allchars = str.maketrans('', '') # 元のまま
		# keepからdelete部を除去する
		delete = allchars.translate(allchars, keep.translate(allchars, delete))
	def translate(s):
		return s.translate(trans, delete)
	return translate

# クロージャ作成
digits_only = translater(keep=str.digits)

digits_only('John Lennon : 090-1234-1234')
# 09012341234

# クロージャ作成
no_digits = translater(delete=str.digits)

no_digits('John Lennon : 090-1234-1234')
# John Lennon : --

# クロージャ作成
digits_to_hash = translater(frm=str.digits, to='#')

digits_to_hash('John Lennon : 090-1234-1234')
# John Lennon : ###-####-####

# クロージャ作成
# delete部がkeep部を負かす
trans = translater(delete='abcd', keep='cdef')

trans('abcdefg')
# ef


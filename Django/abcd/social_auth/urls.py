# Django
from django.urls import path, re_path
# Application
from social_auth import views

app_name = "social_auth"

urlpatterns = [
	path("profile/oauth_prof_create/", views.oauth_prof_create, name="oauth_prof_create"),
]


# 自作テンプレートタグ
# 注意：テンプレートタグを追加したり、編集した際は毎回サーバーを再起動する必要がある！

# カスタムタグを作成する場合： @register.simple_tag

# テンプレートフィルタとテンプレートタグの使い分け
# カスタムフィルタはその関数の引数を1つか2つにする必要があるため(Document参照)、
# それより多い引数が必要な場合には必然的にカスタムタグの方を利用せざるを得ない
# また、カスタムタグの方ではViewからTemplateに渡されるコンテキストをそのロジック内で利用することができる
# 基本的にはフィルタよりもタグの方が複雑な処理に向いている


import math
from datetime import datetime
from django import template

# Djangoテンプレートタグライブラリ
register = template.Library()

@register.simple_tag
def change_greeting_by_time() -> str:
	""" 挨拶を時間帯によって変える """
	dt_now = datetime.now()
	hour = dt_now.hour

	if hour < 4 or 18 <= hour:
		return "Good Evening, "
	elif 4 <= hour < 12:
		return "Good Morning, "
	elif 12 <= hour < 18:
		return "Good Afternoon, "
	else:
		return "Welcome, "


# 重要！！
# カスタムフィルター/タグを定義後はサーバーの再起動が必要


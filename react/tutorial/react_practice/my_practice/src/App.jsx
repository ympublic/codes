import {
  Fragment,
  useState,
  useEffect,
  memo,
  useCallback,
  useContext,
} from "react";
import { ColoredMessage } from "./components/ColoredMessage";
import { CssModules } from "./components/css/CssModules";
import { Emotion } from "./components/css/Emotion";
// import { StyledComponents } from "./components/StyledComponents";

import { StyledJsx } from "./components/css/StyledJsx";
import { TailwindCss } from "./components/css/TailwindCss";
import { Child1 } from "./components/children/Child1";
import { Child4 } from "./components/children/Child4";
import { Card } from "./components/bucket/Card";

import { AdminFlagContext } from "./components/providers/AdminFlagProvider";

// 他のファイルでも使えるようにexportする必要がある
// memo化: Propsに変更がない限り再レンダリングされないようになる
export const App = memo(() => {
  // 再レンダリングの挙動確認用
  console.log("App レンダリング");

  // Stateの定義
  // const [State変数, そのStateを更新するための関数] = useState();
  const [num, setNum] = useState(0); // 初期値を設定

  // Context内のisAdminと更新関数を取得
  const { isAdmin, setIsAdmin } = useContext(AdminFlagContext);

  // 切り替え押下時
  const onClickSwitch = () => setIsAdmin(!isAdmin);

  // ボタンを押した時にStateをカウントアップ
  const onClickButton = () => {
    setNum((prev) => prev + 1);
  };

  // ボタンを押すとカウントアップ中の数値を0に戻す
  // useCallBack: 関数をメモ化
  // useCallBack(実行する関数, [依存する値])
  const onClickReset = useCallback(() => {
    setNum(0);
  }, []);

  // useEffect: ある値が変わった時に限り、ある処理を実行する
  // useEffect(実行する関数, [依存する値]);
  useEffect(() => {
    alert(num);
  }, [num]); // numの値が変わった時だけalert()を実行する

  // JSXの重要なルールとして、return以降は１つのタグで覆われている必要がある
  // オブジェクトなのでCSSも{{color:"red"}}のようにクォートでくくる必要がある
  return (
    <Fragment>
      {console.log("Test")}
      <h1 style={{ color: "red" }}>こんにちは！</h1>
      <ColoredMessage color="blue">お元気ですか？</ColoredMessage>
      <ColoredMessage color="pink">元気ですよ！</ColoredMessage>
      <button onClick={onClickButton}>ボタン</button>
      <p>{num}</p>
      {/* CSSコンポーネント */}
      <CssModules />
      <StyledJsx />
      <Emotion />
      <TailwindCss />
      {/* Children */}
      {/* Propsとして関数を設定 */}
      <Child1 onClickReset={onClickReset} />
      <Child4 />
      {/* 管理者フラグがtrueの時とそれ以外で文字を出し分け */}
      {isAdmin ? <span>管理者です</span> : <span>管理者以外です</span>}
      <button onClick={onClickSwitch}>切り替え</button>
      <Card isAdmin={isAdmin} />
    </Fragment>
  );
});

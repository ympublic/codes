import pytest

@pytest.fixture(scope="function") # autouse=Trueを指定すると引数やデコレータで指定しなくても自動で実行される
def global_fixture():
	""" このconftest.pyの置かれたディレクトリ以下の全てのテストで使えるフィクスチャ """

	# setUp()

	fruits = ("apple", "banana", "orange")

	yield fruits

	# tearDown()

	print("処理終了")



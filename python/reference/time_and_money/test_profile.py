#! /usr/bin/env python

# python -m profile -s profile_module.py

"""profile
実行中に呼び出された各関数等の実行回数・時間についての情報を調べる
"""

import profile 

def bench1(list1, list2): # +=が早いのか？
	list1 += list2
	return list1


def bench2(list1, list2): # +が早いのか？
	list3 = list1 + list2
	return list3


def loop(bench):
	for i in range(10000):
		bench([1,] * i, [2,] * i)


if __name__ == "__main__":
	# 呼び出す時に計測したい処理内容を「文字列」として渡す
	profile.run("loop(bench1)")

	profile.run("loop(bench2)")


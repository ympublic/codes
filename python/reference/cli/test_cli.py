#! /usr/bin/env python

"""コマンドラインオプション
"""

import sys
import getopt # -Rや-vなどを受け取る

def main():
	# スクリプト名を除いた最初のコマンドライン引数を取得
	argv = sys.argv[1] # 例: python test_cli.py orange
	
	print(argv) # orange

	# スクリプト名を除いたコマンドライン引数リストを取得
	argv = sys.argv[1:]

	# getoptの引数で、許可するオプション名や引数の有無を確認
	# この場合、第２引数で指定された-a -bは許されるが、-vはGetoptErrorとなる
	# また、b:としているので、-b には引数が必要となる
	# 第３引数に指定しているので、--appleや--beansも可能
	# --beans=valueの形でも可能
	# これらはoptionsへと渡される
	# -や--で始まらない文字列は引数と解釈され、argumentsへと渡される
	options, arguments = getopt.getopt(argv, "ab:", ("apple", "beans="))

	# オプションへの処理
	for name, value in options:
		# python test_cli.py -b bananaの場合、name=-b、value=banana
		print(f"Name={name}, Value={value}")

	# 引数への処理
	for argument in arguments:
		# # python test_cli.py apple bananaの場合、argument=apple、argument=banana
		print(f"Argument={argument}")


if __name__ == "__main__":
	main()


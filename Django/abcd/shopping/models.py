import os
import uuid
from django.db import models
from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator


####################################################
###   Item
####################################################

class ItemCategory(models.Model):
	parent_id = models.IntegerField("親カテゴリID", default=0) # 親カテゴリID
	category_name = models.CharField("カテゴリ名", max_length=64, default="")
	category_type = models.IntegerField("種類", default=0)
	note = models.TextField("注釈", blank=True, null=True,)

	class Meta:
		db_table = "item_category"
		verbose_name = "商品カテゴリ"
		verbose_name_plural = verbose_name
		indexes = [ # セカンダリインデックスを設定
			models.Index(fields=["parent_id",],), 
			models.Index(fields=["category_type",],), # name引数を指定しない場合はDjangoが自動で一意なインデックス名を命名する
		]

	def __str__(self):
		return str(self.category_name)


class Item(models.Model): # Stud(種馬) # Product
	name = models.CharField("商品名", max_length=255, blank=False, default="")
	price = models.DecimalField("価格", # 通貨はDECIMAL型
		max_digits=12, # 全体の桁数（必須）
		decimal_places=2, # 小数点以下の桁数（必須）
		default=0.00, 
		blank=False,
	) 
	category = models.ForeignKey(ItemCategory, verbose_name="カテゴリー番号", 
		on_delete=models.SET_NULL, null=True,
		related_name="item_category")
	runs = models.PositiveSmallIntegerField("試合数", blank=False, default=0,) # career
	wins = models.PositiveSmallIntegerField("勝利数", blank=False, default=0,)
	description = models.TextField("説明", blank=True,)
	total_sales = models.IntegerField("販売総数", blank=True, default=0)
	created_at = models.DateTimeField("作成日時", auto_now_add=True,)
	updated_at = models.DateTimeField("更新日時", auto_now=True,)
	on_sale = models.BooleanField("販売中", default=True) # フラグ
	note = models.TextField("注釈", blank=True, null=True,) # 何かあった時に管理画面で注釈を付与する予備フィールド

	class Meta:
		db_table = "item"
		verbose_name = "アイテム"
		verbose_name_plural = "アイテム"
		indexes = [ # インデックスをはる（勿論カーディナリティが低いフィールドにははらない）
			# 複合インデックスをはりたい場合は、fields=['name', 'category']の様に複数のフィールド名を記述する
			models.Index(fields=["name",],), # nameを指定しない場合はDjangoが自動で一意なインデックス名を命名する
			models.Index(fields=["category"],),
        ]

	def __str__(self):
		return str(self.name)

	def save(self, *args, **kwargs):
		super().save(*args, **kwargs)

	def delete(self, *args, **kwargs):
		super().delete(*args, **kwargs)


class ItemStock(models.Model): # 在庫管理
	item = models.ForeignKey(Item, on_delete=models.SET_NULL,
		null=True, related_name="item_stock", verbose_name="商品番号")
	stock = models.PositiveSmallIntegerField("在庫数", blank=False, default=0)
	note = models.TextField("注釈", blank=True, null=True,)
	# arrival_date = models.DateTimeField(blank=True,)

	class Meta:
		db_table = "item_stock"
		verbose_name = "在庫数"
		verbose_name_plural = "在庫数"

	def __str__(self):
		return str(self.item.name)


# class ItemShipping(models.Model): pass # 発送テーブル


class G1(models.Model): # 主なGⅠ
	# ForeignKeyフィールド名はモデル名を小文字にしたものを推奨
	item = models.ForeignKey(Item, on_delete=models.CASCADE, 
		related_name="item_g1", verbose_name="商品番号",)
	g1 = models.CharField("GⅠレース", max_length=64, blank=True)
	note = models.TextField("注釈", blank=True, null=True,)

	class Meta:
		db_table = "g1"
		verbose_name = "主なGⅠ"
		verbose_name_plural = "主なGⅠ"

	def __str__(self):
		return str(self.item.name)


class Foal(models.Model): # 代表産駒
	item = models.ForeignKey(Item, on_delete=models.CASCADE, 
		related_name="item_foal", verbose_name="商品番号",)
	name = models.CharField("代表産駒", max_length=64, blank=True)
	note = models.TextField("注釈", blank=True, null=True,)

	class Meta:
		db_table = "foal"
		verbose_name = "代表産駒"
		verbose_name_plural = "代表産駒"

	def __str__(self):
		return f'{self.name}({self.item.name})'


def get_itemimage_path(instance, filename): # オブジェクト自身とファイル名を受け取る
    """ アップロードされたプロフィール画像の名前を変更する """
    # ここではinstance.itemにはItemの__init__内のself.nameが入る
    prefix = 'shopping{}{}{}'.format(os.sep, instance.item, os.sep) 
    name = str(uuid.uuid4()).replace('-', '')
    extension = filename.split('.')[-1]
    return '{}{}.{}'.format(prefix, name, extension)


class ItemPhoto(models.Model): # 画像
	item = models.ForeignKey(Item, on_delete=models.CASCADE,
		related_name="item_photo", verbose_name="商品番号",)
	photo = models.ImageField("商品画像", blank=True, 
		upload_to=get_itemimage_path)
	priority = models.IntegerField("優先順位",)
	note = models.TextField("注釈", blank=True, null=True,)

	class Meta:
		db_table = "item_photo"
		verbose_name = "商品画像"
		verbose_name_plural = "商品画像"
		ordering = ['priority'] # 優先順位に従って画像を並べ替える
		# モデルでデータベースの制約を定義
		constraints = [ # DBにINDEXが作成されるので注意！（不用意に消さない。migrateで失敗する様になる）
			# １つのフィールドだけではなく、複数のフィールドの組み合わせでユニーク制約をつけたい場合
			# ユニーク制約に違反したデータを登録しようとすると、IntegrityErrorが発生する
			models.UniqueConstraint( # 画像の優先順位が重複しないように
				name='unique_photo_priority',
				fields=['item', 'priority'],  # itemとpriorityの組み合わせでデータを複数登録することは出来ない
			),
		]

	def __str__(self):
		return f'{self.item.name}({self.priority})'


class Owner(models.Model): # 出品者
	""" """

	# Djangoのモデルで1対1の関係を作成する方法
	# 1.OneToOneFieldを使う
	# 2.ForeignKeyにunique=Trueを設定して使う
	user = models.OneToOneField( # ForeignKey(unique=True)の様なもの
		settings.AUTH_USER_MODEL,
		on_delete=models.CASCADE,
		verbose_name="ユーザー番号",)
	item = models.ManyToManyField(Item, related_name="owner_item", 
		verbose_name="商品番号",)
	note = models.TextField("注釈", blank=True, null=True,)

	class Meta:
		db_table = "owner"
		verbose_name = "出品者"
		verbose_name_plural = verbose_name
		indexes = [
			# models.Index(fields=["user",], name="owner_user"),
		]

	def __str__(self):
		return str(self.user.email)


####################################################
###   Review
####################################################

class ItemReview(models.Model):
	""" アイテムレビュー """
	STARS_CHOICES = (
		(1, "★☆☆☆☆"),
		(2, "★★☆☆☆"),
		(3, "★★★☆☆"),
		(4, "★★★★☆"),
		(5, "★★★★★"),
	)

	item = models.ForeignKey(Item, verbose_name="商品番号",
		on_delete=models.CASCADE, related_name="item_review",) # PROTECTの方が良い？
	user = models.ForeignKey( 
		settings.AUTH_USER_MODEL,
		verbose_name="ユーザー番号",
		on_delete=models.CASCADE, related_name="user_review",
	)
	stars = models.SmallIntegerField("評価", # evaluation
		choices=STARS_CHOICES, blank=False, default=3,
		validators=[MinValueValidator(1), MaxValueValidator(5)],
		#help_text="Star",
	)
	comment = models.TextField("コメント", blank=True,)
	created_at = models.DateTimeField("投稿日時", auto_now_add=True,)
	updated_at = models.DateTimeField("更新日時", auto_now=True,)
	note = models.TextField("注釈", blank=True, null=True,)

	class Meta:
		db_table = "item_review"
		verbose_name = "商品レビュー"
		verbose_name_plural = verbose_name
		indexes = []
		constraints = [
			models.UniqueConstraint(
				name='unique_user_review',
				fields=['user', 'item',], # 同じユーザーに同じ商品のレビューはさせない
			),
		]

	def __str__(self):
		return "{}(user_id:{})".format(self.item.id, self.user.id)

	def show_stars(self):
		""" 参考として、stars = Models.PositiveIntegerを使う場合 """
		return self.stars * "★"


####################################################
###   Cart
####################################################

class CartItem(models.Model): # Order
	"""  """
	user = models.ForeignKey(settings.AUTH_USER_MODEL, 
		verbose_name="注文者",
		on_delete=models.CASCADE, null=False,
		related_name="cart_user",)
	# session_id = models.UUIDField("セッションID", primary_key=False, 
	# 								default=uuid.uuid4, editable=True, blank=False, null=False,)
	item = models.ForeignKey(Item, verbose_name="商品番号", 
		on_delete=models.CASCADE, 
		related_name="cart_item", null=False,)
	quantity = models.PositiveSmallIntegerField("購入数", default=0) # sales_figures
	created_at = models.DateTimeField('作成時刻', auto_now_add=True)
	updated_at = models.DateTimeField("更新日時", auto_now=True)
	note = models.TextField("注釈", blank=True, null=True,)

	class Meta:
		db_table = "cart"
		verbose_name = "カート"
		verbose_name_plural = "カート"
		constraints = []

	def __str__(self):
		return str(self.id) # 注文番号


####################################################
###   Order
####################################################

class Order(models.Model):
	"""  """

	user = models.ForeignKey(settings.AUTH_USER_MODEL, 
		verbose_name="注文者番号",
		on_delete=models.SET_NULL, # ユーザーが退会しても受注履歴は残したいので
		null=True, # models.SET_NULLを指定するため
		related_name="order_user",
		# default='-1', # on_delete=models.SET_DEFAULTを指定する場合
	)
	total_price = models.DecimalField("合計金額", # お金はDECIMAL型
		max_digits=12, # 全体の桁数
		decimal_places=2, # 小数点以下の桁数
		default=0.00, 
	) 
	created_at = models.DateTimeField("注文確定日時", auto_now_add=True)
	updated_at = models.DateTimeField("更新日時", auto_now=True)
	is_paid = models.BooleanField("支払い状況", default=False)
	note = models.TextField("注釈", blank=True, null=True,)

	class Meta:
		db_table = "order"
		verbose_name = "注文"
		verbose_name_plural = "注文"

	def __str__(self):
		return str(self.id)


class OrderDetail(models.Model):
	"""  """

	# choicesの値を取得する場合は、Table.objects.last().get_{field_name}_display()など
	PAYMENT_CHOICES = (
		(1, "現金払い"),
		(2, "クレジットカード決済"),
		(3, "電子マネー決済"),
		(4, "銀行振込"),
		(5, "キャリア決済"),
		(6, "コンビニ決済"),
		(7, "代引き"),
	)
	order = models.ForeignKey(Order, verbose_name="注文番号", 
		on_delete=models.CASCADE, # 注文が取り消された場合に注文明細も同時に削除されるように
		null=False,
		related_name="orderdetail_orderid",) 
	item = models.ForeignKey(Item, verbose_name="商品番号", 
		on_delete=models.SET_NULL, # 限定販売品などの商品がモデルから削除されても残したいので 
		null=True, # models.SET_NULLを指定するため
		related_name="orderdetail_item", )
	payment = models.SmallIntegerField("支払い方法", choices=PAYMENT_CHOICES, 
		blank=False, default=2)
	# postage = models.IntegerField("送料")
	price = models.DecimalField("価格", # 注文確定時とPriceが違う場合があるのでItem.priceから参照しない
		max_digits=12, # 全体の桁数（必須）
		decimal_places=2, # 小数点以下の桁数（必須）
		default=0.00, 
		blank=False,
	) 
	quantity = models.SmallIntegerField("注文数")
	created_at = models.DateTimeField("作成日時", auto_now_add=True)
	updated_at = models.DateTimeField("更新日時", auto_now=True)
	note = models.TextField("注釈", blank=True,	)

	class Meta:
		db_table = "order_detail"
		verbose_name = "注文明細"
		verbose_name_plural = "注文明細"
		constraints = [
			models.UniqueConstraint(
				name='unique_order_and_items',
				fields=['order', 'item'], # 同じ注文で同じ商品が登録されないように
			),
		]

	def __str__(self):
		return str(self.id)




# ItemPhoto（多側）からItem(一側)を取得する場合
# itemphotos = ItemPhoto.objects.all()
# for photo in itemphotos: print(photo.item.name)

# Item(一側)からItemPhoto（多側）を取得する場合（逆参照）
# items = Item.objects.all()
# for item in items: item.item_photo.all()
# for item in items: item.item_photo.filter(priority=1)

import pdb # pdb.set_trace() or breakpoint()

from django.contrib.auth import get_user_model
from django.test import TestCase, RequestFactory
from django.urls import resolve
from django.test import tag

from snippets.models import Snippet
from snippets.views import top, snippet_new, snippet_edit, snippet_detail

UserModel = get_user_model()


@tag('snippets')
class TopPageTest(TestCase):
    def test_top_page_returns_200_and_expected_title(self):
        response = self.client.get("/snippets/")
        self.assertContains(response, "Djangoスニペット", status_code=200)

    def test_top_page_uses_expected_template(self):
        response = self.client.get("/snippets/")
        self.assertTemplateUsed(response, "snippets/top.html")


@tag('snippets')
class TopPageRenderSnippetsTest(TestCase):
    def setUp(self):
        self.user = UserModel.objects.create(
            email="test@example.com",
            password="top_secret_pass0001",
        )
        self.snippet = Snippet.objects.create(
            title="title1",
            code="print('hello')",
            description="description1",
            created_by=self.user,
        )

    def test_should_return_snippet_title(self):
        request = RequestFactory().get("/")
        request.user = self.user
        response = top(request)
        self.assertContains(response, self.snippet.title)

    def test_should_return_email(self):
        request = RequestFactory().get("/")
        request.user = self.user
        response = top(request)
        self.assertContains(response, self.user.email)


@tag('snippets')
class CreateSnippetTest(TestCase):
    def setUp(self):
        self.user = UserModel.objects.create(
            email="test@example.com",
            password="secret",
        )
        self.client.force_login(self.user) # ユーザーログイン

    def test_render_creation_form(self):
        response = self.client.get("/snippets/new/")
        self.assertContains(response, "スニペットの登録", status_code=200)

    def test_create_snippet(self):
        data = {'title': 'タイトル', 'code': 'コード', 'description': '解説'}
        self.client.post("/snippets/new/", data)
        snippet = Snippet.objects.get(title='タイトル')
        self.assertEqual('コード', snippet.code)
        self.assertEqual('解説', snippet.description)


@tag('snippets')
class SnippetDetailTest(TestCase):
    def setUp(self):
        self.user = UserModel.objects.create(
            email="test@example.com",
            password="secret",
        )
        self.snippet = Snippet.objects.create(
            title="タイトル",
            code="コード",
            description="解説",
            created_by=self.user,
        )
        self.client.force_login(self.user)

    ''' <HttpResponseRedirect status_code=302, url="/accounts/login/?next=/snippets/2/">が返ってくる
    def test_should_use_expected_template(self):
        response = self.client.get("/snippets/%s/" % self.snippet.id)
        pdb.set_trace()
        self.assertTemplateUsed(response, "snippets/snippet_detail.html")

    def test_top_page_returns_200_and_expected_heading(self):
        response = self.client.get("/snippets/%s/" % self.snippet.id)
        self.assertContains(response, self.snippet.title, status_code=200)
    '''

@tag('snippets')
class EditSnippetTest(TestCase):
    def test_should_resolve_snippet_edit(self):
        found = resolve("/snippets/1/edit/")
        self.assertEqual(snippet_edit, found.func)


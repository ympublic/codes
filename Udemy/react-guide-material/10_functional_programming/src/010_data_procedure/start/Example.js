// useReducer: useStateの書き換えに利用する

const Example = () => {
  const [state, setAState] = useState(0);
  const myClick = () => {
    setAState((prev) => ++prev);
  };

  return (
    <>
      <h3>状態管理と処理を分離</h3>
      <button onClick={() => setAState((prev) => prev + 1)}>ボタン</button>
    </>
  );
};

export default Example;

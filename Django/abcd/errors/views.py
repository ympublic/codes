from django.shortcuts import render

####################################################
###   Error
####################################################

# handler400を上書きする
def handler400(request, exception): # 403, 500も同様
	template = "errors/400.html"
	return render(request, template, {}, status=400)

def handler500(request, exception, template_name="errors/500.html"):
	template = "errors/500.html"
	return render(request, template, {}, status=500)

# handler404を上書きする
from urllib.parse import quote
def handler404(request, exception):
	template = "errors/404.html"
	context = {'request_path': quote(request.path)}
	return render(request, template, context, status=404)


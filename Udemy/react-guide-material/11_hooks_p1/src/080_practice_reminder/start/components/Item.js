import { useState } from "react";
import { useTodosDispatch } from "../context/TodoContext";

// グローバルstateを使用しているので、引数にpropsは不要
const Item = (props) => {
  // propsを分割代入
  const { todo } = props;

  // Dispatch
  const dispatch = useTodosDispatch();

  // <span>をダブルクリックで編集できるようにstateを定義
  const [editingContent, setEditingContent] = useState(todo.content);

  // 文字列を入力し変更
  const changeContent = (e) => {
    setEditingContent(e.target.value);
  };

  // <span>をダブルクリックで編集可能に
  const toggleEditMode = () => {
    // editingのboolean値を反転
    const newTodo = { ...todo, editing: !todo.editing };
    dispatch({ type: "update", todo: newTodo });
  };

  const confirmContent = (e) => {
    e.preventDefault();
    // editingの状態とタイトルを変更する
    const newTodo = {
      ...todo,
      editing: !todo.editing,
      content: editingContent,
    };
    dispatch({ type: "update", todo: newTodo });
  };

  const complete = (todo) => {
    dispatch({ type: "delete", todo });
  };

  return (
    <div>
      <div key={todo.id}>
        <button onClick={() => complete(todo)}>完了</button>
        {/** Enterキーで実行できるように<form>で囲む */}
        <form onSubmit={confirmContent} style={{ display: "inline" }}>
          {todo.editing ? (
            <input
              type="text"
              value={editingContent}
              onChange={changeContent}
            />
          ) : (
            <span onDoubleClick={toggleEditMode}>{todo.content}</span>
          )}
        </form>
      </div>
    </div>
  );
};

export default Item;

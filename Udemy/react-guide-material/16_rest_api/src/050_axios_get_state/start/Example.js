import { useEffect, useState } from "react";
import { axios } from "axios";

// 関数コンポーネント内は純粋関数として保つ必要がある
// 非同期通信などように副作用のある処理はuseEffect()の中で書く必要がある
const Example = () => {
  // 注意: useState([])として初期値を設定している
  // 理由としては下記にてusers.map()を行っているが、
  // 初期値を設定しないとusersはundefinedとなる
  // しかしundefinedはmap()メソッドを持っていないのでエラーとなる
  const [users, setUsers] = useState([]);

  useEffect(() => {
    // 返り値はPromiseオブジェクトが返ってくる
    // レスポンスが返ってくるまでに時間がかかるので非同期
    // なので.then(非同期処理)で処理を続ける
    const res = axios.get("http://localhost:3003/user/").then((res) => {
      // サーバーから返ってきたdataを取得
      setUsers(res.data);
    });
    // 無限ループを避けるために、useEffect()の第２引数に空の配列を渡すのを忘れないこと！
  }, []);

  return (
    <div>
      {users.map((user) => {
        return (
          <div key={user.id}>
            <h3>{user.username}</h3>
            <p>age: {user.age}</p>
            <p>hobby: {user.hobbies.join(", ")}</p>
          </div>
        );
      })}
    </div>
  );
};

export default Example;

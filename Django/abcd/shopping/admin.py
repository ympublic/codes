from django.contrib import admin

from shopping.models import (
	Item, ItemCategory, ItemPhoto, ItemStock, ItemReview,
	G1, Foal, Owner, CartItem, Order, OrderDetail,
)


####################################################
###   Item
####################################################

class ItemAdmin(admin.ModelAdmin):
	model = Item
	# adminのチェンジリストページでどのフィールドを表示するか
	list_display = ('name', 'price', 'runs', 'wins',  
		'on_sale', 'dollar',)
	list_editable = ('price', 'on_sale',) # 簡易編集
	list_display_links = ('name',) # リンクの表示カラム
	search_field = ('name',) # 検索フィールド
	list_filter = ('on_sale',) # フィルター
	# actions = ['action',] # アクション / 管理画面の「操作」項目で選択できる

	def dollar(self, obj): # カスタムカラム
		return '${:.2f}'.format(obj.price / 130) # １ドル130円の場合

	# def get_queryset(self, request): return super().get_queryset(request)

	# def action(self, request, queryset): pass # 「選択された項目の日時を更新する」など

admin.site.register(Item, ItemAdmin) # ItemAdminクラスを適用


@admin.register(ItemPhoto) # デコレーターでも登録可
class ItemPhotoAdmin(admin.ModelAdmin):
	model = ItemPhoto
	list_display = ('item', 'photo', 'priority',) # 表示カラム
	list_editable = ('priority',) # 簡易編集
	ordering = ('-priority',) # ソート


@admin.register(G1)
class G1Admin(admin.ModelAdmin):
	model = G1
	list_display = ('item', 'g1',) # 表示カラム
	# exclude = ('note',) # クリック後の非表示カラム


@admin.register(ItemStock)
class ItemStockAdmin(admin.ModelAdmin):
	model = ItemStock
	list_display = ('item', 'stock',) # 表示カラム


@admin.register(Foal)
class FoalAdmin(admin.ModelAdmin):
	model = Foal
	list_display = ('item', 'name',) # 表示カラム


@admin.register(ItemCategory)
class ItemCatrgoryAdmin(admin.ModelAdmin):
	model = ItemStock
	list_display = ('parent_id', 'category_name', 'category_type', ) # 表示カラム


@admin.register(ItemReview)
class ItemReviewAdmin(admin.ModelAdmin):
	model = ItemReview
	list_display = ('item', 'user', 'stars', 'comment',) # 表示カラム


@admin.register(Owner)
class OwnerAdmin(admin.ModelAdmin):
	model = Owner
	list_display = ('user', 'get_item', ) # ManyToManyフィールド'item'はそのままでは表示させられない

	def get_item(self, obj):
		""" ManyToMany Fieldを表示させる """
		return "\n".join([p.name for p in obj.item.all()]) # 商品名を取得


####################################################
###   CartItem
####################################################

@admin.register(CartItem)
class CartAdmin(admin.ModelAdmin):
	model = CartItem
	list_display = ('user', 'item', 'quantity', )


####################################################
###   Order
####################################################

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
	model = Order
	list_display = ('user', 'total_price', 'is_paid', )


@admin.register(OrderDetail)
class OrderdetailAdmin(admin.ModelAdmin):
	model = OrderDetail
	list_display = ('order', 'item', 'price', 'quantity',)


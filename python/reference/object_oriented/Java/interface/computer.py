from tangibleasset import TangibleAsset

class Computer(TangibleAsset):

	"""  """

	def __init__(self, name:str, price:int, color:str, makername:str):
		super().__init__(name, price, color) # TangibleAsset
		self.__makername = makername

	@property
	def makername(self):
		return self.__makername

	@makername.setter
	def makername(self, value):
		self.__makername = value


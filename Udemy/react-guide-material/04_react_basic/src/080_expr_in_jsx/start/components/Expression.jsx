import "./Expression.css";

const Expression = () => {
  const title = "Expression";
  const arry = ["item1", "item2", "item3"];
  const hello = (arg) => `${arg} Function`;
  const jsx = <h3>Hello JSX</h3>;
  // {}の中には式のみで、文は入れることは出来ない
  // className="{title.toLowerCase()}"ではない
  return (
    <div className={title.toLowerCase()}>
      <h3>Hello {title}</h3>
      {/* 配列を渡すと自動で中身が展開される */}
      <h3>{arry}</h3>
      <h3>{hello("Hello")}</h3>
      <h3>{/* 画面上に表示されない */}</h3>
      {/* JSX記法 JavaScriptの式を表示 */}
      {<h3>Hello JSX</h3>}
      {jsx}
    </div>
  );
};

export default Expression;

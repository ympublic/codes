#!/usr/bin/env python3

from book import Book
from computer import Computer

def main():
	# インスタンス作成
	book1 = Book("Beatlesの全て", 1500, "black", "abcd-1234-1234")
	# インタフェースThingからオーバーライドしたメソッドを使用
	book1.setWeight(200)
	book1_weight = book1.getWeight()
	print(f"{book1_weight}g")

	# インスタンス作成
	computer1 = Computer("Surface Pro", 200000, "silver", "microsoft")
	# Setter
	computer1.name = "macbook pro"
	print(f"商品名は{computer1.name}です")


if __name__ == '__main__':
	main()


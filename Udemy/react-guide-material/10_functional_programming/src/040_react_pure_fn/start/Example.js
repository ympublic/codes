let value = 0;

const Child = (props) => {
  const { value } = props;
  return <div>{value}</div>;
};

const Example = () => {
  return (
    <>
      <Child value={++value} />
      <Child />
    </>
  );
};

export default Example;

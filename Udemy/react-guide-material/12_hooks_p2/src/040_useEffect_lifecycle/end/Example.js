// POINT useEffectの実行タイミング

import { useState, useEffect } from "react";

const Example = () => {
  const [state, setState] = useState(0);

  useEffect(
    // 依存配列内のstateが更新された際にupdate()が呼び出される
    function update() {
      // クリーンナップが処理された後に呼ばれる
      console.log("update");

      // クリーンナップ
      return function cleanUp() {
        console.log("update cleanup");
      };
    },
    // 依存配列
    [state]
  );

  useEffect(() => {
    console.log("mount");

    return () => {
      console.log("mount cleanup");
    };
  }, []);

  console.log("render");

  return (
    <>
      <button onClick={() => setState((prev) => prev + 1)}>更新</button>
      <h3>他のレクチャーを選ぶとunmountが呼ばれます。</h3>
    </>
  );
};

export default Example;

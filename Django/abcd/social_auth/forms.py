from allauth.socialaccount.forms import SignupForm
from django.http import Http404

class MyCustomSocialSignupForm(SignupForm):
	# print('★★★ SocialSighup ★★★')
	
	# 注意： settings.pyにて、SOCIALACCOUNT_AUTO_SIGNUP = Falseを指定しないと
	# 以下のsave()が立ち上がらない
	def save(self, request):
		user = super(MyCustomSocialSignupForm, self).save(request)

		# request.userはAnonymousになるが、userはidやemailなどを保持している
		# 他のプロパティを参照したい場合は、
		# user.socialaccount_set.filter(provider='google')[0].extra_data['given_name']
		# from accounts.models import MyUser
		#try:
		#	myuser = MyUser.objects.get(pk=user.id)
		#	myuser.socialaccount_set.filter(provider='google')[0].extra_data['given_name']
		#except MyUser.DoesNotExist:
		#	raise Http404("No User is matched by the given query.")
		#else:
		#	pass
		#	breakpoint()

		return user

	#def signup(self, request, user):
	#	print("★★★ Let's SocialSighup ★★★")


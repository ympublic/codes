import { produce } from "immer";

const state = {
  name: "Tom",
  // Immerでは、ネストされていたとしてもImmutabilityを保持できる
  hobbies: ["soccer", "fishing"],
};

// Immerを使う場合
// produce(baseState, recipe: (draftState) => void): nextState
// 第１引数: 元のオブジェクト
// 第２引数: コールバック関数
const newState = produce(state, (draft) => {
  // Immernai内ではMutableな操作がImmutablityを保持されたものとなる
  draft.name = "John";
  draft.hobbies[0] = "baseball";
  console.log(draft);
  // return文は書かない
});

console.log(state, newState);

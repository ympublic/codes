import { useState } from "react";

const Example = () => {
  const numArray = [1, 2, 3, 4, 5];
  const [nums, setNums] = useState(numArray);
  const suffle = (array) => {
    // 配列をコピー
    const newNums = [...nums];
    const value = newNums.pop();
    newNums.unshift(value);
    setNums(newNums);
  };
  return (
    <>
      <h1>{nums}</h1>
      <button onClick={suffle}>シャッフル</button>
    </>
  );
};

export default Example;

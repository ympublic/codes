# -*- coding: utf-8 -*-
# cf: https://librabuch.jp/blog/2013/05/python_pillow_pil/
# facebook file size: https://ja-jp.facebook.com/help/125379114252045

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from PIL import ImageOps


def img_resize(image, width, height, method="resize", codec="JPEG"):
    img = Image.open(image, 'r')
    if method == "resize":  # resize
        resized_img = img.resize((width, height), Image.ANTIALIAS)
        resized_img.save('resize.jpg', codec, quality=75)
    elif method == "thumbnail":  # thumbnail
        img.thumbnail((width, height), Image.ANTIALIAS)
        img.save('thumbnail.jpg', codec, quality=75)
    elif method == "fit":  # crop
        crop = ImageOps.fit(image, (width, height), Image.ANTIALIAS)
        crop.save('crop.jpg', codec, quality=75)
        

image = "python.gif"
width = 160
height = 160
method = "fit"
codec = "gif"

img_resize(image, width, height, method, codec)
import { useState, useTransition } from "react";

const generateDummyItem = (num) => {
  // map()はundefinedなものはスキップしてしまうので、一度fill(null)をしている
  return new Array(num).fill(null).map((item, index) => `item ${index}`);
};

const dummyItems = generateDummyItem(10000);

const Example = () => {
  const [isPending, startTransition] = useTransition();
  const [filterVal, setFilterVal] = useState("");

  const changeHandler = (e) => {
    // 以下の処理が実行中はisPendingにはtrueが返ってくる
    // 処理が終わった時にfalseが返ってくる
    startTransition(() => {
      // 優先順位が下がる
      setFilterVal(e.target.value);
    });
  };

  return (
    <>
      <input type="text" onChange={changeHandler} />
      {isPending && <div>Loading...</div>}
      <ul>
        {dummyItems
          .filter((item) => {
            // 空文字の場合は全て返す
            if (filterVal === "") return true;
            // 入力された値が含まれているものを返す
            return item.includes(filterVal);
          })
          .map((item) => (
            <li key={item}>{item}</li>
          ))}
      </ul>
    </>
  );
};

export default Example;

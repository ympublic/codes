# 何かしらの処理が完了したことを他の場所に通知する仕組み
# このままだとsignals.pyを読み込んでくれないので、apps.pyを編集

# SignalsはModels.pyやurls.py、rootモジュールには入れずに、signals.pyに書き込む

# django-allauth / Signals
# https://django-allauth.readthedocs.io/en/latest/signals.html

import requests
# Django
from django.dispatch import receiver
from allauth.account.signals import email_confirmed
from allauth.account.signals import user_signed_up
from allauth.socialaccount.signals import pre_social_login
from allauth.socialaccount.signals import social_account_added
from allauth.socialaccount.signals import social_account_updated
from allauth.socialaccount.signals import social_account_removed
from allauth.account.models import EmailAddress
# Third Party
from allauth.socialaccount.models import SocialAccount
# Application
from accounts.models import MyUser
from accounts.models import Profile
from accounts.models import ProfImage


# @receiverデコレーターを使う場合は、apps.pyのready()内でimportする必要がある
# django-allauthではreciever(sender=)は特に不要(kwargsでsignal, sender, request, email_addressなどを受け取っている)
@receiver(social_account_added)
def social_account_add_(request, sociallogin, **kwargs): # 引数のsocialloginはdjango-allauthのDocumentを参照
	"""  """
	print("☆☆☆ Social_account_added ☆☆☆")
	breakpoint()


# email_confirmedというSignalを受け取る度に実行される関数
@receiver(email_confirmed) # メールアドレスの認証が完了した際に出されるSignal
def email_confirmed_(request, email_address, **kwargs):
	""" 新しいメールアドレスを認証が終わったら、古いメールアドレスを削除する """
	user = email_address.user
	old_email = EmailAddress.objects.filter(user=user).exclude(email=email_address.email)
	if old_mail.exists(): # 「条件〇〇」に合うレコードが存在するかどうか
		user.email = email_address.email
		user.save()
		email_address.primary = True
		email_address.save()
	else:
		pass


# pre_social_loginというSignalを受け取ると実行される関数
# ユーザーがソーシャルプロバイダーから認証され、ログインが完全に処理される直前に送信される
# login時だけではなく、サインアップやソーシャルアカウントを追加する際にも送信される
@receiver(pre_social_login) 
def pre_social_login_(request, sociallogin, **kwargs): # 引数のsocialloginはallauthのDocumentを参照
	"""  """
	if sociallogin.is_existing:
		print("★★★ Social_account_is_existing ★★★")
		return
	else:
		print("★★★ Social_account_is_NOT_existing ★★★")
		# サインアップ前なので、ここではMyUserと他のテーブルを紐付けられない


	if not sociallogin.email_addresses:
		return 

	verified_email = None
	for email in sociallogin.email_addresses:
		if email.verified:
			verified_email = email
			break

	if not verified_email:
		return

	try: # ソーシャルアカウントと登録メールアドレスを紐付ける
		existing_email = EmailAddress.objects.get(email__iexact=email.email, verified=True)
	except EmailAddress.DoesNotExist:
		return

	# Signalを無効にしたいときは、disconnect()
	sociallogin.connect(request, existing_email.user)


@receiver(social_account_removed)
def social_account_removed_(request, sociallogin, **kwrags):
	""" ソーシャルアカウントの削除 """
	print("★★★ {}を削除します ★★★".format(sociallogin.user.email))

# @receiverデコレーターを使用しない方法で定義
# social_account_removed.connect(social_account_removed_)


# 注意：普通のサインアップでは以下はproviderがout of rangeになる
@receiver(user_signed_up)
def user_signed_up_(request, user, **kwargs):
	""" social_authのユーザー作成時にSignalを受け取り、ProfileとProfImageテーブルを紐付ける """
	print("☆☆☆ user is signed up ☆☆☆")
	# requestの中身はAnonymousだが、user.idやuser.emailは取得できる
	provider = user.socialaccount_set.all()[0].provider
	extra_data = SocialAccount.objects.filter(user=user, provider=provider)[0].extra_data

	# django-all-authから登録したユーザーを、ProfileとProfImageテーブルに紐付ける

	# プロフィール
	profile_obj = Profile(user=user)
	if provider:
		profile_obj.given_name = extra_data['given_name']
		profile_obj.family_name = extra_data['family_name']
	profile_obj.save()

	# プロフ画像
	profimage_obj = ProfImage(user=user)
	profimage_obj.save()

	# プロフ画像を取得 -> ProfEditForm()
	# response = requests.get(extra_data['picture'])
	# image = response.image

	# create()を使用する場合
	# Profile.objects.create(user=user,)
	# ProfImage.objects.create(user=user,)

	
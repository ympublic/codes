// react-domライブラリから、ReactDOMモジュールをimport
// 注: React V18からはReactDOM.renderは非推奨
// import ReactDOM from "react-dom";
// 代わりにcreatRootを使用する
import { createRoot } from "react-dom/client";

// 分割したコンポーネントをimport
import { App } from "./App";
// ContextのProvider
import { AdminFlagProvider } from "./components/providers/AdminFlagProvider";

/**
 * 再レンダリングが起きる条件
 * 1. Stateが更新されたコンポーネント
 * 2. Propsが変更されたコンポーネント
 * 3. 再レンダリングされたコンポーネント配下のコンポーネント全て
 *
 */

const root = createRoot(document.getElementById("root"));

// ContextのProviderでStateを扱いたいコンポーネントを囲む
root.render(
  <AdminFlagProvider>
    <App />
  </AdminFlagProvider>
);

from hero import Hero

class King(object):

	"""  """

	def talk(self, h:Hero):
		print(f"王様：ようこそ我が国へ、勇者{h.name}よ。")
		print(f"王様：長旅疲れたであろう。")
		print(f"王様：まずは城下町を見てくると良い。ではまた会おう。")
		print(f"王様：ところで、我が娘には手を出してはならぬぞ！")
		print(f"王様：もし万が一のことがあった場合は、命はないと思え！！")
		# h.__die() # クラス外からprivateメソッドにはアクセスできない
		
		if h.maritalStatus: # メソッドと違い getter には () は不要
			print(f"王様：むむ？そなたは既に'Yoko Ono'という最愛の妻がおったのか。")
			print(f"王様：それは失礼なことを申した。")


#! /usr/bin/env python

"""operator
operatorモジュール
演算子に対する関j数が提供されている
"""

import operator

# 算術演算子
print(f"2 + 3: {operator.add(2, 3)}") # 5

print(f"2 - 3: {operator.sub(2, 3)}") # -1

print(f"2 * 3: {operator.mul(2, 3)}") # 6

print(f"2 / 3: {operator.truediv(2, 3)}") # 0.66666666

print(f"2 // 3: {operator.floordiv(2, 3)}") # 0

# 比較演算子
print(f"2 < 3: {operator.lt(2, 3)}") 

print(f"2 <= 3: {operator.le(2, 3)}")

print(f"2 > 3: {operator.gt(2, 3)}") 

print(f"2 >= 3: {operator.ge(2, 3)}") 

print(f"2 == 3: {operator.eq(2, 3)}")

print(f"2 != 3: {operator.ne(2, 3)}") 

# オブジェクトの要素を取得
l = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]

f = operator.itemgetter(3) # 3桁めを取り出す

print(f(l)) # 30

rows = [('Alice', 1), ('Bob', 2), ('Carol', 3)]

o_m_1 = map(operator.itemgetter(0), rows) # 0桁めを取り出す

print(list(o_m_1)) # ['Alice', 'Bob', 'Carol']


class User(object):
	def __init__(self, name):
		self.name = name

rows = [User('Alice'), User('Bob'), User('Carol')]

o_m_2 = map(operator.attrgetter('name'), rows) # name属性のみ取り出す

print(list(o_m_2)) # # ['Alice', 'Bob', 'Carol']


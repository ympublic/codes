// 三項演算子（ ? : ）

const a = 0;
let resultA = a ? "Apple" : "Banana";

// if (a) {
//   resultA = "true";
// } else {
//   resultA = "false";
// }
console.log(resultA);

function getResult() {
  return a ? 10 : -10;
}

console.log(getResult());

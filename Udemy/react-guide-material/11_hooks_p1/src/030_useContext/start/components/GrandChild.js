import { useContext } from "react";
import { MyContext } from "../Example";

const GrandChild = () => {
  // Exampleのコンテキストを取得
  const value = useContext(MyContext);

  return (
    <div style={{ border: "1px solid black" }}>
      <h3>孫コンポーネント</h3>
      {/** コンテキストを使用 */}
      {value}
    </div>
  );
};
export default GrandChild;

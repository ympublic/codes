class ContainerLike(object):
	"""  """

	def __init__(self, size:int) -> None:
		self.size = size 

	def __len__(self) -> int:
		""" 要素数を100倍して返す """
		# integerでreturnしなければいけない
		return self.size * 100

	def __add__(self, other:int) -> int:
		""" 加算結果を100倍にして返す """
		return (self.size + other) * 100

	def __getitem__(self, i) -> int:
		""" in演算子などでも呼ばれる """
		if i >= self.size:
			raise IndexError("要素数が範囲外です")
		if type(i) != int:
			raise TypeError("int型ではありません")
		# 100倍して返す
		return i * 100


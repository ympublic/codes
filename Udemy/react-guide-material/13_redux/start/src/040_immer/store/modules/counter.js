import { createSlice } from "@reduxjs/toolkit";

const counter = createSlice({
  name: "counter",
  initialState: {
    count: 0,
  },
  reducers: {
    add(state, { type, payload }) {
      // ReduxToolkitの中では、Immerのおかげで、
      // Mutableな操作がImmutableな操作に置き換わる
      // returnは不要
      state.count = state.count + payload;
      // const newState = { ...state };
      // newState.count = state.count + payload;
      // return newState;
    },
    minus(state, { type, payload }) {
      // 純粋関数におけるImmutabilityの保持
      const newState = { ...state };
      newState.count = state.count - payload;
      return newState;
    },
  },
});

const { add, minus } = counter.actions;

export { add, minus };
export default counter.reducer;

import unittest

'''
①テストはunittest.TestCaseを継承したクラスの中に書く。
②テストの関数名はtest〜で始める。
③判定はself.assertEqual()等、unittestで用意されている判定を使用する。
python -m unittest tests/test_something.py
'''

####################################################
###   bsNews.py
####################################################


####################################################
###   Reference
####################################################

class TestStringMethods(unittest.TestCase):
    def setUp(self): # テストメソッドの直前に呼び出される
        pass

    def tearDown(self): # テストメソッドが実行され、結果が記録された直後に呼び出される
        pass

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)


def sumXY(x, y):
    return x + y

class TestHoge(unittest.TestCase):
    def test_sum(self):
        # テストパターンをlistで用意
        patterns = [ # (x, y, 正解値)の組み合わせ
            (0, 1, 1),
            (-1, 1, 0),
            (1, -2, -1),
        ]

        #############################
        #####      SubTest
        #############################
        # パターンが多いテストを実行する場合
        # 途中で失敗のパターンがあっても最後まで実行されるし、失敗したパターンも個別で表示してくれる
        for x, y, result in patterns: # 作成したテストパターンをforで回す
            # subTest()の引数には失敗時に出力したい内容を指定。パラメータ全てを入れておくのが無難
            with self.subTest(x=x, y=y, result=result):
                self.assertEqual(sumXY(x, y), result)


# 適当な例外クラス
class HogeError(Exception):
    def __init__(self, code, msg):
        self.code = code 
        self.msg = msg 

    def __str__(self):
        return "[{0}: {1}]".format(self.code, self.msg)


# 例外を返す適当な関数
def raise_error():
    raise HogeError(1234, "hoge")


class HogeTest(unittest.TestCase):
    # 発生した例外の種類だけチェックすれば十分な場合
    def test_hoge(self):
        with self.assertRaises(HogeError):
            raise_error()

        # withを使用しない場合
        self.assertRaises(HogeError, raise_error)

    # 発生した例外のエラーメッセージもチェックしたい場合
    def test_fuga(self):
        # エラーメッセージのチェックには正規表現が使用できる
        with self.assertRaisesRegex(HogeError, ".*1234.*hoge.*"):
            raise_error()

    # 発生した例外の中身もチェックする場合
    def test_piyo(self):
        with self.assertRaises(HogeError) as cm:
            raise_error()

        # コンテキストマネージャに格納された例外オブジェクト
        the_exception = cm.exception

        # 中身を好きにチェックできる
        self.assertEqual(the_exception.code, 1234)


if __name__ == '__main__':
    unittest.main()


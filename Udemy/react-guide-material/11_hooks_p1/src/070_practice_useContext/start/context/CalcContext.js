import { useContext, createContext, useReducer } from "react";

// state管理用コンテキスト
const CalcContext = createContext();
// 更新用関数用コンテキスト
const CalcDispatchContext = createContext();

const initState = {
  a: 1,
  b: 2,
  result: 3,
};

const reducer = (state, { type, payload }) => {
  switch (type) {
    case "change": {
      const { name, value } = payload;
      return { ...state, [name]: value };
    }
    case "add": {
      return { ...state, result: parseInt(state.a) + parseInt(state.b) };
    }
    case "minus": {
      return { ...state, result: state.a - state.b };
    }
    case "divide": {
      return { ...state, result: state.a / state.b };
    }
    case "multiply": {
      return { ...state, result: state.a * state.b };
    }
    default:
      throw new Error("operator is invalid");
  }
};

const CalcProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initState);

  return (
    <CalcContext.Provider value={state}>
      <CalcDispatchContext.Provider value={dispatch}>
        {children}
      </CalcDispatchContext.Provider>
    </CalcContext.Provider>
  );
};

const useCalc = () => {
  return useContext(CalcContext);
};

const useCalcDispatch = () => {
  return useContext(CalcDispatchContext);
};

export { CalcProvider, useCalc, useCalcDispatch };

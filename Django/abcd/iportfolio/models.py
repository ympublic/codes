import os
import uuid
from django.db import models
from django.conf import settings


####################################################
#####     お問い合わせフォーム
####################################################

# 自己バリデーション定義
def domain_cannot_be_test_com(value):
	""" ドメインにtest.comが指定された場合は弾く """
	if 'test.com' in value:
		raise ValidationError(
			_('domain, test.com, can not be used'),
			code='domain invalid', 
		)

	return None


class Contact(models.Model):
	""" お問合せフォームからの投稿を保存 """
	name = models.CharField('名前', max_length=32, blank=False)
	email = models.EmailField('Emailアドレス', max_length=128, blank=False,
		validators=[domain_cannot_be_test_com,], # 自己定義バリデーションを追加 
	)
	# phone = models.CharField('電話番号', max_length=13, blank=True)
	subject = models.CharField('件名', max_length=128, blank=False)
	message = models.TextField('メッセージ', blank=False)
	recieved_at = models.DateTimeField('登録時刻', # datetime: 日付+時刻
		blank=False, auto_now_add=True)
	note = models.TextField("注釈", blank=True, null=True,)

	class Meta:
		db_table = 'contact'
		verbose_name = 'お問い合せ'
		verbose_name_plural = 'お問い合せ'

	def __str__(self): 
		return "{}:{}".format(self.pk, self.subject)


class ContactStatus(models.Model):
	""" お問い合わせ内容の状況 """
	contact = models.OneToOneField(Contact, 
		verbose_name="お問い合わせID",
		on_delete=models.CASCADE,
	)
	is_emailed = models.BooleanField("確認用Email送信済み", default=False,)
	is_replied = models.BooleanField("回答済み", default=False,)
	is_closed = models.BooleanField("完了済み", default=False,) # finished
	note = models.TextField("注釈", blank=True, null=True,)

	class Meta:
		db_table = 'contact_status'
		verbose_name = 'お問い合わせ状況'
		verbose_name_plural = verbose_name

	def __str__(self): 
		return str(self.contact.id) # Contactの__str__()


import os
import uuid
from collections import deque # 先入れ先出し
# Django
from django.conf import settings
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import HttpResponseServerError
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic import FormView
from django.views.generic import DetailView
from django.views.generic import TemplateView
from django.views.generic import CreateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic.edit import ModelFormMixin
from django.views.generic.edit import FormMixin
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.views.decorators.http import require_safe
from django.views.decorators.http import require_http_methods
from django.contrib import messages
from django.core.mail import send_mail
from django.core.mail import EmailMessage # BCC
from django.template.loader import render_to_string # テンプレートを読み込む

# Application
from iportfolio.models import (Contact, ContactStatus, )
from iportfolio.forms import (ContactForm, )

MyUser = get_user_model()


####################################################
#####     お問い合わせフォーム
####################################################

# SESSION絡んだら関数ベースビューの方が実装しやすい...が、クラスベースビューでの実装も一応下記にて
@require_http_methods(['GET', 'POST'])
def iportfolio(request):
	""" iPortfolioトップページ """

	if request.method == 'POST':
		form = ContactForm(request.POST or None)
		if form.is_valid():
			# フォームの投稿内容をセッションに格納
			# if request.session.get("contact") is None: # <= 新しく入力した情報が投稿できなくなる
			request.session["contact"] = form.cleaned_data
			request.session.set_expiry(60 * 30) # 30min	
            
            # messages.add_message(request, level, message)
			messages.add_message(request, messages.SUCCESS, "以下の内容を受け取りました。")
			return redirect("iportfolio:contact_confirm")
		else:
			messages.add_message(request, messages.ERROR, "お問い合わせの送信に失敗しました。")
	else:
		if request.session.get('contact') is None:
			# contactパラメータがないということは、セッション切れかURLを直接入力したか
			form = ContactForm()
		else: # confirmページにて「戻る」ボタンを押した時にセッションの内容を自動入力させる
			form = ContactForm(initial=request.session.get('contact'))

	return render(request, "iportfolio/index.html", {'form': form})


@require_http_methods(['GET', 'POST'])
def contact_confirm(request):
	""" Contactフォーム確認画面 """

	# 直接ページを入力してきた場合、またはセッション切れや空の場合
	if 'HTTP_REFERER' not in request.META or request.session.get('contact') is None:
		return redirect("iportfolio:iportfolio")
	else:
		session_data = request.session.get("contact")

	if request.method == 'POST':
		form = ContactForm(session_data or None)
		if form.is_valid():
			request.session.pop("contact") # Sessionデータを破棄
			
			# DBにお問合せ内容を保存
			contact = form.save(commit=False)
			contact.save()

			# Gmailを送信
			mail_subject = '【Y Miyamoto】お問い合わせありがとうございます'
			ctx = {
				"inquiry": form.cleaned_data,
				"contact": contact,
			}
			# 返信用のテンプレートファイルを利用
			mail_message = render_to_string(f"{settings.BASE_DIR}/iportfolio/templates/iportfolio/mail/mail_template.txt", 
				ctx, request
			)
			mail_from = settings.SERVER_EMAIL
			mail_to = [form.cleaned_data['email'],] # toはlistかtupleである必要がある
			bcc = [settings.BCC,]

			# TODO: メールはその場で送信するよりも、格納しておいてバックグラウンドで送信するような実装を行うべき
			try:
				# 普通の送信方法
				send_mail(mail_subject, mail_message, mail_from, mail_to, fail_silently=False,)
				# print("Mail is sent")	

				# BCCを追加して送る場合
				# bcc_msg =EmailMessage(mail_subject, mail_message, mail_from, mail_to, bcc, fail_silently=False,)
				# bcc_msg.send()

				# DBにis_mailed = Trueを指定して登録
				contact_status = ContactStatus.objects.create(
					contact=contact, # OneToOneFiled
					is_emailed=True,
				)
				# print("ContactStatus is saved on DB")

			except:
				# DBにis_mailed = Falseのままで登録
				contact_status = ContactStatus.objects.create(
					contact=contact, # OneToOneFiled
					is_mailed=False, # Default
				)
				
				''' または
				status_form = ContactStatusForm()
				status = status_form.save(commit=False)
				status.contact = contact # 上のcontactと紐づける
				user.contactstatus.save()
				'''

				return HttpResponseServerError( # ステータスコード500(Internal Server Error)
					'<h2>メール送信エラー</h2>'
					'<p>申し訳ございません。契約サーバーによるメール送信に不具合が発生しております</p>'
					'<p>お問い合わせは完了しておりますので、こちらからの返答をお待ちください</p>'
					'<a href="/iportfolio/" class="btn">戻る</a>'
				)

			return redirect("iportfolio:contact_complete")
	
	template = "iportfolio/contact/contact_confirm.html"
	context = {'data': session_data}
	return render(request, template, context)


def contact_complete(request):
	""" フォーム完了画面 """

	# /contact_confirm/以外から訪問されてきたかどうか
	if 'HTTP_REFERER' not in request.META:
		return redirect("iportfolio:iportfolio")
	elif request.META['HTTP_REFERER'].find('/iportfolio/contact_confirm/') == -1: # orでも良いが長くなるので
		return redirect("iportfolio:iportfolio")

	template = "iportfolio/contact/contact_complete.html"
	return render(request, template)



####################################################
#####     SESSION絡めたクラスベースビューで実装する場合      
####################################################

class TopPageView(FormView):
	"""  """
	template_name = "iportfolio/index.html"
	form_class = ContactForm
	# success_url = reverse_lazy("iportfolio:contact_confirm")
	# context_object_name = "top"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		return context

	def get_form(self, form_class=None):
		if self.request.POST:
			form_data = self.request.POST
		else:
			# フォームに初期値を渡す場合
			form_data = self.request.session.get('contact')

		return self.form_class(form_data)

	'''
	def get_form_kwargs(self):
		kwargs = super().get_form_kwargs()
		if self.request.session.get("contact"):
			data = self.request.session.get("contact")
			kwargs.update(data)
			# kwargs['name'] = data['name']
			# kwargs['email'] = data['email']
			# kwargs['phone'] = data['phone']
			# kwargs['subject'] = data['subject']
			# kwargs['message'] = data['message']

		return kwargs
	'''

	def form_valid(self, form):
		# フォームの投稿内容をセッションに格納
		if self.request.session.get("contact") is None:
			self.request.session["contact"] = form.cleaned_data
			self.request.session.set_expiry(60 * 30) # 30min

		return super().form_valid(form)
	
	def form_invalid(self, form):
		print("##### form_invalid in {} #####".format("TopPageView"))
		return super().form_invalid(form)

	def get_success_url(self):
		return reverse("iportfolio:contact_confirm", kwargs={}) 


class ContactConfirmView(FormView):
	template_name = "iportfolio/contact/contact_confirm.html"
	success_url = reverse_lazy("iportfolio:contact_complete")
	form_class = ContactForm

	def dispatch(self, request, *args, **kwargs):
		# セッション切れや空の場合
		if self.request.session.get("contact") is None: 
			return redirect("iportfolio:iportfolio_top")
	
		self.session_data = self.request.session.get('contact')
		return super().dispatch(request, *args, **kwargs)

	def get_form_kwargs(self, **kwargs):
		kwargs = super().get_form_kwargs(**kwargs)
		# kwargs['request'] = self.requet
		return kwargs

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context["data"] = self.session_data
		return context

	def form_valid(self, form):
		self.request.session.pop('contact')
		print("##### form_valid in {} #####".format("ContactConfirmView"))
		form.save()
		return super().form_valid(form)

	def form_invalid(self, form):
		print("##### form_invalid in {} #####".format("ContactConfirmView"))
		return super().form_invalid(form)


class ContactCompleteView(TemplateView):
	template_name = "iportfolio/contact/contact_complete.html"

	def dispatch(self, request, *args, **kwargs):
		# /contact_confirm/以外から訪問されてきたかどうか
		if 'HTTP_REFERER' not in request.META:
			return redirect("iportfolio:iportfolio_top")
		elif request.META.HTTP_REFERER.find('/iportfolio/confirm/') == -1: # orでも良いが長くなるので
			return redirect("iportfolio:iportfolio_top")
		
		return super().dispatch(request, *args, **kwargs)


const numbers = [957, 268, 838];

const Example = () => {
  const addList = () => {
    const targetUl = document.querySelector("ul");
    //targetUl.
  };

  return (
    <>
      <h3>練習問題</h3>
      <p>
        Profileコンポーネントを使用して、完成コードと同じ画面を作成してください。
      </p>
      <p>
        また、Profileコンポーネント内のリスト表示部分のkeyを設定して、ワーニング表示がされないようにしてください。
      </p>

      <button onClick={addList}>先頭に追加</button>

      <h3>key=(ユニークキー)</h3>
      <ul className="add">
        {numbers.map((number) => {
          return (
            <li key={number}>
              {number}: <input type="text" />
            </li>
          );
        })}
      </ul>

      <h3>key=(index)</h3>
      <ul className="add">
        {numbers.map((number, index) => {
          return (
            <li key={index}>
              {number}: <input type="text" />
            </li>
          );
        })}
      </ul>
    </>
  );
};

export default Example;

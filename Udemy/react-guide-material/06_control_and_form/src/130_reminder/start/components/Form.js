import { useState } from "react";
const Form = (props) => {
  const { createTodo } = props;
  const [enteredTodo, setEnteredTodo] = useState("");
  const myTodo = (e) => {
    setEnteredTodo(e.target.value);
  };
  const addTodo = (e) => {
    // <form>のaction属性を指定していないので
    // ブラウザのデフォルト動作(リロード)を停止させる
    e.preventDefault();

    const newTodo = {
      id: Math.floor(Math.random() * 1e5),
      content: enteredTodo,
    };
    createTodo(newTodo);

    // 追加された後は入力欄を空にしておく
    setEnteredTodo("");
  };
  return (
    <div>
      {/** onSubmit: Enterが押された時 */}
      <form onSubmit={addTodo}>
        <input type="text" value={enteredTodo} onChange={myTodo} />
        <button>追加</button>
      </form>
    </div>
  );
};

export default Form;

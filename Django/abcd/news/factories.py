# FactoryBoy + Fakerを使用
# FactoryBoy: 
# Faker: 

import string

# Django
from django.contrib.auth import get_user_model

# Application
from news.models import News

# Third Party
# Factory Boy
import factory
from factory.faker import faker
from factory import fuzzy # Fuzzy系はランダムな値を生成するためのオブジェクト
# Faker
from faker import Faker

UserModel = get_user_model()
fake = Faker('jp-JP') # 日本語のFakeを作成


####################################################
#####     News
####################################################

class UserFactory(factory.django.DjangoModelFactory):
	class Meta:
		model = UserModel

	email = factory.Faker('email', locale='ja-JP')
	is_staff = False
	is_active = True


# from iportfolio.factories import ContactFactory
class NewsFactory(factory.django.DjangoModelFactory):
	""" news = News() -> news.name のように使う """
	class Meta:
		model = News # 'news.News'

	# https://faker.readthedocs.io/en/master/locales/ja_JP.html#faker-providers-person
	title = fake.sentence() # 日本語の名前を作成
	body = fake.text(200)
	url = fake.url()
	published_at = fake.date_time() 
	image_src = fake.image_url()



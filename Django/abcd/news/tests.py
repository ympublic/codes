# 重要：基本的に、「１つのテストは１つのことを検証すること」が推奨されているのだが...
# DjangoのテストはDEBUG=Falseを指定したものとして実行される
# デフォルトではCSRFチェックが無効となっている 有効にする場合：c = Client(enforce_csrf_checks=True)

# テストデータベースの破棄を防ぐ場合
# test --keepdb
# テストを並列実行させたい場合
# test --parallel
# パスワードのハッシュ生成
# PASSWORD_HASHES = [
#   'django.contrib.auth.hashers.MD5PasswordHasher',
# ]


# コードカバレージを確認
# coverage run --source='.' manage.py test
# coverage report

import pdb # pdb.set_trace() or breakpoint()
from unittest import mock

# Django
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponseRedirect
from django.test import TestCase, RequestFactory, Client
from django.urls import resolve, reverse, reverse_lazy
from django.test import tag
from django.template import Template, Context
from django.core import mail
from django.core.validators import (
MaxLengthValidator, MaxValueValidator,
MinLengthValidator, MinValueValidator,
)
from django.core.exceptions import ValidationError
from django.test.utils import override_settings # @override_settings()

# Applicaion
from news import views
# from news.models import (, )
# from news.forms import (Form, )

from news.factories import NewsFactory # Fake


UserModel = get_user_model()


####################################################
##### NewsListテスト
####################################################

@tag('news', 'view',) # ./manage.py test --tag=news --exclude-tag=complete
class NewsListTest(TestCase): # DBを扱うのでTestCase
	""" news_listをテスト """

	def setUp(self):
		""" 初期設定 """
		# GETやPOSTメソッドのRequestオブジェクトを作成する
		self.client = Client() # response = self.client.get() 
		self.url = "/news/" # reverse(iportfolio:iportfolio_top)
		self.factory = RequestFactory() # request = self.factory.get()
		self.user = UserModel.objects.create_user(
			email='oiocha@itoen.com',
			password='ocyacya',
		)


	#####################################
	##### ビューのテスト
	#####################################
	def test_top_page_uses_expected_template(self):
		""" テンプレートがちゃんと使われているかテスト """
		response = self.client.get(self.url) # 注意：urlのパスのみ渡す（ドメイン全体ではない）
		self.assertTemplateUsed(response, "news/news_list.html")

	@tag('new',)
	def test_bsnews_script(self):
		pass



import { useState, useContext } from "react";
import { MyContext } from "../Example";

const OtherChild = () => {
  // 分割代入
  const [, setState] = useContext(MyContext);
  const clickHandler = (e) => {
    setState((prev) => prev + 1);
  };

  return (
    <div>
      <h3>他の子コンポーネント</h3>
      <button onClick={clickHandler}>+</button>
    </div>
  );
};

export default OtherChild;

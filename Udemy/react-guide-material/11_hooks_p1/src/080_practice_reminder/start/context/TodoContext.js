import { createContext, useContext, useReducer } from "react";

// useReducer用のinitialArg
const initState = [
  {
    id: 1,
    content: "店予約する",
    editing: false,
  },
  {
    id: 2,
    content: "卵買う",
    editing: false,
  },
  {
    id: 3,
    content: "郵便出す",
    editing: false,
  },
];

// useReducer用のreducer(関数) / setState関数の代わり
// 引数はReduxに寄せている
const reducer = (state, action) => {
  switch (action.type) {
    case "add": {
      return [...state, action.todo];
    }
    case "update": {
      return state.map((_todo) => {
        return _todo.id === action.todo.id
          ? { ..._todo, ...action.todo }
          : { ..._todo };
      });
    }
    case "delete": {
      return state.filter((todo) => {
        return todo.id !== action.todo.id;
      });
    }
    default: {
      throw new Error("不明なパラメーターが渡されました");
    }
  }
};

// ⓵
// state用コンテキスト
const TodoContext = createContext();
// dispatch用コンテキスト
const TodoDispatchContext = createContext();

// ⓶
// 他のコンポーネントファイルで使用するコンテキスト関数を設定
const useTodos = () => {
  return useContext(TodoContext);
};

const useTodosDispatch = () => {
  return useContext(TodoDispatchContext);
};

// ⓷
// プロバイダーを設定
const TodoProvider = ({ children }) => {
  // const [state, dispatch] = useReducer(reducer, initialArg, init?)
  const [state, dispatch] = useReducer(reducer, initState);

  return (
    <TodoContext.Provider value={state}>
      <TodoDispatchContext.Provider value={dispatch}>
        {children}
      </TodoDispatchContext.Provider>
    </TodoContext.Provider>
  );
};

// export
export { TodoProvider, useTodos, useTodosDispatch };

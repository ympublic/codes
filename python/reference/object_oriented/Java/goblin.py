from __future__ import annotations # Fileの先頭で宣言する必要あり
from typing import TYPE_CHECKING # ランタイム中にはFalseであり、型チェック中にはTrueとなる
if TYPE_CHECKING:
	from character import Character
from monster import Monster
from typing import Final

class Goblin(Monster):

	"""  """

	def __init__(self, suffix:str="A"):
		super().__init__() # Monster
		self.__species: str = "ゴブリン"
		self.__suffix: str = suffix
		self.__hp: int = 50
		self.__level: Final[int] = 10 # 再代入不可

	# オーバーライド
	def run(self):
		print("{}{}は棍棒を振り回しながら逃げ出した！".format(self.__species, self.__suffix)) 

	# ポリモーフィズム
	# h:Heroではなく、c:Characterと指定することで、
	# Characterクラスを継承した子クラスは全て攻撃対象に指定できるようになる
	def attack(self, c:Character):
		print("{}{}は大きな棍棒を振り回した！".format(self.__species, self.__suffix))
		print("{}は10のダメージを受けた！".format(c.name))
		c.hp = c.hp - 10 # setter & getter

	# 親クラスでメンバ変数への@property, @.setterを定義していても、
	# コンストラクタでそのメンバ変数を継承していなければ
	# private変数へのgetter, setterの再定義が必要
	@property
	def species(self):
		return self.__species

	@property
	def suffix(self):
		return self.__suffix

	@suffix.setter
	def suffix(self, value):
		self.__suffix = value

	@property
	def hp(self):
		return self.__hp

	@property
	def level(self):
		return self.__level

	@species.setter
	def species(self, value):
		self.__species = value

	@hp.setter
	def hp(self, value):
		self.__hp = value

	@level.setter
	def level(self, value):
		self.__level = value


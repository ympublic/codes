class Friends:
    def __init__(self, connections):
        self.connections=[]
        for x in connections:
            if not sorted(x) in self.connections:
                self.connections.append(sorted(x))
        
    def add(self, connection):
        if sorted(connection) in self.connections:
            return False
        else:
            self.connections.append(sorted(connection))
            return True

    def remove(self, connection):
        if not sorted(connection) in self.connections:
            return False
        else:
            self.connections.remove(sorted(connection))
            return True

    def names(self):
        box=set()
        for x in self.connections:
            for w in x:
                box.add(w)
        return box

    def connected(self, name):
        box=set()
        for x in self.connections:
            if name in x:
                for y in x:
                    box.add(y)
                box.remove(name)
        return box



if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    letter_friends = Friends(({"a", "b"}, {"b", "c"}, {"c", "a"}, {"a", "c"}))
    digit_friends = Friends([{"1", "2"}, {"3", "1"}])
    assert letter_friends.add({"c", "d"}) is True, "Add"
    assert letter_friends.add({"c", "d"}) is False, "Add again"
    assert letter_friends.remove({"c", "d"}) is True, "Remove"
    assert digit_friends.remove({"c", "d"}) is False, "Remove non exists"
    assert letter_friends.names() == {"a", "b", "c"}, "Names"
    assert letter_friends.connected("d") == set(), "Non connected name"
    assert letter_friends.connected("a") == {"b", "c"}, "Connected name"
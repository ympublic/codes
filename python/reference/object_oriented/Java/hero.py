from string import Template # 滅多に使われないだろうが、あえてTemplate使用
from sword import Sword
from monster import Monster
from character import Character

import random
from typing import final # クラスの継承不可やオーバーライド禁止に使いたいのだが...

#######################################################
# メンバに関するアクセス就職の定石
# ①変数は全てprivate(メソッドを経由してアクセス)
# ②メソッドは全てpublic
#######################################################

##################################################################################################################
# カプセル化(encapsulate)のメリット
# ①setterを削除すればReadOnly, getterを削除すればのフィールドを実現できる
# ②フィールド名を変更した場合、getter等の内部で使用しているフィールド名を変更するだけで良くなる
# ③setter内にて値の妥当性をチェックするなど、フィールドへのアクセスを検査できる
##################################################################################################################

class Hero(Character): # 抽象クラスを継承

	""" 勇者 """

	#############################
	# static変数（クラス変数）
	#############################
	# 変更するとそのクラスからインスタンスされた全てのインスタンスのクラス変数が変更される
	# Hero.moneyでアクセス、直接変更
	# インスタンス.moneyではアクセスは出来るが、クラス変数自体を直接変更は出来ない
	# インスタンス経由でクラス変数を直接変更したい場合は、インスタンス.__class__.moneyで変更
	money: int 

	###################
	# コンストラクタ
	###################
	def __init__(self, name:str="Clapton"): # self: 自分自信のインスタンス
		# コンストラクタの引数にnameを指定しているので、
		# インスタンス作成時に("Trump")のようなnameの指定が必須

		# 親クラスのコンストラクタを呼び出す
		# 注意！: Pythonでは指定したインスタンス変数以外は継承されない
		super().__init__(name) # この場合nameにアクセスするにはself.nameとなる
		
		#####################
		# インスタンス変数
		#####################
		# インスタンス毎に独立した以下のインスタンス変数を持つ
		# publicメンバ変数
		# 実際は以下のpublic変数は全てカプセル化（private化し、setter, getterを定義）すべき
		self.gender: str

		# protectedインスタンス変数(_を１つ付ける)
		# JAVAではクラス内とクラス自身と継承したクラスのみからアクセスできるのだが、
		# Pythonではクラスの外からでもProtected変数にアクセスしたり直接編集できる
		# そのため、Protected変数にはクラス外からは直接アクセスしないように実装する
		# Protected変数は継承先からも参照できる
		# cf: https://www.nblog09.com/w/2019/01/09/python-protected-private/
		# protectedはあまり使われることはない
		self._quickness = 15

		# privateインスタンス変数(_を２つ付ける)
		# クラスの外からはアクセスできなくなる
		# Heroクラス以外からは、__mpフィールドに値を設定できない
		# 例えばmain.pyでh.__mpとしてもAttributeError発生
		# 継承先でもアクセス出来ない
		# しかしインスタンスメソッドからはアクセス出来る
		# ここではsleep()やincreaseMP()などを経由しなければならない
		# 他の開発者がバグを含んだコードを書いたとしても、__mpに想定外の値を設定出来ない
		# また、_Hero__nameでもアクセス出来るが、このような実装はすべきではない
		self.__mp: int = 10
		self.__marital_status: bool = True
		self.__hp: int  = 100
		self.__sword = Sword() # "has-a"(包含)の関係

	#################
	# デストラクタ
	#################
	def __del__(self):
		pass

	#################
	# 特殊メソッド
	#################
	def __str__(self):
		pass

	def __call__(self):
		pass

	#########################
	# インスタンスメソッド
	#########################
	# インスタンス変数にアクセス出来るが、クラス変数にはアクセス出来ない

	# publicメソッド
	def attack(self, m: Monster): # ポリモーフィズム(Monsterをザックリと捉える)
		tl1 = Template("$nameの攻撃！")
		print(tl1.substitute(name=self.name))
		m.hp -= 5
		tl2 = Template("$species$suffixに5ポイントのダメージを与えた！")
		print(tl2.substitute(species=m.species, suffix=m.suffix))
		tl3 = Template("$species$suffixから200ポイントの反撃を受けた！")
		print(tl3.substitute(species=m.species, suffix=m.suffix))
		self.hp -= 200
		if self.hp <= 0:
			self.__dead()
		
	def run(self):
		tl1 = Template("$nameは後ろも振り返らず、全速力で逃げ出した！")
		print(tl1.substitute(name=self.name))
		print("YOU ARE COWARD!!")

	def sit(self, sec: int):
		"""reStructuredTextスタイル
		:param sec int: 何秒間座るか
		"""
		tl1 = Template("$nameは$sec秒間座った！")
		if self.hp < 95:
			self.hp += sec
			tl2 = Template("HPが$secポイント回復した！")
		else:
			self.hp = 100
			tl2 = Template("HPが完全に回復した！")
		print(tl1.substitute(name=self.name, sec=sec))
		print(tl2.substitute(sec=sec))

	@final # 残念ながらPythonではfinal修飾子を付けてもオーバーライド時にエラーも警告も出ない
	def slip(self):
		self.hp -= 30
		tl = Template("$nameは転んだ！")
		print(tl.substitute(name=self.name))
		print(f"30のダメージを受けた")
		if self.hp <= 0:
			self.hp = 0
			tl2 = Template("$nameは力尽きてしまった...")
			print(tl2.substitute(name=self.name))

	def sleep(self):
		self.hp = 100
		self.__mp = 10 
		tl = Template("$nameは眠ってHPとMPを全回復した！")
		print(tl.substitute(name=self.name)) 

	def showMP(self):
		# インスタンスメソッドからならprivate変数にアクセスできる
		return self.__mp

	def increaseMP(self, mp:int):
		# インスタンスメソッドからならprivate変数を変更もできる
		self.__mp += mp
		return self.__mp

	# privateメソッド(_を２つ付ける)
	# 外部のクラスからは呼び出せなくなるが、同じクラス内のattack()などからは呼び出せる
	# 他のクラスからみだりに呼び出されないようにprivate化
	def __dead(self): # __dead()を呼べるのは、Heroクラスだけ
		tl = Template("$nameは死んでしまった！")
		print(tl.substitute(name=self.name))
		# print("GAMEOVER...")

	###################
	# staticメソッド
	###################
	# インスタンス変数とクラス変数のどちらもアクセス出来ない
	# クラスの他のメソッドにもアクセス出来ない
	# クラスの他の実装に依存しない
	# 継承しても動作が変わらない時、毎回同じ結果を出力したい時など

	@staticmethod
	def setRandomMoney(): # 第１引数にselfやclsは不要	
		Hero.money = int(random.random() * 1000) # 0.0以上1.0未満の浮動小数点数

	###################
	# クラスメソッド
	###################
	# インスタンス変数にアクセス出来ず、クラス変数にはアクセス出来る
	# 他のクラスメソッドやインスタンスメソッドにもアクセス出来る
	# 継承した場合に動作が変わる場合や、インスタンス生成のたびに
	# 同じ処理をしないといけない場合などに利用

	@classmethod
	def ppp(cls): # cls: 呼び出したクラス自身 Hero.pppならばcls=Hero
		# クラス変数へアクセス
		# クラスメソッド内部ではクラス変数にしか利用出来ない
		cls.money = 0

	############
	# Getter
	############
	# @property
	# def インスタンスメソッド(self):
	#	return self.インスタンス変数

	# 注意： 呼び出し元は最後に()は要らない
	# 例： h.maritalStatus()ではなく、h.maritalStatus
	# cf: https://www.headboost.jp/python-property/

	# 上記のように、基本的にフィールドは全てprivateにすべき
	# フィールドには、Getterのようなメソッド経由でアクセスする
	@property
	def maritalStatus(self):
		return self.__marital_status

	@property
	def mp(self):
		return self.__mp

	@property
	def hp(self):
		return self.__hp

	############
	# Setter
	############
	# @インスタンスメソッド.setter
	# def インスタンスメソッド(self, value):
	# 	self.インスタンス変数 = value

	# 注意： setterはインスタンス変数の場合と同じく、hero.name = "Trump"のように書く

	# 重要：検査を徹底させた強固なsetterを書くように心がける

	@maritalStatus.setter
	def maritalStatus(self, value:bool):
		# setter内で値の妥当性をチェックする
		if isinstance(value, bool):
			self.__marital_status = value
		else:
			raise TypeError("bool型で指定してください")

	@mp.setter
	def mp(self, value:int):
		# setter内で値の妥当性をチェックする
		if not isinstance(value, int): # if type(value) is not int:
			raise TypeError("int型で指定してください")

		if value <= 0 and 50 < value:
			raise ValueError("0より大きく、50以下のint型で指定してください")

		self.__mp = value

	@hp.setter
	def hp(self, value:int):
		# TODO: setter内で値の妥当性をチェックする
		if not isinstance(value, int): # if type(value) is not int:
			raise TypeError("int型で指定してください")

		if value <= 0 and 50 < value:
			raise ValueError("0より大きく、50以下のint型で指定してください")

		self.__hp = value

	############
	# deleter
	############
	@maritalStatus.deleter
	def maritalStatus(self):
		del self.__maritalStatus
		# または、 self.__maritalStatus = None


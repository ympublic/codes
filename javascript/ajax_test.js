/* 
 * Ajax復習（10年以上ぶりにjQueryを使わず、Vanilla JavaScriptにて実装）
 * 1. XMLHttpRequestオブジェクトを生成する
 * 2. リクエストを送信する（GET/POST）
 * 3. レスポンスを受信する（同期/非同期）
 * 
 */

"use strict";

/*************************************
 * 1. XMLHttpRequestオブジェクトを作成
 *************************************
 */

var request;
if (window.XMLHttpRequest) {
	request = new XMLHttpRequest();
} else {
	throw new Error('XMLHttpRequestがサポートされていません');
}

/*************************************
 * 2. リクエストを送信する（GET/POST）
 *************************************
 */

////////////////////////////
//     GETリクエストの送信
////////////////////////////
/*
 * open(String Method, String Url, Boolean Async)
 * リクエストパラメータを初期化し、リクエストを指定したHTTPコマンド、URL、同期モードで送信可能な状態にする
 * String Method: 'GET' or 'POST'
 * String Url: リクエストの送信先のURL。サーバ側でリクエストを受け付けるWEBアプリのパスや、
 * サーバ状に配置したファイルのアドレスを指定
 * 注意： GETの場合は、encodeURIComponent()を利用してエンコードする必要がある
 * 例： request.open('GET', '/data/?param=' + encodeURIComponent(document.fm.value), true);
 * Boolean Async: リクエストを非同期通信で処理するか否かのフラグ
 * 
 */

// リクエストを初期化
// Webブラウザへのキャッシュを防ぐためには、以下の様にURLに乱数を設定した意味のないパラメータを指定する
request.open('GET', '/data.txt?param=' + Math.random(), true);  

/*
 * send(Request Body)
 * open()メソッドによって初期化されているリクエストをリクエストボディを蒸して送信する
 * open()メソッドが呼び出されていない場合や、リクエストが初期化されていない場合はError発生
 * Request Body: POSTリクエストを送信する場合は送信するフォームを、
 * GETリクエストを送信する場合はぬっlまたは空文字を指定する
 *
 */

request.send(null);

//////////////////////////////
//     POSTリクエストの送信
//////////////////////////////
request.open('POT', '/data.txt', true);
// POSTメソッドを送信する場合は「Content-Type」ヘッダに「application/x-www-form-urlencoded」を指定する決まりがある
request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
request.send('param1=' + encodeURIComponet(1234) + '&param2=' + encodeURIComponent(abcde));


/*************************************
 * 3. レスポンスを受信する（同期/非同期）
 *************************************
 */

// サーバから返却されたレスポンスデータはXMLHttpRequestオブジェクトのresponseTextプロパティで参照する

//////////////////////////////
//     同期処理の場合
//////////////////////////////
request.open('GET', '/data.txt?param=' + Math.random(), false); // false:同期
request.send(null);
if (request_get.status == 200) { // HTTPステータスコードを確認
	// レスポンスを処理、受信したレスポンスボディを取得
	console.log(request.responseText);
}

//////////////////////////////
//     非同期処理の場合
//////////////////////////////

// 処理が完了するのを待機せずに非同期レスポンスの受信を検出するために「コールバック関数」を利用する
// 非同期処理のレスポンスを扱うには、XMLHttpRequestオブジェクトのonreadystatechangeプロパティを使用する
// このプロパティにメソッドを登録しておくと、要求されたリクエストの状態が変化した時に、登録されたメソッドがコールバックされる

// 非同期時のサーバ応答時の処理を定義
request.onreadystatechange = function () { // 通信状態が変化した際に呼び出すコールバック関数を登録
	/*
	 * readyState
	 * 0: オブジェクトの生成時（初期化中）
	 * 1: コネクション確立
	 * 2: sendメッセージが送信完了、リクエスト送信
	 * 3: レスポンスヘッダ受信
	 * 4: 通信完了、前レスポンス受信
	 * 
	 */
	if (request.readyState == 4 && request.status == 200) {
		// レスポンスを処理
		console.log(request.responseText);
	}
};
request.open('GET', '/data.txt?param=' + Math.random(), true); // true:非同期
request.send(null);


/////////////////////////////////
//     JSON形式のレスポンスを処理
/////////////////////////////////
var request = new XMLHttpRequest();
request.onreadystatechange = function() {
	if (request.readyState == 4 && request.status == 200) {
		// {}で囲まれたオブジェクトリテラル形式のJSONデータを解析するには、以下の様に()を追加する必要がある
		var msgInfos = eval('(' + request.responseText + ')'); // 重要：信頼できないJSONデータを安易にeval()関数で解析しない
		for (var i == 0; i < msgInfos.length; i++) {
			document.write(msgInfos[i].message + '<br />');
		}
	}
};
request.open('GET', '/jasonData.json', true);
request.send(null);


/////////////////////////////////////////
//     Ajaxリクエストのタイムアウトを実装
/////////////////////////////////////////

var request = new XMLHttpRequest();
var timer = setInterval('timeout()', 3000); // 3000ミリ秒（3秒）をタイムアウトにする
request.onreadystatechange = function() {
	if (request.readyState == 4 && request.status == 200) {
		clearInterval(timer); // 正常終了でもタイマーをクリア
		console.log(request.responseText);
	}
};
request.open('GET', '/data.txt', true);
request.send(null);
// タイムアウトの処理
function timeout() {
	clearInterval(timer);
	// 現在のリクエストをキャンセルする
	request.abort();
	console.log('タイムアウト発生');
}


////////////////////////////////////////////
//     GitHubが提供しているAPIよりデータを取得
////////////////////////////////////////////

const url = "https://api.github.com/users";
var request = new XMLHttpRequest();
request.onreadystatechange = function() {
	if (request.readyState == 4 && request.status == 200) {
		console.log(JSON.parse(request.responseText));
	}
};
request.open('GET', url, true);
request.send(null); // これを実行すると、ユーザー情報が沢山出てくる


////////////////////////////////
//     jQueryを利用する場合
////////////////////////////////
$( function() {
	$.ajax({
		url: "https://api.github.com/search/respositories?q=javascript",
		type: 'GET',
		data: {'hello': "world"},
		dataType: 'json',
		cache: false;
		timeout: 3000;
	})
	.done (function(response) { // SUCCESS
		console.log(response);
		if (response.status === "err") {
		    alert("err: " + response.msg);
		} else {
		    alert("OK");
		}
	})
	.fail (function(xhr) { // ERROR
		console.log(jqXHR);
	})
	.always (function(xhr, msg) { // done,failを問わず、常に実行される処理

	}); // メソッドチェーンで繋げていく
});


import { useState } from "react";

const Example = () => {
  // {}ではなく、[]
  const [val, setVal] = useState("");
  const inputText = (e) => {
    setVal(e.target.value);
  };
  const clearVal = (e) => {
    setVal("");
  };
  return (
    <>
      <div>
        <label htmlFor="456">ラベル</label>
        <div>
          <input
            id="123"
            type="text"
            value={val}
            placeholder="こんにちは"
            onChange={inputText}
          />
          <textarea
            id="456"
            value={val}
            placeholder="こんにちは"
            onChange={inputText}
          />
        </div>
        <h3>{val}</h3>
        <button onClick={clearVal}>クリア</button>
      </div>
    </>
  );
};

export default Example;

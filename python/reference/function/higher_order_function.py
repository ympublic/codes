#! /usr/bin/env python

"""高階関数
高階関数:関数を引数に受け取ったり、関数を戻り値とする関数
"""

"""
map(関数, iterableオブジェクト１, iterableオブジェクト)
iterableオブジェクトの各要素に、引数の関数を適用し、イテレータを返す
"""

m1 = map(pow, (2, 3, 4, 5), (1, 2, 3))

# (2, 3, 4, 5)の最後の要素は切り詰められている
print(list(m1)) # [2, 9, 64]


"""
zip(iterableオブジェクト１, iterableオブジェクト２, ...)
引数に複数のシーケンスを受け取り、各要素をタプルにして一組にしたリストを返す
"""

z1 = zip("abcde", (1, 2, 3))

# 短い方に切り詰められる
print(list(z1)) # [('a', 1), ('b', 2), ('c', 3)]


"""
filter(関数, iterableオブジェクト)
iterableオブジェクトの各要素に引数の関数を適用し、
その戻り値がif文でTrueになるものだけをイテレータとして返す
iterableオブジェクトは１つだけ取る
"""

f1 = filter(labmda x: x >= 3, (1, 2, 3, 4, 5)) # [3, 4, 5]

print(list(f1)) # [3, 4, 5]

f2 = fileter(len, [[], [1], [1,2], [], [1,2,3]])

print(list(f2)) # [[1], [1, 2], [1, 2, 3]]


"""
reduce(関数, iterableオブジェクト, 初期値)
iterableオブジェクトの各要素に引数の関数を適用し、その関数の実行結果も
積算的にその関数に適用していく
つまり、引数に渡された関数には２つの引数が渡されることになり、
１つめの引数は１つ前の関数の戻り値、２つめはiterableの値となる
"""

def my_sum(x: int, y: int) -> int:
	print(f"{x} + {y} = {x+y}")
	return x + y

from functools import reduce

r1 = reduce(my_sum, (1,2,3,4))

print(list(r1))
# 1 + 2 = 3
# 3 + 3 = 6 # 一つ前の結果が利用される
# 6 + 4 = 10 # 一つ前の結果が利用される


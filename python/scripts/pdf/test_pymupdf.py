#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import gc
import pytest
# pytest -v -m "unsolved and not solved" --pdb test_pymupdf.py
# --setup-show: どのフィクスチャがいつ実行されたのか確認
import fitz # PyMuPDF
# cf: https://pymupdf.readthedocs.io/en/latest/the-basics.html
# print(fitz.__doc__)

# フィクスチャ定義
@pytest.fixture(scope="function")
def open_pdf_file(): 
	"""  前処理にてディレクトリとファイルを作成し、後処理にてディレクトリを削除するフィクスチャ """
	
	# GIVEN(前提): 
	# WHEN(こうしたら): 
	# THEN(こうなる):

	# pytest --setup-showでフィクスチャの実行タイミングを確認できる
	# 前処理部分(setUp) 
	try:
		doc = fitz.open("kagoshima_population.pdf")

	except Exception as e: # 基底クラスException
		print(e, file=sys.stderr)

	else: # 正常終了時の処理
		print("正常にPDFファイルを開きました")

	finally: # 終了時に常に行う処理
		print("処理が終了しました")

	# yield前がsetUp()、yield後がtearDown()となる
	yield doc # return

	# 後処理部分(tearDown)
	doc.close()


@pytest.mark.solved
def test_should_return_sheetnames():
	"""  """

	with fitz.open("Kagoshima_population.pdf") as doc:
		# ページ数を取得
		assert 63 == doc.page_count


# openpyxlは取得したセルの内容が「数式」となる
# 対して、xlrdの場合は取得したセルの内容は「計算結果」となる
@pytest.mark.solved
def test_should_return_text():
	"""  """

	doc = fitz.open("Kagoshima_population.pdf")
	# ３ページ目のテキストを取得
	assert "人口及び年齢区分別の人口の状況" in doc.get_page_text(2)
	
	# １ページずつテキストを抽出して出力
	# for page in range(len(doc)):
	# 	text = doc[page].get_text()

	doc.close()


# 上記test_should_return_text()をフィクスチャ使用で書き直した場合
@pytest.mark.solved
# フィクスチャを使用する場合は、引数に上記のフィクスチャ関数を指定する
def test_should_return_text_with_fixture(open_pdf_file):
	"""  """
	
	# open_pdf_fileでdocを返す。最後に()は付ける必要はない
	assert "人口及び年齢区分別の人口の状況" in open_pdf_file.get_page_text(2)


@pytest.mark.solved
def test_should_return_image():
	"""  """
	with fitz.open("Kagoshima_population.pdf") as doc:
		# ３ページ目の画像を全て取得
		images = doc.get_page_images(2)
		# ページの表などを右クリックすれば画像かどうか判断できる
		assert len(images) >= 1 


@pytest.mark.solved
def test_should_return_searched_text():
	"""  """
	with fitz.open("Kagoshima_population.pdf") as doc:
		results = []
		for page in doc:
			if page.search_for("自然動態（出生・死亡）と社会動態（転入・転出）の状況"):
				results.append("yes")

		assert results


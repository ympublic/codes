/* 
 * シェルソート（改良挿入法）
 * 挿入ソートを改良したシェルソート
 * 挿入ソートは間隔ごとにざっと整列することで挿入ソートよりも高速に整列できる
 * まずデータ列をある間隔ごとの複数のデータ列に分け、挿入法で整列する
 * 感覚は最終的に１になればいくつでもかまわない
 *
 */ 

"use strict";

var data = new Array(80, 60, 40, 70, 30, 10, 50, 20);

// 間隔の初期値をデータ件数÷2とし、外側ループが１回終了するごとに間隔を2で割っていく
let distance = Math.floor(data.length / 2); // 小数点以下切り捨て
let work;

while (distance > 0) { // 間隔が1までは整列処理を行い、0になったらループを抜ける
    // 外側ループ(外=間隔+1, 件数, 1)
    for (let o = distance; o <= data.length-1; o++) {
        // 内側ループ(内=外-間隔, 1, -間隔)
        for (let i = o - distance; i >= 0; i-=distance){
            if (data[i] > data[i+distance]) {
                // データ（内）とデータ（内+間隔）を交換する
                work = data[i];
                data[i] = data[i+distance];
                data[i+distance] = work;
            }
        }
    }
    distance = Math.floor(distance / 2); // 小数点以下切り捨て
}

console.log(data);
/*
[
  10, 20, 30, 40,
  50, 60, 70, 80
]
*/


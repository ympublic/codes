import Child from "./components/Child";

const Example = () => {
  const hello = (arg) => `Hello ${arg}`;
  // スプレッド演算子でも渡せる
  const ooo = {
    color: "red",
    num: 123,
  };
  return (
    <>
      <Child {...ooo} fn={hello} bool obj={{ name: "Tom", age: 13 }} />
      {/* <Child color="red" /> */}
    </>
  );
};

export default Example;

/* 
 * 挿入ソート（基本挿入法）
 * 整列済みデータ列に新データを挿入する挿入ソート
 * データを挿入するためにはデータをずらさなければならない
 * 
 */

package main

import {
	"fmt"
}

func main() {
	data := [8]int{80, 60, 40, 70, 30, 10, 50, 20}
	var work int

	// 外ループ（外=2, 件数, 1）
	for o := 1; o <= len(data)-1; o++ {
		// 内ループ（内=外-1, 1, -1）, 末尾から挿入位置を探していく
		for i := o-1; 0 <= i; i-- {
			if data[i] > data[i+1] {
				// データ（内）とデータ（内+1）を交換する
				work = data[i]
				data[i] = data[i+1]
				data[i+1] = work
			}
		}
	}

	fmt.Println(data) // [10 20 30 40 50 60 70 80]
}


// useRef: Reactから直接DOMを操作する/命令する

import { useState, useRef } from "react";

const Case1 = () => {
  const [value, setValue] = useState("");
  // Refオブジェクトを生成（currentプロパティが含まれている）
  // Refオブジェクトを作成し、それを対象のDOM要素に渡す
  // ref属性の要素として渡す
  const inputRef = useRef();

  return (
    <div>
      <h3>ユースケース1</h3>
      <input
        type="text"
        ref={inputRef} // refを指定
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />
      <button onClick={() => inputRef.current.focus()}>
        インプット要素をフォーカスする
      </button>
    </div>
  );
};

const Case2 = () => {
  const [playing, setPlaying] = useState(false);
  const videoRef = useRef(); // DOMのメソッドにアクセスする

  return (
    <div>
      {/** refにより対象のタグを設定 */}
      <video style={{ maxWidth: "100%" }} ref={videoRef}>
        {/** パスは、publicフォルダ内のindex.jsから見て */}
        <source src="./sample.mp4"></source>
      </video>
      <button
        onClick={() => {
          if (playing) {
            videoRef.current.pause();
          } else {
            videoRef.current.play();
          }

          setPlaying((prev) => !prev);
        }}
      >
        {playing ? "Stop" : "Play"}
      </button>
    </div>
  );
};

const Case3 = () => {};

const Example = () => {
  return (
    <>
      <Case1 />
      <Case2 />
      <Case3 />
    </>
  );
};

export default Example;

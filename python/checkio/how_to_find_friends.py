def check_connection(networks, first, second):
    future=[]  # set()は変更可能オブジェクトを持てないので.append([1,2])などはTypeError
    visited=[]
    groups=[]
    dict={}
    for network in networks:
        [x,y] = network.split('-')
        future.append([x,y])

    while future:
        for m in future:
            future.remove(m)
            i=0
            for n in future:                
                if n[0] in m and not n[1] in m:
                    m.append(n[1])
                    i+=1
                elif n[1] in m and not n[0] in m:
                    m.append(n[0])
                    i+=1
                else:
                    continue
            print "m: %s" % m
            if i == 0:
                groups.append(m)
            else:
                future.append(m)
                
    for group in groups:
        if first in group and second in group:
            return True
    return False

"""
import collections
def check_connection(network, first, second):
    box=collections.defaultdict(list)
    result={}
    for l in network:
        (x,y) = l.split('-')
        box[x].append(y) 
        box[y].append(x)
    for k1, v1 in box.iteritems():
        #print box[k1]
        for k2, v2 in box.iteritems():
            #print "k1: %s" % k1
            #print "v2: %s" % v2
            if k1 != k2:
                for x in v2:
                    if x in v2 or k1 in v2:
                    #print "##### 合体 #####"
                    #print "v1: %s" % v1
                    #print "v2: %s" % v2
                        v1.extend(v2)
        result[k1] = set(v1)
        #print "v1: %s" % v1
    print "result: %s" % result
    for key in result.keys():
        if first in result[second]:
            return True
        else:
            return False
"""     

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert check_connection(
        ("dr101-mr99", "mr99-out00", "dr101-out00", "scout1-scout2",
         "scout3-scout1", "scout1-scout4", "scout4-sscout", "sscout-super"),
        "scout2", "scout3") == True, "Scout Brotherhood"  # scount2 is connected with scout3
    assert check_connection(
        ("dr101-mr99", "mr99-out00", "dr101-out00", "scout1-scout2",
         "scout3-scout1", "scout1-scout4", "scout4-sscout", "sscout-super"),
        "super", "scout2") == True, "Super Scout"
    assert check_connection(
        ("dr101-mr99", "mr99-out00", "dr101-out00", "scout1-scout2",
         "scout3-scout1", "scout1-scout4", "scout4-sscout", "sscout-super"),
        "dr101", "sscout") == False, "I don't know any scouts."  # dr101 is not a friend of sscout
/* 
 * /python/reference/object_oriented/Java/をJavaScriptで書き直し
 * JavaScriptはプロトタイプベースのオブジェクト指向
 * 自分が最初JavaScriptを学んだ頃、クラスはfunction()ベースで作成していた
 * 2015年よりJavaScriptにもclass構文が追加された
 * cf: https://jsprimer.net/basic/class/
 * 
 */

"use strict";

// TypeScriptにはあるが、Vanilla JavaScriptにはインターフェースはない
class Character {
	// Interfaceはフィールドを１つも持たないので、コンストラクタも不要
	// constructor() {}

	// Interface内の全てのメソッドは抽象メソッド
	attack() {}

	run() {}

}


class Hero extends Character { // JavaScriptは多重継承は出来ない
	// JavaScriptは基本的にプロパティもメソッドもpublicとなる

	// Publicクラスフィールド
	gender; // Undefined

	// Privateクラスフィールドを設定するには、事前定義が必要
	// プロパティの前に#を付ける
	// クラスの外からアクセスできなくなる
	#hp = 100;
	#mp = 10;

	// Staticフィールド(クラスフィールド)
	static money = 1000;
	static JOB = "勇者";

	// Privateクラスフィールドも定義可能
	// static #privateProperty = "This property is Private";

	// コンストラクタ
	// 注意: JavaScriptではconstructorはクラスに１つしか定義できない
	// つまり、オーバーロードは実装できない
	constructor(name) {
		// 引数が指定されていなければErrorを発生させる
		if (name == undefined) {
			throw new Error("引数nameを指定してください");
		}
		// または初期値を与える
		// if (name == undefined) {
		// 	name = "Beatles";
		// }

		// // thisに触れる前に基底クラスのコンストラクタを呼び出さなければReferenceError発生
		super();

		// インスタンスプロパティ
		this.name = name; // getter, setterの定義は不要。というかエラーになる
	}

	// プロトタイプメソッド
	attack(monster) { // ポリモーフィズム
		console.log(this.name + "の攻撃！");
		console.log(monster.name + monster.suffix + "に10ポイントのダメージを与えた");
		monster.hp -= 10;
	}

	run() {
		console.log(this.name + "は後ろも振り返らず逃げ出した！");
	}

	sleep() {
		console.log(this.name + "は眠りについたzzz");
		this.#hp = 100;
		this.#mp = 10;
		console.log(this.name + "のHPとMPが全快した！");
	}

	slip() {
		console.log(this.name + "は転んだ！");
		this.#hp -= 30
		console.log("30のダメージを受けた");
		if (this.#hp <= 0) {
			this.#hp = 0;
			console.log(this.name + "は力尽きてしまった...");
		}
	}

	// Staticメソッド
	static setRandomMoney() {
		// Math.floor(): 引数に指定した数値の端数を切り捨てた値を返す
		// 注意: staticプロパティも呼び出す際にはthisが必要となる
		this.money = Math.floor(Math.random() * 10) * 1000;
	}

	static getMoney() {
		return money;
	}

	// Getter
	get hp() {
		return this.#hp;
	}

	get mp() {
		return this.#mp;
	}

	// Setter
	set hp(value) {
		// データ型を判定
		//if (typeof value != "string") {
		//	throw new Error("文字列で指定してください");
		//}
		//if (value.length < 3) {
		//	throw new Error("３文字以上で指定してください");
		//}
		this.#hp = value;
	}

}


class SuperHero extends Hero {

	// Privateフィールドを事前定義
	#flying = false;

	// コンストラクタ
	constructor(name) {
		// 基底クラスのコンストラクタにアクセス
		// thisに触れる前にsuper()を呼び出さなければReferenceError発生
		super(name);
		this.name = name;
	}

	// プロトタイプメソッド
	run() {
		// 親クラスのメソッドを使用する場合
		// super.run()
		console.log(`${this.name}は何度も転びながらも逃げ出した！`);
	}

	attack(monster) { // ポリモーフィズム
		// 基底クラスのメソッドを呼び出し
		super.attack(monster);
		if (this.#flying) {
			console.log(`更に空からの追加攻撃！`);
			super.attack(monster);
		}
	}

	fly() {
		this.#flying = True;
		console.log(`$this.nameは飛び上がった！`);
	}

	land() {
		this.#flying = False;
		console.log(`$this.nameは地上に降りた！`);
		console.log("着地の際に足を痛めた！20ポイントのダメージ！");
		this.hp -= 20;
	}

	// プロトタイプ継承では、プロトタイプチェーンのおかげで、
	// Staticメソッドも継承される
	// SuperHero.setRandomMoney()が使えるという事

}


// Non Interfaceとして定義
class Monster {
	
	// Privateフィールド
	#species;
	#suffix;
	#hp;

	// constructor() {}

	// プロトタイプメソッド
	attack() {}

	run() {
		console.log("モンスターはスタコラサッサと逃げ出した！");
	}

	// Getter
	get species() {
		return this.#species;
	}

	get suffix() {
		return this.#suffix;
	}

	get hp() {
		return this.#hp;
	}

	// Setter
	set hp(value) {
		this.#hp = value;
	}

}


class Matango extends Monster {

	// Privateフィールド
	// JavaScriptのprivateクラスフィールドはHard Privateなので、
	// 子クラス（派生クラス）を含む、クラスの外からはアクセス出来ない
	#species = "おばけキノコ";
	#suffix = "A";
	#hp = 100;

	constructor(suffix) {
		// thisに触れる前にsuper()を呼び出す
		super();
		this.#suffix = suffix;
	}

	// プロトタイプメソッド
	attack(character) { // ポリモーフィズム
		console.log(`${this.#species}${this.#suffix}の攻撃！`);
		console.log(`${character.name}に15のダメージ！`);
		character.hp -= 15;
	}

	run() {
		console.log(`${this.#species}${this.#suffix}はお尻を振りながら逃げ出した！`);
	}

}


function alertHeroName(obj) {
	// ダックタイピング
	if (obj.name) {
		console.alert(obj.name);
	}

	// ダックタイピングを使わない場合
	// if (obj instanceof Hero) { // クラス判定
	// 	console.alert(obj.name);
	// }
}


const main = function() {
	const h = new Hero("Lennon");
	console.log("勇者" + h.name + "が誕生した！");
	console.log("HP: " + h.hp + " / MP: " + h.mp);
	Hero.setRandomMoney();
	console.log("所持金: " + Hero.money);
	if (h instanceof Character) {
		console.log("hはCharacterを継承しています");
	}
	
	const sh = new SuperHero("Star");
	console.log("勇者" + sh.name + "が誕生した！");
	console.log("HP: " + sh.hp + " / MP: " + sh.mp);
	if (sh instanceof Character) {
		console.log("shはCharacterを継承しています");
	}

	const m1 = new Matango("A");
	console.log(`${m1.species}${m1.suffix}が現れた！`);
	console.log(`HP: ${m1.hp} / MP: ${m1.mp}`);

	sh.attack(m1);
	m1.attack(h);
	h.slip();
	sh.run();
	m1.run();

}

main()


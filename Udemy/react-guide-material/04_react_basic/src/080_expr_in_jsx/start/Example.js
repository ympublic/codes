import { Fragment } from "react";
import Child from "./components/Child";
import Expression from "./components/Expression";

const Example = () => {
  return (
    <Fragment>
      <Child />
      <Expression />
    </Fragment>
  );
};

export default Example;

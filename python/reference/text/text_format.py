#! /usr/bin/env python
""" テキスト整形 """

import re
import gc

# profile.run()をしない場合は、pyhon -m cProfile text_format.py
try:
	import cProfile as profile
except ImportError:
	import profile


def main():
	# 左揃え、右揃え、中央揃え（スペースを20取る）
	print("左揃え、右揃え、中央揃え")
	print("|", "left".ljust(20), "|", "right".rjust(20), "|", "center".center(20), "|\n")

	# 詰め込みに使うキャラクタを指示
	print("詰め込み")
	print(" apple ".center(20, "★") + "\n")

	# スペースを刈り込む
	x = "      banana   "
	print("スペースの刈り込み")
	print("|", x.lstrip(), "|", x.rstrip(), "|", x.strip(), "|\n")

	y = "xyxyxyy  orangeyx  yyxxxy"
	# 文字列の両端のスペースは残る
	print("|{}|\n".format(y.strip("xy")))

	# 文字列連結
	# tupleは変更不可(appendとかも使用不可)なので、Listを使用する
	pieces = ["Apple", "products", "are", "Google"]
	sub_pieces = ["so", "expensive", "??"]
	# リストを拡張
	pieces.extend(sub_pieces)
	# Remove
	pieces.remove("Google")
	#Pop
	pieces.pop()
	# Append
	pieces.append("!!")
	print("文字列連結")
	# パフォーマンス的に文字列連結に+は使わない方が良い！！
	print(" ".join(pieces) + "\n")

	# 文字列を単語ごとに反転させる
	astring = "I tasted a cup of Japanese green tea"
	# 文字列は不変(Immutable)なので、反転するにはコピーを作る必要がある
	revchars = astring[::-1]
	print("文字列を反転")
	print("{}\n".format(revchars))
	# list.reverse()はlist.sort()と同じようにオリジナルのリストに変更を加える
	# オリジナルに変更を加えたくない場合は、reversed(list)やsorted(list)
	revwords = " ".join(reversed(astring.split()))
	# または、revwords = " ".join(astring.split()[::-1]) reversedより[::-1]の方が良い
	print("文字列を単語ごとに反転")
	print("{}\n".format(revwords))
	# スペースに手を加えたくない場合は、正規表現で分割
	re_words = re.split(r"(\s)+", astring)
	print("文字列を正規表現にて分割")
	print("{}\n".format("".join(re_words[::-1])))


if __name__ == '__main__':
	main()
	gc.collect()


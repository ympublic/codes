FIRST_TEN = ("", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX")  # 辞書だと上手くいかない
TEN = ("", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC")
HUNDRED = ("", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" )
THOUSAND = "M"

def checkio(data):
    data2 = data - (data/1000)*1000
    data3 = data2 - (data2/100)*100
    data4 = data3 - (data3/10)*10
    number = (data/1000)*THOUSAND + HUNDRED[(data2/100)] + TEN[(data3/10)] + FIRST_TEN[data4]
    return number
    
    
    
    
""" I will fix this later.
def checkio(data):
    if data<=9:
        number = FIRST_TEN[data]
    if 10<=data<50:
        number = TEN*(data/10) + FIRST_TEN[data%10]
    if 50<=data<100:
        number = FIFTY + TEN*((data-50)/10) + FIRST_TEN[data%10]
    if 100<=data<500:
        data2=data-(data/100)*100
        if data2<50:
            number = HUNDRED*(data/100) + TEN*(data2/10) + FIRST_TEN[data2%10]
        else:
            number = HUNDRED*(data/100) + FIFTY + TEN*((data2-50)/10) + FIRST_TEN[data2%10]
    if 500<=data<1000:
        data2=data-(data/100)*100
        if data2<50:
            number = FIVE_HUNDRED + HUNDRED*((data-500)/100) + TEN*(data2/10) + FIRST_TEN[data2%10]
        else:
            number = FIVE_HUNDRED + HUNDRED*((data-500)/100) + FIFTY + TEN*((data2-50)/10) + FIRST_TEN[data2%10]
    if 1000<=data:
        data3=data-(data/1000)*1000
        data2=data3-(data3/100)*100
        number=THOUSAND*(data/1000)
        print data3
        if 100<=data3<500:
            if data2<50:
                number += HUNDRED*(data3/100) + TEN*(data2/10) + FIRST_TEN[data2%10]
            else:
                number += HUNDRED*(data3/100) + FIFTY + TEN*((data2-50)/10) + FIRST_TEN[data2%10]
        if 500<=data3<1000:
            if data2<50:
                number += FIVE_HUNDRED + HUNDRED*((data3-500)/100) + TEN*(data2/10) + FIRST_TEN[data2%10]
            else:
                number += FIVE_HUNDRED + HUNDRED*((data3-500)/100) + FIFTY + TEN*((data2-50)/10) + FIRST_TEN[data2%10]
"""



if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert checkio(6) == 'VI', '6'
    assert checkio(76) == 'LXXVI', '76'
    assert checkio(499) == 'CDXCIX', '499'
    assert checkio(3888) == 'MMMDCCCLXXXVIII', '3888'
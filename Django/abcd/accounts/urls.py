from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, re_path
from django.contrib.auth import views as auth_views

from accounts import views
from accounts.views import SignupView
from accounts.views import ProfEditView
from accounts.views import ProfImgEditView
from accounts.views import GoodByeView
from accounts.views import (
    QUserListView,
    QUserDetailView,
)

app_name = "accounts"

urlpatterns = [
    path("signup/", SignupView.as_view(), name="signup"), # as_view(): クラスベースビューを関数化する
    path("login/", auth_views.LoginView.as_view(
        redirect_authenticated_user=True, # settings.pyのLOGIN_REDIRECT_URLにリダイレクト
        template_name="accounts/login.html"
    ), name="login"),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("profile/<int:user_id>/", views.profile, name="profile"),
    path("profile/<int:user_id>/create/", views.profcreate, name="prof_create"),
    path("profile/<int:pk>/edit/", ProfEditView.as_view(), name="prof_edit"),
    # UpdataViewはURL変数名が'pk'や'slug'でないとErrorが発生する
    path("profile/<int:pk>/img_edit/", ProfImgEditView.as_view(), name="profimg_edit"),
    path("profile/<int:pk>/goodbye/", GoodByeView.as_view(), name="goodbye"),
    path("q_userlist/", QUserListView.as_view(), name="q_userlist"),
    path("q_userdetail/<int:pk>/", QUserDetailView.as_view(), name="q_userdetail"),
]


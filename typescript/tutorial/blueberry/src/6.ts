/**
 * ユニオン型
 *
 */

type Animal6 = {
  species: string;
  age: number;
};

type Human6 = {
  name: string;
  age: string;
};

// ユニオン型
type User6 = Animal6 | Human6;

const tama: User6 = {
  species: "Felis",
  age: 17,
};

const uhyo: User6 = {
  name: "Uhyo",
  age: "26歳",
};

// ageプロパティは、(string | number)型となる
console.log(tama.age); // 17
console.log(uhyo.age); // "26歳"

type MysteryFunc = ((arg: string) => string) | ((arg: string) => number);

const func: MysteryFunc = (arg) => {
  return arg;
};

// 引数に関数を受け取る、高階関数
function useFunc1(func: MysteryFunc) {
  const result1 = func("uhyo");
  return result1;
}

function useFunc2(func: MysteryFunc) {
  const result2 = func("32");
  return String(result2).length;
}

console.log(useFunc1(func));
console.log(useFunc2(func));

/**
 * インターセクション型
 *
 */

// インターセクション型
type HumanI = Animal6 & {
  name: string;
};

const uhyoI: HumanI = {
  name: "Uhyo",
  age: 26,
  species: "Monkey",
};

console.log(uhyoI);

/**
 * インターセクション型とユニオン型
 *
 */

type Human614 = { name: string };
type Animal614 = { species: string };

const getName = (arg: Human614) => arg.name;
const getSpecies = (arg: Animal614) => arg.species;

// このmysteryFuncは((arg: Human614) => string | (arg: Animal614) => string)型の引数を取る
// よって、mysteryFuncを関数として呼び出すためには、引数には(Human614 & Animal614)型を指定しないといけない
const mysteryFunc = Math.random() < 0.5 ? getName : getSpecies;

const uhyo614: Human614 & Animal614 = {
  name: "Uhyo",
  species: "Monkey",
};

const value614 = mysteryFunc(uhyo614);
console.log(`mysteryFunc: ${value614}`);

/**
 * オプショナルプロパティ
 *
 */

type Human615 = {
  name: string;
  age?: number; // number | undefined型となる
};

const uhyo615: Human615 = {
  name: "Uhyo",
  age: 25,
};

/**
 * オプショナルチェイニング
 *
 */

function func616(human: Human615 | undefined) {
  // ageはnumber | undefined 型
  const age = human?.age;
  console.log(age);
}

type GetTimeFunc = () => Date;

function useTime(func: GetTimeFunc | undefined) {
  // timeOrUndefinedは Date | undefined型
  const timeOrUndefined = func?.();
}

/**
 * リテラル型
 *
 */

const one: 1 = 1;

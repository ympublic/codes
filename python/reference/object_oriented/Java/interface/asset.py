from abc import ABC # Abstract Base Class
from abc import ABCMeta
from abc import abstractmethod

# 抽象クラス
class Asset(ABC):

	"""  """

	def __init__(self, name:str, price:int): # Interfaceではないので、コンストラクタ実装
		self.__name = name
		self.__price = price

	@property
	def name(self):
		return self.__name 

	@property
	def price(self):
		return self.__price

	@name.setter
	def name(self, value):
		self.__name = value

	@price.setter
	def price(self, value):
		self.__price = value




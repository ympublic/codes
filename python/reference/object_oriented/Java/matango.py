# PythonではType Hintsの指定などの場合に、相互にimportし合うと循環参照エラーが発生する
# そこで以下のようにして対処する
# cf: https://zenn.dev/yupix/articles/604e2b9c294cc6
from __future__ import annotations # Fileの先頭で宣言する必要あり
# cf: https://ryry011.hatenablog.com/entry/2021/08/27/155026
from typing import TYPE_CHECKING # ランタイム中にはFalseであり、型チェック中にはTrueとなる
if TYPE_CHECKING:
	from hero import Hero

from character import Character
from monster import Monster

from typing import Final
from functools import singledispatch # Pythonでメソッドのオーバーロードを実現

class Matango(Monster):

	""" 敵キャラ：おばけキノコ """

	# コンストラクタのオーバーロードは__init__()へデフォルト引数を渡すことで対処
	def __init__(self, suffix:str="A"):
		super().__init__() # Monster
		self.__species: str = "おばけキノコ"
		self.__suffix: str = suffix
		self.__hp: int = 50
		self.__level: Final[int] = 10 # 再代入不可

	def run(self):
		print("{}{}は左右ジグザグに動きながら逃げ出した！".format(self.__species, self.__suffix)) 

	# 攻撃対象としてh:Heroをc:Characterにザックリ捉えればポリモーフィズムとなる
	def attack(self, h:Hero):
		print("{}{}の攻撃！".format(self.__species, self.__suffix))
		print("{}は10のダメージを受けた！".format(h.name))
		h.hp = h.hp - 10 # setter & getter

	# 親クラスでメンバ変数への@property, @.setterを定義していても、
	# コンストラクタでそのメンバ変数を継承していなければ
	# private変数へのgetter, setterの再定義が必要
	@property
	def species(self):
		return self.__species

	@property
	def suffix(self):
		return self.__suffix

	@suffix.setter
	def suffix(self, value):
		self.__suffix = value

	@property
	def hp(self):
		return self.__hp

	@property
	def level(self):
		return self.__level

	@species.setter
	def species(self, value):
		self.__species = value

	@hp.setter
	def hp(self, value):
		self.__hp = value

	@level.setter
	def level(self, value):
		self.__level = value


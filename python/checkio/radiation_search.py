from itertools import product  # direct product
from collections import deque

def search_around(number, y, x, matrix, visited, coordinates):
    rows = len(matrix)
    columns = len(matrix[0])
    surround = [(0,-1), (0, 1), (-1, 0), (1, 0)]
    for i, j in surround:
        x1 = x + i
        y1 = y + j
        if 0 <= x1 < columns and 0 <= y1 < rows:
            if matrix[y1][x1] == number:
                if (y1, x1) in coordinates:
                    visited.append((y1, x1))
                    coordinates.remove((y1, x1))
                
def checkio(matrix):
    coordinates = set(product(range(len(matrix)), repeat=2))  # all coordinates
    result = [0, 0]
    
    while coordinates:
        y, x = coordinates.pop()
        number = matrix[y][x]  # target number
        count = 0
        visited = deque([(y, x)])
        while visited:
            count = count + 1
            y, x = visited.pop()
            search_around(number, y, x, matrix, visited, coordinates)
        if count > result[0]:   
            result = [count, number]

    return result
    

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio([
        [1, 2, 3, 4, 5],
        [1, 1, 1, 2, 3],
        [1, 1, 1, 2, 2],
        [1, 2, 2, 2, 1],
        [1, 1, 1, 1, 1]
    ]) == [14, 1], "14 of 1"

    assert checkio([
        [2, 1, 2, 2, 2, 4],
        [2, 5, 2, 2, 2, 2],
        [2, 5, 4, 2, 2, 2],
        [2, 5, 2, 2, 4, 2],
        [2, 4, 2, 2, 2, 2],
        [2, 2, 4, 4, 2, 2]
    ]) == [19, 2], '19 of 2'
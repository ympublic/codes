import Child from "./components/Child";
import { createContext } from "react";

// 初期値を渡す
export const MyContext = createContext("Hello");

// useContext
const Example = () => {
  return <Child />;
};

export default Example;

# 自作テンプレートタグ/フィルタ
# カスタムフィルターを作成する場合： @register.filter
# カスタムタグを作成する場合： @register.simple_tag

# テンプレートフィルタとテンプレートタグの使い分け
# カスタムフィルタはその関数の引数を1つか2つにする必要があるため(Document参照)、
# それより多い引数が必要な場合には必然的にカスタムタグの方を利用せざるを得ない
# また、カスタムタグの方ではViewからTemplateに渡されるコンテキストをそのロジック内で利用することができる
# 基本的にはフィルタよりもタグの方が複雑な処理に向いている

# テンプレートフィルター
# {{ 変数名 | フィルター名:引数 | フィルター名:引数 }}

# Documentより
# カスタムフィルタは単なるPythonの関数で、一つまたは二つの引数を取ります
# フィルタ関数は常に何かを返さねばなりません
# フィルタ関数は例外を送出してはならず、失敗するときは暗黙のうちに失敗せねばなりません
# エラーが生じた場合、フィルタ関数は元の入力そのままか空文字列のうち分かりやすい方を返すようにする


import math
from django import template

# Djangoテンプレートタグライブラリ
register = template.Library()

@register.simple_tag
def quotient_of_divided_by_3(value1: int) -> int:
    """ top: ３で割った商*252(px)を返す """
    return int(252 * (value1 // 3))


@register.simple_tag
def reminder_of_divided_by_3(value1: int) -> int:
    """ left: ３で割った余り*320(px)を返す """
    return int(320 * (value1 % 3))


@register.simple_tag
def show_stars_tag(value1: int) -> str:
    """ レビューの評価を★に変更する """
    stars = "★" * int(value1) + "☆" * (5 - int(value1)) 
    return stars


# 重要！！
# カスタムフィルター/タグを定義後はサーバーの再起動が必要


#! /usr/bin/env python

import sys
# sys.setdefaultencoding("utf-8")
import copy # 注意: 本ファイル名をcopy.pyにするとcopy.deepcopy()などが使えなくなる
import timeit

def main():

	# ==: 全ての要素の同一性を比較
	# 全要素が等しい
	print("==比較: {}".format([1,2,3] == [1,2,3])) # True
	# is: オブジェクトそのものの同一性を比較。つまりJavaScriptの===演算子
	# 同一のオブジェクトではない
	print("is比較: {}".format([1,2,3] is [1,2,3])) # False

	# リスト作成
	a = [1, ["apple"], 2, 3]
	
	# 浅いコピー
	shallow = a.copy() 
	# または、
	# b = a[:]
	# b = list(a)
	# b = copy.copy(a)
	# コピー元のリスト要素も変化する
	# 浅いコピーだから深い階層部分まではコピーされておらず、
	# 深い部分を変更すると参照先が変更されてしまう
	shallow[1].append("shalow_change")
	print("After shallow copy: {}".format(a))
	# 要素の同一性比較
	print("shallow copy 変更後 ==比較: {}".format(a == shallow)) # True
	# オブジェクト比較
	print("shallow copy 変更後 is比較: {}".format(a is shallow)) # False

	# 深いコピー
	deep = copy.deepcopy(a)
	# コピー元のリスト要素は変化しない
	# 深い階層までずっとコピーされているので
	deep[1].append("deep_change")
	print("After deep copy: {}".format(a))
	# 要素の同一性比較
	print("deep copy 変更後 ==比較: {}".format(a == deep)) # False 
	# オブジェクト比較
	print("deep copy 変更後 is比較: {}".format(a is deep)) # False

	

	print()



if __name__ == "__main__":
	main()


# Django
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST, require_GET
from django.views import View
from django.views.generic import ListView, FormView, DetailView
from django.views.generic import CreateView, UpdateView, DeleteView
from django.views.decorators.http import require_safe, require_http_methods
from django.http import HttpResponseForbidden
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model
from django.utils.decorators import method_decorator
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.decorators import user_passes_test
from django.views.generic.edit import ModelFormMixin
from django.views.generic.edit import FormMixin
from django.db.models import Q, F
from django.db import transaction

# Application
from accounts.models import MyUser
from accounts.models import Profile
from accounts.models import ProfImage
from accounts.forms import SignupForm
from accounts.forms import ProfEditForm
from accounts.forms import ProfImgEditForm
from accounts.forms import QUserListForm


####################################################
###   ユーザー登録
####################################################

# @transaction.atomic
class SignupView(CreateView):
	""" Customizedユーザー登録ページ """
	template_name = 'accounts/signup.html'
	form_class = SignupForm

	# 全てのクラスベースビューにて使われる
	# クラスベースビューでGET時に呼び出されるメソッド
	def get_success_url(self):
		# > CreateView を使うとき、self.object にアクセスできます。
		# > これは作成されているオブジェクトです。
		# > オブジェクトがまだ作成されていない場合、値は None になります。
		return reverse("accounts:prof_create", kwargs={'user_id': self.object.pk})


@login_required
@require_http_methods(['POST', 'GET'])
# @transaction.atomic
def profcreate(request, user_id):
	""" profileをMyUserと紐づける """
	user = get_object_or_404(MyUser, pk=user_id) # MyUserのインスタンスを要求されるので

	if user_id != request.user.id:
		return HttpResponseForbidden('この編集は許可されていません。')

	if request.method == 'POST':
		profile_form = ProfEditForm(request.POST)
		profimg_form = ProfImgEditForm(request.POST, request.FILES)

		if profile_form.is_valid() and profile_form.is_valid():
			profile = profile_form.save(commit=False) # commit=Falseにしないとエラー
			profile.user = user # userと紐づける
			user.profile.save()

			profimage = profimg_form.save(commit=False)
			profimage.user = user
			user.profimage.save()

			return redirect("accounts:profile", user_id=user.pk)

	else:
		profile_form = ProfEditForm()
		profimg_form = ProfImgEditForm()

	template = "accounts/profcreate.html"
	context = {
		'profile_form': profile_form,
		'profimg_form': profimg_form,
	}
	return render(request, template, context)


####################################################
###   プロフィール画面
####################################################

@login_required
def profile(request, user_id):
	""" ユーザーのプロフィールページ """
	user = get_user_model()
	# 参照、逆参照でやっても可
	profile = get_object_or_404(Profile, user_id=user_id) # pkではない
	profimage = get_object_or_404(ProfImage, user_id=user_id)

	if user_id != request.user.id:
		return HttpResponseForbidden('この編集は許可されていません。')

	template = "accounts/profile.html"
	context = {
		'profile': profile,
		'profimage': profimage,
		'you': request.user,
	}
	return render(request, template, context)


####################################################
###   プロフィール情報編集
####################################################

class ProfEditView(LoginRequiredMixin, UpdateView): # ログイン必須
	""" プロフィールの変更 """
	template_name = "accounts/profedit.html"
	model = Profile
	form_class = ProfEditForm

	def form_valid(self, form):
		if self.request.user.id != self.kwargs['pk']: # urlパラメータ取得
			return HttpResponseForbidden('この編集は許可されていません。')
		return super().form_valid(form) # get_success_url()メソッドの戻り値にリダイレクト

	def form_invalid(self, form):
		return super().form_invalid(form)
	
	# フォームの送信に成功した場合のリダイレクト先を指定
	def get_success_url(self):
		return reverse("accounts:profile", kwargs={'user_id': self.request.user.id})

	# DetailViewやUpdateViewにて使われる
	# 返された値はテンプレート上で{{ object }}として受け取ることができる
	def get_object(self, **kwargs):
		obj = Profile.objects.get(user_id=self.kwargs['pk'])
		return obj

	# https://itc.tokyo/django/get-form-kwargs/
	# https://stackoverflow.com/questions/45712699/django-how-to-set-value-of-hidden-input-in-template
	# クラス内で設定しているform_classに値を渡す
	def get_form_kwargs(self, *args, **kwargs): # フォームに初期値を渡す
		kwags = super().get_form_kwargs(*args, **kwargs)
		# kwags["user"] = self.requst.user.id
		return kwags


decorators = [login_required,] # ログイン必須
@method_decorator(decorators, name='dispatch') # dispatchメソッドを指定
class ProfImgEditView(UpdateView): # FormViewだとsave()時にINSERT INTOになってしまう
	""" プロフィール画像の変更 """
	template_name = 'accounts/profimgedit.html'
	model = ProfImage
	form_class = ProfImgEditForm

	def form_valid(self, form):
		if self.request.user.id != self.kwargs['pk']: # urlパラメータ取得
			return HttpResponseForbidden('この編集は許可されていません。')
		return super().form_valid(form) # get_success_url()メソッドの戻り値にリダイレクト
	
	def form_invalid(self, form):
		return super().form_invalid(form)

	def get_success_url(self):
		# urlキーワード引数指定
		return reverse("accounts:profile", kwargs={'user_id': self.request.user.id}) 

	# DetailView, Updateiewにて使われる
	def get_object(self, **kwargs):
		obj = ProfImage.objects.get(user_id=self.kwargs['pk'])
		return obj

	# Formを使うクラスベースビューにて使われる
	# クラス内で設定しているform_classに値を渡す
	def get_form_kwargs(self, *args, **kwargs): # フォームに初期値を渡す
		kwags = super().get_form_kwargs(*args, **kwargs)
		return kwags


class OwnerOnly(UserPassesTestMixin): # または@user_passes_test
	""" ログインしたユーザーの情報を、自分自身のみが編集できるようにする """
	def test_func(self):
		""" Booleanを返す """
		user = self.request.user
		return user.pk == self.kwargs['pk']

	# def get_test_func(self):
		

####################################################
###   ユーザー削除
####################################################

# 本来ならis_active=Falseにするべきだが、ここはテストなので
decorators = [login_required,]
@method_decorator(decorators, name='dispatch')
# テンプレート内では{{ object }} 部分に削除対象のモデルが表示される
class GoodByeView(DeleteView):
	""" ユーザーの退会 """
	template_name = 'accounts/goodbye.html'
	model = MyUser
	success_url = reverse_lazy("top")


####################################################
###   ユーザー検索
####################################################

# フォームとリストを混合させるテスト。実際には関数ベースビューで書いた方が良いだろう
class QUserListView(FormMixin, ListView):
	""" 検索box ＆ ユーザーリスト """
	""" 検索はMyUserのemailフィールドでもProfileのnicknameフィールドでも出来るようにした """
	template_name = 'accounts/Q_userlist.html'
	# model = Profile
	form_class = QUserListForm
	success_url = reverse_lazy("accounts:q_userlist")
	context_object_name = 'user_list'

	# ListViewにて使用できる
	# テンプレート内で{{ object_list }}とすることで取得できる
	def get_queryset(self, **kwargs): # querysetはiterable
		# model=Profileの場合、Profile.objects.all() と同じ結果
		# queryset = super().get_queryset(**kwargs) 
		queryset = None # 最初に全てのユーザー情報が表示されてしまうので

		# FormMixinとListViewで２回get_queryset()が呼び出される
		# そして２回目にはrequest.POSTの内容が消えてしまうのでsessionを活用
		# (ListView, FormMixin)の順に入れ替えても駄目
		session_from_data = self.request.session.pop('q', None)
		if session_from_data is not None:
			# N+1問題対策として、事前に結合
			prof_join_user = Profile.objects.select_related('user').all() # テーブルを連結
			queryset = prof_join_user.filter(user_id__in = session_from_data)
		return queryset

	# HTMLテンプレートへ任意のContextを渡すメソッド
	# 引数のkwargsにはurls.pyで指定した名前付き正規表現のプレースホルダにマッチした内容が入る
	def get_context_data(self, **kwargs):
		# 既存のget_context_dataをコール
		context = super().get_context_data(**kwargs)
		# context.update({})
		return context

	def get(self, request, *args, **kwargs):
		self.object = None # 初期値がNone
		return super().get(request, *args, **kwargs)

	def post(self, request, *args, **kwargs):
		self.object = None
		self.object_list = self.get_queryset()
		form = self.get_form()
		if form.is_valid():
			# QuerySetは該当するデータがない場合は、例外を発生させずに空のQuerySetオブジェクトを返す
			# QuerySetは遅延評価される
			search_email = MyUser.objects.filter(email__contains=form.cleaned_data['q']) # QuerySetが返る
			search_nickname = Profile.objects.filter(nickname__contains=form.cleaned_data['q'])
			email_ids = [se.id for se in search_email]
			nickname_ids = [sn.user_id for sn in search_nickname]
			ids = list(set(email_ids + nickname_ids))
			self.request.session['q'] = ids
			# または一気に、
			# q_query = Q(nickname__icontains=form.cleaned_data['q']) 
			#	| Q(user__email__icontains=form.cleaned_data['q'])
			# queryset = Profile.objects.filter(q_query)
			# として、self.request.session['q'] = querysetを渡す

			# Qオブジェクトをadd()でOR検索する場合
			# q = Q()
			# q.add(Q(nickname__icontains=form.cleaned_data['q']), Q.OR)
			# q.add(Q(user__email__icontains=form.cleaned_Data['q']), Q.OR)
			# queryset = Profile.objects.filter(q)

			return self.form_valid(form)
		else:
			return self.form_invalid(form)


class QUserDetailView(DetailView):
	""" ユーザー詳細 """
	template_name = 'accounts/Q_userdetail.html'
	model = Profile
	context_object_name = 'q_user'





# JOIN
# prof = Profile.objects.select_related('user').all()
# for p in prof: print(p.user.email)
# prof.filter(Q(nickname__contains="i"))
# pro2 = Profile.objects.get(nickname="iku") # getでなければ親要素へアクセスできない
# pro2.user
# pro2.user.email
# Profile.objects.filter(user__email__contains='harada') # field名であることに注意
# MyUser.objects.filter(profile__nickname__contains='王子', profile__prefecture='北海道')


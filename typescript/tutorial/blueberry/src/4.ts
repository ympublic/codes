/**
 * コンパイルと実行(/blueberry上にて)
 * npx tsc
 * node dist/4.js
 *
 */

// 関数宣言
function rrr(min: number, max: number): number[] {
  const result = [];
  // 数字を入れ替え
  if (min > max) {
    const container: number = min;
    min = max;
    max = container;
  }
  for (let i = min; i <= max; i++) {
    result.push(i);
  }
  // 返り値はnumber[]
  return result;
}

console.log(rrr(5, 1));

// Void型関数
function rr2(n: number): void {
  for (let i = 0; i < n; i++) {
    console.log("Hello World!");
  }
}

rr2(5);

type Human4 = {
  height: number;
  weight: number;
};

// アロー関数
const rr3 = ({ height, weight }: Human4): number => {
  // BMI
  return (weight / height) ** 2;
};

const itsSecret = { height: 178, weight: 68 };
console.log("BMI: ", rr3(itsSecret));

// コールバック関数(関数の引数に関数を渡す)
const ppp = rrr(1, 5).map((r: number): number => {
  return r > 3 ? r : 0;
});

console.log("ppp", ppp);

// 関数の型定義
type F = (repeatNum: number) => string;

const xRepeat: F = (repeatNum: number): string => {
  return "+".repeat(repeatNum);
};

console.log(xRepeat(20));

// 関数のジェネリクス

function repeat<T>(element: T, length: number): T[] {
  const result: T[] = [];
  for (let i = 0; i < length; i++) {
    result.push(element);
  }
  return result;
}

console.log(repeat<string>("a", 5));
console.log(repeat<number>(123, 3));

// アロー関数
const pair = <Left, Right>(left: Left, right: Right): [Left, Right] => {
  return [left, right];
};

const p = pair<string, number>("my number", 33);

console.log(p);

// 力試し
console.log("☆☆☆  力試し  ☆☆☆");

const getFizzBuzzString = (i: number): string => {
  if (i % 3 === 0 && i % 5 === 0) {
    return "FizzBuzz";
  } else if (i % 3 === 0) {
    return "Fizz";
  } else if (i % 4 === 0) {
    return "Buzz";
  } else {
    return String(i);
  }
};

const sequence = (start: number, end: number): number[] => {
  const result: number[] = [];
  let num: number = start;
  while (num <= end) {
    result.push(num);
    num++;
  }
  return result;
};

console.log(sequence(1, 10));

for (const i of sequence(1, 20)) {
  const message = getFizzBuzzString(i);
  console.log(message);
}

const testData = [1, 1, 2, 3, 5, 8, 13];

const myMap = (array: number[], callback: (x: number) => number): number[] => {
  const result: number[] = [];
  for (const e of array) {
    result.push(callback(e));
  }
  return result;
};

const result = myMap(testData, (x) => x * 10);

console.log(result);

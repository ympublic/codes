def safe_pawns(pawns):
    horizon = {"a":1, "b":2, "c":3, "d":4, "e":5, "f":6, "g":7, "h":8}
    count=0
    safe=[]
    for p in pawns:
        if p[0] == "a":
            s1 = '{0}{1}'.format("b", int(p[1])+1)
            safe.append(s1)
        if p[0] == "b":
            s1 = '{0}{1}'.format("a", int(p[1])+1)
            s2 = '{0}{1}'.format("c", int(p[1])+1)
            safe.append(s1)
            safe.append(s2)
        if p[0] == "c":
            s1 = '{0}{1}'.format("b", int(p[1])+1)
            s2 = '{0}{1}'.format("d", int(p[1])+1)
            safe.append(s1)
            safe.append(s2)
        if p[0] == "d":
            s1 = '{0}{1}'.format("c", int(p[1])+1)
            s2 = '{0}{1}'.format("e", int(p[1])+1)
            safe.append(s1)
            safe.append(s2)
        if p[0] == "e":
            s1 = '{0}{1}'.format("d", int(p[1])+1)
            s2 = '{0}{1}'.format("f", int(p[1])+1)
            safe.append(s1)
            safe.append(s2)
        if p[0] == "f":
            s1 = '{0}{1}'.format("e", int(p[1])+1)
            s2 = '{0}{1}'.format("g", int(p[1])+1)
            safe.append(s1)
            safe.append(s2)
        if p[0] == "g":
            s1 = '{0}{1}'.format("f", int(p[1])+1)
            s2 = '{0}{1}'.format("h", int(p[1])+1)
            safe.append(s1)
            safe.append(s2)
        if p[0] == "h":
            s1 = '{0}{1}'.format("g", int(p[1])+1)
            safe.append(s1)
    for pawn in pawns:
        if pawn in set(safe):
            count+=1
    return count
               

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert safe_pawns({"b4", "d4", "f4", "c3", "e3", "g5", "d2"}) == 6
    assert safe_pawns({"b4", "c4", "d4", "e4", "f4", "g4", "e5"}) == 1
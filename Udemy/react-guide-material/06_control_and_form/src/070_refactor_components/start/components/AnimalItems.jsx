const AnimalItems = (props) => {
  const { animal } = props;
  return (
    <li>
      {animal}
      {animal === "Dog" && "★"}
    </li>
  );
};

export default AnimalItems;

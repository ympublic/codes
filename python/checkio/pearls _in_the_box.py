from itertools import product
 
def checkio(marbles, step):
    def calc_probability(choices):
        white, length = marbles.count('w'), len(marbles)
        ratio = 1
        for c in choices:
            if c == 'w':
                ratio *= white / length
                white -= 1
            else:
                ratio *= 1 - white / length
                white += 1
            if white < 0 or white > length:
                return 0
        return ratio * (white / length)
 
    return round(sum(calc_probability(p) for p in product(['w', 'b'], repeat=step - 1)), 2)

#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio('bbw', 3) == 0.48, "1st example"
    assert checkio('wwb', 3) == 0.52, "2nd example"
    assert checkio('www', 3) == 0.56, "3rd example"
    assert checkio('bbbb', 1) == 0, "4th example"
    assert checkio('wwbb', 4) == 0.5, "5th example"
    assert checkio('bwbwbwb', 5) == 0.48, "6th example"
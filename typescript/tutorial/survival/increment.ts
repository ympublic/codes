// コンパイル
// $ tsc increment.ts

function increment(num) {
    return num + 1;
}

console.log(increment(999));

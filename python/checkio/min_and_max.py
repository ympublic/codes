def abc(comp, source, func):
    """
    たった"<"だけでintからfloatまで全部比較できるとは...
    いったい何日分岐に時間かけたんだ...
    """
    max=min=[]
    if func:
        for n in source:
            if max==[]: max=n
            if min==[]: min=n
            if func(max)<func(n): max=n
            if func(n)<func(min): min=n
    else:
        for n in source:
            if max==[]: max=n
            if min==[]: min=n
            if max<n: max=n
            if n<min: min=n
    if comp=="max": return max
    else: return min
    
def max(*args, **kwargs):
    key = kwargs.get("key", None)
    if len(args)>1: sort_source=args
    else: sort_source=args[0]
    return abc("max", sort_source, key)
    
def min(*args, **kwargs):
    key = kwargs.get("key", None)
    if len(args)>1: sort_source=args
    else: sort_source=args[0]
    return abc("min", sort_source, key)


"""
def abc(comp, source, func):
    max=0
    min1="z"
    min2=9223372036854775807
    if key:
        
    for n in source:
        print "n: %s" % n
        if isinstance(n, float):
            if round(max) < round(n):
                max=n
            if round(n) < round(min2):
                min2=n
        else:
            if ord(str(max)) < ord(str(n)):
                max=n
            if ord(str(n)) < ord(str(min1)):
                min1=n
    if comp=="max": return max
    elif min2 is not 9223372036854775807: return min2
    else: return min1
"""


"""
def max(*args, **kwargs):
    key = kwargs.get("key", None)
    if len(args) > 1:
        sort_source = args
    else:  # list型で渡されるとforで回しても最初のlistが返ってきてしまう
        sort_source = args[0]
    #if isinstance(args, list): pass
    max=0
    for n in sort_source:
        if isinstance(n, str):
            if isinstance(max, int):
                if max<ord(n):
                    max=n
            else:
                if ord(max)<ord(n):
                    max=n
        elif isinstance(n, float):
            if round(max)<round(n):
                max=n
        else:
            if max<n:
                max=n
    
    return max


def min(*args, **kwargs):
    key = kwargs.get("key", None)
    if len(args) > 1:
        sort_source = args
    else:
        sort_source = args[0]
    min=9223372036854775807
    if key:
        for n in sort_source:
            print "n: %s" % n
            #if key(n)<min: min=n
            if map(key, n)<min: min=n
            print "min: %s" % min
    else:
        for n in sort_source:
            if isinstance(n, str):
                if isinstance(min, int):
                    if min>ord(n):
                        min=n
                else:
                    if ord(min)>ord(n):
                        min=n
            elif isinstance(n, float):
                if round(min)>round(n):
                    min=n
            else:
                if min>n:
                    min=n
    
    return min
"""

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert max(3, 2) == 3, "Simple case max"
    assert min(3, 2) == 2, "Simple case min"
    assert max([1, 2, 0, 3, 4]) == 4, "From a list"
    assert min("hello") == "e", "From string"
    assert max(2.2, 5.6, 5.9, key=int) == 5.6, "Two maximal items"
    assert min([[1, 2], [3, 4], [9, 0]], key=lambda x: x[1]) == [9, 0], "lambda key"
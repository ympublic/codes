import { createSlice } from "@reduxjs/toolkit";
import { asyncCount } from "../../api/counter";

const counter = createSlice({
  name: "counter",
  initialState: {
    count: 0,
    status: "",
  },
  reducers: {
    add(state, { type, payload }) {
      // ReduxToolkitの中では、Immerのおかげで、
      // Mutableな操作がImmutableな操作に置き換わる
      // returnは不要
      state.count = state.count + payload;
      // const newState = { ...state };
      // newState.count = state.count + payload
      // return newState;
    },
    minus(state, { type, payload }) {
      state.count = state.count - payload;
      // const newState = { ...state };
      // newState.count = state.count - payload
      // return newState;
    },
  },
});

const { add, minus } = counter.actions;

// 非同期処理
const addAsync = (payload) => {
  return async (dispatch, getState) => {
    const response = await asyncCount(payload);
    // Action Creator: add
    dispatch(add(response.data));
  };
};

export { add, minus, addAsync };
export default counter.reducer;

# settings.pyのSTATICFILES_STORAGEの挙動を変更

from storages.backends.s3boto3 import S3Boto3Storage

class MediaStorage(S3Boto3Storage):
	""" 同名の画像ファイルを上書きさせない """
	# bucket_name = 'my-app-bucket'
	location = "media" # /mediaというURLで配信 # store files under directory `media/` in bucket `my-app-bucket`
	file_overwrite = False # 同名ファイルは上書きせず、似た名前に


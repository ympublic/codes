import os
import uuid
import datetime
# Django
from django.db import models
from django.conf import settings
from django.utils import timezone
# Third Party


# CHAR型は、郵便番号や社員番号など、データの桁数が決まっているもの向け
# VARCHAR型は、氏名や書籍名など、データ桁数が変動する可能性のあるもの向け
# ...ではあるのだが、DjangoにはCharFiledしか無い

####################################################
###   News
####################################################

class News(models.Model):
	""" Yahoo!ニュースからスクレイピングしてきた記事を保存 """
	title = models.CharField('タイトル', max_length=128, blank=False)
	body = models.TextField('本文', blank=False)
	url = models.URLField('URL', blank=False, unique=True)
	published_at = models.CharField('投稿時刻', max_length=64, blank=True)
	image_src = models.URLField('Top画像', blank=True)
	created_at = models.DateTimeField('登録日時', # 日付+時刻, DateFieldは日付だけ
		blank=False, auto_now_add=True) 
	note = models.TextField("注釈", blank=True, null=True,) # 何かあった時に管理画面で注釈を付与する予備フィールド

	class Meta:
		db_table = 'news' # テーブル名変更(注：MySQLでは小文字のテーブル名を使う！)
		verbose_name = 'ニュース'
		verbose_name_plural = 'ニュース'

	def __str__(self): # 公式ドキュメントにも重要とある
		return str(self.title) # 管理画面上の表示を変更



const Example = () => {
  const num = { val: 2 };

  const double = (num) => {
    // Immutablityを保持するために新しく変数を作成
    const newNum = { ...num };
    newNum.val = num.val * 2;
    return newNum;
  };

  const newNum = double(num);
  console.log("newNum: ", newNum, " / num: ", num);

  return (
    <>
      <h3>不変性（Immutability）</h3>
      <p>引数で渡ってきたオブジェクトを変更しない！</p>
      <div></div>
    </>
  );
};

export default Example;

import { useState } from "react";

// カスタムフック(useStateやuseEffectを内部で呼んでいる関数)
const useCount = () => {
  const [count, setCount] = useState(0);

  return { count, setCount };
};

export default useCount;

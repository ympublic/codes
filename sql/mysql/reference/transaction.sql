/* ストレージエンジンの確認 */
SHOW CREATE TABLE mytable \G

/* MyISAMの場合はInnoDBに変更 */
ALTER TABLE mytable ENGINE=InnoDB;

/* トランザクション */
/* トランザクション開始 */
START TRANSACTION;
/* 処理開始 */
DELETE FROM test_transaction;
# もちろんEmpty Setと表示される
SELECT * FROM test_transaction;
/* ロールバックして戻す */
ROLLBACK;
# test_transactionテーブルの内容が元に戻る
SELECT * FROM test_transaction;

/* 今度はロールバックせずにコミットする */
START TRANSACTION;
DELETE FROM test_transaction;
/* コミットで反映させる */
COMMIT;
# test_transactionテーブルの内容は全て消えている
SELECT * FROM test_transaction;

/* 自動コミット機能をオフにする */
SET AUTOCOMMIT=0;
/* オフにした自動コミット機能をオンにする */
SET AUTOCOMMIT=1;
/* 現在の自動コミット機能のモードを確認 */
SELECT @@AUTOCOMMIT


/* 注意！ */
# トランザクションによって何でもロールバックで元に戻せるわけではない
# 例えば次のようなコマンドは、自動的にコミットされてしまう
# DROP DATABASE
# DROP TABLE
# DROP
# ALTER TABLE


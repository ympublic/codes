# 重要：基本的に、「１つのテストは１つのことを検証すること」が推奨されている
# DjangoのテストはDEBUG=Falseを指定したものとして実行される
# デフォルトではCSRFチェックが無効となっている 有効にする場合：c = Client(enforce_csrf_checks=True)

# テストデータベースの破棄を防ぐ場合
# test --keepdb
# テストを並列実行させたい場合
# test --parallel
# パスワードのハッシュ生成
# PASSWORD_HASHES = [
#   'django.contrib.auth.hashers.MD5PasswordHasher',
# ]


# コードカバレージを確認
# coverage run --source='.' manage.py test
# coverage report

import pdb # pdb.set_trace() or breakpoint()
from unittest import mock

# Django
from django.contrib.auth import get_user_model, login
from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponseRedirect
from django.test import TestCase, RequestFactory, Client
from django.urls import resolve, reverse, reverse_lazy
from django.test import tag
from django.template import Template, Context
from django.core.validators import (
    MaxLengthValidator, MaxValueValidator,
    MinLengthValidator, MinValueValidator,
)
from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.test.utils import override_settings
from django.dispatch import Signal
from django.dispatch import receiver # Signals
from allauth.account.signals import email_confirmed
from allauth.account.signals import user_signed_up
from allauth.account.models import EmailAddress

# Applicaion
from accounts.models import (MyUser, Profile,)

# Third Party
from social_auth.factories import UserFactory
from social_auth.factories import ProfileFactory # Faker
from allauth.account.signals import user_signed_up

UserModel = get_user_model()


####################################################
#####     django-allauthテスト
####################################################
# 注意： signals.pyのuser_signed_up_()をコメントアウトしないとIndexError: list index out of range
signup_url = "/social_auth/signup/"
login_url = "/social_auth/login/"

@tag("oauth")
class TestLoginView(TestCase):

    # def __init__(self, *args, **kwargs): super.__init__(*args, **kwargs)

    def setUp(self): # テストメソッドが呼ばれるたびに実行される
        self.client = Client()
        self.email = "test@lemon.com"
        self.password = "ohmypassword"
        # self.user = get_user_model().objects.create_user(email=self.email, password=self.password)
        # get_user_model().objects.create(user=user, email=self.username, primary=True, verified=True)

    # def tearDown(self): pass

    def test_signup(self):
        response = self.client.post(
            "/social_auth/signup/", 
            {
                "email":self.email, "password1": self.password, "password2": self.password,
            },
            follow = True,
        )
        self.assertRedirects(
            response, 
            "/social_auth/confirm-email/",
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=False,
        )

    def test_user_created(self):
        response = self.client.post(
            "/social_auth/signup/", 
            {
                "email":self.email, "password1": self.password, "password2": self.password,
            },
            # follow = True,
        )
        counter = UserModel.objects.count()

        self.assertEqual(counter, 1)
        self.assertEqual(UserModel.objects.first().email, self.email)

    def test_user_verified(self):
        response = self.client.post(
            "/social_auth/signup/", 
            {
                "email":self.email, "password1": self.password, "password2": self.password,
            },
            follow = True,
        )
        user_obj = UserModel.objects.first()
        email_obj = user_obj.emailaddress_set.first() # django-allauthが作成するテーブル
        email_obj.verified = True
        email_obj.save()
        self.assertEqual(email_obj.verified, True)

    def test_login(self):
        response = self.client.post(
            "/social_auth/signup/", 
            {
                "email":self.email, "password1": self.password, "password2": self.password,
            },
            follow = True,
        )
        user_obj = UserModel.objects.first()
        email_obj = user_obj.emailaddress_set.first() # django-allauthが作成するテーブル
        email_obj.verified = True
        email_obj.save()

        self.assertEqual(settings.LOGIN_URL, login_url)
        response2 = self.client.post(
            settings.LOGIN_URL, {"login": self.email, "password": self.password, }
        )
        self.assertRedirects(
            response2, 
            reverse(settings.LOGIN_REDIRECT_URL),
            status_code=302,
            target_status_code=200,
            fetch_redirect_response=False,
        )

    def test_wrong_login(self):
        response = self.client.post(
            settings.LOGIN_URL, {"login": self.email, "password": "wrong_password"}
        )
        # validation_error = 'The e-mail address and/or password you specified are not correct'
        validation_error = "入力されたメールアドレスもしくはパスワードが正しくありません"
        assert validation_error in response.content.decode('utf-8')

    def test_logout(self):
        # reverse("accounts:logout")で自分はログアウトしているが、
        # これでは"/"にリダイレクトされるのでココではdjango-allauthのやり方を適用する
        response = self.client.get("/social_auth/logout/") 
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, settings.ACCOUNT_LOGOUT_REDIRECT_URL)

    def test_redirect_when_authenticated(self):
        user = get_user_model().objects.create(email=self.email, password=self.password, is_verified=True)
        self.client.force_login(user)
        response = self.client.get(settings.LOGIN_URL)
        url = reverse("top")
        self.assertRedirects(response, url, fetch_redirect_response=False)

    def test_verified_user_login(self):
        response = self.client.post(
            "/social_auth/signup/", 
            {
                "email":self.email, "password1": self.password, "password2": self.password,
            },
            follow = True,
        )
        user_obj = UserModel.objects.first()
        email_obj = user_obj.emailaddress_set.first() # django-allauthが作成するテーブル
        email_obj.verified = True
        email_obj.save()
        res_bool_true = self.client.login(email=self.email, password=self.password)
        self.assertEqual(res_bool_true, True)

    '''
    def test_unverified_user_login(self):
        email = "glico@glico.com"
        password = "kinokonosato"
        response = self.client.post(
            "/social_auth/signup/", 
            {
                "email": email, "password1": password, "password2": password,
            },
        )
        res_bool_false = self.client.login(email=email, password=password)
        self.assertEqual(res_bool_false, False) # Trueになってしまう
    '''


# 注意：普通のサインアップをすると、以下はproviderがout of rangeになる
@receiver(user_signed_up)
def user_socialauth_signed_up_(request, user, **kwargs):
    """ social_authのユーザー作成時にSignalを受け取り、ProfileとProfImageテーブルを紐付ける """
    print("☆☆☆ user is signed up through social_auth ☆☆☆")
    # requestの中身はAnonymousだが、user.idやuser.emailは取得できる
    provider = user.socialaccount_set.all()[0].provider
    extra_data = SocialAccount.objects.filter(user=user, provider=provider)[0].extra_data

    # django-all-authから登録したユーザーを、ProfileとProfImageテーブルに紐付ける

    # プロフィール
    profile_obj = Profile(user=user)
    if provider:
        profile_obj.given_name = extra_data['given_name']
        profile_obj.family_name = extra_data['family_name']
    profile_obj.save()

    # プロフ画像
    profimage_obj = ProfImage(user=user)
    profimage_obj.save()

    # プロフ画像を取得 -> ProfEditForm()
    # response = requests.get(extra_data['picture'])
    # image = response.image

    # create()を使用する場合
    # Profile.objects.create(user=user,)
    # ProfImage.objects.create(user=user,)


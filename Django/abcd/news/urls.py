# Django
from django.urls import path
from django.urls import re_path
# Application
from news import views

app_name = "news"

# 関数ベースビュー
urlpatterns = []

# クラスベースビュー
urlpatterns += [ # as_view(): クラスベースビューを関数化する
	path("", views.NewslistView.as_view(), name="news_list"), # "/news/"
	path("<int:pk>/", views.NewsDetailView.as_view(), name="news_detail"),
	path("register/", views.NewsRegisterView.as_view(), name="news_register"),
	path("<int:pk>/edit/", views.NewsEditView.as_view(), name="news_edit"), 
	path("<int:pk>/delete/", views.NewsDeleteView.as_view(), name="news_delete"), 
]


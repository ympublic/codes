/* 
 * アイテム登録
 * SQLiteの場合【シェル】： db.sqlite3 < shopping/shopping.sql
 * SQLiteの場合【コンソール内】： .read shopping/shopping.sql
 * MySQLの場合【シェル】： mysql < shopping/shopping.sql
 * MySQLの場合【コンソール内】： source shopping/shopping.sql
 * 
 */


/* テーブルスキーマ確認 */
/* SQLite */
.schema item
/* MySQL */
DESCRIBE item;


/* カテゴリ */
INSERT INTO `item_category` (parent_id, category_name, category_type) VALUES (0, '種馬', 1);


/* アイテム */

/* 
 * MySQLの場合はNow() 
 * 
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Sunday Silence', 3000, 1, 14, 9, '日本競馬を変えた伝説の種馬', 1531, NOW(), NOW(), False);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Deep Impact', 4000, 1, 14, 12, '競走馬としてだけではなく、種馬としても成功した名馬', 1802, NOW(), NOW(), False);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Orfevre', 300, 1, 21, 12, '世界でも最強格！日本の歴史上最強の競走馬', 881, NOW(), NOW(), True);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Gold Ship', 250, 1, 28, 13, '今も愛される歴史に残る個性派馬。実力もトップクラス', 446, NOW(), NOW(), True);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Stay Gold', 600, 1, 50, 7, '数々の個性的な強馬を生み出した種馬界のダークホース', 1342, NOW(), NOW(), False);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Kitasan Black', 1000, 1, 20, 12, '350万円の馬が大活躍！まさに大穴馬', 288, NOW(), NOW(), True);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Lord Kanaloa', 1500, 1, 19, 13, '産駒も活躍している短距離王', 1335, NOW(), NOW(), True);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Contrail', 1200, 1, 11, 8, '最弱三冠馬の称号を種馬として覆せるか？', 0, NOW(), NOW(), True);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Epiphaneia', 1800, 1, 14, 6, 'なぜ種付け料最高額なのか分からない、産駒がすぐにダメになる種牡馬', 0, NOW(), NOW(), True);
*/


/* 
 * SQLiteの場合はdatetime('now') 
 * または、CURRENT_TIMESTAMP？ 
 * 
 */
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Sunday Silence', 3000, 1, 14, 9, '日本競馬を変えた伝説の種馬', 1531, datetime('now'), datetime('now'), False);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Deep Impact', 4000, 1, 14, 12, '競走馬としてだけではなく、種馬としても成功した名馬', 1802, datetime('now'), datetime('now'), False);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Orfevre', 300, 1, 21, 12, '世界でも最強格！日本の歴史上最強の競走馬', 881, datetime('now'), datetime('now'), True);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Gold Ship', 250, 1, 28, 13, '今も愛される歴史に残る個性派馬。実力もトップクラス', 446, datetime('now'), datetime('now'), True);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Stay Gold', 600, 1, 50, 7, '数々の個性的な強馬を生み出した種馬界のダークホース', 1342, datetime('now'), datetime('now'), False);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Kitasan Black', 1000, 1, 20, 12, '350万円の馬が大活躍！まさに大穴馬', 288, datetime('now'), datetime('now'), True);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Lord Kanaloa', 1500, 1, 19, 13, '産駒も活躍している短距離王', 1335, datetime('now'), datetime('now'), True);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Contrail', 1200, 1, 11, 8, '最弱三冠馬の称号を種馬として覆せるか？', 0, datetime('now'), datetime('now'), True);
INSERT INTO `item` (name, price, category_id, runs, wins, description, total_sales, created_at, updated_at, on_sale)
VALUES ('Epiphaneia', 1800, 1, 14, 6, 'なぜ種付け料最高額なのか分からない、産駒がすぐにダメになる種牡馬', 0, datetime('now'), datetime('now'), True);



/* 
 * Sunday Silence 
 * 
 */
/* 在庫 */
INSERT INTO `item_stock` (item_id, stock) VALUES (1, 0);
/* 主なG1 */
INSERT INTO `g1` (item_id, g1) VALUES (1, 'カリフォルニアンステークス');
INSERT INTO `g1` (item_id, g1) VALUES (1, 'ブリーダーズカップクラシック');
INSERT INTO `g1` (item_id, g1) VALUES (1, 'スーパーダービー');
INSERT INTO `g1` (item_id, g1) VALUES (1, 'プリークネスステークス');
INSERT INTO `g1` (item_id, g1) VALUES (1, 'ケンタッキーダービー');
INSERT INTO `g1` (item_id, g1) VALUES (1, 'サンタアニタダービー');
/* 主な産駒 */
INSERT INTO `foal` (item_id, name) VALUES (1, 'Deep Impact');
INSERT INTO `foal` (item_id, name) VALUES (1, 'Silence Suzuka');
INSERT INTO `foal` (item_id, name) VALUES (1, 'Stay Gold');
INSERT INTO `foal` (item_id, name) VALUES (1, 'Special Week');


/* 
 * Deep Impact 
 * 
 */
INSERT INTO `item_stock` (item_id, stock) VALUES (2, 0);
INSERT INTO `g1` (item_id, g1) VALUES (2, '有馬記念');
INSERT INTO `g1` (item_id, g1) VALUES (2, 'ジャパンC');
INSERT INTO `g1` (item_id, g1) VALUES (2, '天皇賞　春');
INSERT INTO `g1` (item_id, g1) VALUES (2, '宝塚記念');
INSERT INTO `g1` (item_id, g1) VALUES (2, '菊花賞');
INSERT INTO `g1` (item_id, g1) VALUES (2, '皐月賞');
INSERT INTO `g1` (item_id, g1) VALUES (2, '東京優駿');
INSERT INTO `foal` (item_id, name) VALUES (2, 'Gentildonna');
INSERT INTO `foal` (item_id, name) VALUES (2, 'Contrail');
INSERT INTO `foal` (item_id, name) VALUES (2, 'Gran Alegria');


/* 
 * Orfevre 
 * 
 */
INSERT INTO `item_stock` (item_id, stock) VALUES (3, 120);
INSERT INTO `g1` (item_id, g1) VALUES (3, '有馬記念');
INSERT INTO `g1` (item_id, g1) VALUES (3, '宝塚記念');
INSERT INTO `g1` (item_id, g1) VALUES (3, '菊花賞');
INSERT INTO `g1` (item_id, g1) VALUES (3, '皐月賞');
INSERT INTO `g1` (item_id, g1) VALUES (3, '東京優駿');
INSERT INTO `foal` (item_id, name) VALUES (3, 'Lucky Lilac');
INSERT INTO `foal` (item_id, name) VALUES (3, 'Authority');


/* 
 * Gold Ship
 * 
 */
INSERT INTO `item_stock` (item_id, stock) VALUES (4, 100);
INSERT INTO `g1` (item_id, g1) VALUES (4, '有馬記念');
INSERT INTO `g1` (item_id, g1) VALUES (4, '宝塚記念');
INSERT INTO `g1` (item_id, g1) VALUES (4, '菊花賞');
INSERT INTO `g1` (item_id, g1) VALUES (4, '皐月賞');
INSERT INTO `g1` (item_id, g1) VALUES (4, '天皇賞　春');
INSERT INTO `foal` (item_id, name) VALUES (4, 'Uberleben');
INSERT INTO `foal` (item_id, name) VALUES (4, 'Win Kiitos');
INSERT INTO `foal` (item_id, name) VALUES (4, 'Black Hole');


/* 
 * Stay Gold 
 * 
 */
INSERT INTO `item_stock` (item_id, stock) VALUES (5, 0);
INSERT INTO `g1` (item_id, g1) VALUES (5, '香港ヴァーズ');
INSERT INTO `foal` (item_id, name) VALUES (5, 'Orfevre');
INSERT INTO `foal` (item_id, name) VALUES (5, 'Gold Ship');
INSERT INTO `foal` (item_id, name) VALUES (5, 'Oju Chosan');


/* 
 * Kitasan Black
 * 
 */
INSERT INTO `item_stock` (item_id, stock) VALUES (6, 200);
INSERT INTO `g1` (item_id, g1) VALUES (6, '有馬記念');
INSERT INTO `g1` (item_id, g1) VALUES (6, '大阪杯');
INSERT INTO `g1` (item_id, g1) VALUES (6, '菊花賞');
INSERT INTO `g1` (item_id, g1) VALUES (6, 'ジャパンC');
INSERT INTO `g1` (item_id, g1) VALUES (6, '天皇賞　春');
INSERT INTO `g1` (item_id, g1) VALUES (6, '天皇賞　秋');
INSERT INTO `foal` (item_id, name) VALUES (6, 'Equinox');


/* 
 * Lord Kanaloa
 * 
 */
INSERT INTO `item_stock` (item_id, stock) VALUES (7, 150);
INSERT INTO `g1` (item_id, g1) VALUES (7, '安田記念');
INSERT INTO `g1` (item_id, g1) VALUES (7, 'スプリンターズステークス');
INSERT INTO `g1` (item_id, g1) VALUES (7, '高松宮記念');
INSERT INTO `g1` (item_id, g1) VALUES (7, '香港スプリント');
INSERT INTO `foal` (item_id, name) VALUES (7, 'Almond Eye');
INSERT INTO `foal` (item_id, name) VALUES (7, 'Saturnalia');


/* 
 * Contrail
 * 
 */
INSERT INTO `item_stock` (item_id, stock) VALUES (8, 200);
INSERT INTO `g1` (item_id, g1) VALUES (8, 'ジャパンC');
INSERT INTO `g1` (item_id, g1) VALUES (8, '菊花賞');
INSERT INTO `g1` (item_id, g1) VALUES (8, '東京優駿');
INSERT INTO `g1` (item_id, g1) VALUES (8, '皐月賞');


/* 
 * Epiphaneia
 * 
 */
INSERT INTO `item_stock` (item_id, stock) VALUES (9, 160);
INSERT INTO `g1` (item_id, g1) VALUES (9, 'ジャパンC');
INSERT INTO `g1` (item_id, g1) VALUES (9, '菊花賞');
INSERT INTO `foal` (item_id, name) VALUES (9, 'Chrono Genesis');
INSERT INTO `foal` (item_id, name) VALUES (9, 'Daring Tact');
INSERT INTO `foal` (item_id, name) VALUES (9, 'Efforia');




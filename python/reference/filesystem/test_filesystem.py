#! /usr/bin/env python

"""
"""

import pytest
import os
import sys
# sys.setdefaultencoding("utf-8")
import shutil # osより高度な操作を行う場合
import glob
import stat
import tempfile # 一時ファイルやディレクトリを作成
import io # Python2では、import StringIO


@pytest.fixture(scope="function")
def some_data(): 
	"""  """

	# setUp
	
	yield 40

	# tearDown


@pytest.mark.solved
def test_should_use_pytest_tmpdir_fixture(tmpdir): # Pytest標準のtmpdirフィクスチャ
	""" Pytest標準のフィクスチャtmpdirの挙動をテスト """
	
	# Pytest標準のtmpdirフィクスチャを使用
	# scopeがfunction以外、つまりclass、module、sessionの場合、
	# 複数のテストで使用するディレクトリを作成したい場合はtmpdir_factoryを使用

	# ディレクトリを作成
	a_directory = tmpdir.mkdir("test_dir")

	# ファイル作成準備
	a_file = tmpdir.join(f"test_dir{os.sep}test_file.txt")

	# ファイルはこのwrite時に作成される
	a_file.write("これはテスト用のファイルです")

	# ファイル内容が正しいか
	assert a_file.read() == "これはテスト用のファイルです"

	# ディレクトリ名を取得
	# tmpdirの作る一時ディレクトリは当ファイルのディレクトリ以下ではない
	# Windowsであれば、\User\AppData\Local\Temp\以下に作成される
	assert os.path.dirname(a_directory) != os.getcwd()

	# 一時ファイルが作成されたか確認
	assert os.path.exists(a_file)

	# a_fileの作成場所はちゃんとa_directory直下となっているか
	assert os.path.abspath(a_directory) == os.path.dirname(a_file)

	# アクセス権限の変更
	# 所有者などはos.chown()
	os.chmod(a_file, 777)

	# 読み取り可能か
	assert os.access(a_file, os.R_OK)

	# 環境変数にPythonが含まれているか
	# Windowsの場合
	assert "Python" in os.environ["PATH"] 
	# そもそもこのPATHにPython含まれていなければPytestも実行できないが


@pytest.mark.solved
def test_should_return_right_path():
	""" Python標準のos,sysモジュールを使用 """

	# カレントディレクトリの絶対パスを取得し、ファイル名を結合
	# 当ファイルの絶対パスを作成
	this_file = os.path.join(os.getcwd(), "filesystem.py")

	# または
	this_file_sub = os.path.abspath("filesystem.py")

	assert this_file == this_file_sub
	
	# 親ディレクトリ名までを含むパスを取得
	abs_parent_dir = os.path.dirname(this_file)

	# 親ディレクトリ名が含まれているか
	assert "filesystem" in (abs_parent_dir.split(os.sep))
	# パスセパレータで分割するos.path.split()も存在する
	assert "filesystem" in os.path.split(abs_parent_dir)

	# またはos.basename()でパス末端のファイル名/ディレクトリ名を取得する
	parent_dir_name = os.path.basename(os.path.dirname(this_file))

	assert "filesystem" == parent_dir_name

	# globモジュールによりマッチングルールを適用
	assert "filesystem.py" in glob.glob("*.py")

	# 当ファイル名は正しく取得されているか
	assert os.path.basename(this_file) == "filesystem.py"

	# カレントディレクトリの変更
	os.chdir("../") # 親ディレクトリへ移動

	assert "reference" == os.path.basename(os.getcwd())

	# ファイル・ディレクトリ名のリストを取得し、当ファイルの親ディレクトリが存在するか
	assert "filesystem" in os.listdir(".")


@pytest.mark.solved
def test_make_directory_and_remove_directory():
	"""  """

	# 当ファイルの絶対パスを取得
	this_file = os.path.join(os.getcwd(), "filesystem.py")

	# ファイルまたはディレクトリが存在するか
	assert os.path.exists(this_file)

	# ディレクトリかどうか
	assert not os.path.isdir(this_file)

	# ファイルかどうか
	assert os.path,isfile(this_file)

	# リンク(http://など)かどうか
	assert not os.path.islink(this_file)

	# ディレクトリの作成
	# 本当はtry~catchで作成するべき
	# ファイルやディレクトリ操作時にos.path.の.pathは付けない
	os.mkdir("for_test")

	# ディレクトリが作成できたか確認
	assert os.path.isdir("./for_test")

	# 作成した一時ディレクトリを削除
	os.rmdir("./for_test")

	# ディレクトリが削除されたか確認
	assert not os.path.exists("./for_test")

	# ディレクトリを再帰的に作成
	os.makedirs(os.path.join("parent_dir", "child_dir"))

	# 作成した一時ディレクトリを確認
	assert os.path.isdir(f".{os.sep}parent_dir{os.sep}child_dir")

	# ディレクトリ名を変更または移動させる場合
	# より複雑な処理の場合はshutil.move()を使用
	os.rename(f".{os.sep}parent_dir{os.sep}child_dir", f".{os.sep}parent_dir{os.sep}friend_dir",)

	# ディレクトリ名が変更されたか確認
	assert os.path.exists(f".{os.sep}parent_dir{os.sep}friend_dir")
	assert not os.path.exists(f".{os.sep}parent_dir{os.sep}child_dir")

	# ディレクトリを再帰的に全て削除する場合はshutil.rmtree()を使用する
	shutil.rmtree("./parent_dir")

	# ディレクトリが削除されたかを確認
	assert not os.path.exists(f".{os.sep}parent_dir")
	assert not os.path.exists(f".{os.sep}parent_dir{os.sep}friend_dir")

	# ファイルをコピー
	# 他にもshutil.copyfile()やshutil.copy2()も存在
	shutil.copy("./filesystem.py", "./filesystem_bak.py")

	# ファイルが作成されたか
	assert os.path,isfile("./filesystem_bak.py")

	# ファイルの削除
	os.remove("./filesystem_bak.py")

	# ファイルが削除されたか
	assert os.path,exists("./filesystem_bak.py")


@pytest.mark.solved
def test_make_and_remove_temporary_file():
	""" Pythonのtempfileモジュールを利用 """

	# 一時ディレクトリを作成
	# 注意: 明示的に削除する必要がある。自動で削除されない
	tmpdir = tempfile.mkdtemp(dir=".")

	assert os.path.exists(tmpdir)

	# 一時ファイルを作成
	f = tempfile.NamedTemporaryFile(dir=tmpdir)

	# 一時ファイル名にはf.nameでアクセス
	assert os.path.isfile(os.path.join(tmpdir, f.name))

	# NamedTemporaryFileで作成したファイルはf.close()すると自動的に削除される
	f.close()

	assert not os.path.exists(os.path.join(tmpdir, f.name))

	# 一時ディレクトリを削除
	shutil.rmtree(tmpdir)

	assert not os.path.exists(tmpdir)


@pytest.mark.solved
def test_storing_data_to_stringIO():
	""" StringIO利用 """

	message = "大谷翔平は歴史的な野球選手である！\n"

	# ファイルライクオブジェクト作成
	sio = io.StringIO(message)

	# read(), readlines()[0]
	assert sio.read() == message

	# getvalue()
	assert sio.getvalue() == message

	# 書き込み
	sio.write("現代の大リーグで二刀流など、考えられないことである！")

	assert "二刀流" in sio.getvalue()


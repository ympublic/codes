from abc import ABC # Abstract Base Class
from abc import ABCMeta
from abc import abstractmethod

#########################################################
# Java: インタフェースとして特別扱いできる２つの条件
# ①全てのメソッドが抽象メソッドである
# ②基本的にフィールドを１つも持たない
#########################################################

######################################################################
# インタフェースの効果
# ①同じインタフェースをimplementsする複数の子クラス達に、
# 共通のメソッド群を実装するよう強制できる
# ②あるクラスがインタフェースを実装していれば、少なくとも
# そのインタフェースが定めたメソッドは持っていることが保証される
######################################################################

# Interface
# クリーニング店の店頭メニューのよう
class CleaningService(ABC): # PythonにInterface機能はない

	"""  """

	# Interfaceはフィールドを１つも持たないので、コンストラクタも不要

	# Interface内の全てのメソッドは抽象メソッド
	@abstractmethod
	def washShirt(s: Shirt) -> str:
		raise NotImplementedError()

	@abstractmethod
	def washTowl(t: Towl) -> str:
		raise NotImplementedError()

	@abstractmethod
	def washCoat(c: Coat) -> str:
		raise NotImplementedError()


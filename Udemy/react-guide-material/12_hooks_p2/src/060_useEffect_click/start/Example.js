import { useEffect, useState, useLayoutEffect } from "react";

const Example = () => {
  const [isDisp, setIsDisp] = useState(true);

  return (
    <>
      {isDisp && <Timer />}
      <button onClick={() => setIsDisp((prev) => !prev)}>トグル</button>
    </>
  );
};

const Timer = () => {
  const [time, setTime] = useState(0);
  const [timer, setTimer] = useState(false);

  useEffect(() => {
    let intervalId = null;

    // timerが動いている間
    if (timer) {
      intervalId = window.setInterval(() => {
        // console.log('interval running');
        setTime((prev) => prev + 1);
      }, 1000);

      // コールバック関数の戻り値(依存配列を渡さない場合)
      // Timerコンポーネントが消滅する際に実行される
      // コンポーネントが消滅した際の後始末を行う場合に利用
      // クリーンナップ
      return () => {
        // メモリリークを防ぐため
        window.clearInterval(intervalId);
      };
    }
    // timerのboolean値によってTimerを動かすか決めたいので
  }, [timer]);

  useEffect(() => {
    // DOM操作
    document.title = "counter:" + time;
    window.localStorage.setItem("time-key", time);

    // コールバック関数の戻り値(依存配列を渡さない場合)
    // 依存配列のstateが変更される度にコールバック関数が呼ばれるが、
    // コールバック関数が呼ばれる前にreturnが返る
    // つまりここでは"Updated End"が呼ばれてから"Updated init"が呼ばれる

    // クリーンナップ(useEffectで呼ばれた何らかの後始末をする際に利用する)
    return () => {
      // 依存配列が更新される度にクリーンナップが実行される
    };
  }, [time]);

  // useEffectで書くと、コンポーネントが消滅した場合にカウントが0に戻る
  // また、useLayoutEffectはuseEffectよりも先に実行される
  useLayoutEffect(() => {
    const _time = parseInt(window.localStorage.getItem("time-key"));
    if (!isNaN(_time)) {
      setTime(_time);
    }
  }, []);

  const toggleTimer = (e) => {
    // setTimer(!timer);
    setTimer((prev) => !prev);
  };

  const resetTimer = (e) => {
    // timeを0に設定して、ボタンをstartに変更する
    setTime(0);
    setTimer(false);
  };

  return (
    <>
      <h3>
        <time>{time}</time>
        <span>秒経過</span>
      </h3>
      <button onClick={toggleTimer}>{timer ? "Stop" : "Start"}</button>
      <button onClick={resetTimer}>Reset</button>
    </>
  );
};

export default Example;

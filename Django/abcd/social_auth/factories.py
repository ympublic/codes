# FactoryBoy + Fakerを使用
# FactoryBoy: 
# Faker: 

import string

# Django
from django.contrib.auth import get_user_model

# Application
# from social_auth.models import ()
from accounts.models import (Profile, )

# Third Party
# Factory Boy
import factory
from factory.faker import faker
from factory import fuzzy # Fuzzy系はランダムな値を生成するためのオブジェクト
# Faker
from faker import Faker

UserModel = get_user_model()
fake = Faker('jp-JP') # 日本語のFakeを作成


####################################################
#####     
####################################################

class UserFactory(factory.django.DjangoModelFactory):
	class Meta:
		model = UserModel

	email = factory.Faker('email', locale='ja-JP')
	password = fake.password(20)
	is_staff = False
	is_active = True


# test.pyにて、from social_auth.factories import ProfileFactory
class ProfileFactory(factory.django.DjangoModelFactory):
	""" profile = Profile() -> profile.nickname のように使う """
	class Meta:
		model = Profile # 'accounts.Profile'

	nickname = fake.name() # 日本語の名前を作成
	family_name = fake.last_name() # https://faker.readthedocs.io/en/master/locales/ja_JP.html#faker-providers-person
	given_name = fake.first_name()
	phone = fake.phone_number()
	zip_code = fake.postcode()
	country = fake.current_country_code() 
	prefecture = fake.prefecture()
	address1 = '{}{}{}{}{}'.format(fake.city(), fake.town(), fake.chome(), fake.ban(), fake.gou())
	address2 = '{}{}号室'.format(fake.building_name(), fake.building_number())


# cf)
# FAKE = faker.Faker()
# FAKE.name()
# FAKE.address()


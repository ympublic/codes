import { useEffect } from "react";
import { axios } from "axios";

// 関数コンポーネント内は純粋関数として保つ必要がある
// 非同期通信などように副作用のある処理はuseEffect()の中で書く必要がある
const Example = () => {
  useEffect(() => {
    // 返り値はPromiseオブジェクトが返ってくる
    // レスポンスが返ってくるまでに時間がかかるので非同期
    // なので.then(非同期処理)で処理を続ける
    const res = axios.get("http://localhost:3003/user/").then((res) => {
      // サーバーから返ってきたdataを取得
      console.log(res.data);
    });
  });

  return <></>;
};

export default Example;

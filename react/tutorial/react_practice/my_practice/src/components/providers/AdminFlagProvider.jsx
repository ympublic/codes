import { createContext, useState } from "react";

// Contextの器を作成
export const AdminFlagContext = createContext({});

export const AdminFlagProvider = (props) => {
  // Props: children
  const { children } = props;

  // 管理者フラグ
  const [isAdmin, setIsAdmin] = useState(false);

  // ポイント！ Propsとしてchildrenを受け取る
  // AdminFlagContextの中にProviderがあるので、それでchildrenを囲む
  // valueの中にグローバルに扱う実際の値を設定
  // ContextオブジェクトとしてisAdminとsetIsAdminを設定
  return (
    <AdminFlagContext.Provider value={{ isAdmin, setIsAdmin }}>
      {children}
    </AdminFlagContext.Provider>
  );
};

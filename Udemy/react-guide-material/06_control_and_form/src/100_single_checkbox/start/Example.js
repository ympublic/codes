import { useState } from "react";

const Example = () => {
  const [isChecked, setIsChecked] = useState(false);
  const onChange = (e) => {
    setIsChecked((prev) => !prev);
  };
  return (
    <div>
      <label htmlFor="my-check">チェック</label>
      <input
        type="checkbox"
        id="my-check"
        checked={isChecked}
        onChange={onChange}
      />
      <div>{isChecked ? "ON!" : "OFF!"}</div>
    </div>
  );
};

export default Example;

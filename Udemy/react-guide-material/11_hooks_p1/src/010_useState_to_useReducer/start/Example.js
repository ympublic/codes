// useReducer: useStateの書き換えに利用する
import { useState, useReducer } from "react";

const Example = () => {
  const [state, setAState] = useState(0);
  // const [state, dispatch] = useReducer(reducer, initialArg, init?)
  const [rstate, dispatch] = useReducer((prev, { type, step }) => {
    switch (type) {
      case "+":
        return prev + step;
      case "-":
        return prev - step;
      default:
        throw new Error("不明なアクションです");
    }
  }, 0);

  const countUp = () => {
    setAState((prev) => ++prev);
  };
  const rCountUp = () => {
    // 引数を渡す
    dispatch({ type: "+", step: 3 });
  };
  const rCountDown = () => {
    dispatch({ type: "-", step: 2 });
  };

  return (
    <>
      <div>
        <h3>useState</h3>
        <button onClick={countUp}>＋</button>
        {state}
      </div>
      <div>
        <h3>useReducer</h3>
        <button onClick={rCountUp}>＋</button>
        <button onClick={rCountDown}>ー</button>
        {rstate}
      </div>
    </>
  );
};

export default Example;

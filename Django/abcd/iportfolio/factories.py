# FactoryBoy + Fakerを使用
# FactoryBoy: 
# Faker: 

import string

# Django
from django.contrib.auth import get_user_model

# Application
from iportfolio.models import Contact

# Third Party
# Factory Boy
import factory
from factory.faker import faker
from factory import fuzzy # Fuzzy系はランダムな値を生成するためのオブジェクト
# Faker
from faker import Faker

UserModel = get_user_model()
fake = Faker('jp-JP') # 日本語のFakeを作成


####################################################
#####     
####################################################

class UserFactory(factory.django.DjangoModelFactory):
	class Meta:
		model = UserModel

	email = factory.Faker('email', locale='ja-JP')
	is_staff = False
	is_active = True


# from iportfolio.factories import ContactFactory
class ContactFactory(factory.django.DjangoModelFactory):
	""" contact = Contact() -> contact.name のように使う """
	class Meta:
		model = Contact # 'iportfolio.Contact'

	# https://faker.readthedocs.io/en/master/locales/ja_JP.html#faker-providers-person
	name = fake.name() # 日本語の名前を作成
	email = fake.email()
	# phone = fake.phone_number()
	# subject = fuzzy.FuzzyText(prefix='fuzzy_', length=16)
	subject = fake.sentence()
	message = fake.text(200) # 200文字以内？ 


# cf)
# FAKE = faker.Faker()
# FAKE.name()
# FAKE.address()


# ミドルウェアを使うことで、view関数での共通処理をミドルウェアに記述すれば
# すべてのview関数に対して実行できるようになる

# Middlewareのテスト
def set_testcookie(get_response):

	def middleware(request):
		request.session['middleware_test'] = 'ようこそ。ごゆっくりしていって下さい。'
		response = get_response(request)
		return response

	return middleware


# settings.py内に"pooh.middleware.middleware.MyMiddleware"を追記
class MyMiddleware:
	def __init__(self, get_response):
		self.get__response = get_response
		# ①サーバー起動時にのみ実行される処理

	def __call__(self, request):
		# ②view関数が実行される（リクエスト処理）前に実行される処理
		response = self.get_response(request)
		# ④view関数が実行された（リクエスト処理）後に実行される処理
		return response


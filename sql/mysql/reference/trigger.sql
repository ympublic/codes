/* 行トリガー。文トリガーはFOR EACH STATEMENT */
DELIMITER $$
/* BEFORE または AFTER */
CREATE TRIGGER my_trigger BEFORE DELETE ON 売上 FOR EACH ROW
BEGIN
	/* OLD.カラム名 または NEW.カラム名 を使った処理 */
	INSERT INTO 売上_backup VALUES (OLD.id, OLD.name, OLD.price);
	/* CREATE TRIGGER my_trigger AFTER ~ の場合 */
	# SELECT NEW.id, NEW.name, NEW.price FROM 売上;
END
$$
DELIMITER ;

SHOW TRIGGERS;
DROP TRIGGER my_trigger;

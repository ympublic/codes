#! /usr/bin/env python

"""JSON
PythonオブジェクトとJSONデータの間の型変換表
https://docs.python.org/ja/3/library/json.html#encoders-and-decoders
"""

import pytest # pytest -v -m "unsolved" test_json.py --pdb
import json
import os
import datetime

# フィクスチャ作成
@pytest.fixture(scope="function")
def json_data(): 
	""" JSONデータ """

	# setUp

	# JSONオブジェクト
	obj = [
		"Beatles",
		{
			"member": ["Paul", "John", "Ringo", "George"],
		},
	]

	yield obj

	# tearDown


@pytest.mark.solved
def test_should_dumps_and_loads_strings(json_data):
	"""
	Pythonオブジェクト <=> JSON形式の文字列
	pickle.dumps(Pythonオブジェクト) # 最後のsはstringのｓ?
	pickle.loads(JSON形式の文字列)
	"""

	# JSON形式に変換して文字列として出力
	json_str = json.dumps(json_data)

	# JSON形式の文字列からPythonオブジェクトに変換
	serialized = json.loads(json_str)

	assert serialized == json_data


@pytest.mark.solved
def test_should_dump_and_load_stream(tmpdir, json_data): # tmpdirフィクスチャ
	"""
	Pythonオブジェクト <=> JSON形式の文字列
	json.dump(obj, fp)
	json.load(fp)
	"""

	# ファイル保存
	with open(f"{tmpdir}{os.sep}data.json", "w") as f:
		# JSON化（ファイルに保存）
		json.dump(json_data, f)

	# ファイルの読み込み
	with open(f"{tmpdir}{os.sep}data.json", "r") as f:
		# JSON化を復元
		deserialized = json.load(f)

	# 元のデータと内容は一致
	assert deserialized == json_data


@pytest.mark.unsolved
def test_should_encode_by_own_definition(json_data):
	"""
	独自の型変換を定義
	"""

	class MyJSONEncoder(json.JSONEncoder):
		""" JSONEncoderをカスタマイズする """

		# オーバーライド
		# エンコード時に呼ばれる
		def default(self, obj):
			# datetime型の場合
			if isinstance(obj, datetime.datetime):
				# 文字列にして返す
				return str(obj)
			# デフォルトの変換
			else:
				return super().default(obj)

	# 注意: Listのappend()は返り値はNoneであり、元のListを変更する
	# json_data = json_data.append()のようにしてはいけない
	json_data.append({
		"x": 1,
		"updated": datetime.datetime.now(), # "updated": datetime.datetime(2023, 5, 26, 22, 46, 48, 823...)
	})

	# 拡張エンコーダーを指定してJSON形式へ変換
	json_str = json.dumps(json_data, cls=MyJSONEncoder) # "updated": "2023-05-26 22:46:48.823..."

	# Pythonオブジェクトへ変換
	serialized = json.loads(json_str)

	assert serialized != json_data


# cf: https://docs.python.org/ja/3/library/abc.html
from abc import ABC
from abc import ABCMeta
from abc import abstractmethod


# 抽象基底クラス
# Javaでは抽象メソッドを１つでも含むクラス(継承して潜んでいる場合も含む)は、
# 必ずabstract付きのクラスにしなければならない
# 抽象クラスはnewによるインスタンス化が禁止されるのでエラーが発生する
class Character(ABC): # または、Character(metaclass=ABCMeta)

	"""  """

	def __init__(self, name: str):
		self.__name: str = name
		self.__hp: int

	def run(self):
		print(f"{self.__name}は逃げ出した！")

	# 抽象メソッド
	@abstractmethod
	def attack(self):
		pass

	@property
	def name(self):
		return self.__name	

	@property
	def hp(self):
		return self.__hp

	@name.setter
	def name(self, value):
		self.__name = value 

	@hp.setter
	def hp(self, value):
		self.__hp = value 


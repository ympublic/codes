import axios from "axios";
const ENDPOINT_URL = "http://localhost:3003/todo";

const todoApi = {
  // GET
  async getAll() {
    const result = await axios.get(ENDPOINT_URL);
    console.log(result);
    return result.data;
  },
  // POST
  async post(todo) {
    // 第２引数に指定したtodoがJSONの文字列に変換されて、サーバーに対してリクエストとして送信される
    const result = await axios.post(ENDPOINT_URL, todo);
    console.log(result);
    return result.data;
  },
  // DELETE
  async delete() {
    const result = await axios.delete(ENDPOINT_URL + "/" + todo.id);
    return result.data;
  },
  // PUT
  async patch(todo) {
    const result = await axios.put(ENDPOINT_URL + "/" + todo.id, todo);
    return result.data;
  },
};

todoApi.getAll();

// Reactのホットリロード機能を有しているので、当ファイルを保存するだけで以下のデータも登録される
// todoApi.post({
//   id: 12344,
//   content: "my-test",
// });

// todoApi.delete({
//   id: 12344,
// });

// todoApi.patch({
//   id: "f2c38014-e2df-40ae-ac93-36303b8771ce",
//   content: "買い物 with daughter",
//   editing: false,
//   completed: false,
// });

export default todoApi;

from rest_framework import serializers
from drf.models import MusicCategory, MusicItem

# Restful API
# セッション管理を行わない
# HTTPメソッドでリソース(データ)を操作
# 汎用的な構文
# ハイパーメディアによる情報表現

# Django Rest Frameworkの設定４ステップ
# 1. Model / models.py
# 2. Serializer / serializers.py
# 3. ViewSets / views.py または 作成したapi-views.py
# 4. Router / urls.py または 作成したapi-urls.py

# Serializer: モデルをJSON形式に変換するためのもの
# Serializerクラス: データベースから取り出したモデルのオブジェクトをJSONにserializeしたりdeserializeするためのクラス
class CategorySerializer(serializers.ModelSerializer):
	class Meta:
		model = MusicCategory
		fields = ('pk', 'title',) # JSONで出力するフィールド


class ItemSerializer(serializers.ModelSerializer):
	class Meta:
		model = MusicItem # 呼び出すモデル
		fields = ('pk', 'title', 'category',) # API上に表示するモデルのデータ項目


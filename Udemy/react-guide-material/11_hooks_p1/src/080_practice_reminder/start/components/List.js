import Item from "./Item";
import { useTodos } from "../context/TodoContext";

// グローバルstateを使用しているので、引数にpropsは不要
const List = () => {
  // グローバルstateを使用
  const todos = useTodos();

  return (
    <div>
      {todos.map((todo) => (
        <Item todo={todo} key={todo.id} />
      ))}
    </div>
  );
};

export default List;

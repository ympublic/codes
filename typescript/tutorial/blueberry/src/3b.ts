/**
 * コンパイルと実行
 * npx tsc
 * node dist/3b.js
 *
 */

// 注意！ 3.tsでAnimal型を定義している為、このファイルではAnimal型を定義できない
type Animal2 = {
  age: number;
};

// Animalの部分型
type Human2 = {
  age: number;
  name: string;
};

const u: Human2 = {
  name: "John Lennon",
  age: 40,
};

// ジェネリック型
type User2<T> = {
  name: string;
  child: T;
};

const vocal: User2<string> = {
  name: "John Lennon",
  child: "Seon Lennon",
};

console.log(vocal.child);

// readonly配列型
const arr: readonly number[] = [1, 10, 100];

for (let n of arr) {
  console.log(n);
}

// タプル型
let tuple: [string, number] = ["foo", 23];
tuple = ["abcd", 33];
console.log(tuple);

// Map型<Key, Value>
const map: Map<string, number> = new Map();
// 新しいキーペアを追加
map.set("foo", 23);
console.log(map.get("foo"));
console.log(map.get("bar")); // undefined
// fooを削除
map.delete("foo");
console.log(map.get("foo")); //undefined

// 力試し
console.log("☆☆☆  力試し  ☆☆☆");

type MyUser = {
  name: string;
  age: number;
  premiumUser: boolean;
};

// CVSデータを想定
const data: string = `
john, 26, 1
Paul, 24, 0
Ringo, 27, 0
George, 25, 1
`;

const users: MyUser[] = [];

const member = data.split("\n"); // [0]と[-1]は空文字

for (const m of member) {
  if (m === "") {
    continue;
  }
  // undefinedの可能性があるとErrorが出たので、default値を設定した
  const [name = "Tom", ageString = "??", premiumUserString = "0"]: string[] =
    m.split(",");
  const age = Number(ageString.trim());
  const premiumUser = premiumUserString.trim() === "1";

  users.push({
    name,
    age,
    premiumUser,
  });
}

console.log(users);

for (const user of users) {
  if (user.premiumUser) {
    console.log("トレビア～ン");
  } else {
    console.log("無課金ユーザーがッ！");
  }
}

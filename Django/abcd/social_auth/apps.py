# ユーザーがアプリに アプリケーション構成 を含めるのを支援するために自動で作成される
# これを使用して、アプリケーションの属性の一部を構成できる
# 注意： settings.pyのINSTALLED_APPSに追記しなければ読み込まれない。または__init__.pyに登録

from django.apps import AppConfig

class SocialAuthConfig(AppConfig): # ready()で読み込ませる
    """  """

    name = "social_auth" # App名
    # verbose_name= ""

    # 自動的に連番が振られるInterField
    default_auto_field = "django.db.models.BigAutoField" # 普通にAutoFieldでも良いとは思うが 

    # https://docs.djangoproject.com/ja/4.1/ref/applications/#methods
    def ready(self): # Django起動時(つまりアプリケーションロード後)に一度だけ実行したい初期化処理
        # super().ready()
        # print("★★★ SocialAuthConfig is running! ★★★")
        import social_auth.signals # signals.pyを読み込ませる

    # モデルにアクセスしたい場合
    # def get_model(model_name): pass


import os
import sys
import pdb # pdb.set_trace() or breakpoint()
from datetime import datetime
from unittest import mock

# Django
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponseRedirect
from django.test import TestCase, RequestFactory, Client
from django.urls import resolve, reverse, reverse_lazy
from django.test import tag
from django.template import Template, Context
from django.shortcuts import get_object_or_404
from django.db import transaction
from django.db import models
from django.db import IntegrityError
from django.db import DatabaseError
from django.core.exceptions import ValidationError
from django.test.utils import override_settings # @override_settings()
from django.conf import settings

# Applicaion
#from drf.factories import DrfFactory # Fake

UserModel = get_user_model()


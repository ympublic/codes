/*
 * クイックソート
 * 適当な基準値（軸）を決め、データと基準値を比較し、基準値より大きなデータ列と小さなデータ列に振り分け、
 * 分割することを繰り返し、データを整列する
 * ランダムなデータ列を高速に整列する
 * ソートユーティリティの殆どが、このクイックソートを用いている
 * 軸の決め方には色々な方法があるが、ここでは単純にデータ列の中央にあるデータにする
 * データを照準に並べ替えた時に中央にくるデータを軸にすれば、効率の良い整列ができる
 * 
 */

"use strict";

let data = new Array();
let start;
let end; 

data = [70, 60, 40, 80, 30, 10, 50, 20];
start = 0; // 始を軸と設定
end = data.length - 1;

function numDiv(start, end) {
	let pivot = data[start]; // 軸
	/*
	 * 書籍の流れ図ではleft=start-1; right=end+1としているため、
	 * do{}while();の後型終了条件となっている
	 * そのため、do {right -= 1;} while (data[right] > pivot);とする
	 * しかしstartが-1となってしまうため、まるで異なる結果となってしまう
	 * 
	 */
	let left = start;
	let right = end; 
	let work;
	console.log("pivot: " + pivot);

	// 前判定型の繰り返し
	while (left < right) { // 前判定型の終了条件(左>=右)
		while (data[left] < pivot){
			left++;
		}
		while (data[right] > pivot) {
			right -= 1;
		}
		// 交換(データ(左), データ(右))
		console.log("data: " + data);
		console.log("data[left]: " + data[left]);
		console.log("data[right]: " + data[right]);
		work = data[left];
		data[left] = data[right];
		data[right] = work;
	}
	return right;
}

// 軸未満のデータを左に、軸以上のデータを右に振り分ける
function quickSort(start, end) {
	let c; // varなどを付けなければグローバル関数
	// if(始 < 終){}
	if (start < end) {
		c = numDiv(start, end); 
		console.log("c: " + c); // 5, 1, 0, 3, 2, 4, 5
		// if(始 < 中){}
		if (start < c) {
			quickSort(start, c);
		}
		// if(中+1 < 終){}
		if (c + 1 < end) {
			quickSort(c + 1, end);
		}
	}
}


quickSort(start, end);
console.log(data); // Array(8) [ 10, 20, 30, 40, 50, 60, 70, 80 ]


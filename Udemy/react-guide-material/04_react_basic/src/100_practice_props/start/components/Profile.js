import "./Profile.css";

const Profile = (props) => {
  const { name = "John Doe", age = "??", country = "Japan" } = props;
  return (
    <div className="profile">
      <h3>{"Name: " + name}</h3>
      <p>{"Age: " + age}</p>
      <p>{"Country: " + country}</p>
    </div>
  );
};

export default Profile;

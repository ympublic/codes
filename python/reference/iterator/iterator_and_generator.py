#! /usr/bin/env python

import pytest
from typing import Generator # Generator[YieldType, SendType, ReturnType]


"""イテレータ
nextメソッドを実装するオブジェクト
一般にデータを走査するポインタ的な役割を担う
大きなデータをすべてロードしてforで回すより効率的になる
"""

@pytest.mark.solved
def test_should_work_as_iterator():
	""" イテレータの挙動を確認 """

	# イテレータの作成
	fruits = iter(["Apple", "Banana", "Orange"])

	# 注意
	# イテレータに対してリストのスライスは使えない
	# fruits[:2] # TypeError

	# type(fruits) # <class 'list_iterator'>

	assert "__iter__" in dir(fruits)  # __iter__メソッドがあるか
	
	assert "__next__" in dir(fruits)  # __next__メソッドがあるか

	# イテレータを作成したら、nextメソッドを呼び出して、
	# シーケンスを走査し各要素を取り出していく
	assert next(fruits) == "Apple"

	assert fruits.__next__() == "Banana"

	assert next(fruits) == "Orange"

	# 走査終了時にはStopIteration Error
	# next(fruit) # StopIteration Error

	try:
		while True:
			assert next(fruits) in fruits
	except StopIteration as e:
		print("走査が終了しました")

	# forでも回せる
	# しかし、２回目は空となる
	for f in fruits:
		assert f in fruits


"""ジェネレーター
関数に結びつく広義のイテレータ
イテレータを進めるまで、関数内のローカル変数を保持したまま、
次のシーケンス要素生成までの評価が中断される
これにより、遅延評価も可能になる
"""


@pytest.mark.solved
def test_should_work_as_generator():
	""" ジェネレーターの挙動を確認 """

	# ジェネレーター関数
	def gen() -> Generator[int, None, None]:
		# 遅延実行
		# ジェネレータ関数を作った時点では要素数が決まっていない
		# 他から要素を与えられてはじめて確定する
		yield 1 # YieldType: int
		yield 2
		yield 3 # 関数外（例えばfor文内）でyieldを書くとSyntax Errorが発生

	for e in gen():
		assert e in (1, 2, 3)


	# ジェネレーター関数
	def get_primes() -> Generator[int, None, None]:
		""" yield文により素数を返す関数 """

		n = 2 # number
		while True:
			i = 2 # i=2の場合はそのまま返る
			while i < n: # 素数は1とその数自身でしか割り切れない数
				if n % i == 0:
					break
				i += 1

			if i == n:
				yield n # 素数を返す

			n += 1

	# ジェネレーターを作成
	primes = get_primes()

	# type(primes) # <class 'generator'>

	assert "__iter__" in dir(primes)  # __iter__メソッドがあるか
	
	assert "__next__" in dir(primes)  # __next__メソッドがあるか

	assert [next(primes) for i in range(8)] == [2, 3, 5, 7, 11, 13, 17, 19]


@pytest.mark.solved
def test_should_work_as_list_generator():
	""" タプル内包型ジェネレーターの作成 """

	# tupleで内包表記を囲むとジェネレーターとなる
	gen = (i * i for i in range(10))

	assert next(gen) == 0

	assert next(gen) == 1

	assert next(gen) == 4


@pytest.mark.solved
def test_should_use_send():
	""" sendメソッドの利用 """

	# ジェネレーター関数
	def func() -> Generator[int, int, None]:
		# yield文では値を返すだけではなく、イテレータを進める時に
		# データを受け取ることも出来る
		recieved = (yield 3) # 3を返し、sendメソッドの値を受け取る
		print(f"recieved: {recieved}")

	# ジェネレーター作成
	gen = func()

	# イテレーターを進める
	assert next(gen) == 3

	# ジェネレーターに値を渡してイテレータを進める
	# 注意: send()はnext()を１回呼び出した後でなければ使えない
	gen.send(8) # SendType: int

	# つまりyield 8となる
	assert next(gen) == 8


@pytest.mark.solved
def test_should_use_send():
	"""  """

	# TODO: 本当はrandomで最初のカードを渡すべき

	# ジェネレーター関数
	def get_player(name: str, cards: int) -> Generator[int, int, None]:
		"""  """

		turn = 0

		while True:

			print(f"{turn}ターンめ")
		
			if turn == 0:
				card = yield # カードを受け取るまで待機
			else:
				card = (yield cards.pop(0)) # send()によりカードを渡して受け取る
		
			cards.append(card)
		
			print(f"{name} got {card}: {cards}")
		
			turn += 1

	# ジェネレーター作成
	player1 = get_player("player1", [1, 2, 3])
	player2 = get_player("player2", [4, 5, 6])

	# カードを受け取る準備をする
	next(player1) # turn == 0
	next(player2)

	# 準備完了後、最初に渡されるカード
	card = 7

	for turn in range(2):
		# send()によりカードを受け取り、popされたカードを渡す
		card = player1.send(card) # pop()で取り出すが、send()でyield部に値を渡す
		card = player2.send(card)

	# player1 got 7: [1, 2, 3, 7]
	# player2 got 1: [4, 5, 6, 1]
	# player1 got 4: [2, 3, 7, 4]
	# player2 got 2: [5, 6, 1, 2]

	assert 5 == card


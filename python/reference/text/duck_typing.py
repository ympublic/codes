#! /usr/bin/env python
""" オブジェクトが文字列のようなものかテストする """

def isString(anobj):
	# type(anobj) is type("")のようなアプローチは、
	# ポリモーフィズムを破壊してしまうのでダメ

	# 型チェック: Duck Typingを使わない場合
	return isinstance(anobj, str)


def isStringLike(anobj):
	try:
		print("処理を開始します")
		# Duck Typing: ex) if obj.user_id
		anobj + ""
	except: # 異常が発生した際の処理
		print("異常が発生しました")
		return False
	else: # 正常終了時の処理
		print("正常に終了しました")
		return True
	finally: # 処理終了後に必ず行う処理
		print("処理が終了しました")


def main():
	# 文字列
	anobj = "Hello World!"
	print("isString: {}\n".format(isString(anobj)))
	print("isStringLike: {}\n".format(isStringLike(anobj)))

	# バイナリ
	anobj_b = b"\\xe3\\x81\\x82"
	print("isString: {}\n".format(isString(anobj_b)))
	print("isStringLike: {}\n".format(isStringLike(anobj_b)))


if __name__ == "__main__":
	main()


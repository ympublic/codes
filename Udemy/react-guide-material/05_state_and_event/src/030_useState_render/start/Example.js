import { useState } from "react";

const Example = () => {
  const [val, setVal] = useState();
  console.log("再レンダリングされました");
  return (
    <>
      {/** onChangeが実行される度に再レンダリングが実行される */}
      <input
        type="text"
        onChange={(e) => {
          console.log(e.target.value);
          setVal(e.target.value);
          // displayVal = e.target.value;
        }}
      />
      = {val}
    </>
  );
};

export default Example;

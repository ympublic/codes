from abc import ABC # Abstract Base Class
from abc import ABCMeta
from abc import abstractmethod
from typing import Final

class Monster(ABC):

	"""  """

	def __init__(self):
		self.__species: str # = "モンスター"
		self.__suffix: str # = suffix
		self.__hp: int
		self.__level: int

	@abstractmethod
	def run(self):
		print(f"モンスターはスタコラサッサと逃げ出した！")

	@property
	def species(self):
		return self.__species

	@property
	def suffix(self):
		return self.__suffix

	@suffix.setter
	def suffix(self, value):
		self.__suffix = value

	@property
	def hp(self):
		return self.__hp

	@property
	def level(self):
		return self.__level

	@species.setter
	def species(self, value):
		self.__species = value

	@hp.setter
	def hp(self, value):
		self.__hp = value

	@level.setter
	def level(self, value):
		self.__level = value


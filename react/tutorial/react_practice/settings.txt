# node version
node -v

# TypeScript version
npx tsc -v

# yarn install
npm install -g yarn

# yarn version 
npm yarn -v

# 'react-scripts' は、内部コマンドまたは外部コマンド、 
# 操作可能なプログラムまたはバッチファイルとして認識されていません。
# とエラーが出たら、
npm install react-scripts --save

■ブラウザ用拡張機能
・React Developer Tools
https://addons.mozilla.org/en-US/firefox/addon/react-devtools/


##################
###  Chapter4  ###
##################

# Reactプロジェクト"my_practice"を作成
# またはhttps://typescriptbook.jp/tutorials/react-like-button-tutorialのようにyarnを使う
npx create-react-app my_practice

# my_practiceへ移動
cd my_practice

# ReactのVersionを確認
npm list --depth=0

# ローカル環境でreactを立ち上げる
npm start

# srcフォルダを削除しindex.jsを作成
rm -rf src && mkdir src && touch src/index.js

# コンポーネントファイルApp.jsxを作成
touch src/App.jsx

# コンポーネント用のディレクトリを作成
mkdir src/components

# コンポーネントを作成
touch src/components/ColoredMessage.jsx


##################
###  Chapter5  ###
##################

# sassのインストール
npm install sass 
# yarn add sass

# styled-jsxのインストール
npm install styled-jsx 
# yarn add styled-jsx

# styled-componentsのインストール
npm install styled-components@5.3.10 @types/styled-components
# yarn add styled-components

# Tailwindのインストール
npm install -D tailwindcss postcss autoprefixer 
# yarn add -D tailwindcss postcss autoprefixer 

# Emotionのインストール
npm install @emotion/react @emotion/styled 
# yarn add @emotion/react @emotion/styled 

# コンポーネントの作成
touch src/components/CssModules.jsx

# コンポーネントと対になる形でCSSファイルを作成
touch src/components/CssModules.module.scss

# Styled-JSXコンポーネントを作成
touch src/components/StyledJsx.jsx 

# Styled Componentsを作成
touch src/components/StyledComponents.jsx 

# Emotionコンポーネントを作成
touch src/components/Emotion.jsx 

# プロジェクトルート上にて、Tailwind CSSの準備
npx tailwindcss init -p

# index.cssを作成
touch src/index.css

vi tailwind.config.js
# content: ["./src**/*.{{js,jsx,ts,tsx}"], を追加

vi index.css 
# @tailwind base;
# @tailwind components;
# @tailwind utilities;

vi index.tsx 
# import './index.css'; を追記


##################
###  Chapter8  ###
##################

# TypeScriptにてReactプロジェクト"ts-app"を作成
npx create-react-app ts-app --template typescript

# Axiosインストール
npm install axios


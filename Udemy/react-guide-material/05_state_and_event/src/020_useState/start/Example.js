import { useState } from "react";

const Example = () => {
  // Stateの定義
  const [val, setVal] = useState();
  return (
    <>
      <input
        type="text"
        onChange={(e) => {
          setVal(e.target.value);
        }}
      />
      = {val}
    </>
  );
};

export default Example;

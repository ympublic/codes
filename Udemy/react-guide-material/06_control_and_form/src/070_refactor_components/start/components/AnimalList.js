import AnimalItems from "./AnimalItems";

const AnimalList = (props) => {
  const { animals } = props;
  if (animals.length === 0) {
    return <h3>アニマルが見つかりません</h3>;
  }
  return (
    <ul>
      {animals.map((animal) => {
        return <AnimalItems animal={animal} key={animal} />;
      })}
    </ul>
  );
};

export default AnimalList;

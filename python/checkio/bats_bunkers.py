import itertools
import heapq

def range_float(start, stop, step):
    while start < stop:
        yield start
        start += step

def checkio(bunker):
    def get_length(a, b):
        return ((a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2) ** 0.5

    def get_cells_on_line(a, b):
        (x1, y1), (x2, y2) = a, b
        result = set()
        delta = max(abs(x2 - x1), abs(y2 - y1))
        for i in range_float(0.5, delta, 0.5):
            x = x1 + (x2 - x1) * i / delta
            y = y1 + (y2 - y1) * i / delta
            xs = [int(x), int(x) + 1] if x % 1.0 == 0.5 else [round(x)]
            ys = [int(y), int(y) + 1] if y % 1.0 == 0.5 else [round(y)]
            result |= set(itertools.product(xs, ys))
        return result - {a, b}

    def is_connected(a, b):
        return all(map_data[pt] not in 'W' for pt in get_cells_on_line(a, b))

    map_data = {(x, y): c for y, line in enumerate(bunker) for x, c in enumerate(line)}
    bats = {k for k, v in map_data.items() if v in 'AB'}

    heap = [(0, (0, 0), {(0, 0)})]  # (length, pos, visited)
    while heap:
        length, pos, visited = heapq.heappop(heap)
        if map_data[pos] == 'A':
            return round(length, 2)
        for bat in bats - visited:
            if is_connected(pos, bat):
                new_length = length + get_length(pos, bat)
                heapq.heappush(heap, (new_length, bat, visited | {bat}))


#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    def almost_equal(checked, correct, significant_digits=2):
        precision = 0.1 ** significant_digits
        return correct - precision < checked < correct + precision

    assert almost_equal(checkio([
        "B--",
        "---",
        "--A"]), 2.83), "1st example"
    assert almost_equal(checkio([
        "B-B",
        "BW-",
        "-BA"]), 4), "2nd example"
    assert almost_equal(checkio([
        "BWB--B",
        "-W-WW-",
        "B-BWAB"]), 12), "3rd example"
    assert almost_equal(checkio([
        "B---B-",
        "-WWW-B",
        "-WA--B",
        "-W-B--",
        "-WWW-B",
        "B-BWB-"]), 9.24), "4th example"
import React, { useState } from 'react';
import logo from './logo.svg';
// CSSが適用されない場合は以下のimportが書かれているか確認
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          <LikeButton />
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

function LikeButton() {
  const [count, setCount] = useState(999);
  const handleClick = () => {
    setCount(count + 1);

  };
  return (
    <span className="likeButton" onClick={handleClick}>
      ♥{count}
    </span>
  );
}

export default App;

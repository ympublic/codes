#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import gc
import pytest # pytest -v -m "unsolved and not solved" test_openpyxl.py
import openpyxl
from openpyxl.cell.cell import Cell
from openpyxl.styles import Font
from openpyxl.chart import BarChart, Series, Reference


# フィクスチャ定義
@pytest.fixture(scope="function")
def open_excel_file(): 
	"""  前処理にてディレクトリとファイルを作成し、後処理にてディレクトリを削除するフィクスチャ """
	
	# GIVEN(前提): 
	# WHEN(こうしたら): 
	# THEN(こうなる):

	# pytest --setup-showでフィクスチャの実行タイミングを確認できる
	# 前処理部分(setUp) 
	try:
		# ワークブックを読みこむ
		wb = openpyxl.load_workbook("openpyxl_test.xlsx")

	except Exception as e: # 基底クラスException
		print(e, file=sys.stderr)

	else: # 正常終了時の処理
		print("正常にPDFファイルを開きました")

	finally: # 終了時に常に行う処理
		print("処理が終了しました")

	# yield前がsetUp()、yield後がtearDown()となる
	yield wb # return

	# 後処理部分(tearDown)
	# ワークブックを閉じる
	wb.close()


@pytest.mark.solved
def test_should_return_sheetnames_with_fixture():
	"""  """

	# ワークブック（Excelファイル）を読み込む
	# 注意: with openpyxl.load_workbook("openpyxl_test.xlsx") as wb:はAttributeError: __enter__
	wb = openpyxl.load_workbook("openpyxl_test.xlsx")

	# ワークシートの選択
	ws = wb['Sheet1']  # ワークシートを指定
	# ws = wb.active # アクティブなワークシートを選択
	# Sheet名を確認
	assert "Sheet1" == ws.title

	# Sheet名を変更
	ws.title = "New Sheet"	
	# wb.sheetnamesはListで取得される
	assert "New Sheet" in wb.sheetnames

	wb.close()
	

# 上記テストをフィクスチャを使用して書き直した場合
@pytest.mark.solved
# フィクスチャを使用する場合は、引数に上記のフィクスチャ関数を指定する
def test_should_return_sheetnames(open_excel_file):
	"""  """

	# ワークブックを開く
	# open_excel_fileでwbを返す。最後に()は付ける必要はない
	wb = open_excel_file
	# ワークシートの選択
	ws = wb['Sheet1']  # ワークシートを指定
	# Sheet名を確認
	assert "Sheet1" == ws.title

	# 参考
	# Djangoなどでstreamとしてファイルに保存する場合
	#with NamedTemporaryFile() as tmp:
	#	wb.save(tmp.name)
	#	tmp.seek(0)
	#	stream = tmp.read()


# openpyxlは取得したセルの内容が「数式」となる
# 対して、xlrdの場合は取得したセルの内容は「計算結果」となる
# もし「計算結果」が必要な場合は、load_workbook("file.xlsx", data_only=True)を指定する
@pytest.mark.solved
def test_should_return_formula():
	"""  """

	wb = openpyxl.load_workbook("openpyxl_test.xlsx")
	# Sheet2を取得
	sheet2 = wb["Sheet2"]
	# Sheet2のセル（A1）の値を取得
	assert "=1+20" == sheet2["A1"].value

	# Sheet2のcol=3, row=2のセルの値を取得(つまりC2)
	# openpyxlは行と列が1から始まる
	assert "=3+30" == sheet2.cell(column=3, row=2).value
	wb.close()


# 上記のテストを計算結果で取得し直し
# もし「計算結果」が必要な場合は、load_workbook("file.xlsx", data_only=True)を指定する
@pytest.mark.solved
def test_should_return_results_of_calculate():
	"""  """

	# 注意: 以下のdata_onlyは、例えばtest_should_create_a_graph()内のように
	# openpyxlで作成したデータをsave()した場合、再度自分でExcelファイルを
	# 上書き保存しないと動作しない

	# 参考
	# VBAを保持したままワークブックを開くには、以下のようにする
	# openpyxl.load_workbook(ファイル名, keep_vba=True)

	# ワークブックを開く
	wb = openpyxl.load_workbook("openpyxl_test.xlsx", data_only=True)
	# Sheet2を取得
	sheet2 = wb["Sheet2"]
	# Sheet2のセル（A1）の値を取得
	assert 21 == sheet2["A1"].value

	# Sheet2のcol=3, row=2のセルの値を取得(つまりC2)
	# openpyxlは行と列が1から始まる
	assert 33 == sheet2.cell(column=3, row=2).value
	wb.close()


@pytest.mark.solved
def test_should_add_and_delete_sheet():
	"""  """

	wb = openpyxl.load_workbook("openpyxl_test.xlsx")
	# 最後尾に"Added_Sheet"という名のSheetを追加
	wb.create_sheet("Added_Sheet", len(wb.sheetnames)) 
	assert 3 == len(wb.sheetnames)
	assert "Added_Sheet" in wb.sheetnames

	# "Added_Sheet"を削除
	wb.remove(wb["Added_Sheet"])
	assert "Added_Sheet" not in wb.sheetnames
	assert 2 == len(wb.sheetnames)

	# ワークシートをコピー
	# wb.copy_worksheet(wc["Sheet1"])

	# ワークブックを別名でsaveする場合
	# wb.save("openpyxl_test2.xlsx")

	wb.close()


@pytest.mark.solved
def test_should_return_added_values():
	"""  """

	wb = openpyxl.load_workbook("openpyxl_test.xlsx")
	# ワークシートの選択
	ws = wb["Sheet2"]
	# セル(C5)に値を書き込む
	ws["C5"] = "Added_Value"
	# または
	# ws.cell(column=3, row=5).value = "Added_Value"
	assert "Added_Value" == ws.cell(column=3, row=5).value

	# フォントをboldに
	ws["C5"].font = Font(bold=True)
	# セルの書式設定
	# ws["C5"].number_format = "0.00"
	# 日付データの書式設定
	# ws["C5"].number_format = "yyyy/mm/dd"

	# セル範囲の取得
	r_cells = ws['A1':'D10']

	# 値を取得
	# 行列で取得
	matrix_values = [[col.value for col in row] for row in r_cells]
	# Listで取得
	values = [col.value for row in r_cells for col in row]
	assert "Added_Value" in values

	wb.close()


@pytest.mark.solved
def test_should_create_new_workbook():
	"""  """

	# 新規作成
	wb = openpyxl.Workbook()
	wb.save("new_book.xlsx")
	wb.close()

	# ファイルが作成できたか確認
	assert os.path.isfile("./new_book.xlsx")
	assert "./new_book.xlsx" in os.listdir()

	# ファイルを削除
	os.remove("./new_book.xlsx")

	# ファイルが削除できたか確認
	assert not os.path.isfile("./new_book.xlsx")
	assert "./new_book.xlsx" not in os.listdir()


@pytest.mark.solved
def test_should_return_sum_of_cells(open_excel_file):
	"""  """

	# ワークブックを開く
	wb = open_excel_file
	# ワークシートを選択
	ws = wb["Sheet1"]
	# セル範囲の取得
	r_cells = ws['A1':'J1'] # 二層構造の行列となっている
	# セル範囲から取得した数値を取得し合計
	assert 55 == sum(col.value for row in r_cells for col in row)


@pytest.mark.solved
def test_should_create_a_graph(open_excel_file):
	"""  """

	# ワークブックを開く
	wb = open_excel_file
	# ワークシートを選択
	ws = wb["TreeHeight"]
	# バーチャート
	chart = BarChart()
	chart.type = "col"
	chart.title = "Tree Height"
	chart.y_axis.title = "Height(cm)"
	chart.x_axis.title = "Tree Type"
	chart.legend = None
	# ["C2":"C4"]
	data = Reference(ws, min_col=3, min_row=2, max_col=3, max_row=4)
	# ["A2":"A4"]
	categories = Reference(ws, min_col=1, min_row=2, max_col=1, max_row=4)
	chart.add_data(data)
	chart.set_categories(categories)
	# セル"E1"にチャートを描画
	ws.add_chart(chart, "E1")
	wb.save("openpyxl_test.xlsx")


# メモリ解放
gc.collect()


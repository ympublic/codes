import { useUpdateTheme } from "../context/ThemeContext";

const Footer = () => {
  // コンテキストを読み込む（分割代入）
  const setTheme = useUpdateTheme();

  console.log("footer");

  return (
    <footer>
      <span>フッター</span>
    </footer>
  );
};

export default Footer;

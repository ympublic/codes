import "./Child.css";
import { Fragment } from "react";

const Child = () => {
  return (
    <Fragment>
      <div className="component">
        <h3>Hello Component</h3>
      </div>
      <h3>Hello Fragment</h3>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi earum
        expedita, eaque aut harum, nam alias impedit, doloribus odit
        reprehenderit tempore ipsa aspernatur deserunt ratione repudiandae fugit
        dignissimos! Aut, quos.
      </p>
    </Fragment>
  );
};

export default Child;

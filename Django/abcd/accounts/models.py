import os
import uuid
# Django
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import UserManager
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.core.mail import send_mail
from django.utils import timezone
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.utils.translation import gettext_lazy as _
from django.dispatch import receiver # Signals
from django.db.models.signals import post_save
# Third Party
from django_cleanup import cleanup
from sorl.thumbnail import get_thumbnail, delete


####################################################
###   Custumized UserManager
####################################################
# class MyUser()の上に記述する
class MyUserManager(BaseUserManager):
    # use_in_migrations = True

    # _create_user, create_user, create_superuserの３つがある
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            # username=self.normalize_username(username),
        )

        user.set_password(password) # 残りのフィールドの値はデフォルトのFalseが適用される
        user.save(using=self._db)
        return user

    # def _create_user(self, email, password, **extra_fields): pass

    def create_superuser(self, email, password=None, **extra_fields):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.is_admin = True # extra_fields.setdefault('is_admin', True)
        user.is_staff = True # extra_fields.setdefault('is_staff', True)
        user.save(using=self._db)
        return user

    # def with_perm(): pass


class MyUser(AbstractBaseUser, PermissionsMixin):
    # デフォルトから何も変更しない場合
    # pass

    # デフォルトから変更する場合
    username_validator = UnicodeUsernameValidator()

    ''' usernameは省略する
    username = models.CharField( # デフォルトで必須フィールド
    	_("username"), 
    	max_length=50, 
    	validators=[username_validator], 
    	unique=True,
    	error_messages={
    		'unique': _('A user with that username already exists.'),
    	},
    )
    '''
    # request.user.emailで取得できる
    email = models.EmailField( # デフォルトで必須フィールド
    	_("email_address"), 
    	unique=True,
        max_length=100,
        # db_index=True # Indexをはる
    )
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(_("staff status"), default=False) # デフォルトで必須フィールド
    is_active = models.BooleanField(_("active"), default=True)
    is_verified = models.BooleanField(_("verified"), default=False) # 認証済みかどうか
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    # note = models.TextField(_("annotation"), blank=True, null=True,)

    objects = MyUserManager() # カスタム化したUserManagerを使用

    USERNAME_FIELD = "email" # メールアドレスでのログイン方法となる。unique=Trueである必要あり。ユーザーを一意に識別するためのフィールドを指定する
    EMAIL_FIELD = "email" 
    REQUIRED_FIELDS = [] # createsuperuserコマンド実行時に入力受付を行うフィールド名。この場合emailは記述するとError

    class Meta:
        verbose_name = _("user") # 管理画面でのモデル名を指定
        verbose_name_plural = _("users") # 最後にsが付かないようにする
        db_table = 'users' # テーブル名変更（注：MySQLでは小文字のテーブル名を使うこと！！）
        swappable = 'AUTH_USER_MODEL' # settingsファイルのの変数名を指定

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email) # emailをlowecaseで正規化

    def email_user(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def __str__(self):
        return str(self.email)

    def has_perm(self, perm, obj=None): # 特定の権限を所有しているか
        return True

    def has_module_perms(self, app_label): # アプリのModelに接続する権限があるか
        return True

    @property
    def is_staff(self): # Djangoの管理画面に入れる
        return self.is_admin


# 重要！
# 上記を設定後、settings.py内に、
# AUTH_USER_MODEL = "accounts.MyUser"
# を指定する

####################################################
###   Email Confirm
####################################################

class EmailConfirm(models.Model):
    """ ユーザー登録時にメールの認証確認で使う数字 """
    user = models.OneToOneField( # OneToOneFieldはForeignKey(unique=True)の様なもの
        settings.AUTH_USER_MODEL, 
        verbose_name='ユーザー', 
        on_delete=models.CASCADE, blank=False,)
    number = models.PositiveSmallIntegerField('数字')
    expire = models.DateTimeField('期限')

    class Meta:
        db_table = 'email_confirm'
        verbose_name = 'メール認証'
        verbose_name_plural = 'メール認証'

    def __str__(self):
        return str(self.user.id) # str()で括らないとエラーが発生


####################################################
###   Profile
####################################################

# Signal
#@receiver([post_save,], sender=MyUser)
#def create_profile(sender, **kwargs):
    """ 新規ユーザー登録時に、空のProfileを作成 """
#    if kwargs['created']:
#        profile = Profile.objects.get_or_create(user=kwargs['instance'])

# 参考: username, first_name, last_nameは元々Userクラスが継承しているAbstractUserクラス内に存在している
class Profile(models.Model):
    """ マイページ """
    GENDER = (
        ('N', ''), # デフォルトの"---------"をオーバーライド
        ('M', '男'),
        ('W', '女'),
        ('M', 'どちらでもない')
    )

    user = models.OneToOneField(settings.AUTH_USER_MODEL, # myuserにすべきだったか？
        verbose_name='ユーザー', 
        on_delete=models.CASCADE, blank=False,)
    nickname = models.CharField('ニックネーム', max_length=32,)
    family_name = models.CharField('苗字', max_length=32,) # last_name
    given_name = models.CharField('名前', max_length=32,) # first_name
    gender = models.CharField("性別", max_length=1, choices=GENDER, blank=True, default='N')
    birth_year = models.CharField('年', max_length=4, blank=True,) # まとめてDateFieldにすべき？
    birth_month = models.CharField('月', max_length=2, blank=True,)
    birth_date = models.CharField('日', max_length=2, blank=True,)
    zip_code = models.CharField('郵便番号', max_length=8, blank=True,) # 海外の郵便番号は数字以外もあるかもしれないので
    country = models.CharField('国', max_length=2, blank=True,) # ISO 3166-1の国コード。2桁と3桁がある
    prefecture = models.CharField('県', max_length=32, blank=True,) # 都道府県コードを使ってTINYINT型にしたかったが、海外のことを考えるとVARCHAR型かな、と
    address1 = models.CharField('住所１', max_length=128, blank=True,) # 番地まで
    address2 = models.CharField('住所２', max_length=128, blank=True,) # 建物名など
    phone = models.CharField('電話番号', max_length=13, blank=True,)
    note = models.TextField("注釈", blank=True, null=True,)

    class Meta:
        db_table = 'profile'
        verbose_name = 'プロフィール'
        verbose_name_plural = 'プロフィール'
        indexes = [ # セカンダリインデックスを設定
            # INDEX復習
            # =, >, >=, <, <=, BETWEEN演算子や最初の文字がワイルドカードで始まらないLIKE検索時にINDEXが使用される
            # 複合インデックスを持っている場合、オプティマイザーは一番左のカラムから順に使用する
            # よって、複合インデックスの順に沿わないカラムを使用したクエリでは、複合インデックスは使用されないッ！
            # (col1, col2)の複合インデックスの場合、WHERE col2='' AND col1=''では複合インデックスは適用されない
            # 一般的に、カーディナリティが高いカラムにINDEXを張るが、そのカラムの値の分布が極端に偏っている場合、
            # オプティマイザーはそのインデックスを使用しない場合があるッ！
            # ANALYZE TABLEやEXPLAIN SELECT * FROM 〜などで確認してみること
            # (col1, col2, col3)の複合インデックスの場合、col1だけの場合、(col1, col2)の列で検索する場合、
            # 全ての列で検索する場合にそのINDEXが使える
            # 複合インデックスを定義する際に重要なのは、そのINDEXを使えるSQLが出来るだけ大きくなる様に列の順番を決めること
            # (苗字, 名前)を複合インデックスにすることで効率良く「山田 太郎」を検索できるが、
            # カーディナリティの低い「会社」を加え、(苗字, 名前, 会社)としてしまうとインデックスが大きくなり悪影響が出る
            # cf) https://www.gatc.jp/gat/it/it02dbindex.html
            # DBはプライマリーキーが複数ある場合は、全てのカラムに連結インデックスを作成する
            # ただし、複合インデックスを作るとSELECTのパフォーマンスは良くなるが、
            # ストレージ領域の圧迫やメンテナンスのオーバーヘッドが発生する
            # テーブルに対するINDEXの数は少ないほどINSERTやDELETE、UPDATEのパフォーマンスは向上する
            # cf) https://use-the-index-luke.com/ja/sql/where-clause/the-equals-operator/concatenated-keys
            models.Index(fields=['nickname',], name='profile_nickname'),
            models.Index(fields=['family_name', 'given_name']) # 複合インデックス
        ]

    def __str__(self):
        return str(self.user.id)


def get_profimage_path(self, filename) -> str:
    """ アップロードされたプロフィール画像の名前を変更する """
    prefix = 'prof' + os.sep
    # ユーザIDを使用したい時はself.user.idなど
    name = str(uuid.uuid4()).replace('-', '')
    extension = filename.split('.')[-1]
    return '{}{}.{}'.format(prefix, name, extension)


# 自分はdjango-cleanupを使用しているので古いファイルは自動的に削除されるようになっている
# もし古いファイルも残したいのであれば、 @cleanup.ignore デコレータを使用
class ProfImage(models.Model):
    """ プロフィール画像 """
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
        verbose_name='ユーザー', 
        on_delete=models.CASCADE, blank=False,)
    prof_image = models.ImageField('プロフィール画像', 
        upload_to=get_profimage_path, blank=True,)
    note = models.TextField("注釈", blank=True, null=True,)

    class Meta:
        db_table = 'prof_image'
        verbose_name = 'プロフィール画像'
        verbose_name_plural = 'プロフィール画像'

    def __str__(self):
        # 管理画面でDjango __str__ returned non-string (type int)エラーが出るのでstr()化する 
        return str(self.user.id) 
        # return f'{self.prof_image.url}'

    def save(self, *args, **kwargs):
        super(ProfImage, self).save(*args, **kwargs)


#tel_number_regex = RegexValidator(regex=r'^[0-9]+$', message = ("Tel Number must be entered in the format: '09012345678'. Up to 15 digits allowed."))
#tel_number = models.CharField(validators=[tel_number_regex], max_length=15, verbose_name='電話番号')
#postal_code_regex = RegexValidator(regex=r'^[0-9]+$', message = ("Postal Code must be entered in the format: '1234567'. Up to 7 digits allowed."))
#postal_code = models.CharField(validators=[postal_code_regex], max_length=7, verbose_name='郵便番号') 


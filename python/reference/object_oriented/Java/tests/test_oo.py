# 本当はテストを分けるべきだが、
# ここは簡易的に１つの関数内で様々なTDDを行っている

import pytest
# 注意!!
# pytestは__init__.pyが存在しないディレクトリまで再帰的に探索する
# そして__init__.pyが存在しないディレクトリをbasedirとする
# また、__init__.pyがあるディレクトリをパッケージであると判断する
# 当テスト環境では、javaディレクトリ内に__init__.pyを置くことは出来ない
# だからjava/testsディレクトリを作成し、そこに当テストファイルと__init__.pyを作成する
# 同様に以下のようにimportするためにtekiディレクトリを作成し、ファイルと__init__.pyを作成する
from teki.slime import Slime
from teki.monster import Monster
from teki.containerlike import ContainerLike

@pytest.mark.solved
def test_should_return_slime():
	""" TDDにてクラス作成 """

	# Slimeクラスを実装
	# また、Slimeが継承しているMonsterクラスの抽象メソッドrun()をオーバーライド
	assert Slime("A")

	# ドキュメンテーションを確認
	assert Slime.__doc__.find("スライムオブジェクト")

	# インスタンス作成
	slime_1 = Slime("A")

	# Slimeの継承
	assert isinstance(slime_1, Slime)

	# Slimeのsuffixを変更する
	slime_1.suffix = "B"
	assert slime_1.suffix == "B"

	# run()メソッド
	assert slime_1.run()

	# run()メソッドの返り値
	assert slime_1.run().find("ボヨンボヨン")

	# ゲッターによりSlimeのHP取得
	assert slime_1.hp

	# セッターによりSlimeのHP変更
	slime_1.hp = 120

	# HPはint型
	assert isinstance(slime_1.hp, int)
	assert slime_1.hp == 120

	# staticプロパティ
	assert Slime.softness

	# staticメソッドtouch()
	assert Slime.touch()

	assert Slime.touch().find("触り心地") == 0

	# 呼び出し可能かどうか
	assert callable(slime_1)

	# callableオブジェクトを呼び出す
	# 呼び出し可能なオブジェクトは、オブジェクト名()で呼び出せる
	assert slime_1() == "__call__メソッド"


@pytest.mark.solved
def test_should_return_error_message():
	""" 例外補足用のテスト """

	# インスタンス作成
	slime_1 = Slime("A")

	# suffixはstring以外は受け付けない
	with pytest.raises(Exception) as e1:
		slime_2 = Slime(33)

	assert str(e1.value) == "str型で指定してください"

	# HPはinteger以外は受け付けない
	with pytest.raises(Exception) as e2:
		slime_1.hp = "orange"

	assert str(e2.value) == "int型で指定してください"


@pytest.mark.solved
def test_container_like():
	""" 特殊メソッドのテスト """

	# インスタンス作成
	cl = ContainerLike(5)

	# __len__
	assert len(cl) == 5 * 100

	# __add__
	assert (cl + 50) == 5500

	# __getitem__
	assert [i for i in cl] == [0, 100, 200, 300, 400]


# Zip化テスト
# pytest --setup-show -v -m "unsolved and not solved" --pdb test_zipped.py

# Mock
# pip install -U pytest-mock

# Coverage
# pip install -U pytest-cov
# pytest --cov=scripts/python/zipped

import os
import sys
import pdb # pdb.set_trace()
import pytest
import zipfile # ファイル毎
import shutil # フォルダごと

# tmpdirとtmpdir_factoryの２つの組み込みフィクスチャは、テストを実行する前に
# 一時ディレクトリを作成し、テストの終了時にそのディレクトリを削除するために使われる


# pytestのクラスはコンストラクタが使えないのでメンバ変数が設定できない
# フィクスチャ使っても良いが、変数定義で対応
dir_name = "forTest"
uz_dir_name = "uzforTest"
zip_name = "zipped"
num = 10

# ディレクトリ作成
# フィクスチャを使うと毎回テスト関数を実行するたびに呼び出されてしまうので、ここで作成
if not os.path.exists(dir_name):
	os.mkdir(dir_name)
	# ファイルを作成
	for i in range(num):
		file_name = './forTest/file-{}.txt'.format(i)
		if not os.path.exists(file_name):
			with open(file_name, 'w') as f:
				f.write('The number of this file is {}. \n'.format(i))


# フィクスチャを使った場合
@pytest.fixture(scope="function")
# create_and_delete_directory(tmp_dir)とすると一時ディレクトリが作成され、以下のように手動でディレクトリを作成する必要はない
def create_and_delete_directory(): 
	"""  前処理にてディレクトリとファイルを作成し、後処理にてディレクトリを削除するフィクスチャ """
	# GIVEN(前提): 
	# WHEN(こうしたら): 
	# THEN(こうなる):

	# 前処理部分(setUp) # pytest --setup-showでフィクスチャの実行タイミングを確認できる
	result = False
	try:
		# ディレクトリ作成
		if not os.path.exists(dir_name):
			os.mkdir(dir_name)
		#ファイル作成
		for i in range(num):
			file_name = './forTest/file-{}.txt'.format(i)
			if not os.path.exists(file_name):
				with open(file_name, 'w') as f:
					f.write('The number of this file is {}. \n'.format(i))

	except Exception as e: # 基底クラスException
		print(e, file=sys.stderr)

	else: # 正常終了時の処理
		result = True

	finally: # 終了時に常に行う処理
		pass

	yield result # return

	# 後処理部分(tearDown)
	if os.path.exists(dir_name):
		shutil.rmtree(dir_name)


class TestZipFile:
	"""  """
	
	# 注意: PyTestでは__init__()コンストラクタは使用できない

	def test_should_check_whether_the_files_are_exist(self):
		""" 作成したディレクトリとファイルが存在しているかチェック """
		
		assert os.path.exists(dir_name)
		assert os.path.exists(dir_name+'/file-0.txt')
		# ファイル数確認
		file_num = sum(os.path.isfile(os.path.join(dir_name, file_name)) for file_name in os.listdir(dir_name))
		assert num == file_num
		# ファイルの内容が一致するか
		with open(dir_name+'/file-0.txt') as f:
			body = f.read()
			assert 'The number of this file is 0.' in body

		
	def test_should_zip_files(self):
		""" 作成したディレクトリの中身をZIP圧縮 """

		try:
			# フォルダごとZIPに圧縮(base_dirを指定しない場合、ディレクトリforTest自体は含まれない)
			shutil.make_archive(zip_name, format='zip', root_dir=dir_name)
		except Exception as e:
			print(e, file=sys.stderr)

		# .zipが存在するか判定
		assert os.path.exists(zip_name+'.zip')
		# zipか確認(本当はバイナリ判定とかした方が良いだろうが)
		assert zipfile.is_zipfile(zip_name+'.zip')


	def test_should_unpack_zip(self):
		""" unzip出来ているか """

		try:
			# zipを解凍
			shutil.unpack_archive(zip_name+'.zip', uz_dir_name)
		except Exception as e:
			print(e, file=sys.stderr)

		# 解凍されているか確認
		assert os.path.exists(uz_dir_name)
		# 解凍されたディレクトリの内容が正しいか確認
		assert os.path.exists(uz_dir_name+'/file-5.txt')
		# ファイル数確認
		file_num = sum(os.path.isfile(os.path.join(uz_dir_name, file_name)) for file_name in os.listdir(uz_dir_name))
		assert num == file_num
		# ファイルの内容が一致するか
		with open(uz_dir_name+'/file-5.txt') as f:
			body = f.read()
			assert 'The number of this file is 5.' in body


@pytest.mark.sticky_fingers # マーカー設定: pytest -v -m "sticky_fingers" test_zipped.py
def test_should_remove_directory():
	"""  """

	# 作成したディレクトリを中身ごと削除
	try:
		if os.path.exists(dir_name):
			shutil.rmtree(dir_name)
	except Exception as e:
		print(e, file=sys.stderr)
	
	# 元のディレクトリが削除されているか確認
	assert not os.path.exists(dir_name)


@pytest.mark.parametrize(('fruits', 'sports'),[ # パラメーター化
		('apple', 'baseball'),
])
def test_should_remove_zip(fruits, sports):
	"""  """

	# .zip削除
	try:
		if os.path.exists(zip_name+'.zip'):
			os.remove(zip_name+'.zip')
	except Exception as e:
		print(e, file=sys.stderr)
	
	# 元のディレクトリが削除されているか確認
	assert not os.path.exists(zip_name+'.zip')


def test_should_remove_unzipped_directory():
	"""  """

	# 作成したディレクトリを中身ごと削除
	try:
		if os.path.exists(uz_dir_name):
			shutil.rmtree(uz_dir_name)
	except Exception as e:
		print(e, file=sys.stderr)
	
	# 元のディレクトリが削除されているか確認
	assert not os.path.exists(uz_dir_name)


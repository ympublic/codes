const Example = () => {
  const clickHandler = () => {
    alert("クリックされました");
  };
  const clickHandler2 = () => {
    alert("Clicked");
  };
  return (
    <>
      {/** onClick={clickHandler()}とはしないように */}
      <button onClick={clickHandler}>クリックしてね</button>
      <button
        onClick={() => {
          clickHandler2();
        }}
      >
        Please Click Me!!
      </button>
    </>
  );
};

export default Example;

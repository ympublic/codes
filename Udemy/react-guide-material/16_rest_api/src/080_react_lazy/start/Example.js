import { useState, lazy, Suspense, startTransition } from "react";
// import ComponentA from "./components/ComponentA";

// ReactでDynamic Importを利用する場合
// LazyComponentAが必要になった段階で読み込まれることになる
const LazyComponentA = lazy(() => import("./components/ComponentA"));
const LazyComponentB = lazy(() => import("./components/ComponentB"));

const Example = () => {
  const [compA, setCompA] = useState(false);

  return (
    <>
      <button
        onClick={() => {
          startTransition(() => {
            setCompA((prev) => !prev);
          });
        }}
      >
        ComponentA
      </button>
      {/** LazyComponentAが読み込まれるまで以下のSuspenseコンポーネントが表示される */}
      <Suspense fallback={<div>Loading...</div>}>
        {compA ? <LazyComponentA /> : <LazyComponentB />}
      </Suspense>
    </>
  );
};

export default Example;

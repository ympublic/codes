import unicodedata
# Django
from django import forms
from django.forms import modelform_factory
from django.utils.translation import gettext_lazy as _

# Application
from news.models import News 


####################################################
#####     ニュース記事投稿
####################################################

class NewsForm(forms.ModelForm):
	class Meta:
		model = News
		fields = ('title', 'body', 'url', 'published_at', 'image_src',)
		widgets = { # placeholderやclassを追加する
		# https://docs.djangoproject.com/ja/4.1/ref/forms/widgets/#widgets-handling-input-of-text
			'title': forms.TextInput(attrs={'placeholder':'記事のタイトル', 'class':'form-control',}),
			'body': forms.Textarea(attrs={'placeholder':'記事の本文', 'class':'form-control',}),
			'url': forms.URLInput(attrs={'placeholder':'https://news.yahoo.co.jp/articles/', 'class':'form-control',}),
			'published_at': forms.DateTimeInput(attrs={'placeholder':'記事の投稿日時', 'class':'form-control',}),
			'image_src': forms.URLInput(attrs={'placeholder':'記事の画像', 'class':'form-control',}),
		}
		

		# .jpg validate 

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

	# クリーニング（正規化）処理
	def clean_name(self):
		title = self.cleaned_data['title']
		title = unicodedata.normalize('NFKC', title)
		return title

	def clean_body(self):
		body = self.cleaned_data['body']
		body = unicodedata.normalize('NFKC', body)
		return body


class BSNewsForm(forms.Form):
	""" script/bsnews.pyを実行するためのForm """

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)


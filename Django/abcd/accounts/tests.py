import pdb # pdb.set_trace() or breakpoint()
import os
import sys
# Django
from django.test import TestCase
from django.test import Client
from django.test import RequestFactory
from django.test import tag
from django.test import SimpleTestCase
from django.test import override_settings
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.http import HttpRequest
from django.http import HttpResponseRedirect
from django.urls import resolve
from django.urls import path
from django.contrib.auth import get_user_model
from django.urls import reverse, reverse_lazy
from django.dispatch import Signal
from django.dispatch import receiver # Signals
from django.db.models.signals import post_save # Modelインスタンスのsaveメソッドの後に実行
from django.test.utils import override_settings # @override_settings()
# Application
from accounts.views import profcreate
from accounts.models import MyUser
from accounts.models import Profile
from accounts.models import ProfImage


UserModel = get_user_model()
EMAIL = "test@example.com"
PASSWORD = "test001"

best_mlb_player = "Babe Ruth"

@tag('accounts') # python manage.py test --tag=accounts
class ProfileTest(TestCase):

	# def __init__(self, *args, **kwargs): super.__init__(*args, **kwargs)

	def setUp(self): # テストメソッドが呼ばれるたびに実行される
		self.user = UserModel.objects.create_user(
			email=EMAIL,
			password=PASSWORD,
		)
		'''
		self.profile = Profile(
			user = self.user,
			family_name="芋虫",
			given_name="アゲハ蝶",
			nickname="オオゴマダラ",
		)
		self.profile.save()
		'''
		'''
		self.profile = Profile.objects.create(
			user = self.user,
			family_name="芋虫",
			given_name="アゲハ蝶",
			nickname="オオゴマダラ",
		)
		'''
		self.client.force_login(self.user)
		# print("★★★★★ {} ★★★★★".format(self.user.pk))

	# def tearDown(self): pass

	def test_should_passed_on_prof_create(self):
		data = {
			'family_name': '芋虫',
			'given_name':'アゲハ蝶',
			'nickname': 'オオゴマダラ',
		}
		response = self.client.post(
			reverse_lazy("accounts:prof_create", kwargs={"user_id": self.user.pk}),
			data,
		)
		self.assertIsInstance(response, HttpResponseRedirect) # リダイレクト

		profile = Profile.objects.get(user_id=self.user.pk)
		self.assertEqual(profile.family_name, '芋虫')

	def test_should_save_Profile(self):
		"""  """

		@receiver([post_save], sender=UserModel)
		def connect_with_profile(sender, instance, created, **kwargs):
			if created: # 例: if created and instance.email
				profile_obj = Profile(user=instance)
				profile_obj.email = instance.email
				profile_obj.save()

		def test_usermodel_should_be_connected_with_profile(self):
			counter = Profile.objects.count()
			self.assertEqual(counter, 1)
			profile_obj = Profile.objects.first()
			self.assertEqual(profile_obj.user.email, profile_obj.email)

	
	def test_should_recieve_signals(self):
		""" Signalsにより、新規ユーザー登録時にMyUserとProfileテーブルを紐づけられるかテスト """

		@receiver([post_save,], sender=UserModel)
		def my_callback(sender, instance, created, **kwargs):
			global best_mlb_player
			if created: 
				best_mlb_player = "Shouhei Otani"
				print("The best mlb player is {}!".format(best_mlb_player))
				profile = Profile.objects.create(
					user = instance,
					nickname = "ShowTime",
					family_name = "Otani",
					given_name = "Shouhei",
				)
			else:
				best_mlb_player = "Mike Trout"
				print("The best mlb player is not Shouhei Otani but {}!".format(best_mlb_player))

		signal_user = UserModel.objects.create_user(
			email="signals@django.com",
			password="signalstest",
		)

		# 以下のように自分でSignalを送ることもできるが、"The best mlb player is {}"が２回呼ばれることになる
		# post_save.send(UserModel, instance=signal_user, created=True)

		nickname = Profile.objects.get(user__email="signals@django.com").nickname

		# ユーザーが作成されたかテスト
		self.assertTrue(
			get_user_model().objects.filter(
				email="signals@django.com"
			).exists()
		)
		# Signalsでcreated=Trueとなっているか
		self.assertEqual(best_mlb_player, "Shouhei Otani")
		# Signalsを受取り、Profileテーブルに紐付けられ、またちゃんとデータが登録されているか
		self.assertEqual(nickname, "ShowTime")



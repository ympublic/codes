import { useEffect, useState } from "react";

const Example = () => {
  const [checked, setChecked] = useState(false);

  // windowsを使うメソッドは副作用になる
  useEffect(() => {
    checked && window.alert("On");
    // checkedの値が変わるたびにコールバック関数を実行させたいので、
    // 依存配列にはcheckedを入れる
  }, [checked]);

  return (
    <>
      <h3>練習問題</h3>
      <p>
        記述を変更し、完成コードのように、checkedがtrueの場合のみ
        alertで「checked!」と表示されるようにしてください。
        useEffectを用いて実装してください。
      </p>
      <label>
        <input
          type={"checkbox"}
          value={checked}
          onClick={() => setChecked((checked) => !checked)}
        />
        click me
      </label>
    </>
  );
};

export default Example;

# Django
from django.urls import path
from django.urls import re_path
# Application
from iportfolio import views

app_name = "iportfolio"

# 関数ベースビュー
urlpatterns = [
	path("", views.iportfolio, name="iportfolio"), # "/iportfolio/"
	path("contact_confirm/", views.contact_confirm, name="contact_confirm"),
	path("contact_complete/", views.contact_complete, name="contact_complete"),
]

# クラスベースビュー
urlpatterns += [ # as_view(): クラスベースビューを関数化する
	# path("", views.TopPageView.as_view(), name="iportfolio"), 
	# path("contact_confirm/", views.ContactConfirmView, name="contact_confirm"),
	# path("contact_complete/", views.ContactCompleteView, name="contact_complete"),
]

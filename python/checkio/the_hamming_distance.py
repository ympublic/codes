#  cf: difflib module

def checkio(n, m):
    if n < m:
        l = len(bin(m))-2
    else:
        l = len(bin(n))-2
    
    n_b = format(n, '0'+str(l)+'b')
    m_b = format(m, '0'+str(l)+'b')
    
    counter=0
    for i in range(len(n_b)):
        if n_b[i] != m_b[i]:
            counter+=1
    
    return counter

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert checkio(117, 17) == 3, "First example"
    assert checkio(1, 2) == 2, "Second example"
    assert checkio(16, 15) == 5, "Third example"
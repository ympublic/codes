#! /usr/bin/env python

"""正規表現
https://docs.python.org/ja/3/howto/regex.html
"""

# パターンとしては、re.compile(文字列) -> pattern.search(正規表現パターン)

import re
import pytest # pytest -v -m "unsolved" test_regex.py --pdb


# フィクスチャ作成
@pytest.fixture(scope="function")
def regex_pattern_python(): 
	""" 正規表現のパターンをコンパイル """

	# setUp

	pattern = re.compile("[pP]ython")

	# 修飾子を用いる場合
	# pattern = re.compile("[a-z]", re.I) # 大文字小文字を区別しない
	# pattern = re.compile("[a-z]", re.U) # Unicode文字特性データベースに従わせる
	yield pattern

	# tearDown


@pytest.mark.solved
def test_should_search_regex_patterns():
	""" 
	.search(正規表現パターン)
	正規表現パターンで検索
	"""

	# 正規表現パターンをコンパイル
	# re.compile(文字列)
	# 文字列をコンパイルし、正規表現オブジェクトを生成
	pattern = re.compile("[Pp]ython") # Pythonかpythonにマッチ

	# 文字列に対して検索を行い、マッチオブジェクトを取得
	result = pattern.search("I love Python so much!! During using python, I can really enjoy.")

	assert result # <re.Match object; span=(7, 13), match='Python'>


@pytest.mark.solved
def test_should_match_regex_patterns(regex_pattern_python):
	""" 
	.match(正規表現パターン)
	search()と違い、こちらは先頭のみにマッチする 
	"""

	# フィクスチャからパターンを取得
	pattern = regex_pattern_python

	
	result = pattern.match("I love Python so much!! During using python, I can really enjoy.")

	# 先頭文字は"I"であり、"Python"ではないのでFalseとなる
	assert not result


@pytest.mark.solved
def test_should_sub_regex_patterns(regex_pattern_python):
	""" 
	sub(置換後, 置換前) # 引数の順番注意ッ！
	正規表現パターンでマッチした部分を置換する
	"""

	expression = "I love Python so much!! While I use python, I can really enjoy."

	# 対象の文字列中の[pP]ythonをJavaScriptに変換する
	result = regex_pattern_python.sub("JavaScript", expression)

	assert result # "I love JavaScript so much!! While I use JavaScript, I can really enjoy."


@pytest.mark.solved
def test_should_group_regex_patterns():
	""" 
	groups()
	正規表現パターン中で()で囲んだ部分がグループになり部分一致として取得される
	"""

	# raw stringに記法によりパターン作成
	pattern = re.compile(r"(\d+)\.(\d+)\.(\d+)\.(\d+)") # IPv4を取得

	# 検索
	result = pattern.search("192.168.0.1")

	# １から全てのサブグループの文字列を含むタプルを返す
	assert result.groups() == ("192", "168", "0", "1")

	# group(0)を取得する
	assert result.group(0) == "192.168.0.1"

	# ２番目のグループを取得する
	assert result.group(2) == "168"

	# ２番目と４番目のグループを取得する
	assert result.group(2, 4) == ("168", "1")

	# グループ名付与
	# "(?P<グループ名>)"
	# (?P<>)と(?P<>)の間に半角スペースを入れるか[ ]*などがないと、Johまでしか取得されない
	pattern = re.compile("(?P<first_name>[A-Z]?[a-z]+)[ ]*(?P<last_name>[A-Z]?[a-z]+)")

	result = pattern.search("John Lennon")

	assert result.group("first_name") == "John"

	assert result.group("last_name") == "Lennon"

	assert result.groups() == ("John", "Lennon")


@pytest.mark.solved
def test_should_back_reference_regex_patterns():
	""" 
	後方参照
	部分一致内容を直後の置換で利用する機能
	"""

	# ドキュメント: 正規表現で後方参照を含む場合は必ずraw stringを利用する！
	pattern = re.compile(r"(\d{3})(\d{4})")

	# 置換文字列に"\1", "\2"と記述することで取得できる
	result = pattern.sub(r"\1-\2", "1050044") # sub(置換後, 置換前)

	assert result == "105-0044"


@pytest.mark.solved
def test_should_findall_regex_patterns():
	""" 
	findall(キーワード, 抽出対象)
	部分一致したキーワードを手軽に抽出する
	"""

	html = '<a href="https://www.python.org/">Python Official Website</a>'

	pattern = re.compile('http[s]?://[^"]+?/')

	# 注意: findall()は結果を返す前に完全なリストを必ず生成する
	# いっぽうfinditer()メソッドはマッチオブジェクトインスタンスのシーケンスをiteratorとして返す
	result = re.findall('<a href="(http[s]?://[^"]+?)">([^<]+?)</a>', html)

	assert result == [('https://www.python.org/', 'Python Official Website')]

	result_iter = pattern.finditer(html)

	for match in result_iter:
		assert match.group(0) in ('https://www.python.org/', 'Python Official Website')


@pytest.mark.solved
def test_should_split_regex_patterns():
	""" 
	split(分割したい文字列)
	正規表現で一致した文字列の部分で文字列全体を分割する
	"""

	pattern = re.compile(" +") # 空白で分割

	result = pattern.split("aaa bbb ccc")

	assert result == ["aaa", "bbb", "ccc"]

	pattern = re.compile("-+") # -で分割

	result = pattern.split("153-0044")

	assert result == ["153", "0044"]


@pytest.mark.solved
def test_should_escape_regex_patterns():
	""" 
	re.escape(メタ文字を含む文字列)
	全てのメタ文字をエスケープした文字列を返す
	動的に正規表現パターンを生成する場合に有効
	"""

	assert re.escape("^$?+*") == "\\^\\$\\?\\+\\*"


@pytest.mark.solved
def test_should_look_ahead_and_look_behind():
	""" 
	先読みと後読み
	https://www.javadrive.jp/regex-basic/writing/index2.html
	"""

	# 肯定先読み
	# パターンと部分文字列が一致した場合にその部分文字列とマッチするのではなく、
	# 部分文字列の先頭位置にマッチする
	# 例: Word(?=Press)であれば、 最初に対象の文字列の先頭から
	#「Word」とマッチする部分を探す。つまりdとPの間
	# 次に先ほどマッチした部分の次の文字から「Press」と
	# マッチするかどうかを調べる
	pattern = re.compile("Word(?=Press)")

	# ヒットしない
	assert not pattern.search("WordBook")

	# ヒットする
	assert pattern.search("WordPress")

	# 先読みの別パターンを作成
	pattern = re.compile("(?=WordPress)Word")

	# ヒットしない
	assert not pattern.search("WordBook")

	# ヒットする
	assert pattern.search("WordPress")


	# 肯定後読み
	# パターンと部分文字列が一致した場合にその部分文字列と
	# マッチするのではなく、部分文字列の末尾位置にマッチする
	# 例: (?<=pe)Scriptであれば、左側の文字が「pe」とマッチするか調べる
	# マッチした場合、肯定後読みの場合はマッチしたことだけを確認し、
	# 実際にはマッチが成功した位置とマッチする。つまりeとSの間
	# 次に先ほどマッチした部分の次の文字から「Script」と
	# マッチするかどうかをチェックする
	pattern = re.compile("(?<=pe)Script")

	# ヒットしない
	assert not pattern.search("JavaScript")

	# ヒットする
	assert pattern.search("TypeScript")


@pytest.mark.solved
def test_should_compile_Nihongo():
	""" 
	ひらがな、カタカナ、漢字、ASCII文字判定
	"""

	expression = "ひらがなカタカナ漢字ASCII"

	hiragana = re.compile("[\u3041-\u309f]") # Wikipediaの「平仮名（Unicodeのブロック）」より

	katakana = re.compile("[\u30a1-\u30ff]") # Wikipediaの「片仮名（Unicodeのブロック）」より

	kanji = re.compile("[\u4e00-\u9fff]") # Wikipediaの「CJK統合漢字（Unicodeのブロック）」より

	ascii_characters = re.compile("[\u0000-\u007f]")

	assert "****カタカナ漢字ASCII" == hiragana.sub("*", expression)

	assert "ひらがな****漢字ASCII" == katakana.sub("*", expression)

	assert "ひらがなカタカナ**ASCII" == kanji.sub("*", expression)

	assert "ひらがなカタカナ漢字*****" == ascii_characters.sub("*", expression)


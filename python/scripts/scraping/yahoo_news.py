#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' 
Yahoo!Newsのトップページから、記事を取得するスクリプト 
Djangoから実行する場合は、os.getcwd() -> /Users/$USER/git/ProjectX/abcdである点に注意
'''

''' TODO List
Emailの送信は、エラー発生時に逐次送信するのではなく、キューに保存しておくべき
'''

#from __future__ import annotations
import os
import re
import sys
import time
import datetime
import gc
import requests
import lxml.html
import sqlite3 # SQlite
import MySQLdb # MySQL
from collections import namedtuple
from urllib.parse import urlparse
from bs4 import BeautifulSoup
import tenacity
from tenacity import retry
from tenacity import stop_after_attempt
from tenacity import wait_exponential
from tenacity import after_log
from cachecontrol import CacheControl
# from jsonschema import validate
# from pyquery import PyQuery as pq

# Django
from django.conf import settings
from django.core.mail import send_mail
from django.core.mail import EmailMessage # BCC
from django.template.loader import render_to_string
from smtplib import SMTPException

# env取得
from pathlib import Path
from dotenv import load_dotenv # .envを読み込む
BASE_DIR = Path(__file__).resolve().parent.parent.parent.parent
# cf) https://stackoverflow.com/questions/48001164/typeerror-unsupported-operand-types-for-posixpath-and-str
ENV_PATH = BASE_DIR / "private{}.env".format(os.sep) # PosixPathの場合、/ で結合する
load_dotenv(ENV_PATH)


# スクリプトの引数処理
# 使用データベースの設定

database: str = "mysql"


# 定数の設定
USER_AGENT: str = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3864.0 Safari/537.36'
YAHOO_NEWS: str = 'https://news.yahoo.co.jp/'
# YAHOO_NEWS: str = 'https://news.yahoo.co.jp/categories/it' # IT記事
TEMPORARY_ERROR_CODES: tuple = (408, 500, 502, 503, 504) # 一時的なエラーを表すステータスコード
SLEEP_TIME: int = 1
css_path = {
	'urls_from_top': 'section ul li a',
	'title': 'article > header > h1', # >は直接の子ノードを指定する
	'body': 'article > div.article_body > div > p',
	'url': 'article > div > span > a',
	'published_at': 'article time', # 半角スペースは子孫を指定する
	'image_src': 'picture > img',
}

DB_NAME = os.environ.get("DB_NAME")
DB_USER = os.environ.get("DB_USER")
DB_PASSWORD = os.environ.get("DB_PASSWORD")
DB_HOST = os.environ.get("DB_HOST")
DB_PORT = int(os.environ.get("DB_PORT"))


# 関数定義
def one_space(text: str) -> str: # Type Hints
	""" 半角スペースに変換する """
	text = text.replace('　', ' ') # 全角スペースを半角スペースに
	text = text.replace('¥t', ' ') # タブを半角スペースに
	return text
	

def normalize_spaces(s: str) -> str:
	""" 連続する空白を１つのスペースに置き換え、前後の空白を削除した新しい文字列を取得する """
	return re.sub(r'¥s+', ' ', s).strip()


def get_charset(response: str) -> str:
	""" metaタグからエンコーディングを取得 """
	# レスポンスボディの先頭1024バイトをASCII文字列としてデコードする
	# ASCII範囲外の文字はU+FFFD(REPLACEMENT CHARACTER)に置き換え、例外を発生させない
	scanned_text = response.content[:1024].decode('ascii', errors='replace')

	# デコードした文字列から正規表現でcharsetの値を抜き出す
	match = re.search(r'charset=["\']?([\w-]+)', response.text)
	if match:
		response.encoding = match.group(1)
	else:
		# response.encodingは元々HTTPヘッダーから得られたエンコーディングを取得できる
		# response.apparent_encodingはレスポンスボディからエンコーディングを推定できる
		response.encoding = 'utf-8'
		
	return response.encoding


def send_error(url, err=None):
	""" 発生したエラーを送信 """
	mail_subject = '【Scraping is failed】something wrong with bsNews.py.'
	
	mail_message = """
{}にてNews記事を取得中にエラーが発生しました。
error内容 : {} 
	""".format(url, err)

	mail_from = "contact@s-gold.net"
	mail_to = ["contact@s-gold.net",] # toはlistかtupleである必要がある
	try:
		send_mail(mail_subject, mail_message, mail_from, mail_to, fail_silently=False,)
	except BadHeaderError:              # If mail's Subject is not properly formatted.
		print('Invalid header found.')
	except SMTPException as e:          # It will catch other errors related to SMTP.
		print('There was an error sending an email.'+ e)
	except:                             # It will catch All other possible errors.
		print("Mail Sending Failed!")

	return None


# 抽出したデータを検証する
json_validator = {
	'type': 'object', # pythonのdict型
	'properties': { # 各ルールを以下に記述
		'title': {
			'type': 'string',
		},
		'body': {
			'type': 'string',
		},
		'url': {
			'type': 'string',
			'pattern': "https?://[\w/:%#\$&\?\(\)~\.=\+\-]+", # URLの正規表現
		},
		'published_at': {
			'type': 'string',
		},
		'image_src': {
			'type': 'string',
			'pattern': "https?://[\w/:%#\$&\?\(\)~\.=\+\-]+", # URLの正規表現
		},
	},
	'required': ['title', 'body', 'url', 'published_at', 'image_src',], # 必須項目
}



# @retryデコレーターのキーワード引数stopにリトライを終了する条件を、waitにウェイトの取り方を指定
# stop_after_attemptは回数を条件に終了することを表し、引数に最大リトライ回数を指定
# wait_exponentialは指定回数的なウェイトを表し、キーワード引数multiplierに初回のウェイトを秒単位で指定
# Logを出力したい時は、
# import logging
# logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
# logger = logging.getLogger(__name__)
# @retry(after=after_log(logger, logging.DEBUG))
@retry(stop=stop_after_attempt(3), wait=wait_exponential(multiplier=1))
def extract_data_from_yahooNews(url, css_path, need="data") :
	""" 
	指定したURLにリクエストを送り、urlのリストを返す
	一時的なエラーが発生した際には最大３回リトライする
	３回リトライしても成功しなかった場合は例外tenacity.RetryErrorを発生させる
	"""
	# 複数のページをクロールするのでSessionを使用する
	session = requests.Session()
	# sessionをラップしたcached_sessを作成
	cached_session = CacheControl(session)
	# 接続を開始
	print("{}への接続を開始します".format(url))
	response = cached_session.get(url, headers={'user-agent': USER_AGENT})

	# 取得したステータスコードにより処理を分岐
	if response.status_code not in TEMPORARY_ERROR_CODES:
		if need == "url":
			# lxmlによるスクレイピング
			# HTMLを読み込む
			html = lxml.html.fromstring(response.text)
			# 引数のURLを基準として、全てのa要素のhref属性を絶対パスに変換する
			# html.make_links_absolute(response.url)
			# URLをリスト内包型により取得する
			extracted_data = [a.get('href') for a in html.cssselect(css_path)]
		else:
			# BeautifulSoup4によるスクレイピング
			# text: タグ内に含まれるすべての文字列をつなぎ合わせて返却
			soup = BeautifulSoup(response.text, 'html.parser')
			# TODO: 抽出したデータにバリデータを通過させる
			extracted_data = {
				'title': one_space(soup.select_one(css_path['title']).text),
				'body': one_space(soup.select_one(css_path['body']).text),
				'published_at': soup.select_one(css_path['published_at']).text.strip(),
				'image_src': soup.select_one(css_path['image_src']).get('src'),
			}

		# dataが取得できなかった場合はエラーを発生させ、メールで通知する
		if not extracted_data: # extracted_dataが空の場合(PEP8より)
			# エラーを送信
			send_error(url)
			
			raise Exception(f'dataが取得できませんでした')
				
		return extracted_data

	raise Exception(f'Temporary Error: {response.status_code}')


def check_urls():
	""" 取得したurlが既にDBに格納済みかどうか確認する """
	stored_urls = ""
	conn = ""
	try:
		# MySQLに接続
		print("MySQLに接続します")
		conn = MySQLdb.connect(db=DB_NAME, user=DB_USER, passwd=DB_PASSWORD, charset='utf8mb4',
			host=DB_HOST, port=DB_PORT,)
		# カーソルを取得
		cur = conn.cursor()

		# 格納されているurlをSELECT
		sql = "SELECT url FROM news"
		cur.execute(sql)
		# fetchall()はタプルで情報が取得されるがタプルが邪魔なのでurl[0]でurlのみ取得
		stored_urls = [url[0] for url in cur.fetchall()]

	except MySQLdb.Error as e:
		print('MySQLdb.Error: ', e)
		send_error(url, e)
		raise Exception("データベースとの接続でエラーが発生しています。")

	finally:
		# コネクションを閉じる
		print("コネクションを閉じます")
		conn.close()
		return stored_urls


def save_image(image_src: str) -> None:
	""" imageをダウンロードして保存する """
	response = requests.get(image_src)
	image = response.content
	image_name = urlparse(data['image_src']).path.split('/')[-1]
	# imageの保管場所を指定する
	image_path = os.path.join(os.getcwd() + os.sep + 'Downloads', image_name)
	# バイナリモードで開く
	with open(image_path, "wb") as f:
		f.write(image)


# TODO: 画像のリサイズ
def save_data(data: dict) -> None:
	""" 記事ページから抽出したデータをDBに格納する """
	conn = ""
	try:
		# MySQLに接続
		print("MySQLに接続します")
		conn = MySQLdb.connect(db=DB_NAME, user=DB_USER, passwd=DB_PASSWORD, charset='utf8mb4',
			host=DB_HOST, port=DB_PORT,)
		# カーソルを取得
		cur = conn.cursor()

		for url in data:
			title: str = data[url]['title']
			body: str = data[url]['body']
			published_at: str = data[url]['published_at']
			image_src: str = data[url]['image_src']
			created_at = datetime.datetime.now()

			# プレースホルダーを使用してデータをnewsテーブルにINSERT
			sql = "INSERT INTO news (title, body, url, created_at, image_src, published_at) \
					VALUES (%s, %s, %s, %s, %s, %s)"
			cur.execute(sql, (title, body, url, created_at, image_src, published_at) )
			print("commitします")
			conn.commit()
			print("データをMySQLに格納しました")

	except MySQLdb.Error as e:
		print('MySQLdb.Error: ', e)

	finally:
		# コネクションを閉じる
		print("コネクションを閉じます")
		conn.close()


def main():
	""" 
	Yahoo!Newsのトップページから始める
	"""
	try:
		links = extract_data_from_yahooNews(YAHOO_NEWS, css_path['urls_from_top'], need='url')
	# 主にインターネットに接続されていない場合
	except tenacity.RetryError as e:
		# TODO: ここでのsend_errorはキューに格納すべき
		# send_error(YAHOO_NEWS, e)
		raise Exception("エラーが発生したので、処理を終了します。インターネットに接続がされているか確認してください。")

	time.sleep(SLEEP_TIME)

	# 既にDBに格納されているurlの一覧
	stored_urls = check_urls()
	
	data = {} # Python3.9からは辞書の結合は、| や |= で出来る
	for link in links:
		try:
			# 要約ページから記事ページのURLを取得する / jpegより前の部分のみ
			url = extract_data_from_yahooNews(link, css_path['url'], need='url')[0].split('?')[0]
		except tenacity.RetryError as e:
			print("エラーが発生したので、{}を飛ばします".format(url))
			print("エラー報告のため、Emailを送信しています...")
			send_error(url, e)
			continue
		else:
			# send_mail
			pass

		time.sleep(SLEEP_TIME)

		if url in stored_urls:
			print('{}は既に格納済みです'.format(url))
			continue
		else:
			try:
				# 記事ページへ移動し必要な情報を抽出する
				data[url] = extract_data_from_yahooNews(url, css_path, need="data")
			except tenacity.RetryError as e:
				print("エラーが発生したので、{}を飛ばします".format(url))
				print("エラー報告のため、Emailを送信しています...")
				send_error(url, e)
				continue
			else:
				# send_mail
				pass
				
			time.sleep(SLEEP_TIME)

	# Databaseにデータを格納する
	save_data(data)


if __name__ == '__main__':
	main()
	gc.collect()


import { useEffect, useState } from "react";

const Example = () => {
  const [time, setTime] = useState(0);

  // useEffect(実行する関数, [依存する値]);
  useEffect(() => {
    console.log("useEffect is called");
    window.setInterval(() => {
      // timeがsetTimeによって変更される
      setTime((prev) => prev + 1);
    }, 1000);
    // 第２引数は依存配列と呼ばれる
    // この配列に含めたstateが更新されるとコールバック関数が再実行される
    // 注意！依存配列内にあるstateの値をコールバック関数内で変更してはいけない
  }, []);

  // useEffect(() => {
  //   // 依存配列に渡されたtimeが１秒毎に更新されるたびに、
  //   // 以下のコンソールが出力される
  //   console.log("Updated by Effect");
  // }, [time]);

  useEffect(() => {
    // ブラウザのタブのTitleが１秒舞に更新される
    document.title = "counter" + time;
    // Firefoxのツールでは、ストレージのローカルストレージ内に表示される
    window.localStorage.setItem("time-ley", time);
  }, [time]);

  return (
    <h3>
      <time>{time}</time>
      <span>秒経過</span>
    </h3>
  );
};

export default Example;

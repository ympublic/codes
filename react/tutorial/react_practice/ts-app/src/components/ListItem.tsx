// 関数コンポーネント自体の型定義
import type { FC } from "react";
// Propsの型定義
import type { User } from "../types/user";

// props: User
// 関数コンポーネントの型定義
export const ListItem: FC<User> = (props: User) => {
  // 分割代入のデフォルト値を設定
  const { login, id, node_id, gravatar_id = "apple" } = props;

  return (
    <p>
      {login}:{id}({node_id}) / {gravatar_id}
    </p>
  );
};

// defaultPropsを使ってデフォルト値を設定
ListItem.defaultProps = {
  gravatar_id: "Young Men",
};

from typing import Final
from hero import Hero
from wand import Wand

class Wizard(object): # cookbookには(object)を書いた方が良いと書かれている

	""" 魔法使い """

	def __init__(self):
		# privateインスタンス変数
		self.__name: str 
		self.__hp: Final[int] = 35 # 再代入不可
		self.__mp: Final[int] = 30 # 再代入不可
		self.__wand: Wand = Wand() # "has-a"(包含)の関係

	# publicインスタンスメソッド
	def heal(self, h: Hero):
		""" 勇者のMPを10上昇させる """

		print(f"{self.name}はヒールを唱えた！")
		basePoint: int = 10 # 基本回復ポイント
		recoverPoint: int = int(basePoint * self.__wand.power) # getter
		h.mp = h.mp + recoverPoint # setter = getter + int
		print(f"{h.name}のMPが{recoverPoint}上昇した！")

	# Getter
	@property
	def name(self):
		return self.__name

	# Setter
	@name.setter
	def name(self, value):
		# TODO: Validation
		self.__name = value

	@property
	def hp(self):
		return self.__hp

	@hp.setter
	def hp(self, value):
		# TODO: Validation
		self.__hp = value

	@property
	def mp(self):
		return self.__mp

	@mp.setter
	def mp(self, value):
		# TODO: Validation
		self.__mp = value


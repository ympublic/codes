/*
 * クロージャ
 * /python/reference/closure/とほぼ同じ内容
 * 
 */

"use strict";

// Closure
function outer(o) {
	let i = 0;
	function inner(i) {
		console.log("iの値: " + i);
		return o + i;
	};
	console.log("oの値: " + o);
	return inner;
}

let closure = outer(10); // closure作成
console.log("１回目: " + closure(5)) // 10 + 5
console.log("２回目: " + closure(100)) // 15 + 100


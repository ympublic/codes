import os

# Django
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.views import View
from django.views.generic import (
	ListView, UpdateView, CreateView, DetailView, DeleteView
)
from django.views.generic.edit import ModelFormMixin
from django.views.generic.edit import FormMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse, reverse_lazy
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger
from django.core.mail import send_mail
from django.conf import settings
from django.views.decorators.http import require_safe, require_http_methods
from django.views.decorators.cache import cache_page
from django.views.decorators.cache import never_cache
from django.utils.functional import cached_property
from django.utils.decorators import method_decorator
from django.db import transaction # @transaction.atomic / with transaction.atomic():
from django.contrib import messages

# Application
from news.models import News
from news.forms import NewsForm
from news.forms import BSNewsForm

# My Script
from news.scripts import bsNews


UserModel = get_user_model()


####################################################
###   News
####################################################

class NewslistView(ListView, FormMixin):
	""" 取得したニュース記事の一覧を表示 """
	template_name = "news/news_list.html"
	model = News
	ordering = "-id"
	context_object_name = 'news_list' 
	# ページャにて５件の記事のみ表示させる
	paginate_by = 5 # model=Newsで取得された記事
	form_class = BSNewsForm # 不要なのだが、指定しないとErrorになるので

	# Buttonを押したらスクリプトが実行されるようにする
	def post(self, request, *args, **kwargs): # POST
		form = self.form_class(request.POST)
		if form.is_valid():
			# 自己定義スクリプトを実行
			bsNews.main()
			return redirect("news:news_list")

		return render(request, self.template_name, {'form': form})


class NewsDetailView(DetailView):
	""" データベースに格納したNewsの詳細確認 """
	template_name = "news/news_detail.html"
	model = News
	context_object_name = 'news'


class NewsRegisterView(LoginRequiredMixin, CreateView):
	""" Newsをフォームから投稿 """
	template_name = "news/news_register.html"
	form_class = NewsForm
	model = News
	# fields = ('title', 'body', 'url', 'published_at', 'image_src') # form_classと同時には使えない

	def get_form_kwargs(self, *args, **kwargs):
		kwargs = super().get_form_kwargs(*args, **kwargs)
		return kwargs

	def form_valid(self, form):
		# /scripts/python/news/bsNews.py スクリプトを実行
		bsNews.main()
		return super().form_valid(form)

	def form_invalid(self, form):
		# TODO: 失敗を報告するスクリプトを記述
		return super().form_invalid(form)

	def get_success_url(self):
		return reverse("news:news_edit", kwargs={'pk': self.object.pk})


class NewsEditView(LoginRequiredMixin, UpdateView): 
	""" データベースに格納したNewsを編集 """
	template_name = "news/news_edit.html"
	form_class = NewsForm
	model = News
	# fields = ('title', 'body', 'url', 'published_at', 'image_src')

	def get_object(self, **kwargs):
		obj = News.objects.get(pk=self.kwargs['pk'])
		return obj

	def get_form_kwargs(self, *args, **kwargs):
		kwargs = super().get_form_kwargs(*args, **kwargs)
		return kwargs

	def form_valid(self, form):
		return super().form_valid(form)

	def form_invalid(self, form):
		return super().form_invalid(form)
	
	def get_success_url(self):
		return reverse("news:news_edit", kwargs={'pk': self.kwargs['pk']})


decorators = [login_required,]
@method_decorator(decorators, name='dispatch')
class NewsDeleteView(DeleteView):
	""" News記事の削除 """
	template_name = 'news/news_delete.html'
	model = News
	success_url = reverse_lazy("news:news_list")

	def get_object(self, **kwargs):
		obj = News.objects.get(pk=self.kwargs['pk'])
		return obj

	def delete(self, request, *args, **kwargs):
		return super.delete(request, *args, **kwargs)


#! /usr/bin/env python

""" デコレーター
デコレーター: 関数を受け取り、就職した関数を返す関数
クロージャにおける引数を関数にしたものがデコレーター
"""

# デコレーター関数を定義
def my_decorater(func): # 引数に関数を渡す
	# 関数を修飾
	func.x = 1
	# 修飾した関数を返す
	return func


@my_decorater # デコレーターを指定
def func1(): # このfunc1がmy_decorater()の引数のfuncに渡される
	pass


# デコレーター関数を定義
def add_one(func): # 引数に関数を渡す
	# 一般的にはデコレーター関数内で新たに関数を定義することが多い
	# クロージャ
	def new_f(*args, **kwargs):
		# まじない
		result = func(*args, **kwargs) # func2()のreturn 3が代入される
		return result + 1
	# 修飾した関数を返す
	return new_f
	# 文字列を返す時は、以下のように書く
	# return lambda: "{0}{1}{2}".format("apple ", new_f(), " banana")


@add_one # デコレーターを指定
def func2():
	return 3 # 3 + 1が返ることになる


# 引数付きデコレーター関数を定義
# デコレーターに引数を指定したい場合は、その引数を受ける関数を書き、
# その中で、「関数を受け取り、修飾した関数を返す関数」を返す
# つまり、引数用関数の中にデコレーター関数を書く
# ２重クロージャ
def multiple(x): # 引数用関数
	def _multiple(func): # デコレーター用関数
		# クロージャ
		def new_f(*args, **kwargs):
			result = func(*args, **kwargs) # func3()のreturn 3が代入される
			return result * x
		return new_f
	return _multiple


@multiple(2) # 引数付きでデコレーターを指定。ここではmultiple()のx=2
def func3():
	return 3


# デコレーターのネスト
@multiple(2)
@add_one # 下にあるものから順番に処理されていく
def func4():
	return 3


def main():
	print("func1: {0}".format(func1.x)) # 1
	print("func2: {0}".format(func2())) # 4
	print("func3: {0}".format(func3())) # 6
	print("func4: {0}".format(func4())) # 8


if __name__ == "__main__":
	main()


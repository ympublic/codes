// Storeを定義
import { configureStore } from "@reduxjs/toolkit";
import reducer from "./modules/counter";

// configureStore: 引数にはオブジェクトを取る
export default configureStore({
  reducer: {
    counter: reducer,
  },
});

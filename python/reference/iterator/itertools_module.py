#! /usr/bin/env python

import pytest
# イテレーターに対してよく用いられる共通的な処理や生成メソッドが用意されている
import itertools
import random


@pytest.mark.solved
def test_should_use_itertools_islice():
	""" itertools.islice() """

	# イテレータ作成
	fruits = iter(["apple", "banana", "orange"])

	# itertools.islice(iterableオブジェクト, 終了インデックス)
	for fruit in itertools.islice(fruits, 2): # 最初の２要素を切り出す
		assert fruit in ("apple", "banana")


@pytest.mark.solved
def test_should_use_itertools_groupby():
	""" itertools.groupby() """

	def keyfunc(x):
		""" operator.itemgetter(0)でも代用できる """
		return x[0] # 0番目の要素をキーに指定

	def groupby_demo(rows):
		# 要素の切り出し
		# itertools.groupby(iterableオブジェクト, キー関数)
		for key, group in itertools.groupby(rows, keyfunc):
			s = key + ": "
			for row in group:
				s += repr(row) + " " # ポイント
			print(s)

	rows = iter([("A", 1), ("A", 2), ("B", 1), ("C", 1)])

	groupby_demo(rows) 
	# A: ('A', 1) ('A', 2)
	# B: ('B', 1)
	# C: ('C', 1)

	# 注意
	# この後すぐにまたrowsを呼び出すとStopIterationError!!
	# 一度rowsをもう一度定義しなおさないといけない

	def groupby_demo_gen(rows):
		""" 上記関数をジェネレーター化しassertで確認しやすく改良 """
		
		# 重複要素のグループ化
		# itertools.groupby(iterableオブジェクト, キー関数)
		for key, group in itertools.groupby(rows, keyfunc):
			s = []
			for row in group:
				s.append(row)
			yield s

	# 上記のrowsは走査終了しStopIterationになってしまっているため、
	# ここで再度rowsを定義しないといけない
	rows = iter([("A", 1), ("A", 2), ("B", 1), ("C", 1)])

	gd = groupby_demo_gen(rows)
	
	assert [('A', 1),('A', 2)] == next(gd)

	assert [('B', 1)] == next(gd)

	assert [('C', 1)] == next(gd)


	# 注意: キーが連続していない場合はitertools.groupby()ではグループ化されない
	rows = iter([("A", 1), ("B", 1), ("C", 3), ("A", 2)])

	groupby_demo(rows)
	# A: ('A', 1)
	# B: ('B', 1)
	# C: ('C', 1)
	# A: ('A', 2)

	# このような場合は、あらかじめデータをキーでソートしておく
	rows = sorted(rows, key=keyfunc)

	groupby_demo(rows)
	# A: ('A', 1) ('A', 2)
	# B: ('B', 1)
	# C: ('C', 1)


@pytest.mark.solved
def test_should_use_itertools_tee():
	""" itertools.tee() """

	# イテレータをコピー
	# iterator.tee(iterableオブジェクト, コピー数)

	# イテレータ作成
	series = iter([1, 2, 3, 4, 5, 6])

	# ３つコピーする
	copy1, copy2, copy3 = itertools.tee(series, 3)

	# それぞれ独立なイテレータとなる
	assert not copy1 is copy2

	assert 1 == int(next(copy2))

	assert 1 == int(next(copy3))

	assert 2 == int(next(copy2))

	# StopIterationまで進む
	assert [(1, 3, 2), (2, 4, 3), (3, 5, 4), (4, 6, 5)] == list(zip(copy1, copy2, copy3))

	# 参照コピーする場合

	copy4 = series

	assert copy4 is series


@pytest.mark.solved
def test_should_use_itertools_repeat():
	""" itertools.repeat() """

	l = "SHOUHEI OHTANI" # シーケンス

	# 同じ要素が呼び出され続けるイテレータを生成
	# itertools.repeat(関数, リピート数)
	c = [f(l) for f in itertools.repeat(random.choice, 20)]

	s = "".join(c)

	assert len(s) == 20

	assert s[4] in l


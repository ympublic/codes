import { hello, User } from "./module.js";

// default export は{}が不要
import funcB from "./module.js";

hello();

const user = new User("John Lennon");
user.hello();

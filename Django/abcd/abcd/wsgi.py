"""
WSGI config for abcd project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/ja/4.1/howto/deployment/wsgi/
"""

import os
import sys # 追記

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'abcd.settings')
sys.path.append("/home/ec2-user/codecommit/forgitxxx/abcd") # 追記

application = get_wsgi_application()

# 重要！ Testの実行順序は上から下ではない
# cf) https://runebook.dev/ja/docs/django/topics/testing/overview?page=2
# cf) https://inokara.hateblo.jp/entry/2018/03/31/092643#%E7%9F%A5%E8%A6%8B-4-%E3%83%86%E3%82%B9%E3%83%88%E3%81%AE%E5%AE%9F%E8%A1%8C%E9%A0%86%E5%BA%8F%E3%82%92%E5%88%B6%E5%BE%A1%E3%81%99%E3%82%8B-%E3%81%A8%E3%81%84%E3%81%86%E3%81%93%E3%81%A8%E8%87%AA%E4%BD%93%E3%81%8C%E6%AD%A3%E3%81%97%E3%81%84%E3%81%AE%E3%81%8B%E5%90%A6%E3%81%8B%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6%E3%82%82

# 重要：基本的に、「１つのテストは１つのことを検証すること」が推奨されている
# DjangoのテストはDEBUG=Falseを指定したものとして実行される
# デフォルトではCSRFチェックが無効となっている 有効にする場合：c = Client(enforce_csrf_checks=True)

# テストデータベースの破棄を防ぐ場合
# test --keepdb
# テストを並列実行させたい場合
# test --parallel
# パスワードのハッシュ生成
# PASSWORD_HASHES = [
#   'django.contrib.auth.hashers.MD5PasswordHasher',
# ]


# コードカバレージを確認
# coverage run --source='.' manage.py test
# coverage report

import os
import sys
import pdb # pdb.set_trace() or breakpoint()
from datetime import datetime
from unittest import mock

# Django
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponseRedirect
from django.test import TestCase, RequestFactory, Client
from django.urls import resolve, reverse, reverse_lazy
from django.test import tag
from django.template import Template, Context
from django.shortcuts import get_object_or_404
from django.db import transaction
from django.db import models
from django.db import IntegrityError
from django.db import DatabaseError
from django.core.exceptions import ValidationError
from django.test.utils import override_settings # @override_settings()
from django.conf import settings

# Applicaion
from shopping import views
from shopping.models import (
	Item, ItemPhoto, ItemStock, ItemReview,
	G1, Foal, CartItem, Order, OrderDetail,
)
#from shopping.forms import ( )
#from shopping.factories import ContactFactory # Fake

UserModel = get_user_model()


####################################################
#####     トランザクションテスト
####################################################

@tag('shopping',)
class PaymentDetailTest(TestCase): # ./manage.py test --tag=shopping --exclude-tag=complete
	""" ここではトランザクション部分のみテスト """

	def setUp(self):
		""" 初期設定 """
		# GETやPOSTメソッドのRequestオブジェクトを作成する
		self.client = Client() # response = self.client.get() 
		self.url = "/shopping/" # reverse(shopping:shopping_top)
		self.factory = RequestFactory() # request = self.factory.get()
		self.user = UserModel.objects.create_user(
			email='oiocha@itoen.com',
			password='ocyacya',
		)
		# TestではデータをCREATEしないといけない
		self.item = Item.objects.create(
			name='EFFORIA',
			price=300,
			runs=11,
			wins=6,
			created_at=datetime.now(),
			updated_at=datetime.now(),
		)
		self.item_stock = ItemStock.objects.create(
			item=self.item,
			stock=100,
		)
		self.rollback = False

	@tag('shopping', 'complete',)
	def test_should_pass_the_transaction(self):
		""" 正常なトランザクション """
		quantity = 20
		original_stock = ItemStock.objects.get(item_id=self.item).stock
		# CartItemテーブルに商品を入れる
		cart = CartItem.objects.create(user=self.user, item=self.item, 
				quantity=quantity,)

		# CartItemテーブルから該当ユーザーの情報を取得する
		cart_items = CartItem.objects.select_related('item').filter(user=self.user)
		for cart_item in cart_items:
			# トランザクション
			# ItemStockのstockを増減
			try:
				with transaction.atomic():
					# デッドロックに注意
					item_stock = ItemStock.objects.select_for_update().get(item_id=cart_item.item_id)
					item_stock.stock -= cart_item.quantity
					item_stock.save()
					print("★★★ 正常に処理されました ★★★")
			except IntegrityError: # 在庫不足の時
				# Rollbackが発生してDBに保存されなかったフィールドの値については自分で戻す必要がある
				# print("IntegrityError: {}".format(item_stock.stock))
				print("★★★ 在庫不足のためRoolbackが発生しました ★★★")
				item_stock.stock += cart_item.quantity
				self.rollback = True
				continue
			except DatabaseError: # データベースエラーの時
				# Rollbackが発生してDBに保存されなかったフィールドの値については自分で戻す必要がある
				# print("DatabaseError: {}".format(item_stock.stock))
				print("★★★ DatabaseのErrorが発生しました ★★★")
				item_stock.stock += cart_item.quantity
				self.rollback = True

		changed_stock = ItemStock.objects.get(item_id=self.item).stock

		self.assertEqual(changed_stock, original_stock - quantity)
		self.assertEqual(self.rollback, False)

	@tag('shopping', 'complete',)
	def test_should_be_rollbacked(self):
		""" 強制的に在庫不足を引き起こし、ロールバックを発生させる """
		bad_quantity = 200
		original_stock = ItemStock.objects.get(item_id=self.item).stock
		# CartItemテーブルに商品を入れる
		cart = CartItem.objects.create(user=self.user, item=self.item, 
				quantity=bad_quantity,)

		# CartItemテーブルから該当ユーザーの情報を取得する
		cart_items = CartItem.objects.select_related('item').filter(user=self.user)
		for cart_item in cart_items:
			# トランザクション
			# ItemStockのstockを増減
			try:
				with transaction.atomic():
					# デッドロックに注意
					item_stock = ItemStock.objects.select_for_update().get(item_id=cart_item.item_id)
					item_stock.stock -= cart_item.quantity
					item_stock.save()
					print("★★★ 正常に処理されました ★★★")
			except IntegrityError: # 在庫不足の時
				# Rollbackが発生してDBに保存されなかったフィールドの値については自分で戻す必要がある
				# print("IntegrityError: {}".format(item_stock.stock))
				print("★★★ 在庫不足のためRoolbackが発生しました ★★★")
				item_stock.stock += cart_item.quantity
				self.rollback = True
				continue
			except DatabaseError: # データベースエラーの時
				# Rollbackが発生してDBに保存されなかったフィールドの値については自分で戻す必要がある
				# print("DatabaseError: {}".format(item_stock.stock))
				print("★★★ DatabaseのErrorが発生しました ★★★")
				item_stock.stock += cart_item.quantity
				self.rollback = True

		changed_stock = ItemStock.objects.get(item_id=self.item).stock

		self.assertEqual(changed_stock, original_stock)
		self.assertEqual(self.rollback, True)



##################################################################
#####     Amazon S3とのファイルの送受信テスト（フォーム未使用）
##################################################################
# boto3公式 https://boto3.amazonaws.com/v1/documentation/api/latest/guide/quickstart.html
# cf) https://serip39.hatenablog.com/entry/2020/07/31/210000
# cf) https://secure-strings.com/handle-django-media-file-by-aws-s3/

import logging
import boto3
from boto3.session import Session
from pathlib import Path
from botocore.exceptions import ClientError

@tag("AWS")
class ConnectingAmazonS3Test(TestCase):

	def setUp(self):
		# bucket
		self.s3 = boto3.resource('s3')
		self.bucket_name = settings.AWS_STORAGE_BUCKET_NAME
		self.bucket = self.s3.Bucket(self.bucket_name)
		self.region_name = settings.AWS_S3_REGION_NAME

		# session
		self.session = boto3.session.Session(
			aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
			aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
		)
		# s3 = session.resource('s3')
		# s3.Bucket(AWS_BUCKET_NAME).put_object(Key=cloudFilename, Body=fileToUpload)

		# client
		self.s3_client = boto3.client(
			's3',
			aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
			aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
			region_name=settings.AWS_S3_REGION_NAME,
		)

		# ファイル作成
		self.upload_file = 'test_upload.txt'
		self.download_file = 'test_download.txt'
		self.object_name = os.path.basename(self.upload_file)
		self.file_body = 'This is a test file.\n'

	def test_bucket_name_should_be_included_in_s3_bucket(self):
		""" バケットが存在していることを確認  """
		
		# バケットがS3上に存在するか
		list_buckets = self.s3_client.list_buckets()
		bucket_list = [b.get('Name') for b in list_buckets.get('Buckets')]
		self.assertEqual(self.bucket_name in bucket_list, True)

	def test_a_file_should_be_sent_to_Amazon_S3(self):
		""" バケットへのファイル送信を確認 ＆ アップロードしたファイルを削除"""

		# S3のバケットにファイルを送信
		# TODO: zipfile化(shutilかzipfileモジュールを使用: zipfileがファイル毎、shutilがフォルダごと)
		with open(self.upload_file, mode='w', encoding='UTF-8') as f:
			f.write(self.file_body)
		try:
			self.bucket.upload_file(str(Path(self.upload_file)), self.object_name)
			print("Amazon S3にファイルを送信しました")
		except ClientError as e:
			print("Amazon S3へのファイル送信に失敗しました")
			logging.error(e)

		# バケットにアップロードしたファイルが存在するか確認
		list_objects = self.s3_client.list_objects(Bucket=self.bucket_name)
		object_list = [b.get('Key') for b in list_objects.get('Contents')] # [obj.key for obj in self.bucket.objects.all()]
		self.assertEquals(self.object_name in object_list, True)

		# アップロードしたファイルを削除
		os.remove(self.upload_file)
		self.assertEquals(os.path.exists(self.upload_file), False)

	def test_should_download_a_file_from_Amazon_S3(self):
		""" バケットからファイルをダウンロードできているか ＆ アップロードしたファイルとダウンロードしたファイルを削除 """
		
		# S3のバケットからファイルをダウンロード 
		self.bucket.download_file(self.object_name, str(Path(self.download_file)))
		self.assertEqual(os.path.exists(self.download_file), True)

		# バケットにアップロードしたテストファイルを削除
		response = self.bucket.delete_objects(
			Bucket = self.bucket_name,
			Delete = {
				'Objects': [
					{'Key': self.object_name}, # S3は基本的に最初の文字は大文字
				]
			}
		)
		list_objects = self.s3_client.list_objects(Bucket=self.bucket_name) 
		object_list = [b.get('Key') for b in list_objects.get('Contents')] # [obj.key for obj in self.bucket.objects.all()]
		self.assertEquals(self.object_name in object_list, False)

		# ダウンロードしたテストファイルを削除
		os.remove(self.download_file)
		self.assertEquals(os.path.exists(self.download_file), False)



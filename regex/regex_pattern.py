#! /usr/bin/env python
# -*- coding: utf-8 -*-

""" 正規表現パターン一覧 """

import re
import pytest # pytest -v regex_pattern.py --pdb


# フィクスチャ作成
@pytest.fixture(scope="function")
def regex_pattern(): 
	""" 正規表現のパターンをコンパイル """

	# setUp

	# 辞書型で正規表現パターンを格納
	regex_pattern = {
		# 文頭に^が必要、row string記法が必要
		# 文の末尾に/が要るかどうかは環境次第だと思う
		"url": re.compile(r"^https?://[\w!?/+\-_~=;.,*&@#$%()'[\]]+/$"),
		"email": re.compile(r"[\w\-._]+@[\w\-._]+\.[A-Za-z]+"),
		# Wikipediaの「平仮名（Unicodeのブロック）」より
		"hiragana": re.compile("[\u3041-\u309f]"),
		"katakana": re.compile("[\u30a1-\u30ff]"),
		"kanji": re.compile("[\u4e00-\u9fff]"),
	}

	yield regex_pattern

	# tearDown


# pytest.iniに登録していないので、@pytest.mark.unsolvedはPytestUnknownMarkWarningが発生
# @pytest.mark.unsolved
# 複数のパターンをテストする
@pytest.mark.parametrize(
	"url", 
	[
		"https://www.google.com/search?client=firefox-b-d&q=%E6%AD%A3%E8%A6%8F%E8%A1%A8%E7%8F%BE/",
	],
)
def test_should_search_url_pattern(regex_pattern, url):
	""" 
	URLのパターンを検索
	"""

	# 正規表現パターン
	pattern = regex_pattern.get("url")

	# 部分一致などはまた後ほど
	assert pattern.search(url)



@pytest.mark.parametrize(
	"evil_url", 
	[
		"https://evil/.example.jp@evil",
		"jojohttp://evil.com/",
		"javascript://www.example.com/%0Aalert(1)"
	],
)
def test_should_check_evil_url_pattern(regex_pattern, evil_url):
	""" 
	URLのパターンを検索
	参考: 
	"""

	# 正規表現パターン
	pattern = regex_pattern.get("url")

	# evilなパターンを弾いて欲しい
	assert not pattern.search(evil_url)


def test_should_search_email_pattern(regex_pattern):
	""" 
	Emailのパターンを検索
	"""

	pass


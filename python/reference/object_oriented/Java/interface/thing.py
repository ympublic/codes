from abc import ABC # Abstract Base Class
from abc import ABCMeta
from abc import abstractmethod

# Interface
class Thing(ABC): 

	"""  """

	# Interfaceはフィールドを１つも持たないので、コンストラクタも不要

	# Interface内の全てのメソッドは抽象メソッド
	@abstractmethod
	def getWeight(self) -> float:
		raise NotImplementedError()

	@abstractmethod
	def setWeight(self, weight: float) -> None:
		raise NotImplementedError()


/*
 * 選択ソート（基本選択法）
 * 最大値のデータを選択して、末尾に置く選択ソート
 * 
 */

"use strict";


var data = [80, 60, 40, 70, 30, 10, 50, 20]; // new Array(80, 60, ...);

console.log(data.length) // 8

let max; // 再代入可能な変数
let work; // 再代入不可な変数はconst

// 外側ループ(外=件数, 2, -1)
for (let o = data.length - 1; o >= 1; o-=1) { // o: out 
	max = o; // 最大番号 <- outer, 末尾のデータを最大値とする
	// 内側ループ(内=1, 外-1, 1), 最大値のデータを選択する
	for (let i = 0; i <= o-1; i+=1) { // i: in
		if(data[i] > data[max]){
			max = i; // 最大番号 <- inner
		}
	}
	// ワークエリアを使用し、データ（外）とデータ（最大番号）を交換する
	work = data[o];
	data[o] = data[max]; // 最大値のデータを末尾に置く
	data[max] = work;
}

console.log(data)
/* [
  10, 20, 30, 40,
  50, 60, 70, 80
]
*/


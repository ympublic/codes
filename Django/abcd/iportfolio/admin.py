# Django
from django.contrib import admin

# Application
from iportfolio.models import (Contact, ContactStatus,)


####################################################
#####     Contact
####################################################

class ContactAdmin(admin.ModelAdmin):
	model = Contact
	# list_display = ('__str__',)を指定するとmodels.pyのdef __str__()の内容が表示される
	list_display = ('name', 'email', 'subject', 'message', 'is_closed', 'note',) # adminのチェンジリストページでどのフィールドを表示するか
	list_editable = ('note',) # list_displayに指定されたフィールドの簡易編集
	# list_display_links = ('name',) # レコードの詳細へ飛べるフィールドを選択
	# list_filter = ('is_active',) # フィルター項目からフィルタリングできる
	ordering = ('-recieved_at',) # ソート
	search_field = ('email',) # 検索フィールド
	# actions = ['action',] # アクション / 管理画面の「操作」項目で選択できる

	# カスタムカラム（list_display内で指定）
	def is_closed(self, obj): 
		return obj.contactstatus.is_closed

	# def get_queryset(self, request): 
	# 	qs = super().get_queryset(request)
	# 	return qs

	# def action(self, request, queryset): pass # 「選択された項目の日時を更新する」など

admin.site.register(Contact, ContactAdmin) # ContactAdminクラスを適用


@admin.register(ContactStatus) # デコレーターでも登録可
class ContactStatusAdmin(admin.ModelAdmin):
	model = ContactStatus
	list_display = ('contact', 'email', 'is_emailed', 'is_replied', 'is_closed', 'note',) # 表示カラム
	# list_select_related = ('contact', ) # select_related('contact')を使う場合
	list_editable = ('note',) # list_displayで指定されたフィールドの簡易編集
	# list_display_links = ('name',) # レコードの詳細へ飛べるフィールドを選択
	list_filter = ('is_emailed', 'is_replied', 'is_closed', 'contact__email') # フィルター項目からフィルタリングできる
	ordering = ('is_replied', 'is_closed') # ソート
	search_field = ('is_emailed', 'is_replied', 'is_closed',) # 検索フィールド
	# actions = ['action',] # アクション / 管理画面の「操作」項目で選択できる

	# def get_queryset(self, request): 
	# 	qs = super().get_queryset(request).select_related('contact')
	# 	return qs

	# カスタムカラム（list_display内で指定）
	# @admin.display(ordering='contact__email')
	def email(self, obj):
		""" Contactのemailフィールドを表示させる場合 """
		return obj.contact.email


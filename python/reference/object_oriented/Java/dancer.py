from character import Character
from matango import Matango

class Dancer(Character):

	"""  """

	def __init__(self, name: str):
		super().__init__(name)
		# 注意！ Pythonではsuper().__initi__()で指定したインスタンス変数以外は継承されない
		# getter, setterも定義し直さないといけない
		self.__hp: int = 30 # 初期値

	def dance(self):
		# self.__nameではない点に注意
		print(f"{self.name}は情熱的に踊った！")

	def attack(self, m: Matango):
		print(f"{self.name}の攻撃！")
		print(f"敵に3ポイントのダメージ！")
		m.hp -= 3

	def run(self):
		print(f"{self.name}はお尻をくねくねと妖艶に振りながら逃げ出した！")

	@property 
	def hp(self):
		return self.__hp

	@hp.setter 
	def hp(self, value):
		self.__hp = value


import { createSlice } from "@reduxjs/toolkit";

const counter = createSlice({
  name: "counter",
  initialState: 0,
  reducers: {
    // メソッドで記述する
    add(state, { type, payload }) {
      return state + payload;
    },
    minus(state, { type, payload }) {
      return state - payload;
    },
  },
});

// Action Creater
const { add, minus } = counter.actions;

// 以下のAction Createrは、ReduxのToolkitを使った場合は、
// 上記のように記述すれば自動的に作成される
// const add = (payload) => {
//   return {
//     type: "counter/+",
//     payload,
//   };
// };
// const minus = (payload) => {
//   return {
//     type: "counter/-",
//     payload,
//   };
// };

export { add, minus };
export default counter.reducer;

#! /usr/bin/env python

"""Pickle
オブジェクトをファイルにセーブしたり、ネットワークで送信する際には、
直列化が必要になる
Pythonでは直列化にはpickleモジュールを使用する

警告！
https://docs.python.org/ja/3/library/pickle.html
pickleモジュールは安全ではありません
信頼できるデータのみを非pickle化してください
"""

import pytest # pytest -v -m "unsolved" test_pickle.py --pdb
import pickle
import os


@pytest.mark.solved
def test_should_dumps_and_loads_strings():
	"""
	文字列ベースのオブジェクトの直列化と復元
	pickle.dumps(直列化するオブジェクト) # 最後のsはstringのｓ?
	pickle.loads(復元するオブジェクト)
	"""

	data = {
		"大阪杯": "ジャックドール",
		"桜花賞": "リバティアイランド",
		"皐月賞": "ソールオリエンス",
	}

	# 直列化
	serialized = pickle.dumps(data)

	# 固有のバイト列に変換されている
	# Python3はbytes型になる
	assert not serialized == data

	# 直列化を復元
	deserialized = pickle.loads(serialized)

	# 元のデータと内容は一致
	assert deserialized == data

	# ただし、同じオブジェクトではない
	assert not deserialized is data


@pytest.mark.solved
def test_should_dump_and_load_stream(tmpdir): # tmpdirフィクスチャ
	"""
	ファイルオブジェクトなどのストリーム上での直列化と復元
	pickle.dump(直列化するオブジェクト, ファイルオブジェクト) # dumps()ではない点に注意
	pickle.load(ファイルオブジェクト)
	"""

	data = {
		"大阪杯": "ジャックドール",
		"桜花賞": "リバティアイランド",
		"皐月賞": "ソールオリエンス",
	}

	# ファイル保存
	with open(f"{tmpdir}{os.sep}data.pkl", "wb") as f:
		# 直列化（ファイルに保存）
		pickle.dump(data, f)

	# ファイルの読み込み
	with open(f"{tmpdir}{os.sep}data.pkl", "rb") as f:
		# 直列化を復元
		deserialized = pickle.load(f)

	# 元のデータと内容は一致
	assert deserialized == data


// 非同期処理（await, async）
let a = 0;

init();
async function init() {
  try {
    const result = await new Promise((resolve, reject) => {
      setTimeout(() => {
        a = 1;
        reject(a);
        // debugger;
      }, 2000);
    });
    console.log(result);
  } catch (err) {
    console.log("catchが実行される", err);
  }
}

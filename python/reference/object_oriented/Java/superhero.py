from hero import Hero
from monster import Monster

######################################################################################
# 正しい継承: 「is-aの原則」に則っている継承
# is-aの関係: 子クラス is-a 親クラス(子クラスは、親クラスの一種である)
# 子クラスになるほど具体化(特化)していき、親クラスになるほど一般化(汎化)していく
# 継承は「ある２つのクラスに特化・汎化の関係があることを示す」ための機能
# is-aの原則が成立しないなら、楽が出来るとしても継承を使ってはいけない
# ①将来クラスを拡張していった場合に現実世界との矛盾が生じるから
# ②オブジェクト指向の３大機能の１つ「ポリモーフィズム」を利用できなくなるから
######################################################################################

# JAVAでは多重継承は許可されていないが、PythonではDjangoのように多重継承は可能
class SuperHero(Hero): # Heroクラスを継承

	"""  """

	def __init__(self, name:str): # 親クラスと同じくnameも必要
		# JAVAは内側のインスタンス部のコンストラクタから呼び出される
		# JAVAはコンストラクタの最初の行でsuper(引数);を呼び出さなければならない
		super().__init__(name) # 親クラスのコンストラクタ
		# AttributeError: 'SuperHero' object has no attribute '_SuperHero__flying'のような
		# エラーが出た場合は、メンバ変数に値が設定されていない場合である
		self.__flying: bool = False # 追加したメンバ変数

	# publicインスタンスメソッド
	def fly(self):
		self.__flying = True
		print(f"{self.name}は飛び上がった！")

	def land(self):
		self.__flying = False
		print(f"{self.name}は着地した！")

	# 親クラスのメソッドをオーバーライド
	# 多重構造のインスタンスは、極力外側にある子インスタンス部分のメソッドで対応しようとする
	# ここではSuperHeroのrun()
	def run(self):
		print("撤収ッ！！")

	# 攻撃対象をザックリとMonsterとして捉えたポリモーフィズム
	def attack(self, m:Monster):
		# super()を使えば、親インスタンス部のメソッドやフィールドに子インスタンス部からアクセス出来る
		super().attack(m) # 親インスタンス部のattack()を呼び出し
		if self.__flying:
			print(f"さらに空から{self.name}の追加攻撃！")
			super().attack(m) # 親インスタンス部のattack()を呼び出し


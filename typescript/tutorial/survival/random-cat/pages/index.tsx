// この型を注釈しておくと、関数の実装gあページコンポーネントの要件を満たしているかチェックできる

// アロー関数で書く場合
import { NextPage, GetServerSideProps } from "next";
import { useEffect, useState } from "react";
import styles from "./index.module.css";

// getServerSidePropsから渡されるpropsの型
type Props = {
  initialImageUrl: string;
};

// ページコンポーネント関数にpropsを受け取る引数を追加する
const IndexPage: NextPage<Props> = ({ initialImageUrl }) => {
  // useStateを使って状態を定義する
  // 画像のURLが代入される変数
  const [imageUrl, setImageUrl] = useState(initialImageUrl); // 初期値を渡す
  // APIを呼び出し中かどうかを管理(true: 呼び出し中)
  const [loading, setLoading] = useState(false); // 初期値をfalseにしておく
  // マウント時に画像を読み込む宣言
  //useEffect(() => {
  // 注: useEffectに非同期関数を直接渡すことは出来ない
  // つまり、useEffect(async () => {})とは書けない
  //fetchImage().then((newImage) => {
  // 画像URLの状態を更新する
  //setImageUrl(newImage.url);
  // ローディング状態を更新する
  //setLoading(false);
  //});
  //}, []);

  // ボタンをクリックした時に画像を読み込む処理
  const handleClick = async () => {
    // 読み込み中フラグを立てる
    setLoading(true);
    const newImage = await fetchImage();
    // 画像urlの状態を更新
    setImageUrl(newImage.url);
    // 読み込み中フラグを倒す
    setLoading(false);
  };

  // ローディング中でなければ、画像を表示する
  // 注: JSXの{}で囲った部分には文が書けないので、{if (!loading) }のようには書けない
  // 条件分岐を使用したい場合は、代わりに論理演算子や参考演算子を利用する
  return (
    <div className={styles.page}>
      <button onClick={handleClick} className={styles.button}>
        他のにゃんこも見る
      </button>
      <div className={styles.frame}>{loading || <img src={imageUrl} />}</div>
    </div>
  );
};

export default IndexPage;

// サーバサイドで実行する処理
// getServerSideProps: ページがリクエストされるたびにサーバサイドで実行され、ページのプロパティを返す
// リクエストごとのページのデータを取得できる
// またクライアントサイドでルーティングが発生した場合もこの関数がサーバサイドで実行される
export const getServerSideProps: GetServerSideProps<Props> = async () => {
  const image = await fetchImage();
  return {
    props: {
      initialImageUrl: image.url,
    },
  };
};

// 関数宣言で書く場合
/**
import { ReactElement } from "react";

export default function IndexPage(): ReactElement<any, any> | null {
	return <div>猫描画予定地（関数宣言）</div>;
}
*/

// 非同期処理
const fetchImage = async (): Promise<Image> => {
  // 注意: awaitはasync関数の中でしか使えない
  // fetch: HTTPリクエストでリソースを取得するブラウザ標準のAPI
  const response = await fetch("https://api.thecatapi.com/v1/images/search");
  const images = await response.json();
  // 配列として表現されているか？
  // https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Global_Objects/Array/isArray
  if (!Array.isArray(images)) {
    throw new Error("猫の画像が取得できません");
  }
  const image: unknown = images[0];
  //Imageの構造を成しているか＿
  if (!isImage(image)) {
    throw new Error("猫の画像が取得できませんよ");
  }
  return image;
};

// 型ガード関数
const isImage = (value: unknown): value is Image => {
  // 値がオブジェクトなのか？
  if (!value || typeof value !== "object") {
    return false;
  }
  // urlプロパティが存在し、かつ、それが文字列なのか？
  return "url" in value && typeof value.url === "string";
};

// ブラウザのコンソール上にAPIからのURLが取得されているか確認
//fetchImage().then((image) => {
//console.log(image.alt);
//});

type Image = {
  url: string;
};

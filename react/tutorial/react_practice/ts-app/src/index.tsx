import React from "react";
import { createRoot } from "react-dom/client";
import { App } from "./App";

// TypeScript を使う場合は document.getElementById('root') が
// undefined になる可能性があるとして警告が出るので
//  ! (Non-Null Assertion Operator) を使うと良い
const root = createRoot(document.getElementById("root")!);

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

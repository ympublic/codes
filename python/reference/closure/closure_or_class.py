#########################################
#    クロージャで書く場合
#########################################
# 注意：python2で書くのと、python3で書くのとでは大きく異なる
# Python3型はclosure_or_class3.ipynbにて

import string

# 全キャラクタによる再利用可能な文字列を作る
# 「全く変換しない」変換テーブルの役目も担わせる
allchars = string.maketrans('', '')

# Python2型
def makefilter2(keep):
	""" 
	文字列を取り、keepにあるキャラクタのみによる部分コピーを返すような関数を渡す
	"""

	# keepにないキャラクタのみから成る文字列を作る
	# これはkeepの補集合で、削除しなければならないキャラクタ群
	delchars = allchars.translate(allchars, keep) # 以下の例で言えば、aiueo以外のアルファベット文字

	# 目的のフィルタリング関数を作ってクロージャとして返す
	def thefilter(s):
		#translate(table, 削除文字列)
		return s.translate(allchars, delchars) # keep文字以外を全て削除

	return thefilter
	# または thefilterを定義する代わりに、
	# return lambda s: s.translate(allchars, delchars)


if __name__ == '__main__':
	# クロージャ作成。keep='aiueo'を持った関数thefilter()が返る
	just_vowels = makefilter('aiueo')

	print(just_vowels('Wow! This is the show time!!'))
	# oiieoie

	print(just_vowels('Shouhei Ohtani is one of the best baseball player!!'))



##########################
#    クラスで書く場合
##########################

class Keeper(object):
	""" (Unicode文字列の)キャラクタ置換や削除を行う """

	# コンストラクタ
	def __init__(self, keep): # self: インスタンス自身
		self.keep = set(map(ord, keep)) # Unicode値に変換

	# self[key]の値評価を実現するために呼び出される
	# オブジェクトに[]でアクセスした際の挙動を定義する
	# ex: a = Sample() a["foo"] a[1]など
	def __getitem__(self, n):
		if n not in self.keep:
			return None
		return unichr(n)

	# インスタンスが関数として呼ばれた際に呼び出される
	def __call__(self, s):
		return s.translate(self)


# インスタンス作成
makefilter = Keeper

if __name__ == '__main__':
	just_vowels = makefilter('aiueo') # keep='aiueo'

	print(just_vowels('Wow! This is the show time!!'))
	# oiieoie

	print(just_vowels('Shouhei Ohtani is one of the best baseball player!!'))



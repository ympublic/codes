import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import { asyncCount } from "../../api/counter";

const counter = createSlice({
  name: "counter",
  initialState: {
    count: 0,
    status: "",
  },
  reducers: {
    add(state, { type, payload }) {
      state.count = state.count + payload;
      // const newState = { ...state };
      // newState.count = state.count + payload
      // return newState;
    },
    minus(state, { type, payload }) {
      state.count = state.count - payload;
      // const newState = { ...state };
      // newState.count = state.count - payload
      // return newState;
    },
  },
  extraReducers: (builder) => {
    // pendingは自動的に設定される
    builder
      .addCase(addAsyncWithStatus.pending, (state) => {
        state.status = "Loading...";
      })
      // 非同期処理が終わった場合、続けて記載できる
      // 非同期処理が成功して終了した場合はfulfilledとなる
      .addCase(addAsyncWithStatus.fulfilled, (state, action) => {
        state.status = "Finished...";
        state.count = state.count + action.payload;
      })
      // 非同期処理でエラーが発生した場合はrejectedとなる
      .addCase(addAsyncWithStatus.rejected, (state) => {
        state.status = "Error...";
      });
  },
});

const { add, minus } = counter.actions;

// createAsyncThunkでtype(第１引数)を一意に設定して、
// 第２引数で非同期処理を記述する
// 非同期処理のreturnで戻す値がactionのpayloadに乗ってくるので、
// extraReducersにpending, fulfilled, rejectedで分岐する
const addAsyncWithStatus = createAsyncThunk(
  "counter/asyncCount",
  // 非同期処理
  async (payload) => {
    const response = await asyncCount(payload);
    // returnで戻したい値を返す
    return response.data;
  }
);

const addAsync = (payload) => {
  return async (dispatch, getState) => {
    const state = getState();
    console.log(state);
    const response = await asyncCount(payload);
    dispatch(add(response.data));
  };
};

export { add, minus, addAsync, addAsyncWithStatus };
export default counter.reducer;

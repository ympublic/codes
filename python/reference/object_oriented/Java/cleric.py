from typing import Final
import random
from wand import Wand

class Cleric(object):

	""" 僧侶 """

	# クラス変数
	MAX_HP: int = 50
	MAX_MP: int = 10

	def __init__(self):
		# publicインスタンス変数
		# 実際は以下のpublic変数は全てカプセル化（private化し、setter, getterを定義）すべき
		self.name: str 
		self.hp: int = 50
		self.mp: int = 10

	# publicインスタンスメソッド
	def selfAid(self):
		"""  """

		print(f"{self.name}はセルフエイドを唱えた！")
		if self.mp >= 5:
			self.hp = Cleric.MAX_HP # クラス変数へアクセス
			self.mp -= 5
			print(f"{self.name}の体力が最大まで回復した！")
		else:
			print(f"しかしMPが足りなかった！")

	def pray(self, sec: int) -> int:
		"""  """

		print(f"{self.name}は{sec}秒間天に祈った！")
		
		# 論理上の回復量を乱数を用いて決定
		recover: int = random.randint(0, 3) # 0 <= n <= 3の整数

		# 実際の回復量を計算
		recoverActual: int = min([Cleric.MAX_MP - self.mp, recover]) # クラス変数へアクセス

		self.mp += recoverActual
		print(f"{self.name}のMPが{recoverActual}回復した！")

		return recoverActual


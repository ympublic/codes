#! /usr/bin/env python
""" ord()で変換した文字をchr()にて戻す """
import sys
# sys.setdefaultencoding("utf-8")

if __name__ == "__main__":
	# exp = map(ord, "Hello World!")
	exp = (72, 101, 108, 108, 111, 32, 87, 111, 114, 108, 100, 33)
	# 注意: chr()とstr()は異なる
	print("".join([chr(w) for w in exp]))

	print("文字コード確認")
	print("sys.stdin.encoding: {}".format(sys.stdin.encoding))
	print("sys.stdout.encoding: {}".format(sys.stdout.encoding))
	print("sys.stderr.encoding: {}".format(sys.stderr.encoding))

	# utf-8の文字列をshift-jisにencode -> decodeしShift-JISの文字列
	print("Shift-JIS: {}".format("あいうえお".encode("shift-jis").decode("shift-jis")))
	
from django.urls import path, include
from rest_framework import routers
from . import views

app_name = 'drf'

# Routerクラスを用いて、パスとViewSetを紐付ける
# Routerクラスをインスタンス化
default_router = routers.DefaultRouter()
# category/ にCategoryViewSetをルーティング
default_router.register("music_category", views.CategoryViewSet)
# item/ にItemViewSetをルーティング
default_router.register("music_item", views.ItemViewSet)

urlpatterns = [
	# Routerクラスを使用すると、簡潔に書ける
	# default_routerをincludeする
	path("api/", include(default_router.urls), name="drf_top"),
]

# https://127.0.0.1:8000/drf/api/ にアクセス
# https://127.0.0.1:8000/drf/api/rest_cateory/ にアクセス

# 外部からAPIにアクセスして、データを取得する場合
# https://127.0.0.1:8000/drf/api/item_format=json


import { useEffect, useState } from "react";
import { ListItem } from "./components/ListItem";
import axios from "axios";
// API情報の型定義
import type { User } from "./types/user";

export const App = () => {
  // 取得したユーザー情報
  // never型によるエラーを解決する
  // cf: https://zenn.dev/kaz_z/articles/usestate-type
  const [users, setUsers] = useState<User[]>([]);

  // 画面表示時にユーザー情報取得
  useEffect(() => {
    axios.get<User[]>("https://api.github.com/users").then((res) => {
      setUsers(res.data);
    });
  }, []);

  return (
    <div>
      {users.map((api: User) => (
        <ListItem
          login={api.login}
          id={api.id}
          node_id={api.node_id}
          gravatar_id={api.gravatar_id}
        />
      ))}
    </div>
  );
};

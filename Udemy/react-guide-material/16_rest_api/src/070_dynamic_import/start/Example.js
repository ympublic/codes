import { add } from "./add";

const Example = () => {
  // Dynamic Importは関数となる
  // Dynamic ImportはReactではなくJavaScriptの機能
  const dynamicImport = async () => {
    const module = await import("./add");
    console.log(module);
  };
  dynamicImport();

  // Dynamic Import
  // Promiseで返ってくるので、.then()で繋げることが出来る
  //   import("/.add").then((module) => {
  //     console.log(module);
  //   });

  console.log(add(1, 2));
};

export default Example;

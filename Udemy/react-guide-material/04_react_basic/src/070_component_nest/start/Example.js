// export defaultの場合は、{}は要らない
import Child from "./components/Child";
const Example = () => {
  return <Child />;
};

export default Example;

import { useState, useContext, createContext } from "react";

export const ThemeContext = createContext({});
export const ThemeUpdateContext = createContext({});

const ThemeProvider = ({ children }) => {
  const [theme, setTheme] = useState("light");

  return (
    <ThemeContext.Provider value={theme}>
      <ThemeUpdateContext.Provider value={setTheme}>
        {children}
      </ThemeUpdateContext.Provider>
    </ThemeContext.Provider>
  );
};

export default ThemeProvider;

// theme
export const useTheme = () => useContext(ThemeContext);
// setTheme
export const useUpdateTheme = () => useContext(ThemeUpdateContext);

# Django
from django.shortcuts import render, redirect
# Django Rest Framework
from rest_framework import viewsets
# from rest_framework.generics import ListCreateAPIView # API
# Application
from drf.models import MusicCategory, MusicItem
from drf.serializers import CategorySerializer, ItemSerializer

# ViewSet: SerializerとModelを紐付けるためのもの

# ModelViewSetクラス: モデルに対してCRUD処理を一括で作成できるクラス
class CategoryViewSet(viewsets.ModelViewSet):
	# モデルのオブジェクトを取得
	queryset = MusicCategory.objects.all() # filter(pk=1)
	# シリアライザーを取得
	serializer_class = CategorySerializer # Serializer


class ItemViewSet(viewsets.ModelViewSet):
	queryset = MusicItem.objects.all()
	serializer_class = ItemSerializer

	# APIのアクセス権を付与したい場合
	# from rest_framework.permissions import IsAuthenticated
	# permission_classes = [IsAuthenticated, ]


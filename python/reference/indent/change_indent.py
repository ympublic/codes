#! /usr/bin/env python
# -*- coding: utf-8 -*-

################################
#    インデントの変更
################################

def reindent(s, numSpaces):
    """ 各行のインデントを整形 """
    leading_space = numSpaces * ' '
    # splitlines(): 文字列を改行部分で分割し、各行からなるリストを返す
    lines = [leading_space + line.strip() for line in s.splitlines()]
    # リストのそれぞれの要素の最後に改行を加えて結合
    return '\n'.join(lines) # ex: s=','.join(word) # aaa,bbb,ccc


def addSpaces(s, numAdd):
    white = " " * numAdd
    # splitlines(True): 出力されるリストの中に改行コードは除去されず、付与した状態で格納
    return white + white.join(s,splitlines(True))


def numSpaces(s):
    """ 左端のスペースの数 """
    return [len(line) - len(line.lstrip()) for line in s.splitlines()]


def delSpaces(s, numDel):
    """ 左端からの距離を変えたい場合（スペースを削除） """
    if numDel > min(numSpaces(s)):
        raise ValueError("Removing more spaces than there are!")
    return '\n'.join([line[numDel:] for line in s.splitlines()])


def unIndentBlock(s):
    return delSpaces(s, min(numSpaces(s)))
    print("スペース削除\n {}".format(delSpaces(s, 4)))


def main():
    s = """  Los Angeles Angels superstar Shohei Ohtani is 
     having a historic season. 
    To be fair, that's been the moment he entered the majors, 
    but through five starts this season, he's been a monster. 
         If Ohtani is on the mound, it's almost a guaranteed victory
          for the Halos. 
    Fans and commentators are astonished by his historic start; 
       the people over at Bally Sports West broke down the numbers, 
       and you wouldn't believe it unless you saw it. 
   Shohei's numbers for each pitch are just ridiculous. 
     His favorite pitch, the sweeper, 
   batters are averaging a .063 average and has struck out 18 batters.  """

    print("整形前\n {}".format(s))
    print("整形後\n {}".format(reindent(s, 2)))

    print("スペース削除\n {}".format(delSpaces(s, 2)))


if __name__ == '__main__':
    main()


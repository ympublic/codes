#! /usr/bin/env python3

import pprint

from character import Character
from hero import Hero
from matango import Matango
from cleric import Cleric
from wizard import Wizard
from sword import Sword
from inn import Inn
from king import King
from superhero import SuperHero
from poisonmatango import PoisonMatango
from dancer import Dancer
from goblin import Goblin


def main():
	# インスタンス化
	h = Hero("Lennon")
	# private変数もインスタンスメソッドからならアクセス出来る
	print("勇者{0}が誕生した！ (現在HP: {1}、MP: {2})".format(h.name, h.hp, h.showMP()))
	print("勇者{}は魔法の瓶を手に入れた。MPが{}に上昇した".format(h.name, h.increaseMP(7)))
	# Pythonでは、protected変数にクラス外でもアクセスできてしまう
	print("すばやさ：{}".format(h._quickness))

	h2 = Hero("John")
	h2 = h # hとh2の両方に、メモリの同じ番地情報が入る
	h2.hp = 200
	print("h(HP): {}".format(h.hp)) # h(HP): 200

	# クラス変数（static変数）
	Hero.money = 1000
	print("{0}は王様に{1}ゴールドをもらった！".format(h.name, Hero.money))
	print("しかし{}はお金が入った財布を盗まれてしまった！".format(h.name))
	# クラスメソッド（staticメソッド）
	Hero.setRandomMoney()
	# インスタンスからでもクラス変数はアクセス化
	print("{}は母親の元へ行き、{}ゴールドを借りることにした！".format(h.name, h.money))

	h3 = Hero("Paul")
	h3.hp = 85
	print("偽勇者{0}が誕生した！ (現在HP: {1})".format(h3.name, h3.hp))

	s = Sword()
	s.name = "エクスカリバー"
	s.damage = 10
	h.sword = s # swordフィールドに生成済みのインスタンス（の番地）を代入
	print("現在の武器は'{}'".format(h.sword.name))

	# インスタンス化
	c = Cleric()
	c.name = "Yoko"
	c.hp = 50
	c.mp = 10
	print("僧侶{0}が仲間になった！ (現在HP: {1}, MP: {2})".format(c.name, c.hp, c.mp))

	# インスタンス化
	w = Wizard()
	w.name = "Sean" # Setter
	w.hp = 35 # Setter
	w.mp = 30 # Setter
	print("魔法使い{0}が仲間になった！ (現在HP: {1}, MP: {2})".format(w.name, w.hp, w.mp))

	# おばけキノコA生成
	matango1 = Matango("A")
	matango1.hp = 50
	#matango1.suffix = "A"
	print("{}{}が現れた！".format(matango1.species, matango1.suffix))

	# おばけキノコB生成
	matango2 = Matango("B")
	matango2.hp = 40
	# Setterでsuffix変更
	matango2.suffix = "C"
	print("おばけキノコ{}が現れた！".format(matango2.suffix))

	# 5秒間座らせる
	h.sit(5)

	# 転ばせる
	h.slip()

	# 25秒間座らせる
	h.sit(25) # MAX_HP = 100

	# Monsterクラスを継承している敵へ攻撃(ポリモーフィズム)
	h3.attack(matango1)

	# 休ませる
	inn = Inn()
	inn.checkIn(h3)
	print("{0}のHP: {1} / MP: {2}".format(h3.name, h3.hp, h3.showMP()))

	# おばけキノコAが逃げる
	matango1.run()

	# 僧侶が回復魔法を唱える
	c.selfAid()

	# 僧侶が祈る
	c.pray(3)

	# Monsterクラスを継承している敵へ攻撃(ポリモーフィズム)
	h.attack(matango2)

	# おばけキノコBが逃げる
	matango2.run()

	# おばけキノコが落としていったお金
	# 重要！Pythonでクラス変数をインスタンスから直接変更させるには、__class__が必要！
	# cf: https://aiacademy.jp/media/?p=922
	# またはHero.moneyとクラス変数を使う
	h.__class__.money += 300 
	print("現在の所持金：{}ゴールド".format(h.money)) 

	# 魔法使いが勇者のHPを回復させる
	w.heal(h)

	# 逃げるよう指示
	h.run()

	# 別インスタンスからクラス変数へアクセス
	# クラス変数は１つで同じもの
	print("所持金{}ゴールドはなくなってしまった".format(h3.money)) # 別のインスタンスからも同じもの
	
	# 王様との謁見
	king = King()
	king.talk(h)

	# SuperHero誕生
	sh = SuperHero("Hendrix")
	print("なんと、Heroを超えたSuperHero{}が誕生した！".format(sh.name))

	# is-aの関係
	if isinstance(sh, Hero):
		print(f"'{sh.name}'はHeroクラスを継承しています")

	# 多重継承での継承順を確認
	print("SuperHeroの継承順の表示")
	pprint.pprint(SuperHero.__mro__)

	# オブジェクトの属性を調べる
	print("shの属性 => {0}".format(sh.__class__))

	# おばけ毒キノコA登場
	poison = PoisonMatango("A")
	print("おばけ毒キノコ{0}が現れた！".format(poison.suffix))
	# おばけ毒キノコAの攻撃
	poison.attack(sh)

	# Heroに無い、SuperHero独自のメソッド
	sh.fly()
	# Heroのattack()をオーバライドしたattack
	sh.attack(matango1)
	# Heroから継承したrun()を実行
	sh.run()

	# 抽象クラスをNewするとTypeError
	# cr = Character()

	# 抽象クラスCharacterを継承したDancer登場
	ds = Dancer("Monroe")
	ds.dance()

	# ゴブリンA登場
	goblin = Goblin("A")
	print("ゴブリン{}が現れた！".format(poison.suffix))
	# ポリモーフィズムにてゴブリン->Characterにattack()
	goblin.attack(ds)
	# オーバーライドしたメソッド
	goblin.run()

	# ポリモーフィズム(同じ名称のメソッドで異なる挙動を実現)
	print("ポリモーフィズム...")
	for c in (h, ds, matango1, goblin):
		c.run()


if __name__ == '__main__':
	main()


"""abcd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/ja/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings
from django.http import HttpResponse
from iportfolio import views

urlpatterns = [
    path("admin/", admin.site.urls),
    path("", views.iportfolio, name="top"),
    path("accounts/", include("accounts.urls")),
    path("iportfolio/", include("iportfolio.urls")),
    path("news/", include("news.urls")),
    path("shopping/", include("shopping.urls")),
    path("social_auth/", include("social_auth.urls")),
    path("drf/", include("drf.urls")),
    path("snippets/", include("snippets.urls")),
    path("__debug__/", include("debug_toolbar.urls")), # django-debug-toolbar
    # robots.txt
    re_path(r'^robots.txt$', lambda r: HttpResponse("User-agent: *\nDisallow: /", content_type="text/plain")),
]

if settings.SOCIAL_AUTH == True:
    urlpatterns += [
        # 以下を設定するだけで自分でルーティングの設定をすることなく、
        # django-allauthがデフォルトで設定している認証関係のページを利用できる 
        # ただ、login.html、signup.htmlなどをオーバーライドすることは可能
        # macにAnacondaをインストールしている自分の環境では以下の場所にtemplateは存在
        # /Users/$USER/opt/anaconda3/lib/python3.9/site-packages/allauth/templates/account
        path("social_auth/", include("allauth.urls")), # django-allauth
    ]

from django.conf import settings
from django.conf.urls.static import static

if settings.DEBUG: # DEBUG=Falseの場合は自動的に無効化される
    # MEDIA_URLとMEDIA_ROOTの関連付け
    # リクエストされたURLが/MEDIA_URLから始まる場合にMEDIA_ROOTからファイルを取得するよう設定
    # URLが/media/{$file_path}である場合に、MEDIA_ROOT/{$file_path}のファイルが取得されるようになる
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    # urlpatterns += static(settings.IMAGE_URL, document_root=settings.IMAGE_ROOT)

from errors import views
# handler400 = "myapp.views.handler400" # Bad Request
# handler403 = "myapp.views.handler403" # Forbidden
handler404 = views.handler404 # Not Found
# handler500 = views.handler500 # Internal Server Error


from django.contrib import admin

# Application
from news.models import News

####################################################
#####     News
####################################################

@admin.register(News) # デコレーターでも登録可
class NewsAdmin(admin.ModelAdmin):
	model = News
	list_display = ('title', 'body', 'url', 'published_at', 'image_src',) # 表示カラム
	# list_editable = ('',) # 簡易編集
	# list_display_links = ('title',) # リンクの表示カラム
	list_filter = ('title', 'url',) # フィルター項目からフィルタリングできる
	ordering = ('-published_at', ) # ソート
	search_field = ('title', 'url',) # 検索フィールド



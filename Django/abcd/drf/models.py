from django.db import models

# serializers.pyにて使われるモデル
class MusicCategory(models.Model):
	# category_id = models.IntegerField(primary_key=True, unique=True)
	title = models.CharField(max_length=50)

	class Meta:
		# app_label = 'drf' # RuntimeError: Model class drf.models.MusicCategory doesn't declare an explicit app_label and isn't in an application in INSTALLED_APPS.が出た <- kaihatsu.settings使っていたからだった...
		db_table = 'music_category'
		verbose_name = 'カテゴリー'
		verbose_name_plural = verbose_name


class MusicItem(models.Model):
	# item_id = models.IntegerField(primary_key=True, unique=True)
	title = models.CharField(max_length=50)
	category = models.ForeignKey(MusicCategory, on_delete=models.CASCADE)

	class Meta:
		# app_label = 'drf'
		db_table = 'music_item'
		verbose_name = 'アイテム'
		verbose_name_plural = verbose_name


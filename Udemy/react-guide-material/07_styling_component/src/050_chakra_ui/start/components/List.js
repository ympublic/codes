import {
  VStack,
  HStack,
  StackDivider,
  IconButton,
  Text,
} from "@chakra-ui/react";
import { VscCheck } from "react-icons/vsc";
const List = ({ todos, deleteTodo }) => {
  const complete = (id) => {
    deleteTodo(id);
  };
  return (
    <VStack
      divider={
        <StackDivider
          borderColor="black.100"
          borderWidth="1px"
          borderRadius="30px"
          p={5}
          alignItems="start"
          // color={{sm: `red.600`, md: `blue.600`, lg: `green.500`, xl: `red.600`}}
        />
      }
    >
      {todos.map((todo) => {
        return (
          <HStack key={todo.id} spacing="5">
            <IconButton
              onClick={() => complete(todo.id)}
              isRound="true"
              icon={<VscCheck />}
              bgColor="cyan.100"
              opaity="0.8"
            >
              完了
            </IconButton>
            <Text>{todo.content}</Text>
          </HStack>
        );
      })}
    </VStack>
  );
};

export default List;

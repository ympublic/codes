import numpy as np
import cv2
import matplotlib.pyplot as plt
from PIL import Image, ImageFont, ImageDraw
 
 
movie_name = input("movie name ->")
cap_file = cv2.VideoCapture(movie_name)
print(type(cap_file))
 
if cap_file.isOpened():
    print('frame width:', cap_file.get(cv2.CAP_PROP_FRAME_WIDTH))
    print('frame height:', cap_file.get(cv2.CAP_PROP_FRAME_HEIGHT))
    print('FPS:', cap_file.get(cv2.CAP_PROP_FPS))
    print('frame count:', cap_file.get(cv2.CAP_PROP_FRAME_COUNT))
    
    print('video play time:', int(cap_file.get(cv2.CAP_PROP_FRAME_COUNT)/cap_file.get(cv2.CAP_PROP_FPS)))
 
    selected_process = input("select process 0:one shot, 1:all start ->")
    
    if selected_process == '0':
        res, frame = cap_file.read()
        print(type(frame))
        
        print(frame.shape)
        
        cv2.imshow('frame', frame)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        cv2.imwrite('output_image.jpg', frame)
        
    elif selected_process == '1':
        w = int(cap_file.get(cv2.CAP_PROP_FRAME_WIDTH))
        h = int(cap_file.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fps = int(cap_file.get(cv2.CAP_PROP_FPS))
        fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        video = cv2.VideoWriter('output.mp4', fourcc, fps, (w, h))
        
        delay = 1
        
        while True:
            res, frame = cap_file.read()
            
            if res:
                imgray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                ret, thresh = cv2.threshold(imgray, 100, 255, 0)
                thresh = cv2.bitwise_not(thresh)
                
                contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                
                frame = cv2.drawContours(frame, contours, -1, (0, 255, 0), 3)
                
                font_path = 'NotoSansCJKjp-Regular.otf'
                font_size = 24
                font = ImageFont.truetype(font_path, font_size)
                frame = Image.fromarray(frame)
                draw = ImageDraw.Draw(frame)
                tw, th = draw.textsize("画像処理", font)
                draw.text((100, 50), "画像処理", font=font, fill=(0, 0, 0, 0))
                
                frame = np.array(frame)
                
                cv2.imshow('frame', frame)
                video.write(frame)
                if cv2.waitKey(delay) & 0xFF == ord('q'):
                    break
            
            else:
#               cap_file.set(cv2.CAP_PROP_POS_FRAMES, 0)
                break
        
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        video.release()
    
    else:
        pass
        
    cap_file.release()
    
else:
    pass
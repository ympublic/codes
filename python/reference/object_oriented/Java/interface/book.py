from tangibleasset import TangibleAsset

class Book(TangibleAsset):

	"""  """

	def __init__(self, name:str, price:int, color:str, isbn:str):
		super().__init__(name, price, color) # TangibleAsset
		self.__isbn = isbn # 追加のメンバ変数

	@property
	def isbn(self):
		return self.__isbn

	@isbn.setter
	def isbn(self, value):
		self.__isbn = value


/*
 * 再帰関数
 *
 */

"use strict";

function rec(n) {
	if (n < 0 || !Number.isInteger(n)) {
		throw new Error("0以上の整数を指定してください");
	} else if (n === 0) {
		return 1;
	} else {
		// 再帰呼び出し
		// JavaScriptの場合、calleeプロパティには関数自身への参照が格納されている
		return arguments.callee(n-1) * n; // rec(n-1) * n;
	}
}

let n = 5;
console.log(rec(n)); // 120

n = 0;
console.log(rec(n)); // 1

n = 5.5;
console.log(rec(n)); // Error

n = -1;
console.log(rec(n));  // Error


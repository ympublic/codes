#! /usr/bin/env python

"""datetime
datetime.datetime: 日時（日付と時刻）
datetime.date: 日付
datetime.time: 時刻
datetime.timedelta: 時間差・経過時間
"""

import pytest
import datetime
from dateutil import relativedelta # pip install -U dateutils


@pytest.mark.solved
def test_should_return_current_time_with_datetime():
	""" datetime.datetimeを使用して現在の日時を表示 """

	# 現在の日時を取得
	dt = datetime.datetime.now()

	print(type(dt))

	# 本日（2023/05/17 14:25:00 水曜日）の日時が正しいか
	assert dt.year == 2023

	assert dt.month == 5

	assert dt.day == 17

	assert dt.hour == 14

	# 曜日を取得(0: 月曜日、 6: 日曜日)
	dt_date = datetime.datetime(2023, 5, 17).weekday()

	assert dt_date == 2

	# UTC標準時を取得
	utc_dt = datetime.datetime.utcnow()

	# 日本標準時との時間差は9時間
	assert utc_dt.hour + 9 == dt.hour

	# 日時を書式化された文字列として返す
	# dt.strftime("%Y-%m-%d %H:%M:%S")
	assert dt.strftime("%Y-%m-%d") == "2023-05-17"


@pytest.mark.solved
def test_should_return_result_of_calculated_time():
	""" datetime.timedeltaにより日時を計算 """

	# 現在の日時を取得
	dt = datetime.datetime.now()

	# timedeltaにより日時の加算
	dt_added =  dt + datetime.timedelta(days=7)

	assert dt_added.day == 24 # 17日の7日後

	# 日時の差分を計算
	delta = dt_added - dt

	assert delta.days == 7 # 当然7日

	# 日時の比較
	assert dt < dt_added


@pytest.mark.solved
def test_should_return_current_time_with_date():
	""" dateによる日にちのみの表現 """

	# 本日を取得
	d = datetime.date.today()

	assert d.year == 2023

	assert d.day == 17

	# strftime()による整形
	assert d.strftime("%Y/%m/%d") == "2023/05/17"

	# datetimeオブジェクトとdateオブジェクトを比較するには、
	# datetimeオブジェクトをdateオブジェクトに変換する必要がある
	
	# 現在の日時を取得
	dt = datetime.datetime.now()

	# datetimeオブジェクトをdateオブジェクトへ変換
	dt_date = dt.date()

	assert dt_date == d


@pytest.mark.solved
def test_should_return_calculated_result_with_dateutils():
	""" relativedeltaによる計算 """

	# 本日の日にちを取得
	d = datetime.date.today()

	# 年の加算(1年)
	# 引数にyears, months, daysを指定すると加算となる
	assert d + relativedelta.relativedelta(years=1) == datetime.date(2024, 5, 17)

	# 引数にdayを指定すると、月初と月末を取得できる
	assert d + relativedelta.relativedelta(day=1) == datetime.date(2023, 5, 1)
	assert d + relativedelta.relativedelta(day=99) == datetime.date(2023, 5, 31)


@pytest.mark.solved
def test_should_convert_string_into_datetime():
	""" datetime.strptimeにより文字列をdatetime型へ変換 """

	time_string = "2023-05-25 10:30:45"

	# 文字列からdatetime型へ変換
	# 第２引数は第１引数のフォーマット
	to_datetime = datetime.datetime.strptime(time_string, "%Y-%m-%d %H:%M:%S")

	# (2023, 05, 25)のように05などはErrorとなるので注意
	assert to_datetime == datetime.datetime(2023, 5, 25, 10, 30, 45)


@pytest.mark.solved
def test_should_convert_string_into_date():
	""" datetime.strptimeにより文字列をdate型へ変換 """

	time_string = "2023-05-25 10:30:45"

	# 文字列からdatetime型へ変換
	# 第２引数は第１引数のフォーマット
	to_datetime = datetime.datetime.strptime(time_string, "%Y-%m-%d %H:%M:%S")

	# datetime型からdate型へ変換
	to_date = datetime.date(to_datetime.year, to_datetime.month, to_datetime.day)

	assert to_date == datetime.date(2023, 5, 25)


@pytest.mark.solved
def test_should_convert_datetime_into_string():
	""" datetime.strptimeによりdatetime型からStringへ変換 """

	time_string = "2023-05-25 10:30:45"

	my_datetime = datetime.datetime(2023, 5, 25, 10, 30, 45)

	# datetime型からstringへ変換
	to_string = my_datetime.strftime("%Y-%m-%d %H:%M:%S")

	assert time_string == to_string


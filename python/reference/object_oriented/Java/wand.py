class Wand(object): # またはWeaponを継承させる

	"""  """

	# カプセル化
	def __init__(self):
		# privateインスタンス変数
		self.__name: str # 杖の名前
		self.__power: float = 2.5 # 杖の魔力

	# getter
	@property
	def name(self):
		return self.__name

	@property
	def power(self):
		return self.__power

	# setter
	@name.setter
	def name(self, value: str):
		if not isinstance(value, str):
			raise TypeError("str型で指定してください")

		if len(value) < 3 and 15 < value:
			raise ValueError("3文字以上、15文字以下のstr型で指定してください")

		self.__name = value 

	@power.setter
	def power(self, value: float):
		if not isinstance(value, float):
			raise TypeError("float型で指定してください")

		if value <= 0.5 and 100 < value:
			raise ValueError("0.5より大きく、100以下のfloat型で指定してください")

		self.__power = value 


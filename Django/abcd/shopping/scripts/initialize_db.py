import uuid
from collections import deque # 先入れ先出し
# Django
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.urls import reverse_lazy
from django.views.generic import ListView
from django.views.generic import FormView
from django.views.generic import DetailView
from django.views.generic import TemplateView
from django.views.generic import CreateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic.edit import ModelFormMixin
from django.views.generic.edit import FormMixin
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.views.decorators.http import require_http_methods

# Application
from shopping.models import (
	Item, ItemPhoto, ItemStock, ItemReview,
	G1, Foal, Cart,
)
from shopping.forms import ItemForm
from shopping.forms import G1Form
from shopping.forms import FoalForm
from shopping.forms import ItemPhotoForm
from shopping.forms import CartForm

MyUser = get_user_model()

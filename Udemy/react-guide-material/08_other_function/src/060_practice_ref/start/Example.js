import { useState, useRef, forwardRef, useImperativeHandle } from "react";

const Video = forwardRef(({ path }, ref) => {
  // 子要素用のrefオブジェクトを作成
  const childRef = useRef();
  console.log(childRef);

  useImperativeHandle(ref, () => ({
    myPlay() {
      childRef.current.play();
    },
    myStop() {
      childRef.current.pause();
    },
  }));

  return (
    // ここにrefを追加する
    <video style={{ maxWidth: "100%" }} ref={childRef}>
      <source src={path}></source>
    </video>
  );
});

const Example = () => {
  const [playing, setPlaying] = useState(false);

  // refオブジェクトを作成
  const ref = useRef();

  return (
    <div>
      <h3>練習問題</h3>
      <p>
        useRef、useImperativeHandle、forwardRefを使って完成系の動画再生機能を作成してください。※useImperativeHandleでplay(再生)、stop(停止)メソッドを定義すること。
      </p>
      <Video ref={ref} path="./sample.mp4" />
      <button
        onClick={() => {
          if (playing) {
            // このrefはuseImperativeHandleのref
            ref.current.myStop();
          } else {
            ref.current.myPlay();
          }
          setPlaying((prev) => !prev);
        }}
      >
        {playing ? "Stop" : "Play"}
      </button>
    </div>
  );
};

export default Example;

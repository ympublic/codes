# テストドリブン開発で作るためのクラス

from monster import Monster

class Slime(Monster):
	""" スライムオブジェクトを作成 """

	# staticプロパティ
	softness: int = 8

	def __init__(self, suffix:str="A"):
		super().__init__()
		# privateインスタンスプロパティ
		if not isinstance(suffix, str):
			raise TypeError("str型で指定してください")
		self.__suffix: str = suffix
		self.__species: str = "スライム"
		self.__hp: int = 100

	def __call__(self):
		""" 呼び出し可能なオブジェクトを作る """
		print("オッ！このcallableメソッドに気が付きましたね！")
		return "__call__メソッド"

	# インスタンスメソッド
	def run(self):
		message = f"{self.__species}{self.__suffix}はボヨンボヨンと跳ねながら逃げ出した！"
		return message
		
	# staticメソッド
	@staticmethod
	def touch():
		return f"触り心地は{Slime.softness}である。極上のふわふわやわらかさで気持ち良い！"

	# Getter
	@property
	def hp(self):
		return self.__hp

	# Setter
	@hp.setter
	def hp(self, value):
		if not isinstance(value, int):
			raise TypeError("int型で指定してください")
			return False
		self.__hp = value 


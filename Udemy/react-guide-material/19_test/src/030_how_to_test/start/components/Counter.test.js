/* POINT テストコードを書く際の注意点
Arrage:(テストデータ、条件、環境の設定). / データの準備
Act:(ロジックの実行、関数の実行). / 検証したいものの実行
Assertion:(実行結果と期待する結果の比較). / 実際に結果を取得して確認
*/

import { render, screen, fireEvent } from "@testing-library/react";
import Counter from "./Counter";

describe("Counterコンポーネントの動作確認", () => {
  test("「カウントアップ」ボタン押下で「現在のカウント」が+1されるか？", () => {
    // Arrange
    render(<Counter originCount={0} />);

    // 更新前
    const spanElBeforeUpdate = screen.getByText("現在のカウント:0");
    expect(spanElBeforeUpdate).toBeInTheDocument();

    // Act
    const btn = screen.getByRole("button", { name: "カウントアップ" });
    fireEvent.click(btn);

    // Assertion（更新後）
    const spanEl = screen.getByText("現在のカウント:1");
    expect(spanEl).toBeInTheDocument();
  });
});

#! /usr/bin/env python

"""pytest
"""

import pytest


@pytest.mark.skip
def test_shold_be_skipped():
	""" @pytest.mark.skipにてスキップされるテスト """

	assert 1 == 2


def test_exception():
	""" 例外を発生させてテスト """

	with pytest.raises(ValueError):
		raise ValueError("ValueErrorを発生させました")


def test_should_print_strings():
	""" -s オプションでprint()を出力させる """

	# pytest -v -s test_pytest.py --pdb
	print("☆☆☆☆☆　PRINT　☆☆☆☆☆")

	assert 1 == 1


@pytest.mark.parametrize("x,y,z", [(1,2,3), (4,5,9)])
def test_should_give_parameters(x:int, y:int, z:int):
	""" テストのパラメータに値を提供する """

	assert (x + y) == z


def test_should_use_fixture_from_conftest(global_fixture):
	""" conftest.pyに記述されているフィクスチャを使用する """

	assert global_fixture == ("apple", "banana", "orange")


@pytest.fixture()
def sample_fixture():
	apple = ("iPhone", "macbook", "iPad", "AppleWatch")

	yield apple


@pytest.mark.usefixtures("sample_fixture")
class TestSample(object):
	""" 複数のテストにまとめて同じフィクスチャを設定したい場合は、クラスにusefixturesを設定する """

	def test_sample1(self):
		# sample_fixtureが処理前に呼ばれる
		print(f"sample_fixture: {sample_fixture}")
		# 注意: フィクスチャのようにsample_fixtureを呼び出すことは出来ない
		pass

	def test_sample2(self):
		# sample_fixtureが処理前に呼ばれる
		pass


# Django
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
# Application
from accounts.models import MyUser
from accounts.models import Profile
from accounts.models import ProfImage
from accounts.forms import ProfEditForm
from accounts.forms import ProfImgEditForm
from social_auth.forms import MyCustomSocialSignupForm


# django-allauthのテンプレートの場所
# https://itc.tokyo/django/update-allauth-template/


####################################################
###   OAuthによるユーザー作成
####################################################

# ACCOUNT_SIGNUP_REDIRECT_URLを設定してviewにてMyUserとProfileなどを紐付ける場合に使用
# またはACCOUNT_SIGNUP_REDIRECT_URLで指定したページに移行した際に、
# プロフィール登録->画像登録のようにページをどんどん移行していく方法の方が良いかと思う
# とは言え、自分はsocial_auth.signalsにてSignalを受け取って紐付ける手法をとった
@login_required
def oauth_prof_create(request):
	
	# social_info = SocialAccount.objects.filter(user=request.user)
	# google_id = SocialAccount.objects.filter(user=request.user, provider='google')[0].uid
	# socialaccountテーブルのextra_dataフィールドの内容を確認
	# 'id', 'name', 'given_name', 'family_name', 'email', 'locale', 'picture' などが取得可能
	# provider = user.socialaccount_set.all()[0].provider # プロバイダー取得
	# data = request.user.socialaccount_set.filter(provider=provider)[0].extra_data
	# print(data['given_name'])
	# for i, v in data: print({} : {}.format(i, v))

	user = MyUser.objects.get(pk=request.user.id)

	profile_form = ProfEditForm()
	profimg_form = ProfImgEditForm()
	
	profile = profile_form.save(commit=False)
	profile.user = user
	user.profile.save()

	profimage = profimg_form.save(commit=False)
	profimage.user = user
	user.profimage.save()

	return redirect("accounts:profile", user_id=user.pk)


""" 上記をSignalで書く場合の例
@login_required
def oauth_prof_create_with_signals(request):
	if request.method == 'POST':
		form = MyCustomSocialSignupForm
		if form.is_valid():
			user = form.save()
			# Signalがプロファイルインスタンスを作成するために同期させる必要がある
			# そのためrefresh_form_db()を呼び出す
			user.refresh_form_db() # データベースからオブジェクト/モデルの値を再読み込みする
			user.profile.nickname = form.cleaned_data.get('nickname')
			user.save() # profile.save()
"""


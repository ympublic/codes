import { useEffect, useState } from "react";

const Example = () => {
  const [time, setTime] = useState(0);

  // useEffect(実行する関数, [依存する値]);
  useEffect(() => {
    window.setInterval(() => {
      setTime((prev) => prev + 1);
    }, 1000);
    // useEffectの第２引数に[]を設定すると、コンポーネントを
    // 表示した初回のみ実行するような処理となる
  }, []);

  return (
    <h3>
      <time>{time}</time>
      <span>秒経過</span>
    </h3>
  );
};

export default Example;

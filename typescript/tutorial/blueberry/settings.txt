# package.jsonを作成
npm init --yes

# package.jsonを確認
more package.json

# "type": "module", を追記
vi package.json

# TypeScriptを開発用としてインストール
npm install --save-dev typescript @types/node

# git cloneしてきた場合は
npm install --save-dev typescript @types/node

# .gitignoreにnode_modules/ とdist/ を追記
vi .gitignore 

# tsconfig.jsonを作成
npx tsc --init 

# tsconfig.jsonを確認
more tsconfig.json 

# tsconfig.jsonの設定を変更
"target": "es2020"
"module": "esnext" 
"moduleResolution": "node"
"outDir": "./dist"
"exactOptionalPropertyTypes": true
"noUncheckedIndexedAccess": true
"noImplicitOverride": true

# tsconfig.jsonの下から２行目に以下を追記
"include" ["./src/**/*.ts"],

# srcディレクトリを作成する
mkdir src 

# index.tsを作成する
touch index.ts

# コンパイル
npx tsc

# 作成されたJavaScriptファイルを実行
node dist/index.js


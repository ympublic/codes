from cleaningservice import CleaningService

# Javaではインタフェースを継承し子クラスを定義する場合はimplementsを利用するが、
# Pythonにはimplementsは無い
# 同種(クラス同士、インタフェース同士)の継承の場合はextends、異種ならimplementsを使う
# 親インタフェースで未定だった各メソッドの内容をオーバーライドして実装し確定させる
# Java: インタフェースでは特別に多重継承が許されている
class KyotoCleaningShop(CleaningService): 

	"""  """

	self.__ownerName: str
	self.__address: str
	self.__phone: str

	def washShirt(s: Shirt) -> Shirt:
		# 大型洗濯機15分
		# 業務用乾燥機30分
		# スチームアイロン5分
		return s

	def washTowl(t: Towl) -> Towl:
		return t

	def washCoat(c: Coat) -> Coat:
		return c


# 重要：基本的に、「１つのテストは１つのことを検証すること」が推奨されているのだが...
# DjangoのテストはDEBUG=Falseを指定したものとして実行される
# デフォルトではCSRFチェックが無効となっている 有効にする場合：c = Client(enforce_csrf_checks=True)

# テストデータベースの破棄を防ぐ場合
# test --keepdb
# テストを並列実行させたい場合
# test --parallel
# パスワードのハッシュ生成
# PASSWORD_HASHES = [
#   'django.contrib.auth.hashers.MD5PasswordHasher',
# ]


# コードカバレージを確認
# coverage run --source='.' manage.py test
# coverage report

import pdb # pdb.set_trace() or breakpoint()
from unittest import mock

# Django
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponseRedirect
from django.test import TestCase, RequestFactory, Client
from django.urls import resolve, reverse, reverse_lazy
from django.test import tag
from django.template import Template, Context
from django.core import mail
from django.core.validators import (
    MaxLengthValidator, MaxValueValidator,
    MinLengthValidator, MinValueValidator,
)
from django.core.exceptions import ValidationError
from django.test.utils import override_settings # @override_settings()

# Applicaion
from iportfolio import views
from iportfolio.models import (Contact, ContactStatus, )
from iportfolio.forms import (ContactForm, )

from iportfolio.factories import ContactFactory # Fake


UserModel = get_user_model()


####################################################
#####     TopPageテスト
####################################################

@tag('iportfolio', 'view', 'complete',) # ./manage.py test --tag=iportfolio --exclude-tag=complete
class TopPageViewTest(TestCase): # DBを扱うのでTestCase
    """ トップページのビュー部分のテスト """

    def setUp(self): # テストメソッドが呼ばれるたびに実行される
        """ 初期設定 """
        # GETやPOSTメソッドのRequestオブジェクトを作成する
        self.client = Client() # response = self.client.get() 
        self.url = "/iportfolio/" # reverse(iportfolio:iportfolio_top)
        self.factory = RequestFactory() # request = self.factory.get()
        self.user = UserModel.objects.create_user(
            email='oiocha@itoen.com',
            password='ocyacya',
        )
        # /.manage.py migrate
        self.contact = Contact.objects.create(
            name='Jimmy Page',
            email='led@zeppelin.com',
            # phone='080-0000-0000',
            subject='Heart Breaker',
            message='We are the hottest rock band, ever!!',
        )
        self.contact_status = ContactStatus.objects.create(
            contact=self.contact, # OneToOneField
            is_emailed=False,
            is_replied=False,
            is_closed=False,
        )

    # def tearDown(self): pass

    #####################################
    #####     ビューのテスト
    #####################################
    def test_top_page_uses_expected_template(self):
        """ テンプレートがちゃんと使われているかテスト """
        response = self.client.get(self.url) # 注意：urlのパスのみ渡す（ドメイン全体ではない）
        self.assertTemplateUsed(response, "iportfolio/index.html")

    def test_top_page_returns_expected_HTML_comment(self):
        """ TopPageが表示されるか、またHTML内に想定している文字列が含まれているかテスト """
        response = self.client.get(self.url)
        self.assertIn("iPortfolio", str(response.content))

    def test_should_return_registered_name(self):
        """  """
        self.assertEqual('Jimmy Page', self.contact.name)

    # 関数ベースビューでないと上手くいかない
    def test_should_resolve_toppageview(self):
        found = resolve(self.url)
        self.assertEqual(views.iportfolio, found.func)
    
    def test_should_return_context_and_status_200(self):
        """  """
        # GETリクエストのインスタンスを作成
        request = self.factory.get(self.url)
        # request.user = self.user / AnonymourUser() のような処理を施す

        # クラスベースビューの場合（公式ドキュメントより）
        # view = TopPageView()
        # response = TopPageView.as_view()(request) # Response
        # self.assertIn("form", context)
        # self.assertContains(response, 'form', status_code=200)

        # 関数ベースビューの場合
        request.user = self.user # これを渡さないとテスト内でrequest.userが存在しない
        request.session = self.client.session # ここでセッションを初期化する必要あり！！
        response = views.iportfolio(request) # Response
        self.assertContains(response, 'form', status_code=200)

    def test_should_return_302_and_redirect_to_contact_confirm(self):
        c_f = ContactFactory()
        params = {
            'name': c_f.name,
            'email': c_f.email,
            # 'phone': c_f.phone,
            'subject': c_f.subject,
            'message': c_f.message,
        }
        request = self.factory.post(self.url, data=params) # フォームに内容を渡す
        request.session = self.client.session # ここでセッションを初期化する必要あり！！ <- ここのエラー解決に１日以上かかった...
        request.user = self.user # ユーザーも渡す必要あり
        response = views.TopPageView.as_view()(request)
        self.assertIsInstance(response, HttpResponseRedirect) # 302 "/iportfolio/contact_confirm/"

    def test_should_return_302_and_redirect_to_contact_confirm2(self):
        c_f = ContactFactory()
        params = {
            'name': c_f.name,
            'email': c_f.email,
            # 'phone': c_f.phone,
            'subject': c_f.subject,
            'message': c_f.message,
        }

        response = self.client.post(self.url, params,) # 下記はこれ１行で済む
        ''' ↑を複雑に記述するならば
        request = self.factory.post(self.url, params,)
        request.session = self.client.session # ここでセッションを初期化する必要あり！！
        request.user = self.user # ユーザーも渡す必要あり
        response = views.iportfolio(request)
        response.client = Client() # これをしないと、AttributeError: 'HttpResponseRedirect' object has no attribute 'client'
        # しかしこのやり方では、AttributeError: 'WSGIRequest' object has no attribute '_messages'発生
        '''
        
        self.assertRedirects(response, reverse_lazy("iportfolio:contact_confirm"), 
            status_code=302,
            target_status_code=302, # target先もリダイレクトするので
        )

    def test_redirect_chain_should_returns_2(self):
        c_f = ContactFactory()
        params = {
            'name': c_f.name,
            'email': c_f.email,
            # 'phone': c_f.phone,
            'subject': c_f.subject,
            'message': c_f.message,
        }
        c = Client()
        response = c.post(self.url, params,
            follow=True, # 全てのリダイレクトを巡り、redirect_chainを使えるようにする
            secure=True, # HTTPSリクエストをエミュレート
        )
        self.assertEqual(len(response.redirect_chain), 2)

    def test_greeting_template_tag(self):
        """ カスタムテンプレートタグのテスト """
        response = self.client.get(self.url)
        #self.assertIn('Good ', str(response.content)) # Good Morning ~ Evening
        self.assertContains(response, 'Good')


@tag('iportfolio', 'template',) 
class TopPageTemplateTest(TestCase):
    """ トップページのテンプレート部分のテスト """

    def setUp(self):
        """ 初期設定 """
        # GETやPOSTメソッドのRequestオブジェクトを作成する
        self.factory = RequestFactory()
        self.url = "/iportfolio/" # reverse(iportfolio:iportfolio_top)

        self.user = UserModel.objects.create_user(
            email='oiocha@itoen.com',
            password='ocyacya',
        )

        # /.manage.py migrate
        self.contact = Contact.objects.create(
            name='Jimmy Page',
            email='led@zeppelin.com',
            # phone='080-0000-0000',
            subject='Heart Breaker',
            message='We are the hottest rock band, ever!!',
        )
        self.contact_status = ContactStatus.objects.create(
            contact=self.contact, # OneToOneField
            is_emailed=False,
            is_replied=False,
            is_closed=False,
        )

    #####################################
    #####     テンプレートのテスト
    #####################################
    def test_context_confirm(self):
        template = Template("{{ top | truncatechars:5 }}")
        context = Context({ "top": "テンプレートテスト" })
        actual = template.render(context)
        self.assertEqual(len(actual), 5)


@tag('iportfolio', 'db',) 
class TopPageDBTest(TestCase):
    """ トップページのDB部分のテスト """

    def setUp(self):
        """ 初期設定 """
        # GETやPOSTメソッドのRequestオブジェクトを作成する
        self.factory = RequestFactory()
        self.url = "/iportfolio/" # reverse(iportfolio:iportfolio_top)

        self.user = UserModel.objects.create_user(
            email='oiocha@itoen.com',
            password='ocyacya',
        )

        # /.manage.py migrate
        self.contact = Contact.objects.create(
            name='Jimmy Page',
            email='led@zeppelin.com',
            # phone='080-0000-0000',
            subject='Heart Breaker',
            message='We are the hottest rock band, ever!!',
        )
        self.contact_status = ContactStatus.objects.create(
            contact=self.contact, # OneToOneField
            is_emailed=False,
            is_replied=False,
            is_closed=False,
        )

    #####################################
    #####     DBのテスト
    #####################################
    def test_contact_message_must_be_stored3(self):
        """ DBに格納されたデータの数が正しいかテスト """
        params = {
            'name': 'John Lennon',
            'email': 'lennon@beatles.com',
            # 'phone': '090-0000-0000',
            'subject': 'Yoko Ono',
            'message': 'I love Yoko!!',
        }
        contact = Contact()
        form = ContactForm(params, instance=contact)
        form.save() # DBに格納
        # 保存データの個数が合っているかどうか（上記のself.contactとPOSTによる投稿）
        self.assertEqual(Contact.objects.count(), 2)
        # ちゃんと保存されたか
        self.assertEqual(Contact.objects.filter(email='lennon@beatles.com').count(), 1)

    def test_contact_message_must_be_stored4(self):
        """ DBに格納されたデータが一致するかテスト """
        params = {
            'name': 'John Lennon',
            'email': 'lennon@beatles.com',
            # 'phone': '090-0000-0000',
            'subject': 'Yoko Ono',
            'message': 'I love Yoko!!',
        }
        contact = Contact()
        form = ContactForm(params, instance=contact)
        form.save()
        lennon = Contact.objects.get(name='John Lennon')
        self.assertEqual('lennon@beatles.com', lennon.email)


@tag('iportfolio', 'form',) 
class TopPageFormTest(TestCase):
    """ トップページのフォーム部分のテスト """

    def setUp(self):
        """ 初期設定 """
        # GETやPOSTメソッドのRequestオブジェクトを作成する
        self.factory = RequestFactory()
        self.url = "/iportfolio/" # reverse(iportfolio:iportfolio_top)

        self.user = UserModel.objects.create_user(
            email='oiocha@itoen.com',
            password='ocyacya',
        )

        # /.manage.py migrate
        self.contact = Contact.objects.create(
            name='Jimmy Page',
            email='led@zeppelin.com',
            # phone='080-0000-0000',
            subject='Heart Breaker',
            message='We are the hottest rock band, ever!!',
        )
        self.contact_status = ContactStatus.objects.create(
            contact=self.contact, # OneToOneField
            is_emailed=False,
            is_replied=False,
            is_closed=False,
        )

    #####################################
    #####     フォームのテスト
    #####################################
    def test_contact_message_must_be_stored(self):
        """ お問い合わせフォームに入力されたメッセージをDBに格納できるかテスト """
        c_f = ContactFactory() # このインスタンス時、DBに新たにデータが格納される
        params = {
            'name': c_f.name,
            'email': c_f.email,
            # 'phone': c_f.phone,
            'subject': c_f.subject,
            'message': c_f.message,
        } 
        contact = Contact()
        form = ContactForm(params, instance=contact)
        # form.is_valid()を通るかどうか
        self.assertTrue(form.is_valid())

    def test_contact_message_must_be_stored2(self):
        """ is_valid()後のデータ内容が一致するかテスト """
        params = {
            'name': 'John Lennon',
            'email': 'lennon@beatles.com',
            # 'phone': '090-0000-0000',
            'subject': 'Yoko Ono',
            'message': 'I love Yoko!!',
        }
        contact = Contact()
        form = ContactForm(params, instance=contact)
        form.is_valid()
        # 投稿内容が一致するかどうか
        self.assertEqual('John Lennon', form.cleaned_data['name'])

    def test_contact_email_should_be_correct(self):
        """ お問い合わせフォームに入力されたEmailの形式が正しいかテスト """
        params = {
            'name': 'John Lennon',
            'email': 'war_is_over!!', # Email形式ではない文字を入力
            # 'phone': '090-0000-0000',
            'subject': 'Yoko Ono',
            'message': 'I love Yoko!!',
        }
        contact = Contact()
        form = ContactForm(params, instance=contact)
        # form.is_valid()を通るかどうか
        self.assertFalse(form.is_valid())

    def test_contact_email_should_be_written(self):
        """ お問い合わせフォームにEmailを書かずに送信してエラーが出るかテスト """
        params = {
            'name': 'John Lennon',
            'email': '', # blank=False
            # 'phone': '090-0000-0000',
            'subject': 'Yoko Ono', 
            'message': 'I love Yoko!!',
        }
        contact = Contact()
        form = ContactForm(params, instance=contact)
        self.assertFalse(form.is_valid())

    def test_should_be_passed_through_customed_validations(self):
        """ 自己定義バリデーションを通過するかテスト """
        contact = Contact()

        params = {
            'name': 'John Lennon',
            'email': 'lennon@beatles.com',
            # 'phone': '０９０−０００', # 数字を全角に & １０文字未満に
            'subject': 'No',
            'message': 'I love Yoko!!',
        }
        form = ContactForm(params, instance=contact)
        # ３文字未満は弾かれるかテスト
        self.assertFalse(form.is_valid())

    def test_username_validation(self):
        valid_usernames = ('glan', 'GOOO', 'K-dash',)
        invalid_usernames = ('@*?^¥p', '+|!!<>', 'K_dash_',)
        test_validator = MaxLengthValidator(7)

        for valid in valid_usernames:
            with self.subTest(valid=valid):
                test_validator(valid)


# cf: https://freeheroblog.com/mail-unit-test/
@tag('email')
class EmailTest(TestCase):
    """ メール送信部分のテスト """

    def setUp(self):
        """ 初期設定 """
        mail.outbox = []

    #####################################
    #####     Emailのテスト
    #####################################
    def test_send_mail(self):
        mail.send_mail(
            'Subject Desu',
            'Message Desu',
            'from@example.com',
            ['to@example.com',],
            fail_silently=False,
        )

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, 'Subject Desu')
        self.assertEqual(mail.outbox[0].to[0], 'to@example.com')


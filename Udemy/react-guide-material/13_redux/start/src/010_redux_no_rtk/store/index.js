// グローバルstate
import { createStore } from "redux";

const initialState = 0;
const reducer = (state = initialState, { type, step }) => {
  switch (type) {
    case "+":
      return state + step;
    case "-":
      return state - step;
    // Toolkitの場合はdefaultは書かない
    default:
      // Reduxではthrow new Errorではなく、渡ってきた値をそのまま返す
      return state;
  }
};

export default createStore(reducer);

def checkio(number):
    minute = 0
    pigeons = 0
    fed_pigeons = 0
    remains_food = number
    while True:
        minute += 1
        old_pigeons = pigeons
        pigeons += minute
        if number < pigeons:
            remains_food = number - old_pigeons
            if remains_food > 0:
                fed_pigeons += remains_food
            break
        else:
            number -= pigeons
            fed_pigeons += minute
    return fed_pigeons
        
                
    """ I correct later
    i=1
    p=0
    while i*(i+1)/2 < number:  # sequence of numbers with common difference
        #print "total: %d" % total
        #print "number: %d" % number
        if number - i*(i+1)/2 < 0:
            break
        else:
            number = number - i*(i+1)/2  # 2
            p+=i  # 1
            i+=1  # 2
    p+=number  # 1
    return p
    """
    
if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert checkio(1) == 1, "1st example"
    assert checkio(2) == 1, "2nd example"
    assert checkio(5) == 3, "3rd example"
    assert checkio(10) == 6, "4th example"
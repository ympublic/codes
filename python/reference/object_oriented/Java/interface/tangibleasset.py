from asset import Asset
from thing import Thing

class TangibleAsset(Asset, Thing): # extends Asset implements Thing

	"""  """

	def __init__(self, name:str, price:int, color:str):
		super().__init__(name, price) # Asset
		# 追加のメンバ変数
		self.__color: str = color 
		self.__weight: float

	@property
	def color(self):
		return self.__color

	@color.setter
	def color(self):
		self.__color = value

	# オーバーライド
	def getWeight(self):
		print("重さを取得します")
		return self.__weight

	def setWeight(self, weight:float):
		print("重さを設定します")
		self.__weight = weight


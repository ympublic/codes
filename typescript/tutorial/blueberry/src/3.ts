/**
 * コンパイルと実行(/blueberry上にて)
 * npx tsc
 * node dist/3.js
 *
 */

type MyObj = {
  readonly foo: string;
  readonly bar: number;
  baz?: boolean;
};

const obj: MyObj = { foo: "apple", bar: 23 };

const obj2: MyObj = { foo: "banana", bar: 19, baz: false };

console.log(obj);

console.log(obj2);

// readonlyのプロパティを変更しようとすると、コンパイルエラー
// obj.foo = "you are out!";

type Animal = {
  readonly gender: string;
};

// HumanはAnimalの部分型
type Human = {
  readonly gender: string;
  readonly name: string;
};

// HumanはAnimalの部分型なので、Animal型のオブジェクトが必要な場面ではHumanが使える
const cat: Human = {
  gender: "male",
  name: "Mike",
};

console.log(cat);

type AnimalFamily = {
  familyName: string;
  mother: Animal;
  father: Animal;
  child: Animal;
};

type HumanFamily = {
  familyName: string;
  mother: Human;
  father: Human;
  child: Human;
};

// ジェネリック型
type User<T> = {
  name: string;
  child: T;
};

/** Chapter3 */

type Family<Parent, Child> = {
  mother: Parent;
  father: Parent;
  child: Child;
};

const familyObj: Family<number, string> = {
  mother: 0,
  father: 100,
  child: "1000",
};

console.log(typeof familyObj.father); // number

console.log(typeof familyObj.child); // string

// 部分型関係による型引数の制約

type HasName = {
  name: string;
};

type Family2<Parent extends HasName, Child extends HasName> = {
  mother: Parent;
  father: Parent;
  child: Child;
};

// 配列スプレッド構文
const arr1: readonly number[] = [4, 5, 6];
const arr2: number[] = [1, 2, 3, ...arr1];

console.log(arr2);

console.log(arr2[3]);

const arr00: readonly (number | string | boolean)[] = [100, "moji", false];

// readonlyなので、変更はコンパイルエラーとなる
// arr00[0] = 300;
// arr1.push(30);
// arr1.unshift(50);
// console.log(arr1.includes("Yoyo"));

// もちろんreadonlyを付与していないarr2にはpush()もunshift()も可能
arr2.push(30);
arr2.unshift(50);
console.log(arr2.includes(50));

// for ~ of ...
for (const elem of arr2) {
  console.log(elem);
}

// Tuple
let myTuple: [string, number, boolean?] = ["foo", 54];

myTuple = ["bar", 77];

const str1: string = myTuple[0];
const str2: number = myTuple[1];

// 分割代入
const { mother, father, child } = familyObj;

console.log(`mother: ${mother}`);

const nested = {
  numN: 123,
  objN: {
    fooN: "hello",
    barN: "world",
  },
};

// ネストしたパターンを分割代入
const {
  numN,
  objN: { fooN },
} = nested;

console.log(`numN: ${numN}`);
console.log(`objN.fooN: ${fooN}`);

import unicodedata

# Django 
from django import forms
from django.forms import modelform_factory
from django.utils.translation import gettext_lazy as _

# Application
from iportfolio.models import Contact


####################################################
#####     Contact
####################################################

number_to_hankaku = str.maketrans('ー０１２３４５６７８９', '-0123456789')


class ContactForm(forms.ModelForm):
	"""  """	
	class Meta:
		model = Contact
		fields = ('name', 'email', 'subject', 'message',)
		labels = {
        	'name': 'Your Name',
        	'email': 'Your Email',
        	# 'phone': 'Your Phone',
        	'subject': 'Subject',
        	'message': 'Message',
        }
        # widget変更
        # モデルフォームならば、class Meta内で行う
        # 通常のフォームならばフォームフィールドとして設定
        # 一括で変更・設定する必要がある場合や複雑な変更は__init__()内で変更
        # 動的に変更され、フォーム内で対応するのが厄介なケースはビューで変更
		widgets = { # placeholderやclassを追加する
			# 自分はwidget内で変更したが、class名の一括指定は下に書いた様に__init__()内で変更した方が楽
			'name': forms.TextInput(attrs={'placeholder':'苗字　名前', 'class':'form-control',}),
			'email': forms.TextInput(attrs={'placeholder':'yourmail@example.com', 'class':'form-control',}),
			# 'phone': forms.TextInput(attrs={'placeholder':'090-0000-0000', 'class':'form-control',}),
			'subject': forms.TextInput(attrs={'placeholder':'件名', 'class':'form-control',}),
			'message': forms.Textarea(attrs={'placeholder':'メッセージ内容', 'class':'form-control',}),
		}

	# クリーニング（正規化）処理
	def clean_name(self):
		name = self.cleaned_data['name']
		name = unicodedata.normalize('NFKC', name)
		return name

	"""
	def clean_phone(self):
		phone = self.cleaned_data['phone']
		phone = phone.translate(number_to_hankaku)
		if len(phone) < 10:
			raise forms.ValidationError( # 公式の推奨スタイル
				_('Too short!'),
				code='phone_number_too_short',
			)
		return phone
	"""

	def clean_subject(self):
		subject = self.cleaned_data['subject']
		subject = unicodedata.normalize('NFKC', subject)
		if len(subject) < 3:
			raise forms.ValidationError(
				_('Too short!'),
				code='subject_too_short',
			)
		return subject

	def clean_message(self):
		message = self.cleaned_data['message']
		message = unicodedata.normalize('NFKC', message)
		if len(message) < 3:
			raise forms.ValidationError(
				_('Too short!'),
				code='message_too_short',
			)
		return message

	# formにてrequestも受け入れる場合: ContactForm(request)
	# def __init__(self, request, *args, **kwargs): pass

	'''
	# FormViewにてget_form_kwargs()を設定した場合は以下も指定する
	def __init__(self, *args, **kwargs):
		self.name = kwargs.pop('name')
		self.email = kwargs.pop('email')
		# self.phone = kwargs.pop('phone')
		self.subject = kwargs.pop('subject')
		self.message = kwargs.pop('message')
		super().__init__(*args, **kwargs)
		self.fields['name'].initial = self.name
		
		# クラスを一括設定する場合
		for field in self.fields.values():
			field.widget.attr['class'] = 'form-control'

	'''


import List from "./List";
import Form from "./Form";
import { useState } from "react";

const Todo = (props) => {
  const todosList = [
    {
      id: 1,
      content: "店予約する",
    },
    {
      id: 2,
      content: "卵買う",
    },
    {
      id: 3,
      content: "郵便出す",
    },
  ];

  // state: 初期値はtodosList
  const [todos, setTodos] = useState(todosList);

  // setTodosを呼んでstateの状態を更新してやる必要がある
  const deleteTodo = (id) => {
    // 渡ってきたidと一致するようであれば削除する
    // 渡ってきたidと一致しないものを新しい配列に入れる
    const newTodos = todos.filter((todo) => {
      return todo.id !== id;
    });
    setTodos(newTodos);
  };

  const createTodo = (todo) => {
    // todosにtodoを末尾に追加
    setTodos([...todos, todo]);
  };

  return (
    <>
      <List todos={todos} deleteTodo={deleteTodo} />
      <Form createTodo={createTodo} />
    </>
  );
};

export default Todo;

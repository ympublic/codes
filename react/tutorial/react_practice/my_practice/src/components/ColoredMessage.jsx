// Propsを受け取る
export const ColoredMessage = (props) => {
  // console.log(props); // {color: "blue", message: "お元気ですか？"}

  //Propsを分割代入
  const { color, children } = props;

  // CSSオブジェクト
  const contentStyle = {
    color: color,
    fontSize: "20px",
  };

  return <p style={contentStyle}>{children}</p>;
};

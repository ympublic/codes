def checkio(game_result):
    g = game_result
    # 斜め
    if g[0][2] == g[1][1] == g[2][0] or g[0][0] == g[1][1] == g[2][2]:
        if g[1][1] != ".":  # is notと!=は異なるらしい...めんどくさい
            return g[1][1]
            
    i=0
    for row in g:
        #if g[0][i] == "." or g[i][0] == ".":
            #continue
        # 縦一列
        if g[0][i] == g[1][i] == g[2][i]:
            if g[0][i] != ".":
                return g[0][i]

        # 横一列
        elif g[i][0] == g[i][1] == g[i][2]:
            if g[i][0] != ".":
                return g[i][0]
        
        i+=1

    return "D"


if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert checkio([
        u"X.O",
        u"XX.",
        u"XOO"]) == "X", "Xs wins"
    assert checkio([
        u"OO.",
        u"XOX",
        u"XOX"]) == "O", "Os wins"
    assert checkio([
        u"OOX",
        u"XXO",
        u"OXX"]) == "D", "Draw"
    assert checkio([
        u"O.X",
        u"XX.",
        u"XOO"]) == "X", "Xs wins again"
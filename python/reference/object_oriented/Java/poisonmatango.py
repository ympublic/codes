from matango import Matango
from hero import Hero

class PoisonMatango(Matango):

	""" 毒攻撃が出来る、おばけキノコ """

	def __init__(self, suffix:str):
		super().__init__(suffix) # 内部インスタンス部(親クラス)のコンストラクタ呼び出し
		# 注意！: Pythonでは指定したインスタンス変数以外は継承されない
		# cf: http://www.nct9.ne.jp/m_hiroi/light/python06.html
		self.__poisoncount: int = 5 # 追加

	# オーバーライド
	def attack(self, h:Hero):
		super().attack(h) # 親インスタンス部のメソッド
		if self.__poisoncount > 0:
			print("さらに毒の胞子をばら撒いた！")
			damage: int = h.hp // 5 # 割り算の整数部。 /演算子は浮動小数点を返す
			h.hp = h.hp - damage
			print("{}は{}ポイントのダメージを受けた！".format(h.name, damage))
			self.__poisoncount -= 1

	@property
	def poisonCount(self):
		return self._poisoncount
	
	@poisonCount.setter
	def poisonCount(self, value):
		self.__poisoncount = value

